set serveroutput on size 100000
spool 902_create_RI_users_UDEBS.log

set define off

PROMPT Setup of new RI users
declare
 clID number;
 usID number;
 function createRIClient(ivDescription in varchar2, ivHostClientID in varchar2, inClientCategId in number) return number is
  vClientID number:=null;                                                                                                                                                                                                                        
 begin
    dbms_output.put_line('Create RI client '||ivDescription||' in UDEBS'); 
    begin
      select clientId into vClientID from clienthostallocation where hostclientid = ivHostClientID;
    exception when no_data_found then
      INSERT INTO clients (CLIENTID,mastersystemcode,FEECLIENTCATEGID,STATUSID,DESCRIPTION,BORNDATE,BRANCHID,MASTERUSERID,CONTACTNAME,CONTACTPHONE,CONTACTFAX,CONTACTGSM,CONTACTEMAIL,ADDNEWACCOUNT,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,FEESTARTDATE,CLIENTCATEGID,ALLOWREMOTESIGN,DMTIMESTAMP,LEGALCATEGID,ENTITYTIMESTAMP,IDENTIFICATIONTYPEID,IDENTIFICATIONNUMBER,BIRTHDATE)
        VALUES (SEQ_CLIENTS.nextval,'EXTERNAL_CLIENTS',0,1,ivDescription,sysdate,'0',null,ivDescription,null,null,null,null,1,null,null,null,null,sysdate,inClientCategId,0,'0000000001 06.06.2017 22:15:02',1,'20170606130902557893000',1,null,null)
        returning CLIENTID into vClientID;
      INSERT INTO clienthostallocation (clientid,hostclientid) VALUES (SEQ_CLIENTS.currval,ivHostClientID);
      INSERT INTO clientchannelprofiles (CLIENTID,CHANNELID,RIGHTSUBSETID,STATUSID,FEESTARTDATE,DMTIMESTAMP,ENTITYTIMESTAMP,BORNDATE) 
        VALUES (SEQ_CLIENTS.CURRVAL,7,1,1,sysdate,'0000000003 06.06.2017 22:16:25','20170606091556424891000',sysdate);
      INSERT INTO clientrights (CLIENTRIGHTID,CLIENTID,RIGHTID)
        SELECT SEQ_CLIENTRIGHTS.NEXTVAL, SEQ_CLIENTS.CURRVAL, RIGHTID FROM RightTypes where channelid = 7;
    end;
    return vClientID;
 end;
 function createRIUser(inClientID in number, ivFirstName in varchar2, ivLastName in varchar2, ivUserID in varchar2, ivPhoneNr in varchar2, ivEmail in varchar2) return number is
  lnUserId number:=null;
  tmp number;
 begin
    dbms_output.put_line('Create RI user '||ivFirstName||' '||ivLastName||' in UDEBS');
    begin
      select userId into lnUserId from users where hostUserId = ivUserID;
    exception when no_data_found then
      INSERT INTO users (USERID,STATUSID,BORNDATE,LANGID,FIRSTNAME,LASTNAME,PERSONALNUM,CONTACTPHONE,CONTACTFAX,CONTACTGSM,CONTACTEMAIL,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,DMTIMESTAMP,HOSTUSERID,ENTITYTIMESTAMP) 
        VALUES (SEQ_USERS.nextval,1,sysdate,'cs',ivFirstName,ivLastName,ivUserID,ivPhoneNr,null,ivPhoneNr,ivEmail,null,null,null,null,'0000000005 06.06.2017 22:19:33',ivUserID,'20170606135006238994000')
        returning UserID into lnUserId;
    end;
    begin
      select CLIENTUSERSID into tmp from ClientUsers where clientid = inClientID and userid = lnUserId;
    exception when no_data_found then
      INSERT INTO clientusers (CLIENTUSERSID,CLIENTID,USERID,STATUSID,ENTITYTIMESTAMP) 
        VALUES (SEQ_CLIENTUSERS.NEXTVAL,inClientID,lnUserId,1,'20170606151121652091000');
      INSERT INTO clientuserchannelprofiles (CLIENTUSERSID,CHANNELID,CLIENTID,STATUSID,BORNDATE,DMTIMESTAMP,ENTITYTIMESTAMP) 
        VALUES (SEQ_CLIENTUSERS.CURRVAL,7,inClientID,1,sysdate,'0000000007 06.06.2017 22:20:23','20170606135006238994000');
      INSERT INTO clientuserchannelauthmethods (CLIENTUSERSID,CHANNELID,AUTHENMETHODID) VALUES (SEQ_CLIENTUSERS.CURRVAL,7,1);
      INSERT INTO clientuserchannelcertmethods (CLIENTUSERSID,CHANNELID,CERTIFMETHODID) VALUES (SEQ_CLIENTUSERS.CURRVAL,7,3);
      INSERT INTO clientrightprofiles (RIGHTPROFILEID,CLIENTID,RIGHTPROFILENAME,DEFAULTPROFILE,STATUSID,BORNDATE,DESCRIPTION,ADDNEWACCOUNT,ENTITYTIMESTAMP) 
        VALUES (SEQ_CLIENTRIGHTPROFILES.NEXTVAL,inClientID,ivFirstName||' '||ivLastName||' - Default profile',1,1,sysdate,'Default profile',1,'20170606091541918082000');
      INSERT INTO clientuserrightprofiles (CLIENTUSERRIGHTPROFILEID, RIGHTPROFILEID,CLIENTUSERSID)
        VALUES (seq_clientuserrightprofiles.NEXTVAL, SEQ_CLIENTRIGHTPROFILES.CURRVAL, SEQ_CLIENTUSERS.CURRVAL); 
      INSERT INTO rightprofiledefinitions (clientrightid, rightprofileid) 
        SELECT clientrightid, SEQ_CLIENTRIGHTPROFILES.CURRVAL FROM ClientRights where clientId = inClientID;
    end;      
    return lnUserId;      
 end;
 procedure createRIAccount(inClientID in number, inUserID in number, ivAccountID in varchar2, ivCurrencyId in varchar2, ivAccountname in varchar2, inRightSubsetMask in number, ivIban in varchar2 default null) is
  vnClientRightProfileID number;
  lnAccountID number;
 begin
    dbms_output.put_line('Create RI account '||ivAccountID||' in UDEBS');
    begin
      select accountID into lnAccountID from Accounts where BISACCOUNTID = nvl(ivIban,ivAccountID);
    exception when no_data_found then   
      INSERT INTO accounts (ACCOUNTID,CURRENCYID,ACCTYPEID,NAME,BISACCOUNTID,IBAN,BRANCHID,ACCNUM,STATUSID,DMTIMESTAMP,MULTICURRENCYFLAG,ENTITYTIMESTAMP) 
        VALUES (seq_accounts.nextval,ivCurrencyId,1,ivAccountname,nvl(ivIban,ivAccountID),ivIban,0,ivAccountID,1,'0000000004 06.06.2017 22:16:50',0,'20170606095910006857000')
        returning AccountID into lnAccountID;
      INSERT INTO clientaccounts (ACCOUNTACCESSID,ACCOUNTID,CLIENTID,STATUSID,INCLUDEINFFM,ENTITYTIMESTAMP) 
        VALUES (SEQ_CLIENTACCOUNTACCESS.nextval,seq_accounts.currval,inClientID,1,0,'20170606151122748269000');
    end;  
    select rightProfileId into vnClientRightProfileID from ClientUserRightProfiles 
      where CLIENTUSERSID in (select CLIENTUSERSID from ClientUsers where clientId = inClientID and userId = inUserID);   
    INSERT INTO rightassignclientaccounts (clientrightid, rightprofileid, accountaccessid) 
      SELECT CR.clientrightid, vnClientRightProfileID, CA.accountaccessid 
      FROM ClientRights CR, ClientAccounts CA, RightTypes RT, OperationTypes OT, RightInitSubsetDef RISD
        where CR.clientId = inClientID and CA.clientId = inClientID and CA.AccountID = lnAccountID
          and CR.rightid = RT.rightid and RT.operId = OT.operId and RT.channelId = 7 and OT.accountsensitive=1
          and CR.rightid = RISD.rightid and RISD.rightsubsetId = inRightSubsetMask;
 end;
begin
 -- predefined list of RI clients and users
 clID := createRIClient('PROTAX CZ', '12376', 3);
 usID := createRIUser(clID, 'Marek', utl_raw.cast_to_varchar2(hextoraw('C5BD656C657A6EC3BD')), '57235', '11111111', 'marek.zelezny@mailinator.com');
 createRIAccount(clID, usID, '19486688', 'CZK', utl_raw.cast_to_varchar2(hextoraw('486C61766EC3AD20C3BAC48D6574')), 1);
 createRIAccount(clID, usID, '19486709', 'EUR', utl_raw.cast_to_varchar2(hextoraw('5A616872616E69C48D6EC3AD206F6263686F64')), 1);
 createRIAccount(clID, usID, '19486696', 'CZK', utl_raw.cast_to_varchar2(hextoraw('446F6DC3A163C3AD206F6263686F64')), 1);
 usID := createRIUser(clID, 'Jakub', 'Strnad', '57238', '11111111', 'jakub.strnad@mailinator.com');
 createRIAccount(clID, usID, '19486688', 'CZK', utl_raw.cast_to_varchar2(hextoraw('486C61766EC3AD20C3BAC48D6574')), 1);
 createRIAccount(clID, usID, '19486709', 'EUR', utl_raw.cast_to_varchar2(hextoraw('5A616872616E69C48D6EC3AD206F6263686F64')), 1);
 createRIAccount(clID, usID, '19486696', 'CZK', utl_raw.cast_to_varchar2(hextoraw('446F6DC3A163C3AD206F6263686F64')), 1);
 usID := createRIUser(clID, 'Cyril', 'Holub', '59534', '11111111', 'cyril.holub@mailinator.com');
 createRIAccount(clID, usID, '19486688', 'CZK', utl_raw.cast_to_varchar2(hextoraw('486C61766EC3AD20C3BAC48D6574')), 2);
 createRIAccount(clID, usID, '19486709', 'EUR', utl_raw.cast_to_varchar2(hextoraw('5A616872616E69C48D6EC3AD206F6263686F64')), 2);
 createRIAccount(clID, usID, '19486696', 'CZK', utl_raw.cast_to_varchar2(hextoraw('446F6DC3A163C3AD206F6263686F64')), 2);
 clID := createRIClient('Jakub Strnad', '57238', 1);
 usID := createRIUser(clID, 'Jakub', 'Strnad', '57238', '11111111', 'jakub.strnad@mailinator.com');
 createRIAccount(clID, usID, '17797191', 'CZK', utl_raw.cast_to_varchar2(hextoraw('506F646E696BC3A16EC3AD'))||' CZK', 1);
 createRIAccount(clID, usID, '19468527', 'EUR', utl_raw.cast_to_varchar2(hextoraw('506F646E696BC3A16EC3AD'))||' EUR', 1);
 createRIAccount(clID, usID, '19468535', 'USD', utl_raw.cast_to_varchar2(hextoraw('506F646E696BC3A16EC3AD'))||' USD', 1);
 createRIAccount(clID, usID, '19468543', 'CZK', utl_raw.cast_to_varchar2(hextoraw('536F756B726F6DC3BD20C3BAC48D6574')), 1);
 commit;
 -- special Georgia test client/user
 clID := createRIClient('Lia Mikhelashvili', '57240', 1);
 usID := createRIUser(clID, 'Lia', 'Mikhelashvili', '57240', '11111111', 'lia.mikhelashvili@mailinator.com');
 createRIAccount(clID, usID, '0780745060622336', 'GEL', 'LIA GEL', 1, 'GE07TB0780745060622336');
 createRIAccount(clID, usID, '1180736612500002', 'USD', 'LIA USD', 1, 'GE42TB1180736612500002');
 createRIAccount(clID, usID, '7003807366100040', 'EUR', 'LIA EUR', 1, 'GE17TB7003807366100040');
 createRIAccount(clID, usID, '1234878430121301', 'GEL', 'LIA Secondary GEL', 1, 'GE43TB1234878430121301');
 commit; 
end;
/

spool off

EXIT