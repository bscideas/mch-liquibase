set serveroutput on size 1000000
spool data_cleanup.log

set define off

PROMPT Cleanup script - cleans data from all UDEBS tables
declare
 x pls_integer:=1;
 y pls_integer:=0;
 z pls_integer;
begin
 loop
  for rec in (select * from user_tables) loop
   begin
    execute immediate 'select count(*) from '||rec.table_name into z;
    if z > 0 then
     begin
      execute immediate 'delete from '||rec.table_name;
      x:=x+1;
     exception when others then null;
     end;
    end if;
   exception when others then null;
   end;
  end loop;
  if x=y then
   exit;
  end if;
  y:=x;
 end loop;
end;
/

spool off
EXIT