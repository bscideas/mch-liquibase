set serveroutput on size 1000000
spool full_cleanup.log

set define off

PROMPT Cleanup script - cleans everything created by install scripts including tablespaces, except profile and basic XA role - MUST BE RUN UNDER SYS USER !!!

PROMPT basic drops

drop user UDEBS cascade ;
drop user GUS_APPUSER cascade ;
drop user WAC_APPUSER cascade ;
drop role UDEBSOWNER;
drop role UDEBS_GUS;
drop role UDEBS_WAC;

PROMPT drop all synonyms
begin
 for rec in (select * from all_synonyms where table_owner in ('UDEBS') and owner = 'PUBLIC') loop
  begin
   execute immediate 'drop public synonym '||rec.synonym_name;
  exception when others then null;
  end;
 end loop;
end;
/
begin
 for rec in (select * from all_synonyms where table_owner in ('UDEBS') and owner != 'PUBLIC') loop
  begin
   execute immediate 'drop synonym '||rec.synonym_name;
  exception when others then null;
  end;
 end loop;
end;
/

PROMPT Drop tablespaces
drop tablespace UDEBSACCOUNT INCLUDING CONTENTS;
drop tablespace UDEBSARCH INCLUDING CONTENTS;
drop tablespace UDEBSARCHINDEX INCLUDING CONTENTS;
drop tablespace UDEBSCLIENT INCLUDING CONTENTS;
drop tablespace UDEBSINDEX INCLUDING CONTENTS;
drop tablespace UDEBSJOURNAL INCLUDING CONTENTS;
drop tablespace UDEBSLOB INCLUDING CONTENTS;
drop tablespace UDEBSREF INCLUDING CONTENTS;
drop tablespace UDEBSTRANS INCLUDING CONTENTS;

PROMPT count of invalid objects
select count(*) from all_objects where status !='VALID';

spool off
EXIT