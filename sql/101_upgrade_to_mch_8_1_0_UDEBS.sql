set serveroutput on size 1000000
spool 101_upgrade_to_mch_8_1_0_UDEBS.log

set define off

PROMPT PATCH 7371 FROM ORIGINAL RS PATCHWORK TO UPGRADE MCH TO 8.1.0
CREATE OR REPLACE
Function vidle_hex(ivhex varchar2) return varchar2 is
Begin
  Return utl_raw.cast_to_varchar2(hextoraw(ivhex));
End;
/
CREATE OR REPLACE
Procedure vidle_txt(atid number,apid varchar2,alng varchar2,atxt varchar2) is
 tmp_cnt pls_integer;
Begin
 select count(*) into tmp_cnt from supportedlang where langid = alng;
 if tmp_cnt > 0 then
  Insert into LANGUAGETEXTS(PurposeID,TextID,LangID,Text)values(apid,atid,alng,atxt);
 end if; 
Exception
  When DUP_VAL_ON_INDEX then
    Update LANGUAGETEXTS set Text=atxt where textID=atid and PurposeID=apid and LangID=alng;
  When OTHERS then
    dbms_output.put_line(substr('PurposeID: '||apid||', LangID: '||alng||', Text: '||atxt,1,255));
    dbms_output.put_line(substr(sqlerrm,1,255));
End;
/
PROMPT ;
PROMPT ---------------------------------------------------------------;
PROMPT TABLE GLOBALSYSTEMPARAMETER
PROMPT __________________________________________________________
PROMPT Data inserting/updating for PARAMNAME='MCH_STORE_RIGHT_PROFILE_ACCOUNT_OPER_GROUP_RIGHT_INIT_SUBSET_DEFINITION'...
DECLARE N Number;
BEGIN
BEGIN
 Insert into GLOBALSYSTEMPARAMETER(PARAMNAME,VALUE,BINVALUE,FORFRONTEND,ENTITYTIMESTAMP,EDITABLE,BINARY,FORADMINISTRATION)values('MCH_STORE_RIGHT_PROFILE_ACCOUNT_OPER_GROUP_RIGHT_INIT_SUBSET_DEFINITION','0',empty_blob(),1,'20181108105158601340000',0,0,1);
EXCEPTION When DUP_VAL_ON_INDEX then
 Update GLOBALSYSTEMPARAMETER set VALUE='0',BINVALUE=empty_blob(),FORFRONTEND=1,ENTITYTIMESTAMP='20181108105158601340000',EDITABLE=0,BINARY=0,FORADMINISTRATION=1 where PARAMNAME='MCH_STORE_RIGHT_PROFILE_ACCOUNT_OPER_GROUP_RIGHT_INIT_SUBSET_DEFINITION';
END;
 Select distinct B.textID into N from GLOBALSYSTEMPARAMETER A, (SELECT textID FROM TEXTS) B where A.TEXTID=B.TextID(+) and PARAMNAME='MCH_STORE_RIGHT_PROFILE_ACCOUNT_OPER_GROUP_RIGHT_INIT_SUBSET_DEFINITION';
 if N is null then 
  N:=GETNEXTTEXTID('GLOBALSYSTEMPARAMETER');
  Update GLOBALSYSTEMPARAMETER set TEXTID=N where PARAMNAME='MCH_STORE_RIGHT_PROFILE_ACCOUNT_OPER_GROUP_RIGHT_INIT_SUBSET_DEFINITION';
 end if;
 vidle_txt(N,'DEFAULT','en','Flag if functionality of storing of right initialization subset for right profile along with operation group and client account relation is allowed');
 vidle_txt(N,'DEFAULT','cs',vidle_hex('50C599C3AD7A6E616B206A6573746C69206A6520706F766F6C656E6F20756B6CC3A164C3A16EC3AD20696E696369616C697A61C48D6EC3AD686F2073756273657475207072C3A176206B2070726F66696C75207072C3A1762073706F6C7520736520736B7570696E6F75206F7065726163C3AD20612076617A626F75206B6C69656E7461206E6120C3BAC48D6574'));
 vidle_txt(N,'DEFAULT','sk',vidle_hex('5072C3AD7A6E616B20C48D69206A6520706F766F6C656EC3A920756B6C6164616E696520696E696369616C697A61C48D6EC3A9686F2073756273657475207072C3A176206B2070726F66696C75207072C3A1762073706F6C75207A6F20736B7570696E6F75206F706572C3A16369C3AD20612076617A626F75206B6C69656E7461206E6120C3BAC48D6574'));
END;
/
PROMPT ---------------------------------------------------------------;
commit;
DROP function vidle_hex
/
DROP procedure vidle_txt
/

PROMPT PATCH 7450 FROM ORIGINAL RS PATCHWORK TO UPGRADE MCH TO 8.1.0
PROMPT New sequence seq_texts
declare
  ti number;
  le1 exception;
  pragma exception_init(le1, -00955);
begin
 Select nvl(max(textid),0)+1 into ti from texts;
 execute immediate 'CREATE SEQUENCE seq_texts
  INCREMENT BY 1
  START WITH '||to_char(ti)||
'  MAXVALUE 9999999999
  NOCYCLE
  ORDER
  NOCACHE';
exception
  when le1 then null;
end;
/

PROMPT PATCH 7451 FROM ORIGINAL RS PATCHWORK TO UPGRADE MCH TO 8.1.0
CREATE OR REPLACE
FUNCTION GETNEXTTEXTID
  (tabn varchar2 default null) RETURN  Number  IS
-----------------
-- $release: 7451 $
-- $version: 8.0.2.0 $
-----------------
  tid  Number;
BEGIN
  LOOP
    Select seq_texts.nextval into tid from dual;
    BEGIN
      Insert into texts(textid,texttype) values(tid,null);
      Exit;
    Exception
      When DUP_VAL_ON_INDEX then null;
    END;
  End LOOP;
  Return tid;
END;
/

PROMPT PATCH 7452 FROM ORIGINAL RS PATCHWORK TO UPGRADE MCH TO 8.1.0
CREATE OR REPLACE trigger TBI_TEXTS
 BEFORE
  INSERT
 ON texts
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
 
WHEN (new.textid is NULL)
BEGIN
  :NEW.textid := seq_texts.NEXTVAL;
END;
/

comment on table TEXTS is 'Texts with assigned texttypes. $version: 8.0.1.0 $';

PROMPT PATCH 7507 FROM ORIGINAL RS PATCHWORK TO UPGRADE MCH TO 8.1.0
CREATE OR REPLACE
Function vidle_hex(ivhex varchar2) return varchar2 is
Begin
  Return utl_raw.cast_to_varchar2(hextoraw(ivhex));
End;
/
CREATE OR REPLACE
Procedure vidle_txt(atid number,apid varchar2,alng varchar2,atxt varchar2) is
 tmp_cnt pls_integer;
Begin
 select count(*) into tmp_cnt from supportedlang where langid = alng;
 if tmp_cnt > 0 then
  Insert into LANGUAGETEXTS(PurposeID,TextID,LangID,Text)values(apid,atid,alng,atxt);
 end if; 
Exception
  When DUP_VAL_ON_INDEX then
    Update LANGUAGETEXTS set Text=atxt where textID=atid and PurposeID=apid and LangID=alng;
  When OTHERS then
    dbms_output.put_line(substr('PurposeID: '||apid||', LangID: '||alng||', Text: '||atxt,1,255));
    dbms_output.put_line(substr(sqlerrm,1,255));
End;
/
PROMPT ;
PROMPT ---------------------------------------------------------------;
PROMPT TABLE GLOBALSYSTEMPARAMETER
PROMPT __________________________________________________________
PROMPT Data inserting/updating for PARAMNAME='ADMIN_CONTRACT_GENERATION_ALLOW_EDITABLE'...
DECLARE N Number;
BEGIN
BEGIN
 Insert into GLOBALSYSTEMPARAMETER(PARAMNAME,VALUE,BINVALUE,FORFRONTEND,ENTITYTIMESTAMP,EDITABLE,BINARY,FORADMINISTRATION)values('ADMIN_CONTRACT_GENERATION_ALLOW_EDITABLE','0',empty_blob(),1,'20181222145158601340000',0,0,1);
EXCEPTION When DUP_VAL_ON_INDEX then
 Update GLOBALSYSTEMPARAMETER set VALUE='0',BINVALUE=empty_blob(),FORFRONTEND=1,ENTITYTIMESTAMP='20181222145158601340000',EDITABLE=0,BINARY=0,FORADMINISTRATION=1 where PARAMNAME='ADMIN_CONTRACT_GENERATION_ALLOW_EDITABLE';
END;
 Select distinct B.textID into N from GLOBALSYSTEMPARAMETER A, (SELECT textID FROM TEXTS) B where A.TEXTID=B.TextID(+) and PARAMNAME='ADMIN_CONTRACT_GENERATION_ALLOW_EDITABLE';
 if N is null then 
  N:=GETNEXTTEXTID('GLOBALSYSTEMPARAMETER');
  Update GLOBALSYSTEMPARAMETER set TEXTID=N where PARAMNAME='ADMIN_CONTRACT_GENERATION_ALLOW_EDITABLE';
 end if;
 vidle_txt(N,'DEFAULT','en','Flag if functionality of printing editable contract is allowed');
 vidle_txt(N,'DEFAULT','cs',vidle_hex('50C599C3AD7A6E616B206A6573746C69206A652066756E6B63696F6E616C697461207469736B7520656469746F766174656C6EC3A9686F206B6F6E7472616B747520706F766F6C656E61'));
 vidle_txt(N,'DEFAULT','sk',vidle_hex('5072C3AD7A6E616B20C48D69206A652066756E6B63696F6E616C69746120746C61C48D6520656469746F76617465C4BE6EC3A9686F206B6F6E7472616B747520706F766F6C656EC3A1'));
END;
/
PROMPT ---------------------------------------------------------------;
commit;
DROP function vidle_hex
/
DROP procedure vidle_txt
/

PROMPT Create private synonyms for new objects in UDEBS schema
DECLARE
  le1 EXCEPTION;
  PRAGMA EXCEPTION_INIT(le1, -0955);
BEGIN
  EXECUTE IMMEDIATE 'create synonym GUS_APPUSER.SEQ_TEXTS for SEQ_TEXTS';
EXCEPTION
  WHEN le1 THEN
    NULL;
END;
/
DECLARE
  le1 EXCEPTION;
  PRAGMA EXCEPTION_INIT(le1, -0955);
BEGIN
  EXECUTE IMMEDIATE 'create synonym WAC_APPUSER.GETNEXTTEXTID for GETNEXTTEXTID';
EXCEPTION
  WHEN le1 THEN
    NULL;
END;
/

PROMPT Grant rights on objects to roles for new objects in UDEBS schema
grant SELECT on SEQ_TEXTS to UDEBS_GUS;
grant SELECT on SEQ_TEXTS to WAC_APPUSER;
grant EXECUTE on GETNEXTTEXTID to UDEBS_GUS;
grant EXECUTE on GETNEXTTEXTID to WAC_APPUSER;

PROMPT Removed unnnecessary constraints to Texts table, except from LanguageTexts
begin
 for rec in (select c.*, cl.column_name from user_constraints c, user_cons_columns cl where c.r_constraint_name = 'PK_TEXTS' and c.table_name != 'LANGUAGETEXTS' and c.constraint_name = cl.constraint_name) loop
   begin
     execute immediate 'ALTER TABLE '||rec.table_name||' DROP CONSTRAINT '||rec.constraint_name;
   exception when others then
     dbms_output.put_line('Problem dropping constraint '||rec.constraint_name);
   end;
 end loop;
end;
/

PROMPT Recompile invalid objects in UDEBS schema

CREATE TABLE comp_usr_tab (id NUMBER, name VARCHAR2(30));
CREATE SEQUENCE comp_st_seq;

DECLARE
  nBefore NUMBER;
  nAfter  NUMBER;
  CURSOR cur IS
    SELECT object_type, object_name
      FROM user_objects
     WHERE status<>'VALID'
     ORDER BY OBJECT_NAME;
   PROCEDURE jevtmp_Vykonaj(PR2 IN VARCHAR2) IS
     Acursor INTEGER;
     rows_processed INTEGER;
   BEGIN
     Acursor := dbms_sql.open_cursor;
     dbms_sql.parse(Acursor, PR2, dbms_sql.v7);
      rows_processed := dbms_sql.execute(Acursor);
    dbms_sql.close_cursor(Acursor);
   EXCEPTION
     WHEN OTHERS THEN
       dbms_sql.close_cursor(Acursor);
      raise;
   END;
BEGIN
  LOOP
    SELECT COUNT(*) INTO nBefore FROM user_objects WHERE status <>'VALID';
    INSERT INTO comp_usr_tab(id,name) VALUES (0,'----');
    COMMIT;
    FOR i IN cur LOOP
      INSERT INTO comp_usr_tab(id,name) VALUES (comp_st_seq.nextval,i.object_name);
      COMMIT;
      BEGIN
        IF i.object_type='PACKAGE BODY' THEN
          jevtmp_vykonaj('alter package '||i.object_name||' compile body');
        ELSE
          jevtmp_vykonaj('alter '||i.object_type||' '||i.object_name||' compile');
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
    END LOOP;
    SELECT COUNT(*) INTO nAfter FROM user_objects WHERE status <>'VALID';
    dbms_output.put_line('Before:'||to_char(nBefore)||' After:'||to_char(nAfter));
    EXIT WHEN nBefore=nAfter;
  END LOOP;
END;
/
SELECT SUBSTR(object_name,1,30) object_name, object_type
  FROM user_objects
 WHERE status<>'VALID' AND object_type not like '%JAVA%'
 ORDER BY 1;
DROP TABLE comp_usr_tab;
DROP SEQUENCE comp_st_seq;

spool off

EXIT
