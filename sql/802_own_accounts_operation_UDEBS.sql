set serveroutput on size 1000000
spool 802_own_accounts_operation_UDEBS.log
set define off
PROMPT Adding operation types for UDEBS

CREATE OR REPLACE TYPE VARCHAR_ARRAY AS VARRAY(100) OF VARCHAR2(255);
/

/* ------------ BUILD ID FOR RIGHT TYPE FUNCTION ------------ */
CREATE OR REPLACE FUNCTION build_right_type_id(accTypeId IN NUMBER, operId IN NUMBER, channelId IN NUMBER) RETURN NUMBER IS

  BEGIN
    --RETURN operId||LPAD(TO_CHAR(channelId),2,'0')||'00'||LPAD(TO_CHAR(LOG(2, accTypeId) + 1),2,'0');
    RETURN operId||LPAD(TO_CHAR(channelId),2,'0')||LPAD(TO_CHAR(ROUND(LOG(2, accTypeId)) + 1),4,'0');
  END;
/

/* ------------ INSERT RIGHT TYPES FOR OPERATION AND CHANNEL FUNCTION ------------ */
CREATE OR REPLACE PROCEDURE create_right_types(oper_id IN NUMBER, channel_id IN number, right_types IN VARCHAR_ARRAY) IS
  CURSOR access_types_cursor IS SELECT oat.ACCESSTYPEID access_type_id
                      FROM OPERATIONACCESSTYPES oat INNER JOIN LANGUAGETEXTS lt ON oat.TEXTID = lt.TEXTID
                      WHERE lt.TEXT IN (SELECT COLUMN_VALUE FROM TABLE (right_types));
  n NUMBER;

  BEGIN
    FOR curs IN access_types_cursor LOOP
      BEGIN
        SELECT RIGHTID INTO n FROM RIGHTTYPES WHERE CHANNELID = channel_id AND OPERID = oper_id AND ACCESSTYPEID = curs.access_type_id;
      exception When NO_DATA_FOUND then
        dbms_output.put_line('Creating right type for operation: '||oper_id||' and channel: '||channel_id);
        INSERT INTO RIGHTTYPES (RIGHTID, CHANNELID, ACCESSTYPEID, OPERID) VALUES (build_right_type_id(curs.access_type_id, oper_id, channel_id), channel_id, curs.access_type_id, oper_id);
      END;
    END LOOP;
  END;
/

/* ------------ MULTI LANGUAGE TEXT FUNCTION ------------ */
CREATE OR REPLACE PROCEDURE multi_lang_txt(atid number,apid varchar2,alng varchar2,atxt varchar2) IS
    tmp pls_integer;
BEGIN
    SELECT COUNT(*) INTO tmp FROM supportedlang WHERE langid = alng;
    if tmp > 0 then
        INSERT INTO LANGUAGETEXTS(PurposeID,TextID,LangID,Text) values (apid,atid,alng,atxt);
    end if;
EXCEPTION
    when DUP_VAL_ON_INDEX then
        UPDATE LANGUAGETEXTS set Text=atxt WHERE textID=atid and PurposeID=apid and LangID=alng;
    when OTHERS then
        dbms_output.put_line(substr('PurposeID: '||apid||', LangID: '||alng||', Text: '||atxt,1,255));
        dbms_output.put_line(substr(sqlerrm,1,255));
END;
/

/* ------------ FUNCTION FOR NEW EVENT TYPE CREATION ------------ */
CREATE OR REPLACE PROCEDURE insert_event_type(iEVENTID in number, iTEXT in varchar2, iRETENTION in number, iRESULTSTATUSFLAG in number, iGROUPID in number) IS
    n number;
    tmp pls_integer;
BEGIN
    select count(*) into tmp from eventtypes where eventid = iEVENTID;
    if tmp =0 then
        n:=getnexttextid('EVENTTYPES');
        INSERT INTO eventtypes (EVENTID,TEXTID,RETENTION,RESULTSTATUSFLAG,GROUPID) VALUES (iEVENTID,n,iRETENTION,iRESULTSTATUSFLAG,iGROUPID);
        insert into languagetexts (textid, purposeid, langid, text) values (n, 'DEFAULT', 'en', iTEXT);
    END IF;
END;
/

/* ------------ OPERATIONS ------------ */

/* ------------ OPERATION [313 - Payment order within own accounts] ------------ */
DECLARE
  n NUMBER;
BEGIN
  dbms_output.put_line('Begin inserting operation [313, ''Payment order within own accounts'']');
  SELECT og.GROUPID INTO n FROM OPERATIONGROUPS og INNER JOIN LANGUAGETEXTS t ON og.TEXTID = t.TEXTID WHERE t.TEXT = 'Single Payments';
  BEGIN
    INSERT INTO OPERATIONTYPES (OPERID, OPERTYPECODE, GROUPID, ACCOUNTSENSITIVE, AUTHENREQ, CERTIFREQ, GROUPORDER, SIGNRULESREQ,
     SYSTABINDICATOR, PRIORITY, HIDDEN, HOMETABLENAME, PRIORITY_S, CHARGED, STATUSID, MANUALLYPROCESSED, AVAILABLEFORCLIENT,
     VISIBLEFOROPERATOR, AVAILABLEFOROPERATOR, AUTHORREQ)
    values (313, 'CEXPO', n, 1, 1, 0, 1,
     0, 0, 50, 0, 'TRANSACTIONS', 50, 0, 1, 0, 1, 0, 1, 1);
  EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
    UPDATE OPERATIONTYPES SET
      OPERTYPECODE = 'CEXPO',
      ACCOUNTSENSITIVE = 1,
      GROUPORDER = 1,
      AUTHORREQ = 1,
      CERTIFREQ = 0,
      SIGNRULESREQ = 0,
      HOMETABLENAME = 'TRANSACTIONS'
    WHERE OPERID = 313;
  END;

  SELECT DISTINCT B.textID INTO n FROM OPERATIONTYPES A, (SELECT textID FROM TEXTS) B WHERE A.textID=B.textID(+) and OPERID='313';
  IF n IS NULL THEN
    n:=getnexttextid('');
    UPDATE OPERATIONTYPES SET textID=n WHERE OPERID='313';
  end if;
  multi_lang_txt(n,'DEFAULT','en',UTL_RAW.Cast_To_Varchar2(HexToRaw('5061796D656E74206F726465722077697468696E206F776E206163636F756E7473')));

  DELETE FROM OPERATIONSUPPACCTYPES WHERE OPERID=313;

  INSERT INTO OPERATIONSUPPACCTYPES(ACCTYPEID,OPERID,DOMESTICCURRENCY,FOREIGNCURRENCY)
    SELECT ACCTYPEID, 313, 1, 1 FROM ACCOUNTTYPES WHERE ACCTYPEBISCODE IN (
      SELECT COLUMN_VALUE FROM TABLE(VARCHAR_ARRAY('CUR','SAV'))
    );

  create_right_types(313, 7, VARCHAR_ARRAY('Create','Cancellation','Edit','View'));
  create_right_types(313, 21, VARCHAR_ARRAY('Create','Cancellation','Edit','View'));

  declare
    inNewOperId number(10) := 313;
  begin
    -- full rollout of rigths on onew operation to existing clients
    begin
      insert into clientrights (clientrightid, clientid, rightid)
          select seq_clientrights.nextval, clientid, rightid from clients, righttypes where operid = inNewOperId;
    exception when dup_val_on_index then
      dbms_output.put_line('Client rights on operation '||to_char(inNewOperId)||' are already set');
    end;
    begin
      insert into rightprofiledefinitions (clientrightid, rightprofileid)
          select clientrightid, rightprofileid from ClientRights CR, ClientRightProfiles CRP where CR.clientId = CRP.ClientID
              and CR.rightid in (select rightid from righttypes where operid = inNewOperId);
    exception when dup_val_on_index then
      dbms_output.put_line('Client rights on operation '||to_char(inNewOperId)||' into profile definitions are already set');
    end;
    begin
      insert into rightassignclientaccounts (clientrightid, rightprofileid, accountaccessid)
          select CR.clientrightid, RPD.RightProfileID, CA.accountaccessid
        from ClientRights CR, ClientAccounts CA, Accounts A, RightTypes RT, OperationTypes OT, OperationSuppAccTypes OSAT, rightprofiledefinitions RPD
          where CR.clientId = CA.clientId and CA.AccountID = A.AccountID and A.AccTypeId = OSAT.AccTypeId and OSAT.OperId = RT.operId
            and CR.rightid = RT.rightid and RT.operId = OT.operId and OT.OperId = inNewOperId and OT.accountsensitive=1
            and CR.clientrightid = RPD.clientrightid;
    exception when dup_val_on_index then
      dbms_output.put_line('Client rights on operation '||to_char(inNewOperId)||' into profile definition assignments to accounts are already set');
    end;
    commit;
  end;

END;
/

/* ------------ EVENT TYPES FOR OPERATION [313 - Payment order within own accounts] ------------ */

PROMPT New event types for operId 313
BEGIN
insert_event_type(31300000,'Request to Payment order within own accounts',30,0,12);
insert_event_type(31300001,'Successfully created new request for Payment order within own accounts',30,1,12);
insert_event_type(31300999,'Failed attempt to create new request for Payment order within own accounts',30,2,12);
COMMIT;
END;
/

PROMPT Added mapping of operation type 313 to event type
BEGIN
delete from eventoperactionmapping where operid=313;
INSERT INTO eventoperactionmapping(OPERID,ACTIONTYPE,EVENTID) VALUES (313,'CREATE',31300000);
INSERT INTO eventoperactionmapping(OPERID,ACTIONTYPE,EVENTID) VALUES (313,'EDIT',31300000);
INSERT INTO eventoperactionmapping(OPERID,ACTIONTYPE,EVENTID) VALUES (313,'CANCEL',31300000);
COMMIT;
END;
/

/* ------------ RIGHT MASKS ------------ */

commit;
spool off
EXIT