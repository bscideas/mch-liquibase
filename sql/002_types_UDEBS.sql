set serveroutput on size 1000000
spool 002_types_UDEBS.log

set define off

PROMPT Create types in UDEBS schema

PROMPT CREATE OR REPLACE TYPE ERR_OBJ
CREATE OR REPLACE 
TYPE ERR_OBJ AS OBJECT (
-- DOC :
-- Z:\BSC_Praha\Banky\GEMINI\Docs\rules\Exceptions_Classification.doc
-- Z:\BSC_Praha\Banky\GEMINI\Docs\rules\GeminiExceptions.xls
--
  ErrorId   NUMBER(10),     -- HEXA kod chyby v tvare AAVCNNN, AA - originator, V - verzia, C - category, NNN - poradove cislo
  Text      VARCHAR2(512 CHAR),  -- anglicky text ureeny pro nas (a banku), obsahujici popis poieiny chyby (zapisovano do aplikaeniho logu)
  Param0    VARCHAR2(4000 CHAR), -- xPath (if applicable) oznaeeni polozky-prvku, ke kteremu se chyba vztahuje (napo. xPath k polozce formulaoe)
  Param1    VARCHAR2(4000 CHAR), -- odpovida dnesnimu RelData, tj. hodnota, ktera je chybna
  Param2    VARCHAR2(4000 CHAR),
  Param3    VARCHAR2(4000 CHAR),
  Param4    VARCHAR2(4000 CHAR),
  Param5    VARCHAR2(4000 CHAR),
  Param6    VARCHAR2(4000 CHAR),
  Param7    VARCHAR2(4000 CHAR),
  Param8    VARCHAR2(4000 CHAR),
  Param9    VARCHAR2(4000 CHAR),
  OccurDate DATE
,static function init(
        p1 NUMBER:=null,
        p2 VARCHAR2:=null,
        p3 VARCHAR2:=null,
        p4 VARCHAR2:=null,
        p5 VARCHAR2:=null,
        p6 VARCHAR2:=null,
        p7 VARCHAR2:=null,
        p8 VARCHAR2:=null,
        p9 VARCHAR2:=null,
        p10 VARCHAR2:=null,
        p11 VARCHAR2:=null,
        p12 VARCHAR2:=null,
        p13 date:=sysdate) return ERR_OBJ
)
/

CREATE OR REPLACE 
TYPE BODY ERR_OBJ AS
  static function init(
        p1 NUMBER:=null,
        p2 VARCHAR2:=null,
        p3 VARCHAR2:=null,
        p4 VARCHAR2:=null,
        p5 VARCHAR2:=null,
        p6 VARCHAR2:=null,
        p7 VARCHAR2:=null,
        p8 VARCHAR2:=null,
        p9 VARCHAR2:=null,
        p10 VARCHAR2:=null,
        p11 VARCHAR2:=null,
        p12 VARCHAR2:=null,
        p13 date:=sysdate) return ERR_OBJ is
    x ERR_OBJ:=ERR_OBJ(p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12,p13);
  begin
    return x;
  end;
END;
/

PROMPT CREATE OR REPLACE TYPE ERR_TAB
CREATE OR REPLACE 
TYPE ERR_TAB as table of ERR_OBJ
/

PROMPT CREATE OR REPLACE TYPE SUBTOTAL_ALL_OBJ
CREATE OR REPLACE 
TYPE SUBTOTAL_ALL_OBJ AS OBJECT(
    CurrencyID VARCHAR2(3), -- mena subtotalu
    Scheme VARCHAR2(10),    -- schema pre inkasa
    TranCount  NUMBER(10),  -- pocet transakci zahrnutych do subtotalu
    OrigCurrAmount NUMBER,  -- castka v mene subtotalu
    AccCurrAmount NUMBER    -- castka v mene uctu
)
/

PROMPT CREATE OR REPLACE TYPE SUBTOTAL_ALL_TAB
CREATE OR REPLACE 
TYPE SUBTOTAL_ALL_TAB AS TABLE OF subtotal_all_obj;
/

PROMPT CREATE OR REPLACE TYPE SUBTOTAL_OBJ
CREATE OR REPLACE 
TYPE SUBTOTAL_OBJ AS OBJECT(
    CurrencyID VARCHAR2(3), -- mena subtotalu
    TranCount  NUMBER(10),  -- pocet transakci zahrnutych do subtotalu
    OrigCurrAmount NUMBER,  -- castka v mene subtotalu
    AccCurrAmount NUMBER    -- castka v mene uctu
)
/

PROMPT CREATE OR REPLACE TYPE SUBTOTAL_TAB
CREATE OR REPLACE 
TYPE SUBTOTAL_TAB AS TABLE OF subtotal_obj;
/

PROMPT CREATE OR REPLACE TYPE SUBTOTALS_ALL_OT
CREATE OR REPLACE 
TYPE SUBTOTALS_ALL_OT is object
   (subtotals subtotal_all_tab);
/

PROMPT CREATE OR REPLACE TYPE SUBTOTALS_OT
CREATE OR REPLACE 
TYPE SUBTOTALS_OT is object
   (subtotals subtotal_tab);
/

spool off

EXIT
