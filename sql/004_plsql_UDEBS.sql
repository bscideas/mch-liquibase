set serveroutput on size 1000000
spool 004_plsql_UDEBS.log

set define off

PROMPT Create functions in UDEBS schema

PROMPT CREATE OR REPLACE function B24_FORMAT_ACCNUM
CREATE OR REPLACE 
function B24_FORMAT_ACCNUM(ivAccNum in Accounts.Accnum%type)
return varchar2
is
------------------
-- $release: 605 $
-- $version: 8.0.0.2 $
------------------
--
-- Purpose: Format account number (ACCOUNTS.ACCNUM = ACC1) for B24
--
-- MODIFICATION HISTORY
-- Person      Date         Comments
-- ---------   -----------  ------------------------------------------
-- Ilenin      12.01.2015   UDEBS RS
--
--------------------------------------------------------------------
  lvAccNum       Accounts.Accnum%type;
  lvAccPrefix    Accounts.Accnum%type;
  lvAcc          Accounts.Accnum%type;
begin
  if instr(ivAccNum, '-') > 0 then
    lvAccPrefix := ltrim(substr(ivAccNum, 1, instr(ivAccNum, '-')-1), '0');
    lvAcc := ltrim(substr(ivAccNum, instr(ivAccNum, '-')+1, length(ivAccNum)),'0');
  else
    lvAccPrefix := null;
    lvAcc := ltrim(ivAccNum,'0');
  end if;
  --
  if lvAccPrefix is not null then
    lvAccNum := substr(lvAccPrefix,1,6)||'-'||substr(lvAcc,1,10);
  else
    lvAccNum := substr(lvAcc,1,10);
  end if;
  --
  return lvAccNum;
end;
/

PROMPT CREATE OR REPLACE function GET_BRANCHID
CREATE OR REPLACE 
FUNCTION GET_BRANCHID (
    pAccountID in Accounts.AccountID%Type,
    pPackageID in ClientTransactionPackages.PackageID%Type default NULL
) RETURN NUMBER IS
------------------
-- $release: 009 $
-- $version: 8.0.0.0 $
------------------
--
-- Purpose: Ziskani BranchID podle uctu nebo packageid
--
-- MODIFICATION HISTORY
-- Person      Date       Comments
-- ----------- ---------- ---------------------
-- Ilenin      07.07.2014 UDEBS RS
--------------------------------------------------------
  lnBID  Branches.BranchID%TYPE;
  lvAcc  Accounts.AccountID%Type;
BEGIN
  if pAccountID is not null then
    select BranchID into lnBID from Accounts where AccountID=pAccountID;
  elsif pPackageID is not null then
    select AccountID into lvAcc from ClientTransactionPackages where PackageID=pPackageID;
    if lvAcc is not null then
      select BranchID into lnBID from Accounts where AccountID=lvAcc;
    end if;
  end if;
  return nvl(lnBID,0);
EXCEPTION WHEN OTHERS THEN
  return 0;
END Get_BranchID;
/

PROMPT CREATE OR REPLACE function GET_CLIENTSIGNRULELIMIT
CREATE OR REPLACE 
FUNCTION GET_CLIENTSIGNRULELIMIT(inSignRuleSeq IN ClientSignRuleLimits.ClientSignRuleSeq%TYPE,
                                                   inPeriod      IN ClientSignRuleLimits.Period%TYPE)
  RETURN NUMBER RESULT_CACHE IS
  ----------------
  -- $release: 2005 $- release ###
  -- $version: 8.0.0.1 $- release ###
  ----------------
  -- Funkce vraci AmountLimit, je pouzita pri otevreni kurzoru z IIF_CCFG005.GetValidSignatureRules
  -- MODIFICATION HISTORY
  -- Person      Date       Comments
  -- ----------- ---------- ---------------------
  -- Ilenin      07.07.2014 UDEBS RS
  -- Emmer       27.01.2016 Add RESULT_CACHE
  --------------------------------------------------------
  lnAmountLimit ClientSignRuleLimits.AmountLimit%TYPE;
BEGIN
  SELECT AmountLimit
    INTO lnAmountLimit
    FROM ClientSignRuleLimits
   WHERE ClientSignRuleSeq = inSignRuleSeq
     AND Period = inPeriod;
  RETURN lnAmountLimit;
EXCEPTION
  WHEN no_data_found THEN
    RETURN NULL;
END;
/

PROMPT CREATE OR REPLACE function GET_CTST_VALUES
CREATE OR REPLACE 
FUNCTION GET_CTST_VALUES (
    ivAttrName  in Varchar2,
    inStatusID  in ClientTransactionStatusTypes.StatusID%Type,
    inChannelId IN Channels.channelid%TYPE DEFAULT NULL,
    inOperId    IN Operationtypes.operid%TYPE DEFAULT NULL,
    ivDefVal    in Varchar2 default null,
    inRefTab    in Number default 0
) RETURN VARCHAR2 IS
------------------
-- $release: 440 $
-- $version: 8.0.1.0 $
------------------
-- $Header: /Oracle/UDEBS_RS/UDEBSTS.GET_CTST_VALUES.FNC 3     1.04.03 16:24 Ilenin $
-- Purpose:
--
-- MODIFICATION HISTORY
-- Person      Date       Comments
-- ----------- ---------- ---------------------
-- Ilenin      07.07.2014 UDEBS RS
-- Ilenin      02.12.2014 Override AllowSign
--------------------------------------------------------
  v_Result varchar2(1024 CHAR);
  cmd      varchar2(1024 CHAR);
BEGIN
  if inRefTab = 0 THEN
    IF upper(ivAttrName) IN('ALLOWDEL','ALLOWCANCEL','ALLOWEDIT','REPORTTOCLIENT','ALLOWSIGN') THEN
      cmd:='DECLARE'||CHR(10)||
             'CURSOR c_sr IS SELECT '|| ivAttrName || ' FROM StatusReporting WHERE StatusId = :StatusId AND'||CHR(10)||
              '(NVL(ChannelId,:ChannelId) = :ChannelId OR (ChannelId IS NULL AND :ChannelId IS NULL)) AND'||CHR(10)||
              '(NVL(OperId,:OperId) = :OperId OR (OperId IS NULL AND :OperId IS NULL)) ORDER BY OperId, ChannelId;'||CHR(10)||
           'BEGIN'||CHR(10)||
             'OPEN c_sr;'||CHR(10)||
             'FETCH c_sr INTO :Ret;'||CHR(10)||
             'IF c_sr%NOTFOUND THEN'||CHR(10)||
               'BEGIN'||CHR(10)||
                 'Select '|| ivAttrName || ' into :Ret from ClientTransactionStatusTypes where StatusId = :StatusId;'||CHR(10)||
               'EXCEPTION WHEN OTHERS THEN'||CHR(10)||
                 ':Ret := :DefVal;'||CHR(10)||
               'END;'||CHR(10)||
             'END IF;'||CHR(10)||
             'CLOSE c_sr;'||CHR(10)||
           'END;';
      execute immediate cmd using in inStatusID, inChannelId, inOperId, out v_Result, in ivDefval;
    ELSE
      cmd:='begin select '||ivAttrName||' into :ret from ClientTransactionStatusTypes where StatusID=:StatID;'||CHR(10)||
           'exception when no_data_found then :ret := :defval; end;';
      execute immediate cmd USING out v_Result, in inStatusID, ivDefval;
    END IF;
  ELSE
    IF upper(ivAttrName) IN('ALLOWDEL','ALLOWCANCEL','ALLOWEDIT','REPORTTOCLIENT','ALLOWSIGN') THEN
      cmd:='begin :ret:= reftab_ctst.'||ivAttrName||'(:StatID,:Channel,:OperId,:defval); end;';
      execute immediate cmd using out v_result, in inStatusID, inChannelId, inOperId, ivDefval;
    ELSE
      cmd:='begin :ret:= reftab_ctst.'||ivAttrName||'(:StatID,:defval); end;';
      execute immediate cmd using out v_Result, in inStatusID, ivDefval;
    END IF;
  end if;
  return v_Result;
END Get_CTST_values;
/

PROMPT CREATE OR REPLACE function GET_OT_VALUES
CREATE OR REPLACE 
FUNCTION GET_OT_VALUES (
    ivAttrName in Varchar2,
    inOperid   in OperationTypes.OperID%Type,
    inBranchID in OperPaymentSystems.BranchID%Type default 0, -- pro PaymentSystemID
    ivDefVal   in Varchar2 default null,
    inRefTab   in Number default 0                            -- 0 - select z OT, 1 - volani REFTAB_OT.xxx
) RETURN VARCHAR2 IS
------------------
-- $release: 009 $
-- $version: 8.0.0.0 $
------------------
--
-- Purpose: Vraci hodnotu predaneho sloupce z tabulky OPERTIONTYPES
--
-- MODIFICATION HISTORY
-- Person      Date       Comments
-- ----------- ---------- ---------------------
-- Ilenin      07.07.2014 UDEBS RS
--------------------------------------------------------
  lnPBID Branches.BranchID%TYPE:=inBranchID;
  v_Result varchar2(1024 CHAR);
  cmd      varchar2(1024 CHAR);
BEGIN
  if inRefTab = 0 then
    if upper(ivAttrName) = 'PAYMENTSYSTEMID' then
      cmd:='select '||ivAttrName||' from OperPaymentSystems where operid=:opid and branchid=:bid';
      execute immediate cmd into v_Result using inOperID,lnPBID;
    else
      cmd:='select '||ivAttrName||' from OperationTypes where operid=:opid';
      execute immediate cmd into v_Result using inOperID;
    end if;
  elsif upper(ivAttrName) = 'OPERID' then
    cmd:='select reftab_ot.existing(:opid) from dual';
    execute immediate cmd into v_Result using inOperID;
  elsif upper(ivAttrName) = 'PAYMENTSYSTEMID' then
    cmd:='select reftab_ot.'||ivAttrName||'(:opid,:bid,:defval) from dual';
    execute immediate cmd into v_Result using inOperID,lnPBID,ivDefVal;
  else
    cmd:='select reftab_ot.'||ivAttrName||'(:opid,:defval) from dual';
    execute immediate cmd into v_Result using inOperID,ivDefVal;
  end if;
  return v_Result;
EXCEPTION WHEN OTHERS THEN -- pro inRefTab=0 a ivAttrName='PAYMENTSYSTEMID'
  LOOP
    BEGIN
      Select ParentBranchID into lnPBID from Branches
       where BranchID=lnPBID;
      execute immediate cmd into v_Result using inOperID,lnPBID;
      return v_Result;
    EXCEPTION
      WHEN OTHERS THEN
        IF lnPBID IS NULL THEN
          RETURN ivDefVal; -- kdyz se nenajde BranchID v Branches;
        ELSE
          NULL;
        END IF;
    END;
  END LOOP;
END Get_ot_values;
/

PROMPT CREATE OR REPLACE function GETCHANGEDAMOUNT
CREATE OR REPLACE 
FUNCTION GETCHANGEDAMOUNT(p_nAmount         IN NUMBER,
                                            p_cFromCurrencyID IN VARCHAR2,
                                            p_cToCurrencyID   IN VARCHAR2,
                                            p_nBranchID       IN Branches.BranchID%TYPE,
                                            p_nMiddleRate     IN NUMBER DEFAULT 1) --0 - do not use middle rate, 1 use middle rate - used for amount calculation for eval of sig rules
 RETURN NUMBER IS
  ------------------
  -- $release: 6570 $
  -- $version: 8.0.9.1 $
  ------------------
  --
  -- Purpose: Funkce prepocitava zadanou castku v mene p_cFromCurrencyID a vraci ekvivalent
  -- v mene p_cToCurrencyID. Pro prepocet pouziva tabulku hodnot z kurzovniho listku.
  -- Pritom pro prepocet musi existovat kurzovni listek na menu uctu.
  -- Tento listek musi obsahovat sazbu pro typ sazby 3 = DEVIZA_PRODEJ z meny castky.
  -- MODIFICATION HISTORY
  -- Person      Date       Comments
  -- ----------- ---------- ---------------------
  -- Ilenin      07.07.2014 UDEBS RS
  -- Bocak       19.02.2015 input parameter p_nMiddleRate for deciding whether to use middle rate
  -- Bocak       13.03.2015 added MiddleRate into recursive calling
  -- Pataky      25.03.2015 Compatibility changes to support other banks (with non reversed exchange rate lists)
  -- Pataky      22.05.2015 Performance optimization - 20009183-2902
  -- Bocak       06.06.2015 get newest valied rate 20009183-3118
  -- Simunek     20.10.2015 Performance optimization (20009183-4734)
  -- Bocak       31.05.2016 Truncate only seconds from sysdate - 20009238-191
  -- Furdik      27.10.2016 added options for TATR - 20009239-186
  -- Emmer       09.04.2018 add EXCHANGERATELISTUNITS.UNIT (MYG-7673) - P20009301-68
  -- Pataky      09.04.2015 Generalized usage of units and reversed exchange rate lists (MYG-7673) - P20009301-68
  --------------------------------------------------------
  v_RateValue NUMBER;
  v_RateValue1 NUMBER;
  v_RateValue2 NUMBER;
  v_RateListType NUMBER;
  v_AmountTo NUMBER;
  v_ExchangeListSeq NUMBER;
  --
  v_LocalCurrency Currencies.CURRENCYID%TYPE := reftab_gsp('LOCALCURRENCY', 0, 'EUR');
  v_FromCurrencyID Currencies.CURRENCYID%TYPE := p_cFromCurrencyID;
  v_ToCurrencyID Currencies.CURRENCYID%TYPE := p_cToCurrencyID;
  --
  v_BuyRateType NUMBER; --Non Cash Buy
  v_SellRateType NUMBER; --Non Cash Sell
  --
  v_SysDate DATE := TRUNC(SYSDATE, 'MI'); -- for using SQL RESULT_CACHE
  v_BuyRateTypeMidDefault VARCHAR2(2);
  v_SellRateTypeMidDefault VARCHAR2(2);
  v_RATELISTUNITSETID EXCHANGERATELISTUNITS.RATELISTUNITSETID%TYPE;
  v_RATELISTREVERSED EXCHANGERATELISTS.RATELISTREVERSED%TYPE;
BEGIN
  --
  IF p_cFromCurrencyID IS NULL THEN
    v_FromCurrencyID := v_LocalCurrency;
  END IF;
  IF p_cToCurrencyID IS NULL THEN
    v_ToCurrencyID := v_LocalCurrency;
  END IF;
  --
  IF v_FromCurrencyID = v_ToCurrencyID
     OR v_FromCurrencyID IS NULL
     OR v_ToCurrencyID IS NULL THEN
    RETURN p_nAmount;
  END IF;
  --
  SELECT /*+ RESULT_CACHE */
   RateListTypeID, RateTypeID, RateTypeID
    INTO v_RateListType, v_BuyRateType, v_SellRateType
    FROM Branches
   WHERE RateListTypeID IS NOT NULL
     AND rownum = 1
   START WITH BranchID = p_nBranchID
  CONNECT BY BranchID = PRIOR ParentBranchID;
  --
  v_BuyRateTypeMidDefault := '7';
  v_SellRateTypeMidDefault := '7';
  IF Reftab_GSP('LOCALBANKCODEALPHA') = 'TATR' THEN
    v_BuyRateTypeMidDefault := '5';
    v_SellRateTypeMidDefault := '5';
  END IF;
  --
  IF Reftab_GSP('LOCALBANKCODEALPHA') IN ('SLSP', 'TATR') THEN
    IF p_nMiddleRate = 1 THEN
      v_BuyRateType := TO_NUMBER(reftab_gsp('UDEBS.BUYRATETYPE_MID', 0, v_BuyRateTypeMidDefault));
      v_SellRateType := TO_NUMBER(reftab_gsp('UDEBS.SELLRATETYPE_MID', 0, v_SellRateTypeMidDefault));
    ELSE
      v_BuyRateType := TO_NUMBER(reftab_gsp('UDEBS.BUYRATETYPE', 0, '1'));
      v_SellRateType := TO_NUMBER(reftab_gsp('UDEBS.SELLRATETYPE', 0, '3'));
    END IF;
  END IF;
  --
  IF Reftab_GSP('LOCALBANKCODEALPHA') IN ('SLSP', 'TATR') THEN
    IF v_ToCurrencyID = v_LocalCurrency THEN
      BEGIN
        v_RateValue1 := 1;
        SELECT /*+ RESULT_CACHE */
         decode(nvl(erl.ratelistreversed,0),1, nvl(eru.unit,1) / erv.RATEVALUE, erv.RATEVALUE / nvl(eru.unit,1))
          INTO v_RateValue2
          FROM EXCHANGERATELISTS erl, EXCHANGERATEVALUES erv,
            (select RATELISTUNITSETID, UNIT from EXCHANGERATELISTUNITS
               where TOCURRENCYID = v_LocalCurrency
                 AND FROMCURRENCYID = v_FromCurrencyID
                 AND RATELISTTYPEID = v_RateListType) eru
         WHERE nvl(erl.RATELISTUNITSETID,-1) = eru.RATELISTUNITSETID (+)
           AND erv.EXCHANGERATELISTSEQ = erl.EXCHANGERATELISTSEQ
           AND erl.RATELISTTYPEID = v_RateListType
           AND v_SysDate BETWEEN erl.RATEVALUEDATE AND NVL(erl.RATEENDDATE, v_SysDate)
           AND erl.TOCURRENCYID = v_FromCurrencyID
           AND erv.FROMCURRENCYID = v_LocalCurrency
           AND erv.RATETYPEID = v_SellRateType;
        v_AmountTo := p_nAmount * v_RateValue1 / v_RateValue2;
        RETURN v_AmountTo;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          raise_application_error(-20102,
                                  'UDEBS::GETCHANGEDAMOUNT Cannot find general exchange rate list for branch ' ||
                                  p_nBranchID);
      END;
    ELSIF v_FromCurrencyID = v_LocalCurrency THEN
      BEGIN
        v_RateValue2 := 1;
        SELECT /*+ RESULT_CACHE */
         RATEVALUE
          INTO v_RateValue1
          FROM (SELECT decode(nvl(erl.ratelistreversed,0),1, nvl(eru.unit,1) / erv.RATEVALUE, erv.RATEVALUE / nvl(eru.unit,1)) AS RATEVALUE
                  FROM EXCHANGERATELISTS erl, EXCHANGERATEVALUES erv,
                  (select RATELISTUNITSETID, UNIT from EXCHANGERATELISTUNITS
                    where TOCURRENCYID = v_LocalCurrency
                      AND FROMCURRENCYID = v_ToCurrencyID
                      AND RATELISTTYPEID = v_RateListType) eru
                 WHERE nvl(erl.RATELISTUNITSETID,-1) = eru.RATELISTUNITSETID (+)
                   AND erv.EXCHANGERATELISTSEQ = erl.EXCHANGERATELISTSEQ
                   AND erl.RATELISTTYPEID = v_RateListType
                   AND v_SysDate BETWEEN erl.RATEVALUEDATE AND NVL(erl.RATEENDDATE, v_SysDate)
                   AND erl.TOCURRENCYID = v_ToCurrencyID
                   AND erv.FROMCURRENCYID = v_LocalCurrency
                   AND erv.RATETYPEID = v_BuyRateType
                 ORDER BY erl.RATEVALUEDATE DESC)
         WHERE ROWNUM = 1;
        v_AmountTo := p_nAmount * v_RateValue1 / v_RateValue2;
        RETURN v_AmountTo;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          raise_application_error(-20102,
                                  'UDEBS::GETCHANGEDAMOUNT Cannot find general exchange rate list for branch ' ||
                                  p_nBranchID);
      END;
    ELSE
      v_AmountTo := GETCHANGEDAMOUNT(p_nAmount,
                                     v_FromCurrencyID,
                                     v_LocalCurrency,
                                     p_nBranchID,
                                     p_nMiddleRate);
      v_AmountTo := GETCHANGEDAMOUNT(v_AmountTo,
                                     v_LocalCurrency,
                                     v_ToCurrencyID,
                                     p_nBranchID,
                                     p_nMiddleRate);
      RETURN v_AmountTO;
    END IF;
  ELSE
    BEGIN
      SELECT ExRateListSeq, nvl(RATELISTUNITSETID,-1), nvl(RATELISTREVERSED,0)
        INTO v_ExchangeListSeq, v_RATELISTUNITSETID, v_RATELISTREVERSED
        FROM ExchangeRateLists ERL,
             (SELECT MAX(EXCHANGERATELISTSEQ) ExRateListSeq
                FROM ExchangeRateLists
               WHERE RateListTypeID = v_RateListType
                 AND SYSDATE BETWEEN RateValueDate AND NVL(RATEENDDATE, SYSDATE)) ESEQ
       WHERE ERL.ExchangeRateListSeq = ESEQ.ExRateListSeq;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        raise_application_error(-20102,
                                'UDEBS::GETCHANGEDAMOUNT Cannot find general exchange rate list for branch ' ||
                                p_nBranchID);
    END;
    --
    IF v_LocalCurrency = v_FromCurrencyID THEN
      v_RateValue1 := 1;
    ELSE
      BEGIN
        SELECT decode(v_RATELISTREVERSED,1,eru.unit / erv.RateValue, erv.RateValue / eru.unit)
          INTO v_RateValue1
          FROM ExchangeRateValues erv,
                (select RATELISTUNITSETID, UNIT from EXCHANGERATELISTUNITS
                  where TOCURRENCYID = v_LocalCurrency
                    AND FROMCURRENCYID = v_FromCurrencyID
                    AND RATELISTTYPEID = v_RateListType) eru
         WHERE eru.RATELISTUNITSETID (+) = v_RATELISTUNITSETID
           AND erv.FromCurrencyID = v_FromCurrencyID
           AND erv.RateTypeID = v_BuyRateType
           AND erv.ExchangeRateListSeq = v_ExchangeListSeq;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          raise_application_error(-20102,
                                  'UDEBS::GETCHANGEDAMOUNT Cannot find exchange rate for exchange from currency ' ||
                                  nvl(v_FromCurrencyID, 'currency not set'));
      END;
    END IF;
    IF v_LocalCurrency = v_ToCurrencyID THEN
      v_RateValue2 := 1;
    ELSE
      BEGIN
        SELECT decode(v_RATELISTREVERSED,1,eru.unit / erv.RateValue, erv.RateValue / eru.unit)
          INTO v_RateValue2
          FROM ExchangeRateValues erv,
                (select RATELISTUNITSETID, UNIT from EXCHANGERATELISTUNITS
                  where TOCURRENCYID = v_LocalCurrency
                    AND FROMCURRENCYID = v_ToCurrencyID
                    AND RATELISTTYPEID = v_RateListType) eru
         WHERE eru.RATELISTUNITSETID (+) = v_RATELISTUNITSETID
           AND erv.FromCurrencyID = v_ToCurrencyID
           AND erv.RateTypeID = v_SellRateType
           AND erv.ExchangeRateListSeq = v_ExchangeListSeq;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          raise_application_error(-20102,
                                  'UDEBS::GETCHANGEDAMOUNT Cannot find exchange rate for exchange from currency ' ||
                                  nvl(v_ToCurrencyID, 'currency not set'));
      END;
    END IF;
    --
    v_AmountTo := p_nAmount * v_RateValue1 / v_RateValue2;
    --
    RETURN v_AmountTO;
  END IF;
END;
/

PROMPT CREATE OR REPLACE function GETIBAN
CREATE OR REPLACE 
FUNCTION GETIBAN ( vBankID IN VARCHAR2, vAccountNum IN VARCHAR2,
                  vCountry IN VARCHAR2 default 'SK') return varchar2
 as
--------------------
-- $release: 009 $-- $release:
-- $version: 8.0.0.0 $-- $version:
-- $Header:
--------------------
--
-- Purpose: Generate IBAN
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------
-- Ilenin      07.07.2014 UDEBS RS
--
--
   ibanacc varchar2(34);
   Aibanacc varchar2(38);
   checksum varchar2(2);
   Aibannum number (38);
   tAccountNum varchar2(34);
 begin
   select lpad(substr(vAccountNum,1,instr(replace(vAccountNum,'-',' '),' ')-1),6,'0') ||
    lpad(substr(vAccountNum,instr(replace(vAccountNum,'-',' '),' ')+1),10,'0') into tAccountNum from dual;
   Aibanacc :=  upper(LPAD(nvl(vBankID,'0'),4,'0') || LPAD(nvl(tAccountNum,'0'),16,'0') || nvl(vCountry,'CZ') ||'00');
   Aibanacc :=  replace (Aibanacc,' ','');
   Aibanacc :=  replace (Aibanacc,',','');
   Aibanacc :=  replace (Aibanacc,'-','');
   Aibanacc :=  replace (Aibanacc,'.','');
   Aibanacc :=  replace (Aibanacc,'/','');
   Aibanacc :=  replace (Aibanacc,'\','');
   Aibanacc :=  replace (Aibanacc,'_','');
   Aibanacc :=  replace (Aibanacc,'(','');
   Aibanacc :=  replace (Aibanacc,')','');
   Aibanacc :=  replace (Aibanacc,'[','');
   Aibanacc :=  replace (Aibanacc,']','');
   Aibanacc :=  replace (Aibanacc,';','');
   Aibanacc :=  replace (Aibanacc,':','');
   Aibanacc :=  replace (Aibanacc,'A','10');
   Aibanacc :=  replace (Aibanacc,'B','11');
   Aibanacc :=  replace (Aibanacc,'C','12');
   Aibanacc :=  replace (Aibanacc,'D','13');
   Aibanacc :=  replace (Aibanacc,'E','14');
   Aibanacc :=  replace (Aibanacc,'F','15');
   Aibanacc :=  replace (Aibanacc,'G','16');
   Aibanacc :=  replace (Aibanacc,'H','17');
   Aibanacc :=  replace (Aibanacc,'I','18');
   Aibanacc :=  replace (Aibanacc,'J','19');
   Aibanacc :=  replace (Aibanacc,'K','20');
   Aibanacc :=  replace (Aibanacc,'L','21');
   Aibanacc :=  replace (Aibanacc,'M','22');
   Aibanacc :=  replace (Aibanacc,'N','23');
   Aibanacc :=  replace (Aibanacc,'O','24');
   Aibanacc :=  replace (Aibanacc,'P','25');
   Aibanacc :=  replace (Aibanacc,'Q','26');
   Aibanacc :=  replace (Aibanacc,'R','27');
   Aibanacc :=  replace (Aibanacc,'S','28');
   Aibanacc :=  replace (Aibanacc,'T','29');
   Aibanacc :=  replace (Aibanacc,'U','30');
   Aibanacc :=  replace (Aibanacc,'V','31');
   Aibanacc :=  replace (Aibanacc,'W','32');
   Aibanacc :=  replace (Aibanacc,'X','33');
   Aibanacc :=  replace (Aibanacc,'Y','34');
   Aibanacc :=  replace (Aibanacc,'Z','35');
   Aibannum := to_number(Aibanacc);
   checksum := LPAD(to_char(98-mod(Aibannum,97)),2,'0');
   ibanacc := upper(nvl(vCountry,'CZ') || checksum || LPAD(nvl(vBankID,'0'),4,'0') || LPAD(nvl(tAccountNum,'0'),16,'0'));
   return ibanacc;
 end;
/

PROMPT CREATE OR REPLACE function GETNEXTTEXTID
CREATE OR REPLACE 
FUNCTION GETNEXTTEXTID
  (tabn varchar2) RETURN  Number  IS
-----------------
-- $release: 7451 $
-- $version: 8.0.2.0 $
-----------------
  tid  Number;
BEGIN
  LOOP
    Select seq_texts.nextval into tid from dual;
    BEGIN
      Insert into texts(textid,texttype) values(tid,null);
      Exit;
    Exception
      When DUP_VAL_ON_INDEX then null;
    END;
  End LOOP;
  Return tid;
END;
/

PROMPT CREATE OR REPLACE function GETPACKAGEAMOUNT
CREATE OR REPLACE 
FUNCTION GETPACKAGEAMOUNT (
    otSubtotals OUT Subtotal_tab,
    inPackageId IN  ClientTransactionPackages.PackageId%TYPE,
    inOperId    IN  ClientTransactionPackages.OperId%TYPE DEFAULT NULL,
    inStatusId  IN  ClientTransactionPackages.StatusId%TYPE DEFAULT NULL,
    inSeqNum    IN  Transactions.SeqNum%TYPE DEFAULT NULL)
RETURN NUMBER
IS
------------------
-- $release: 5028 $
-- $version: 8.0.6.0 $
------------------
-- $Header: /Oracle/UDEBS_XPE/UDEBSXPE.GETPACKAGEAMOUNT.FNC 2     22.04.14 16:47 Pataky $
-- posledni patch z UDEBS - 2506
------------------
--
-- Purpose:
-- Funkce pro zadanou package spocita celkovou castku transakci ve statusu p_nStatusId. Pokud neni zadany status,
-- jsou do souctu zahrnuty vsechny transakce.
--
-- Funkce prevadi castky transakci do meny uctu. Mena debetniho uctu je ulozena v prislusne tabulce spolecne s castkou
-- a menou castky. Predpokladem je vyplneni meny debetniho uctu v tabulce.
-- Prevod castky je provaden pomoci funkce GetChangedAmout.
--
-- MODIFICATION HISTORY
-- Person      Date       Comments
-- ----------- ---------- ---------------------
-- Ilenin      07.07.2014 UDEBS RS
-- Pataky      20.07.2015 For RBCZ collection permission LimitAmount changed to MaxAmount - 20009203-825
-- Bocak       12.10.2015 return 0 - SLSP only
-- Pataky      09.11.2015 Package amount can be recounted into local currency, based on parameter (because of multicurrency accounts)
-- Simunek     17.02.2016 SQL optimization (20009183-4886)
-- Pataky      09.03.2017 Added different xpath for currency for cash withdrawals (MYG-3224, MYG-3436) - 20009203-6741
-- Furdik      10.04.2017 return 0 for SLSP, TATR - 20009239-4035
--------------------------------------------------------
  lnPackageSum       NUMBER;
  lnOperId           ClientTransactionPackages.OperId%TYPE;
  lvAccoutCurrencyId Accounts.CurrencyId%TYPE;
  lnBranchID         Branches.BranchID%TYPE;
  lvFromCurr         varchar2(3 CHAR);
  lnAmount           number;
  lnBatch            pls_integer;
  lnAccountId        Accounts.accountid%TYPE;
  lvStructCode       Operationtypes.structcode%TYPE;
  lvXPathAmount      GDM_Parser.xpath%TYPE := GDM_PARSER001.getxpath('Amount');
  lvCollPerXPathAmount GDM_Parser.xpath%TYPE;
  lvXPathCurrId      GDM_Parser.xpath%TYPE := GDM_PARSER001.getxpath('CurrencyId');
  --
  lnPackageId        ClientTransactionPackages.packageId%TYPE := null;
  lvLocalBankCode    varchar2(20);
  lvUseLocalCurrencyInCTP varchar2(10);
  lvLocalCurrency    varchar2(5) := Reftab_GSP('LOCALCURRENCY',0,'X');
BEGIN
  lvLocalBankCode := Reftab_GSP('LOCALBANKCODEALPHA',0,'X');
  lvUseLocalCurrencyInCTP := nvl(reftab_gsp('REQUESTS.USE_LOCAL_CURRENCY_IN_CTP',0,'0'),'0');
  IF lvLocalBankCode IN ('SLSP','TATR') THEN
    otSubtotals:=Subtotal_tab(); otSubTotals.extend;
    otSubtotals(1):=Subtotal_obj('',0,0,0);
    RETURN 0;
  END IF;
  -- kontrola existencie package
  SELECT packageId
    INTO lnPackageId
    FROM ClientTransactionPackages
   WHERE packageId = inPackageId;
  IF lnPackageId IS NULL THEN
    im_error.AddError( 2113559,null,'/unspecified/GetPackageAmount/PackageId',inPackageID);
    RETURN 0;
  END IF;
  --
  SELECT nvl(inOperId,CTP.OperID),
         decode(lvUseLocalCurrencyInCTP,'1',lvLocalCurrency,A.CurrencyID),
         A.BranchID,
         get_OT_values('Batch',nvl(inOperId,CTP.OperID)),
         get_OT_values('StructCode',NVL(inOperId,CTP.operid))
    INTO lnOperId,
         lvAccoutCurrencyId,
         lnBranchID,
         lnBatch,
         lvStructCode
    FROM ClientTransactionPackages CTP,
         Accounts A
   WHERE PackageId = inPackageId
     AND A.AccountID(+)=CTP.AccountID; --outer join kvuli stornu(oper 12), ktere neni accountsensitive a nema vyplneny ucet
  IF lvStructCode = 'FPO' THEN
    lvXPathAmount := GDM_PARSER001.getxpath('PayAmount');
  END IF;
  IF lvStructCode = 'CASHWITHDR' THEN
    lvXPathCurrId := GDM_PARSER001.getxpath('WithdrawalCurrencyId');
  END IF;
  if lvLocalBankCode = 'RBCZ' then
    lvCollPerXPathAmount := GDM_PARSER001.getxpath('MaxAmount');
  else
    lvCollPerXPathAmount := GDM_PARSER001.getxpath('LimitAmount');
  end if;
  IF lvStructCode in ('COLLPER_A','COLLPER_N') THEN
    lvXPathAmount:=lvCollPerXPathAmount;
  END IF;
  IF inSeqNUm is not null THEN
    IF lvStructCode = 'CANC' THEN
      SELECT char2num(extractvalue(T.trandata,decode(get_OT_values('StructCode',CC.operid),
             'FPO',GDM_PARSER001.getxpath('PayAmount'),'COLLPER_N',lvCollPerXPathAmount,'COLLPER_A',lvCollPerXPathAmount,GDM_PARSER001.getxpath('Amount')))),
             nvl(T.CURRENCYID,CTP.CurrencyID),
             CC.accountid
        INTO lnAmount,
             lvFromCurr,
             lnAccountId
        FROM Transactions T,
             Transactions TT,
             ClientTransactionPackages CTP,
             ClientTransactionPackages CC
       WHERE T.PackageId = TT.cancelledpackageid
         AND T.seqnum = DECODE(TT.cancelledseqnum,0,T.SeqNum,TT.cancelledseqnum)
         AND CTP.PackageId = inPackageId
         AND CTP.PackageID=TT.PackageId
         AND NVL(inStatusId, TT.StatusId) = TT.StatusId
         AND TT.SeqNum = inSeqNum
         AND T.packageid = CC.packageid;
    ELSE
      SELECT T.AMOUNT,
             nvl(T.CURRENCYID,CTP.CurrencyID)
        INTO lnAmount,
             lvFromCurr
        FROM Transactions T,
             ClientTransactionPackages CTP
       WHERE CTP.PackageId = inPackageId
         AND CTP.PackageID=T.PackageId
         AND NVL(inStatusId, T.StatusId) =  T.StatusId
         AND T.StatusId <> 139 -- neberu sa do uvahy transakcie, ktore nepresli validaciami
         AND SeqNum = inSeqNum;
    END IF;
    BEGIN
      SELECT /*+ RESULT_CACHE */ CurrencyID
        into lvFromCurr
        FROM Currencies
       WHERE CurrencyID=lvFromCurr
         AND StatusID=1;
      IF lvAccoutCurrencyId IS NULL THEN  -- storno (operid 12)
        SELECT decode(lvUseLocalCurrencyInCTP,'1',lvLocalCurrency,CurrencyID),
               BranchId
          INTO lvAccoutCurrencyId,
               lnBranchID
          FROM Accounts
         WHERE AccountId = lnAccountId;
      END IF;
      lnPackageSum:=nvl(GetChangedAmount( lnAmount, lvFromCurr, lvAccoutCurrencyId, lnBranchID ),0 );
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        lnPackageSum:=null;
    END;
    IF lnBatch = 1 THEN
      otSubtotals:=Subtotal_tab(); otSubTotals.extend;
      otSubtotals(otSubtotals.last):=Subtotal_obj(lvFromCurr,1,lnAmount,lnPackageSum);
    END IF;
  ELSE
    IF lnBatch = 0 THEN
      IF lvStructCode = 'CANC' THEN
        IF lvAccoutCurrencyId IS NOT NULL THEN -- storno (operid 35)
          SELECT /*+ INDEX(Transactions IDX_PAY$PACKAGEID_SEQNUM)*/
                 NVL( SUM( GetChangedAmount( char2num(extractvalue(T.trandata,decode(get_OT_values('StructCode',CC.operid),
                 'FPO',GDM_PARSER001.getxpath('PayAmount'),'COLLPER_N',lvCollPerXPathAmount,'COLLPER_A',lvCollPerXPathAmount,GDM_PARSER001.getxpath('Amount')))),
                 nvl(T.CURRENCYID,CC.CurrencyID), lvAccoutCurrencyId, lnBranchID ) ),0 ) INTO lnPackageSum
            FROM Transactions T,
                 Transactions TT,
                 ClientTransactionPackages CTP,
                 Currencies C,
                 ClientTransactionPackages CC
           WHERE CTP.PackageId = inPackageId
             AND CTP.PackageID=TT.PackageId
             AND NVL(inStatusId, TT.StatusId) =  TT.StatusId
             AND T.PackageId = TT.cancelledpackageid
             AND T.seqnum = DECODE(TT.cancelledseqnum,0,T.SeqNum,TT.cancelledseqnum)
             AND T.packageid = CC.packageid
             AND C.CurrencyID=nvl(T.CURRENCYID,CC.CurrencyID)
             AND C.StatusID=1;
        ELSE  -- storno (operid 12)
          SELECT /*+ INDEX(Transactions IDX_PAY$PACKAGEID_SEQNUM)*/
                 NVL( SUM( GetChangedAmount( char2num(extractvalue(T.trandata,decode(get_OT_values('StructCode',CC.operid),
                 'FPO',GDM_PARSER001.getxpath('PayAmount'),'COLLPER_N',lvCollPerXPathAmount,'COLLPER_A',lvCollPerXPathAmount,GDM_PARSER001.getxpath('Amount')))),
                 nvl(T.CURRENCYID,CC.CurrencyID), decode(lvUseLocalCurrencyInCTP,'1',lvLocalCurrency,A.CurrencyID), A.BranchID ) ),0 ) INTO lnPackageSum
            FROM Transactions T,
                 Transactions TT,
                 ClientTransactionPackages CTP,
                 Currencies C,
                 ClientTransactionPackages CC,
                 Accounts A
           WHERE CTP.PackageId = inPackageId
             AND CTP.PackageID=TT.PackageId
             AND NVL(inStatusId, TT.StatusId) =  TT.StatusId
             AND T.PackageId = TT.cancelledpackageid
             AND T.seqnum = DECODE(TT.cancelledseqnum,0,T.SeqNum,TT.cancelledseqnum)
             AND T.packageid = CC.packageid
             AND CC.accountid = A.accountid
             AND C.CurrencyID=nvl(T.CURRENCYID,CC.CurrencyID)
             AND C.StatusID=1;
        END IF;
      ELSE
        SELECT /*+ INDEX(Transactions IDX_PAY$PACKAGEID_SEQNUM)*/
               NVL( SUM( GetChangedAmount( T.AMOUNT, nvl(T.CURRENCYID,CTP.CurrencyID), lvAccoutCurrencyId, lnBranchID ) ),0 )
          INTO lnPackageSum
          FROM Transactions T,
               ClientTransactionPackages CTP,
               Currencies C
         WHERE CTP.PackageId = inPackageId
           AND CTP.PackageID=T.PackageId
           AND NVL(inStatusId, T.StatusId) =  T.StatusId
           AND T.statusId <> 139 -- neberu sa do uvahy transakcie, ktore nepresli validaciami
           AND C.CurrencyID=nvl(T.CURRENCYID,CTP.CurrencyID)
           AND C.StatusID=1;
      END IF;
    ELSE
      IF lvStructCode = 'CANC' THEN
        IF lvAccoutCurrencyId IS NOT NULL THEN -- storno (operid 35)
          WITH TRAN AS (
            SELECT /*+ INDEX(Transactions IDX_PAY$PACKAGEID_SEQNUM)*/
                   C.Currencyid CurrID,
                   char2num(extractvalue(T.trandata,
                     -- xpath:
                     CASE get_OT_values('StructCode', CC.operid)
                       WHEN 'FPO' THEN GDM_PARSER001.getxpath('PayAmount')
                       WHEN 'COLLPER_N' THEN lvCollPerXPathAmount
                       WHEN 'COLLPER_A' THEN lvCollPerXPathAmount
                       ELSE GDM_PARSER001.getxpath('Amount')
                     END)) Amount
              FROM ClientTransactionPackages CTP
              INNER JOIN Transactions TT
                ON (CTP.PackageID = TT.PackageId
                AND (TT.StatusId = inStatusId OR inStatusId is NULL))
              INNER JOIN Transactions T
                ON (T.PackageId = TT.cancelledpackageid
                AND T.seqnum = CASE TT.cancelledseqnum
                                 WHEN 0 THEN T.SeqNum
                                 ELSE TT.cancelledseqnum
                               END)
              INNER JOIN ClientTransactionPackages CC
                ON (T.packageid = CC.packageid)
              LEFT OUTER JOIN Currencies C
                ON (C.Currencyid = coalesce(T.CURRENCYID, CC.Currencyid)
                AND C.Statusid = 1)
             WHERE CTP.PackageId = inPackageId
          )
          SELECT CAST(MULTISET(
            SELECT
              TRAN.CurrID as CurrID,
              COUNT(*) Cnt,
              SUM(TRAN.Amount) as Amount,
              SUM(CASE TRAN.CurrID
                WHEN NULL THEN NULL
                ELSE coalesce(
                  GetChangedAmount(
                    TRAN.Amount,
                    TRAN.CurrID,
                    lvAccoutCurrencyId,
                    lnBranchID),
                  0)
                END) AccAmount
            FROM TRAN
            GROUP BY TRAN.CurrID
          ) AS subtotal_tab)
          INTO otSubtotals
          FROM dual;
        ELSE  -- storno (operid 12)
          WITH TRAN AS (
            SELECT /*+ INDEX(Transactions IDX_PAY$PACKAGEID_SEQNUM)*/
                   C.Currencyid CurrID,
                   char2num(extractvalue(T.trandata,
                     -- xpath:
                     CASE get_OT_values('StructCode', CC.operid)
                       WHEN 'FPO' THEN GDM_PARSER001.getxpath('PayAmount')
                       WHEN 'COLLPER_N' THEN lvCollPerXPathAmount
                       WHEN 'COLLPER_A' THEN lvCollPerXPathAmount
                       ELSE GDM_PARSER001.getxpath('Amount')
                     END)) Amount,
                   A.CurrencyID AccCurrID,
                   A.BranchID AccBranchID
              FROM ClientTransactionPackages CTP
              INNER JOIN Transactions TT
                ON (CTP.PackageID = TT.PackageId
                AND (TT.StatusId = inStatusId OR inStatusId is NULL))
              INNER JOIN Transactions T
                ON (T.PackageId = TT.cancelledpackageid
                AND T.seqnum = CASE TT.cancelledseqnum
                                 WHEN 0 THEN T.SeqNum
                                 ELSE TT.cancelledseqnum
                               END)
              INNER JOIN ClientTransactionPackages CC
                ON (T.packageid = CC.packageid)
              INNER JOIN Accounts A
                ON (CC.accountid = A.accountid)
              LEFT OUTER JOIN Currencies C
                ON (C.Currencyid = coalesce(T.CURRENCYID, CC.Currencyid)
                AND C.Statusid = 1)
             WHERE CTP.PackageId = inPackageId
          )
          SELECT CAST(MULTISET(
            SELECT
              TRAN.CurrID as CurrID,
              COUNT(*) Cnt,
              SUM(TRAN.Amount) as Amount,
              SUM(CASE TRAN.CurrID
                WHEN NULL THEN NULL -- mena v Currencies neexistuje nebo je neplatna => bez prevodu
                ELSE coalesce(
                  GetChangedAmount(
                    TRAN.Amount,
                    TRAN.CurrID,
                    decode(lvUseLocalCurrencyInCTP, '1', lvLocalCurrency, TRAN.AccCurrID),
                    TRAN.AccBranchID),
                  0)
                END) AccAmount
            FROM TRAN
            GROUP BY TRAN.CurrID
          ) AS subtotal_tab)
          INTO otSubtotals
          FROM dual;
        END IF;
      ELSE
        WITH TRAN AS (
          SELECT /*+ INDEX(Transactions IDX_PAY$PACKAGEID_SEQNUM)*/
                 C.Currencyid CurrID,
                 T.AMOUNT
          FROM ClientTransactionPackages CTP
          INNER JOIN Transactions T
            ON (CTP.PackageID = T.PackageId
            AND (T.StatusId = inStatusId OR inStatusId is NULL)
            -- neberu sa do uvahy transakcie, ktore nepresli validaciami
            AND T.statusId <> 139)
          LEFT OUTER JOIN Currencies C
            ON (C.Currencyid = coalesce(T.CURRENCYID, CTP.Currencyid)
            AND C.Statusid = 1)
          WHERE CTP.PackageId = inPackageId
        )
        SELECT CAST(MULTISET(
          SELECT
            TRAN.CurrID as CurrID,
            COUNT(*) Cnt,
            SUM(TRAN.Amount) as Amount,
            SUM(CASE TRAN.CurrID
              WHEN NULL THEN NULL -- mena v Currencies neexistuje nebo je neplatna => bez prevodu
              ELSE coalesce(
                GetChangedAmount(
                  TRAN.Amount,
                  TRAN.CurrID,
                  lvAccoutCurrencyId,
                  lnBranchID),
                0)
              END) AccAmount
          FROM TRAN
          GROUP BY TRAN.CurrID
        ) AS subtotal_tab)
        INTO otSubtotals
        FROM dual;
      END IF;
      IF otSubtotals.COUNT > 0 THEN -- pri prazdne package (statusid=29) se nenaplni
        FOR i IN otSubtotals.FIRST..otSubtotals.LAST LOOP
          lnPackageSum:=NVL(lnPackageSum,0)+NVL(otSubtotals(i).AccCurrAmount,0);
        END LOOP;
      END IF;
    END IF;
  END IF;
  RETURN lnPackageSum;
EXCEPTION
  WHEN NO_DATA_FOUND THEN -- pri select pro inSeqNum > 0
    IF lnBatch = 1 THEN
      otSubtotals:=Subtotal_tab(); otSubTotals.extend;
    END IF;
    RETURN 0;
END GetPackageAmount;
/

PROMPT CREATE OR REPLACE function CHAR2NUM
CREATE OR REPLACE 
FUNCTION CHAR2NUM
  (A Varchar2,
   inDefaultVal IN NUMBER DEFAULT NULL,
   ivFormat IN VARCHAR2 DEFAULT NULL) RETURN NUMBER DETERMINISTIC
IS
-------------------
-- $release: 009 $
-- $version: 8.0.0.0 $
-------------------
-- MODIFICATION HISTORY
-- Person      Date       Comments
-- ----------- ---------- ---------------------
-- Ilenin      07.07.2014 UDEBS RS
--------------------------------------------------------
   N Number;
BEGIN
  BEGIN
    N:=TO_NUMBER('9.9');
    IF ivFormat IS NULL THEN
      Return TO_NUMBER(REPLACE(A,',','.'));
    ELSE
      Return TO_NUMBER(REPLACE(A,',','.'), ivFormat);
    END IF;
  EXCEPTION
    When OTHERS then
      IF ivFormat IS NULL THEN
        Return TO_NUMBER(REPLACE(A,'.',','));
      ELSE
        Return TO_NUMBER(REPLACE(A,',','.'), ivFormat);
      END IF;
  END;
EXCEPTION
  WHEN others THEN
    IF sqlcode = -6502 AND inDefaultVal IS NOT NULL THEN
      -- v pripade chyby konverzie retazca na cislo, ak je zadana default hodnota, vratim ju
      RETURN inDefaultVal;
    ELSE
      -- inak raisnem chybu
      RAISE;
    END IF;
END;
/

PROMPT CREATE OR REPLACE function GETPACKAGEAMOUNT_ALL
CREATE OR REPLACE 
FUNCTION GETPACKAGEAMOUNT_ALL (
    otSubtotals_All OUT Subtotal_all_tab,
    inPackageId     IN  ClientTransactionPackages.PackageId%TYPE,
    inOperId        IN  ClientTransactionPackages.OperId%TYPE DEFAULT NULL,
    inTMP           IN  NUMBER DEFAULT 1)
RETURN NUMBER
IS
------------------
-- $release: 4330 $
-- $version: 8.0.9.0 $
------------------
-- $Header:  $
-- posledni patch z UDEBS - 2506
------------------
--
-- Purpose:
-- Funkce pro zadanou package spocita celkovou castku transakci ve statusu p_nStatusId. Pokud neni zadany status,
-- jsou do souctu zahrnuty vsechny transakce.
--
-- Funkce prevadi castky transakci do meny uctu. Mena debetniho uctu je ulozena v prislusne tabulce spolecne s castkou
-- a menou castky. Predpokladem je vyplneni meny debetniho uctu v tabulce.
-- Inkasa sa zgrupuju aj podla schemy.
-- Prevod castky je provaden pomoci funkce GetChangedAmout.
--
-- MODIFICATION HISTORY
-- Person      Date       Comments
-- ----------- ---------- ---------------------
-- Bocak       14.04.2015 UDEBS RS
-- Bocak       12.10.2015 tunning main select
-- Pataky      09.11.2015 Package amount can be recounted into local currency, based on parameter (because of multicurrency accounts)
-- Bocak       11.12.2015 recalculate only TMP transactions if exixt
-- Simunek     12.02.2016 SQL optimization (20009183-4886)
-- Bocak       08.03.2016 changed to_number on char2num
-- Bocak       10.06.2016 extended select by joinig on CURRENCY - 20009245-1591
-- Bocak       21.10.2016 added /*+ USE_HASH (C)  */ - 20009245-3261
-- Bocak       31.01.2017 new input parameter inTMP (calling from getpackageamounts) - 20009238-485
--------------------------------------------------------
  lnPackageSum       NUMBER;
  lvAccoutCurrencyId Accounts.CurrencyId%TYPE;
  lnBranchID         Branches.BranchID%TYPE;
  lnBatch            pls_integer;
  lvXPathAmount      GDM_Parser.xpath%TYPE := GDM_PARSER001.getxpath('Amount');
  lvXPathCurrId      GDM_Parser.xpath%TYPE := GDM_PARSER001.getxpath('CurrencyId');
  lvXPathScheme      GDM_Parser.xpath%TYPE := GDM_PARSER001.getxpath('Scheme');
  lvUseLocalCurrencyInCTP varchar2(10);
  --
  lnCount            NUMBER;
  ltSubtotals_All    Subtotal_all_tab:=Subtotal_all_tab();
BEGIN
  lvUseLocalCurrencyInCTP := nvl(reftab_gsp('REQUESTS.USE_LOCAL_CURRENCY_IN_CTP',0,'0'),'0');
  SELECT decode(lvUseLocalCurrencyInCTP,'1',Reftab_GSP('LOCALCURRENCY',0,'X'),A.CurrencyID),
         A.BranchID,
         get_OT_values('Batch',nvl(inOperId,CTP.OperID))
    INTO lvAccoutCurrencyId,
         lnBranchID,
         lnBatch
    FROM ClientTransactionPackages CTP,
         Accounts A
   WHERE PackageId = inPackageId
     AND A.AccountID(+)=CTP.AccountID;
  --
  SELECT COUNT(1) INTO lnCount FROM Transactions_TMP WHERE PackageId = inPackageId;
  IF lnCount>0 AND inTMP=1 THEN
    --
    select subtotals_all into ltSubtotals_All from clienttransactionpackages where packageid=inPackageId;
    --
    --
    IF ltSubtotals_All IS NOT NULL THEN
      for i in ltSubtotals_All.first..ltSubtotals_All.last
      LOOP
        --
        for j in (select tt.seqnum seqnum,CHAR2NUM(extractvalue(tt.trandata,'/TranData/Amount')) Amount,extractvalue(tt.trandata,'/TranData/CurrencyID') CurrencyID,extractvalue(tt.trandata,'/TranData/Scheme') Scheme,
                         NVL(t.seqnum,0) seqnum_old,CHAR2NUM(extractvalue(t.trandata,'/TranData/Amount')) Amount_old,extractvalue(t.trandata,'/TranData/CurrencyID') CurrencyID_old,extractvalue(tt.trandata,'/TranData/Scheme') Scheme_old
                  from transactions_tmp tt,
                       Transactions T
                 WHERE Tt.PackageId=inPackageId
                   AND t.packageid(+)=tt.packageid
                   AND t.transactionid(+)=tt.transactionid
                   AND t.seqnum(+)=tt.seqnum)
        LOOP
          --
          if CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID_old and j.CurrencyID=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')=NVL(j.Scheme,'x') and j.Amount!=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA bola zmenena, MENA nebola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Odrata novu sumu a prepocitanu sumu');
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount-j.Amount;
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID_old and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')=NVL(j.Scheme,'x') and j.Amount!=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA bola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Odrata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount-1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount-j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount-GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID_old and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')=NVL(j.Scheme,'x') and j.Amount=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA nebola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Odrata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount-1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount-j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount-GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')=NVL(j.Scheme,'x') and j.Amount!=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA bola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Prirata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount+1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount+j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount+GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')=NVL(j.Scheme,'x') and j.Amount=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA nebola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Prirata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount+1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount+j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount+GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
    ---------------------------
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID_old and j.CurrencyID=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')!=NVL(j.Scheme,'x') and j.Amount!=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA bola zmenena, MENA nebola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Odrata novu sumu a prepocitanu sumu');
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount-j.Amount;
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID_old and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')!=NVL(j.Scheme,'x') and j.Amount!=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA bola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Odrata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount-1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount-j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount-GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID_old and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')!=NVL(j.Scheme,'x') and j.Amount=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA nebola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Odrata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount-1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount-j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount-GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')!=NVL(j.Scheme,'x') and j.Amount!=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA bola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Prirata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount+1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount+j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount+GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')!=NVL(j.Scheme,'x') and j.Amount=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA nebola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Prirata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount+1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount+j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount+GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))=0 AND ltSubtotals_All(i).CurrencyID=j.CurrencyID AND NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme,'x') THEN
    ---------------------------
            DBMS_OUTPUT.PUT_LINE('* Nova tranzakcia !');
            DBMS_OUTPUT.PUT_LINE('* Prirata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount+1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount+j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount+GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          end if;
        end loop;--j
        --
        if ltSubtotals_All(i).TranCount=0 then
          DBMS_OUTPUT.PUT_LINE('delete');
          ltSubtotals_All.delete(i);
        end if;
      end loop;--i
      --
      -- new transactions
      FOR j IN (select COUNT(*) cnt,
                       SUM(CHAR2NUM(extractvalue(tt.trandata,'/TranData/Amount'))) Amount,
                       extractvalue(tt.trandata,'/TranData/CurrencyID') CurrencyID,
                       extractvalue(tt.trandata,'/TranData/Scheme') Scheme
                  from transactions_tmp tt
                 WHERE Tt.PackageId=inPackageId
                   AND tt.transactionid NOT IN (SELECT t.transactionid FROM Transactions t WHERE t.PackageId=inPackageId)
                   AND (extractvalue(tt.trandata,'/TranData/CurrencyID') NOT IN (SELECT CURRENCYID FROM TABLE(ltSubtotals_All)) OR
                        extractvalue(tt.trandata,'/TranData/Scheme') NOT IN (SELECT SCHEME FROM TABLE(ltSubtotals_All)))
                 GROUP BY extractvalue(tt.trandata,'/TranData/CurrencyID'),extractvalue(tt.trandata,'/TranData/Scheme'))
      LOOP
          ltSubtotals_All.extend;
          ltSubtotals_All(ltSubtotals_All.LAST):=Subtotal_all_obj(NULL,NULL,NULL,NULL,NULL);
          ltSubtotals_All(ltSubtotals_All.LAST).TranCount:=j.cnt;
          ltSubtotals_All(ltSubtotals_All.LAST).OrigCurrAmount:=j.Amount;
          ltSubtotals_All(ltSubtotals_All.LAST).AccCurrAmount:=GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          ltSubtotals_All(ltSubtotals_All.LAST).Scheme:=J.Scheme;
          ltSubtotals_All(ltSubtotals_All.LAST).CurrencyID:=j.CurrencyID;
      END LOOP;
      --
      otSubtotals_All:=ltSubtotals_All;
    END IF;
  ELSE
    --
    WITH TRAN AS (
      SELECT /*+ USE_HASH (C)  */ nvl(T.CURRENCYID, CTP.CurrencyID) CurrID,
             CASE WHEN lvXPathScheme IS NULL THEN NULL ELSE extractvalue(trandata,lvXPathScheme) END Scheme,
             T.AMOUNT Amount
      FROM Transactions T,
           ClientTransactionPackages CTP,
           Currencies C
      WHERE CTP.PackageId = inPackageId
        AND CTP.PackageID = T.PackageId
        AND C.CurrencyID=nvl(T.CURRENCYID,CTP.CurrencyID)
        AND C.StatusID=1
        AND T.StatusId!=0
    )
    SELECT CAST(MULTISET(
      SELECT
        TRAN.CurrID,
        TRAN.Scheme,
        COUNT(*) Cnt,
        SUM(TRAN.Amount) Amount,
        NVL( SUM( GetChangedAmount( TRAN.Amount, TRAN.CurrID, lvAccoutCurrencyId, lnBranchID ) ),0 ) AccAmount
      FROM TRAN
      GROUP BY
        TRAN.CurrID,
        TRAN.Scheme
    ) AS SUBTOTAL_ALL_TAB)
    INTO otSubtotals_All
    FROM DUAL;
  END IF;
  --
  IF otSubtotals_All IS NOT NULL THEN
    IF otSubtotals_All.COUNT > 0 THEN     -- pri prazdne package (statusid=29) se nenaplni
      FOR i IN otSubtotals_All.FIRST..otSubtotals_All.LAST LOOP
        lnPackageSum:=NVL(lnPackageSum,0)+NVL(otSubtotals_All(i).AccCurrAmount,0);
      END LOOP;
    END IF;
  END IF;
  --
  RETURN lnPackageSum;
EXCEPTION
  WHEN NO_DATA_FOUND THEN -- pri select pro inSeqNum > 0
dbms_output.put_line('no data found');
    IF lnBatch = 1 THEN
      otSubtotals_All:=Subtotal_all_tab(); otSubTotals_All.extend;
    END IF;
    RETURN 0;
END GetPackageAmount_ALL;
/

PROMPT CREATE OR REPLACE function GETTEXT
CREATE OR REPLACE 
FUNCTION GETTEXT
  -- Funkcia vrati text zodpovedajuci text z tabulky LanguageTexts_T
  (
  aTextID       NUMBER,                     -- TextID
  aLangID       VARCHAR2,                   -- LangID
  aPurposeID    VARCHAR2 DEFAULT 'DEFAULT'  -- PurposeID  - parameter specifikuje ucel (DFAULT - povodny text pre prislusne TID, LTD);
  ) RETURN VARCHAR2 IS
  ------------------
  -- $release: 009 $
  -- $version: 8.0.0.0 $
  ------------------
  vOutText        LanguageTexts.Text%TYPE;
  vSubstPurposeID TextPurposeTypes.SubstPurposeID%TYPE := null;
  --
BEGIN
  BEGIN
    SELECT Text INTO vOutText
      FROM LanguageTexts
     WHERE PurposeID = aPurposeID
       AND TextID = aTextID
       AND LangID = aLangID;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      BEGIN
        SELECT Text INTO vOutText
          FROM LanguageTexts
         WHERE PurposeID = aPurposeID
           AND TextID = aTextID
           AND LangID = (SELECT SubstLangID FROM SupportedLang WHERE LangID=aLangID);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          BEGIN
            SELECT SubstPurposeID INTO vSubstPurposeID
              FROM TextPurposeTypes
             WHERE PurposeID != SubstPurposeID
               AND PurposeID = aPurposeID;
            BEGIN
              SELECT Text INTO vOutText
                FROM LanguageTexts
               WHERE PurposeID = vSubstPurposeID
                 AND TextID = aTextID
                 AND LangID = aLangID;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                BEGIN
                  SELECT Text INTO vOutText
                    FROM LanguageTexts
                   WHERE PurposeID = vSubstPurposeID
                     AND TextID = aTextID
                     AND LangID = (SELECT SubstLangID FROM SupportedLang WHERE LangID=aLangID);
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    vOutText := TO_CHAR(NULL);
                END;
            END;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              vOutText := TO_CHAR(NULL);
          END;
      END;
  END;
  RETURN vOutText;
END;
/

PROMPT CREATE OR REPLACE function REFTAB_GSP
CREATE OR REPLACE 
FUNCTION REFTAB_GSP (
    ivParam    IN GlobalSystemParameter.ParamName%TYPE,
    inBranchID IN NUMBER default 0,
    ivDefault  IN GlobalSystemParameter.Value%TYPE DEFAULT NULL)
RETURN VARCHAR2 RESULT_CACHE RELIES_ON (GLOBALSYSTEMPARAMETER) IS
-------------------
-- $release: 5422 $
-- $version: 8.0.3.1 $
-------------------
  lvValue           GlobalSystemParameter.Value%TYPE;
BEGIN
  SELECT Value  INTO lvValue FROM GlobalSystemParameter
   WHERE Upper(ParamName) = Upper(ivParam);
  RETURN nvl(lvValue,ivDefault);
EXCEPTION WHEN NO_DATA_FOUND THEN
  RETURN ivDefault;
END;
/

PROMPT CREATE OR REPLACE function GRANTS
CREATE OR REPLACE 
FUNCTION GRANTS (aChannelId       IN Channels.ChannelId%TYPE,                -- ak je kanal NULL, prava sa netestuju (volanie zo spravcovskej aplikacie)
                 aClientId        IN Clients.ClientId%TYPE,                  -- vzdy vyplnene
                 aUserId          IN Users.UserId%TYPE,                      -- ak nie je vyplnene, nejedna sa o prihlasenie cez konkretne zariadenie - testuje sa len podla ClientRights, nie podla profilu (v tom pripade musi byt vyplnene ClientId)
                 aAccessTypeId    IN OperationAccessTypes.AccessTypeId%TYPE, -- typ pristupu - ak existuje vyssie pravo ako view, tj. create, edit, delete, ... nie je nutne aby existoval zaznam s pravom view a test prav uspesne prejde
                 aOperId          IN OperationTypes.OperId%TYPE,             -- prava sa testuju iba ak je operacia AuthorReq
                 aAccountId       IN Accounts.AccountId%TYPE,                -- ak nie je vyplnene testuje sa bez prepojenia profilu na ucet, ak je vyplnene, testuje sa iba ak operacia je AccountSensitive
                 aCheckAnyChannel IN PLS_INTEGER default 0)                  -- ak je poslane 1, tak bez ohledu na aChannelId sa kontroluje existance prava alespon do jednoho (libovolneho) kanalu
RETURN PLS_INTEGER
AUTHID DEFINER
RESULT_CACHE RELIES_ON (RightProfileDefinitions,ClientUsers,ClientUserChannelProfiles,RightAssignClientAccounts)
IS
------------------
-- $release: 6663 $
-- $version: 8.0.14.0 $
------------------
--
-- MODIFICATION HISTORY
--
-- Person       Date       Comments
-- ------------ ---------- ---------------------------------------------------------
-- Ilenin       24.06.2014 UDEBS RS
-- Bocak        23.01.2015 Added check if operation ID exists
-- Pataky       10.03.2015 Added multicurrency support
-- Pataky       27.03.2015 Exception for operid 501 supressed for SLSP
-- Pataky       31.03.2015 Removed exception for operid 501 - 20009183-2059
-- Bocak        22.06.2015 Extend queries by joining on ClientRightProfiles (20009183-3365)
-- Bocak        25.06.2015 Check ClientUsers.StatusId=1 (20009183-3486)
-- Pataky       10.09.2015 Added relies on clause to flush cache to keep rights consistent after DML
-- Pataky       15.04.2016 Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-- Pataky       07.10.2016 Added AvailableFor columns checking - 20009213-2051
-- Pataky       20.01.2017 Added aCheckAnyChannel parameter - 20009213-1912
-- Pataky       24.01.2017 Changed aCheckAnyChannel to PLS_INTEGER - 20009213-1912
-- Emmer        23.05.2017 Rewrite acc sensitive select + hint FIRST_ROWS (MYG-4841) - P20009246-337
-- Pataky       03.05.2018 Added check of OperationTypes.StatusID column - P20009317-19
-- --------------------------------------------------------------------------------
  vPoc                  PLS_INTEGER := 0; -- standardne nepovolime nic
  vAlwaysSuppAccessVIEW GlobalSystemParameter.Value%TYPE;
  vCheckChannelStatus   GlobalSystemParameter.Value%TYPE := '1';
  lnUserId              Users.UserId%type := null;
BEGIN
  SELECT 1 INTO vPoc FROM OperationTypes WHERE OperId=aOperId and nvl(StatusID,1)=1 and nvl(AvailableForClient,1)=1; -- check inOperID, StatusID and AvailableForClient flag
  vPoc:=0;
  --
  IF reftab_ot.AuthorReq(aOperId,0) = 1 AND (aChannelId IS NOT NULL or nvl(aCheckAnyChannel,0)=1) THEN -- ak sa maju pre operaciu overovat prava, v spravcovskych aplikaciach sa nepozaduje ziadne pravo
    IF aUserId IS NULL THEN -- testuje sa len klient, nejedna sa o prihlasenie cez konkretne zariadenie
      vCheckChannelStatus := reftab_gsp('AlwaysCheckChannelStatus',0,'1');
      SELECT 1
        INTO vPoc
        FROM RightTypes rt,
             ClientRights cr,
             ClientChannelProfiles cchp
       WHERE rt.OperId = aOperId
         AND (rt.ChannelId = aChannelId or nvl(aCheckAnyChannel,0)=1)
         AND ((aAccessTypeId=1 AND rt.AccessTypeId>=1) OR (rt.AccessTypeId = aAccessTypeId))
         AND rt.RightId = cr.RightId
         AND cr.ClientId = aClientId
         AND cr.ClientId = cchp.ClientId
         AND (cchp.StatusId = 1 OR vCheckChannelStatus='0');
    ELSE -- aUserId IS NOT NULL
      IF aAccountId IS NULL OR reftab_ot.AccountSensitive(aOperId)=0 THEN
         vCheckChannelStatus := reftab_gsp('AlwaysCheckChannelStatus',0,'1');
          SELECT /*+FIRST_ROWS*/ 1
            INTO vPoc
            FROM RightTypes rt,
                 RightProfileDefinitions rpd,
                 ClientRights cr,
                 ClientUserRightProfiles curp,
                 ClientChannelProfiles cchp,
                 ClientUserChannelProfiles cuchp,
                 ClientUsers cu,
                 ClientRightProfiles crp
           WHERE -- joins
                 rt.RightId = cr.RightId
             AND rpd.ClientRightId = cr.ClientRightId
             AND rpd.RightProfileId = curp.RightProfileId
             AND curp.clientusersid = cu.clientusersid
             AND cuchp.clientusersid = cu.clientusersid
             AND cu.clientid = cr.ClientId
             AND cchp.ClientId = cr.ClientId
                 -- filters
             AND rt.OperId = aOperId
             AND (rt.ChannelId = aChannelId or nvl(aCheckAnyChannel,0)=1)
             AND ((aAccessTypeId = 1 AND rt.AccessTypeId >= 1) OR (rt.AccessTypeId = aAccessTypeId))
             AND cu.UserId = aUserId
             AND cu.clientid = aClientId
             AND (cchp.StatusId = 1  or vCheckChannelStatus='0')
             AND (cuchp.StatusId = 1  or vCheckChannelStatus='0')
             AND crp.RightProfileId=rpd.RightProfileId
             AND crp.ClientId=cchp.ClientId
             AND crp.StatusId=1
             AND cu.StatusId=1;
       ELSE
        vAlwaysSuppAccessVIEW := reftab_gsp('AlwaysSuppAccessVIEW');
          vCheckChannelStatus := reftab_gsp('AlwaysCheckChannelStatus',0,'1');
          SELECT /*+FIRST_ROWS*/ 1
            INTO vPoc
            FROM ClientUsers cu
            JOIN ClientAccounts ca ON ca.AccountId = aAccountId AND ca.clientid = cu.clientid AND (ca.StatusId = 1 OR (vAlwaysSuppAccessVIEW = '1' AND aAccessTypeId = 1))
            JOIN ClientUserRightProfiles curp ON curp.clientusersid = cu.clientusersid
            JOIN ClientRightProfiles crp ON crp.RightProfileId=curp.RightProfileId AND crp.ClientId=cu.clientid AND crp.StatusId=1
            JOIN ClientUserChannelProfiles cuchp ON cuchp.clientusersid = cu.clientusersid AND (cuchp.StatusId = 1 or vCheckChannelStatus = '0')
            JOIN ClientRights cr ON cuchp.ClientId = cr.ClientId
            JOIN RightTypes rt ON rt.RightId = cr.RightId AND rt.OperId = aOperId
                                  AND ((aAccessTypeId = 1 AND rt.AccessTypeId >= 1) OR (rt.AccessTypeId = aAccessTypeId))
                                  AND (rt.ChannelId = aChannelId or nvl(aCheckAnyChannel,0)=1)
            JOIN RightAssignClientAccounts raca ON raca.ClientRightId = cr.ClientRightId AND raca.RightProfileId = curp.RightProfileId AND raca.AccountAccessId = ca.AccountAccessId
           WHERE cu.clientid = aClientId
             AND cu.UserId = aUserId
             AND cu.StatusId = 1;
      END IF;
    END IF;
  ELSIF reftab_ot.AuthorReq(aOperId,0) = 1 AND reftab_ot.AccountSensitive(aOperId,0) = 0 AND aChannelId IS NULL THEN -- ak sa maju pre operaciu overovat prava, v spravcovskych aplikaciach sa nepozaduje ziadne pravo
    IF aUserId IS NULL THEN -- testuje sa len klient, nejedna sa o prihlasenie cez konkretne zariadenie
      SELECT 1
        INTO vPoc
        FROM RightTypes rt,
             ClientRights cr
       WHERE rt.OperId = aOperId
         AND ((aAccessTypeId=1 AND rt.AccessTypeId>=1) OR (rt.AccessTypeId = aAccessTypeId))
         AND rt.RightId = cr.RightId
         AND cr.ClientId = aClientId;
    END IF;
  ELSIF reftab_ot.AuthorReq(aOperId,0) = 0 THEN
    vPoc := 1;
  END IF;
  RETURN vPoc;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 0;
  WHEN TOO_MANY_ROWS THEN
    RETURN 1;
END grants;
/

PROMPT CREATE OR REPLACE function OPERATOR_GRANTS2
CREATE OR REPLACE 
FUNCTION OPERATOR_GRANTS2 (inChannelId    IN Channels.ChannelId%TYPE, -- channel which is used to realize the operation, mandatory (for admin API it is always "system administration" channel)
                 inClientId              IN Clients.ClientId%TYPE, -- the client under which the operation is to be realized, optional (for admin API it is always "bank" service client - if not filled then selected from GSP parameter)
                 inUserId                IN Users.UserId%TYPE, -- client user ID
                 inOperatorId            IN Users.UserId%TYPE,  -- operator user ID, mandatory
                 inAccessTypeId          IN OperationAccessTypes.AccessTypeId%TYPE,  -- operation access type to test for operator, mandatory
                 inOperId                IN OperationTypes.OperId%TYPE,   -- operation ID, mandatory
                 ivOperatorRoles         IN VARCHAR2,    -- comma delimited list of names of operator roles - i.e., right profiles assigned to operator
                 ivMessageCategoryId     IN VARCHAR2 DEFAULT NULL,    -- ID of checked message category - i.e., whether a right for this msg category exists in some of client operator profiles, optional
                 inMessageCategoryCheck  IN NUMBER DEFAULT NULL,      -- flag if message category assignment should be checked, mandatory
                 inTargetClientCategID   IN NUMBER DEFAULT NULL,      -- target client ID - client whose data will be modified or accessed, optional, default is taken from GSP parameter; if used, target client's segment will be checked against role segments
                 inClientCategoryCheck   IN NUMBER DEFAULT NULL       -- flag if target client segment assignment should be checked - optional, default is taken from GSP parameter
                 )
RETURN PLS_INTEGER
AUTHID DEFINER
RESULT_CACHE RELIES_ON (RightProfileDefinitions,RightProfileClientCategories,RightProfileMessageCategories)
IS
------------------
-- $release: 6664 $
-- $version: 8.0.4.0 $
------------------
--
-- MODIFICATION HISTORY
--
-- Person       Date       Comments
-- ------------ ---------- ---------------------------------------------------------
-- Pataky       31.01.2017 Initial version, based on OPERATOR_GRANTS function, only TargetClientID changed to TargetClientCategID (MYG-2294, MYG-2317, MYG-2122, MYG-2280) - 20009203-6996
-- Pataky       12.04.2017 Added check on channel availability for inClientCategoryCheck parameter (MYG-3454) - 20009246-337
-- Pataky       19.07.2017 Updated check on channel availability for inClientCategoryCheck parameter - 20009271-178
-- Pataky       03.05.2018 Added check of OperationTypes.StatusID column - P20009317-19
-- --------------------------------------------------------------------------------
--
  vPoc                  PLS_INTEGER := 0;
  vResult               PLS_INTEGER := 0; -- standardne nepovolime nic
  lnUserId              Users.UserId%type := null;
  lnClientID            Clients.ClientID%type := inClientID;
  lnChannelId           Channels.ChannelId%TYPE;
  lnRightProfileID      ClientRightProfiles.rightprofileid%TYPE;
  lnRightProfileTypeID  ClientRightProfiles.rightprofiletypeid%TYPE;
  row_count             NUMBER := 0;
  lbRightsSet           BOOLEAN := FALSE;
  lbSegmentsSet         BOOLEAN := FALSE;
  lnMessageCategoryCheck pls_integer:=inMessageCategoryCheck;
  lnClientCategoryCheck pls_integer:=inClientCategoryCheck;
BEGIN
  lnChannelId := 11; -- default 11 - System Administration channel
  IF inChannelId is not null THEN
    lnChannelId := inChannelId;
  END IF;
  SELECT 1 INTO vPoc FROM OperationTypes WHERE OperId=inOperId and nvl(StatusID,1)=1 and nvl(AvailableForOperator,1)=1; -- check inOperID, StatusID and AvailableForOperator flag
  IF inClientId is not null then
    SELECT ClientID INTO lnClientID FROM Clients -- check ClientID against GSP TECHNICAL_BANK_CLIENT_ID
      WHERE ClientID=inClientId AND inClientId=(select to_number(value) from GlobalSystemParameter where paramname='TECHNICAL_BANK_CLIENT_ID');
  else
    select to_number(value) INTO lnClientID from GlobalSystemParameter where paramname='TECHNICAL_BANK_CLIENT_ID';
  end if;
  if lnMessageCategoryCheck is null then
    begin
      select nvl(to_number(value),0) INTO lnMessageCategoryCheck from GlobalSystemParameter where paramname='OPERATOR_MESSAGE_CATEGORIES_FILTERING';
    exception when no_data_found then
      lnMessageCategoryCheck:=0;
    end;
  end if;
  begin
    select nvl(to_number(value),lnClientCategoryCheck) INTO lnClientCategoryCheck from GlobalSystemParameter
      where paramname='USE_CLIENT_CATEG_FOR_OPERATOR_AUTH_IN_ONBEHALF' and lnChannelId=41; -- OnBehalf channel exception
  exception when no_data_found then
    null;
  end;
  if lnClientCategoryCheck is null or lnClientCategoryCheck = 1 then
    begin
      select nvl(to_number(value),nvl(lnClientCategoryCheck,0)) INTO lnClientCategoryCheck from GlobalSystemParameter where paramname='OPERATOR_CLIENT_CATEGORIES_FILTERING';
    exception when no_data_found then
      lnClientCategoryCheck:=0;
    end;
  end if;
  vResult := 0;
DECLARE
  CURSOR c1 IS
    select regexp_substr(ivOperatorRoles,'[^,]+', 1, level) rpname from dual
      connect by regexp_substr(ivOperatorRoles, '[^,]+', 1, level) is not null;
    r1 c1%ROWTYPE;
    vRows NUMBER := 0;
  BEGIN
    OPEN c1;
    LOOP
      FETCH c1 INTO r1;
      row_count := row_count + 1;
      EXIT WHEN c1%NOTFOUND;
      BEGIN
       SELECT distinct crp.rightprofileid, nvl(crp.rightprofiletypeid,7) INTO lnRightProfileID, lnRightProfileTypeID
       FROM ClientRightProfiles crp
        WHERE crp.rightprofilename = r1.rpname AND crp.clientid = lnClientId AND crp.statusid = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          CONTINUE;
      END;
      -- if profile type id is 1 or 7, check rights
      if lnRightProfileTypeID in (1,7) then
        BEGIN
         SELECT  count(rt.RightId) INTO vPoc
         FROM RightTypes rt, ClientRights cr, RightProfileDefinitions rpd
          WHERE cr.clientid = lnClientId
           AND rt.OperId = inOperId
           AND rt.ChannelId = lnChannelId
           AND ((inAccessTypeId=1 AND rt.AccessTypeId>=1) OR (rt.AccessTypeId = inAccessTypeId))
           AND rt.RightId = cr.RightId
           AND cr.clientrightid = rpd.clientrightid
           AND rpd.rightprofileid = lnRightProfileID;
         -- for profile type 7, check also segments and message categories
         if vPoc > 0 then
          if lnRightProfileTypeID = 7 then
           IF lnClientCategoryCheck = 1 AND inTargetClientCategID IS NOT NULL THEN
             select count(1) into vPoc
              from rightprofileclientcategories where rightprofileid=lnRightProfileID
                and clientCategID in (select clientcategid from clientcategories
                    connect by clientcategid = prior parentclientcategid
                    start with clientcategid = inTargetClientCategID)
                and rownum <=1;
             IF vPoc > 0 THEN vResult := 1; END IF;
           ELSE
              vResult := 1;
           END IF;
           IF vResult = 1 THEN
             IF lnMessageCategoryCheck = 1 AND ivMessageCategoryId IS NOT NULL THEN
                select count(1) into vPoc
                from RightProfileMessageCategories rpmc
                where rpmc.messagecategoryid = ivMessageCategoryId
                  and rpmc.rightprofileid = lnRightProfileID;
                IF vPoc < 1 THEN vResult := 0; END IF;
             END IF;
           END IF;
           IF vResult = 1 THEN
             lbRightsSet:=true;
             lbSegmentsSet:=true;
           END IF;
          else
           lbRightsSet:=true;
          end if;
         else
          CONTINUE;
         end if;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            CONTINUE;
        END;
      end if;
      IF lnClientCategoryCheck = 1 AND inTargetClientCategID IS NOT NULL THEN
        -- if profile type id is 2, check segments only
        if lnRightProfileTypeID in (2) then
           select count(1) into vPoc
            from rightprofileclientcategories where rightprofileid=lnRightProfileID
              and clientCategID in (select clientcategid from clientcategories
                  connect by clientcategid = prior parentclientcategid
                  start with clientcategid = inTargetClientCategID)
              and rownum <=1;
            IF vPoc > 0 THEN lbSegmentsSet:=true; END IF;
        end if;
      ELSE
        lbSegmentsSet:=true;
      END IF;
      --
      EXIT WHEN lbRightsSet and lbSegmentsSet;
    END LOOP;
    CLOSE c1;
  EXCEPTION
    WHEN OTHERS THEN
      --vResult := 0; -- ?? 1 ?
      lbRightsSet:=false;
      lbSegmentsSet:=false;
      IF c1%ISOPEN THEN
        CLOSE c1;
      END IF;
  END;
  --
  IF lbRightsSet and lbSegmentsSet THEN
    RETURN 1;
  ELSE
    RETURN 0;
  END IF;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 0;
  WHEN TOO_MANY_ROWS THEN
    RETURN 0; --?? 1 ?
  WHEN OTHERS THEN
    RETURN 0;
END OPERATOR_GRANTS2;
/

PROMPT CREATE OR REPLACE function OPERATOR_GRANTS
CREATE OR REPLACE 
FUNCTION OPERATOR_GRANTS (inChannelId    IN Channels.ChannelId%TYPE, -- channel which is used to realize the operation, mandatory (for admin API it is always "system administration" channel)
                 inClientId              IN Clients.ClientId%TYPE, -- the client under which the operation is to be realized, optional (for admin API it is always "bank" service client - if not filled then selected from GSP parameter)
                 inUserId                IN Users.UserId%TYPE, -- client user ID
                 inOperatorId            IN Users.UserId%TYPE,  -- operator user ID, mandatory
                 inAccessTypeId          IN OperationAccessTypes.AccessTypeId%TYPE,  -- operation access type to test for operator, mandatory
                 inOperId                IN OperationTypes.OperId%TYPE,   -- operation ID, mandatory
                 ivOperatorRoles         IN VARCHAR2,    -- comma delimited list of names of operator roles - i.e., right profiles assigned to operator
                 ivMessageCategoryId     IN VARCHAR2 DEFAULT NULL,    -- ID of checked message category - i.e., whether a right for this msg category exists in some of client operator profiles, optional
                 inMessageCategoryCheck  IN NUMBER DEFAULT NULL,      -- flag if message category assignment should be checked, mandatory
                 inTargetClientID        IN NUMBER DEFAULT NULL,      -- target client ID - client whose data will be modified or accessed, optional, default is taken from GSP parameter; if used, target client's segment will be checked against role segments
                 inClientCategoryCheck   IN NUMBER DEFAULT NULL       -- flag if target client segment assignment should be checked - optional, default is taken from GSP parameter
                 )
RETURN PLS_INTEGER
IS
------------------
-- $release: 5257 $
-- $version: 8.0.7.0 $
------------------
--
-- MODIFICATION HISTORY
--
-- Person       Date       Comments
-- ------------ ---------- ---------------------------------------------------------
-- Furdik       25.02.2016 Initial version, based on GRANTS function
-- Furdik       09.03.2016 Added check of Client Categories
-- Pataky       31.08.2016 Changes to support right profile types
-- Pataky       06.09.2016 inClientID, inClientCategoryCheck and inMessageCategoryCheck is optional - taken from GSP parameters
-- Pataky       07.10.2016 Added AvailableFor columns checking - 20009213-2051
-- Bocak        17.01.2017 RESULT_CACHE - 20009245-3714
-- Pataky       31.01.2017 Performance issues fixed regarding client categories (MYG-2294, MYG-2317, MYG-2122, MYG-2280) - 20009203-6996
-- Pataky       15.05.2017 Added check on operator care for client (MYG-4674) - 20009246-337
-- Pataky       22.05.2017 Updated check on operator care for client (MYG-4760) - 20009246-337
-- --------------------------------------------------------------------------------
--
  lnClientCategID       Clients.ClientCategID%TYPE:=null;
  lnOperatorHasCare     pls_integer;
BEGIN
  IF inTargetClientID is not null and nvl(inClientCategoryCheck,1)=1 then
    -- check if operator has given client in care, currently regardles of care type
    SELECT /*+ RESULT_CACHE */ count(*) INTO lnOperatorHasCare FROM ClientOperatorCare COC, Users U
      where COC.ClientId=inTargetClientID and U.HostUserID = COC.HostOperatorID and U.UserID=inOperatorId and rownum<=1;
    IF lnOperatorHasCare=0 THEN
      SELECT ClientCategID INTO lnClientCategID FROM Clients WHERE ClientID=inTargetClientID;
    END IF;
  end if;
  RETURN OPERATOR_GRANTS2 (inChannelId,
                 inClientId,
                 inUserId,
                 inOperatorId,
                 inAccessTypeId,
                 inOperId,
                 ivOperatorRoles,
                 ivMessageCategoryId,
                 inMessageCategoryCheck,
                 lnClientCategID,
                 inClientCategoryCheck
                 );
END OPERATOR_GRANTS;
/

PROMPT Create packages in UDEBS schema
PROMPT CREATE OR REPLACE PACKAGE GDM_PARSER001
CREATE OR REPLACE 
PACKAGE GDM_PARSER001 IS
------------------
-- $release: 3122 $
-- $version: 8.0.5.0 $
------------------
--
-- $Header: /Oracle/UDEBS_XPE/UDEBSXPE.GDM_PARSER001.PSK 2     27.01.14 18:24 Pataky $
--
-- MODIFICATION HISTORY
--
-- Person      Date       Comments
-- ----------- ---------- ---------------------------------------------------------
-- Ilenin      07.07.2014 UDEBS RS
-- Pataky      20.07.2015 Optimization of getXPath caching
-- Snederfler  19.08.2015 20009203-1183 - ParseXMLForBranchAndOper - optimization: move select for getting GroupID nearly before using it
--                                        GetDTDAttributes - new function of getting common DTD attributes
-- Emmer       26.02.2016 20009203-3907 - GetDTDAttributes modification
-- Pataky      15.04.2016 Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-- --------------------------------------------------------------------------------
--
PROCEDURE GetDTDAttributes(
    ovDocType   OUT DTDStore.DocType%TYPE,
    ovIIFAttr   OUT DTDStore.IIFAttr%TYPE,
    ovIIFAttr2  OUT DTDStore.IIFAttr2%TYPE,
    ovIIFAttr3  OUT DTDStore.IIFAttr3%TYPE,
    inOperID    IN  DTDStore.operid%TYPE,
    inDocType   IN  DTDStore.DocType%TYPE DEFAULT NULL
);
--
FUNCTION SaveSpaces(ixInputData IN XMLTYPE) RETURN XMLTYPE;
--
FUNCTION ReturnSpaces(ixInputData IN XMLTYPE) RETURN XMLTYPE;
--
FUNCTION getXPath (ivIIFAttrName GDM_Parser.IIFAttrName%TYPE, ivDefault VARCHAR2 DEFAULT NULL) RETURN VARCHAR2 RESULT_CACHE;
--
END GDM_PARSER001;
/

CREATE OR REPLACE 
PACKAGE BODY GDM_PARSER001 IS
------------------
-- $release: 3122 $
-- $version: 8.0.5.0 $
------------------
--
-- $Header: /Oracle/UDEBS_XPE/UDEBSXPE.GDM_PARSER001.PBK 2     27.01.14 18:24 Pataky $
--
-- MODIFICATION HISTORY
--
-- Person       Date       Comments
-- ------------ ---------- ---------------------------------------------------------
-- Ilenin      07.07.2014 UDEBS RS
-- Pataky      20.07.2015 Optimization of getXPath caching
-- Snederfler  19.08.2015 20009203-1183 - ParseXMLForBranchAndOper - optimization: move select for getting GroupID nearly before using it
--                                        GetDTDAttributes - new function of getting common DTD attributes
-- Emmer       26.02.2016 20009203-3907 - GetDTDAttributes modification
-- Pataky      15.04.2016 Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-- Bocak       18.07.2016 GetDTDAttributes - added filling of IIFAttr3 - 20009245-2041
-- Emmer       17.10.2018 ParseXMLForBranchAndOper skipped for RBCZ
-- --------------------------------------------------------------------------------
  gvBankCode       varchar2(4);
-- -------------------------------------------------------------------------------------------
--
FUNCTION getXPath (ivIIFAttrName GDM_Parser.IIFAttrName%TYPE, ivDefault VARCHAR2 DEFAULT NULL ) RETURN VARCHAR2 RESULT_CACHE IS
  v_XPath GDM_Parser.XPath%TYPE := null;
BEGIN
  Begin
    Select XPath
      into v_XPath
      from GDM_Parser
     where IIFAttr = upper(ivIIFAttrName)
       and statusid=1;
  Exception
    When NO_DATA_FOUND Then
      v_XPath := ivDefault;
  End;
  RETURN v_XPath;
END;
-- ======================================= GetDTDAttributes ==========================================
PROCEDURE GetDTDAttributes(
    ovDocType   OUT DTDStore.DocType%TYPE,
    ovIIFAttr   OUT DTDStore.IIFAttr%TYPE,
    ovIIFAttr2  OUT DTDStore.IIFAttr2%TYPE,
    ovIIFAttr3  OUT DTDStore.IIFAttr3%TYPE,
    inOperID    IN  DTDStore.operid%TYPE,
    inDocType   IN  DTDStore.DocType%TYPE DEFAULT NULL
)
IS
  lbNeXTSearch BOOLEAN := FALSE;
  lnGroupID    DTDStore.groupid%TYPE;
  CURSOR c2 IS SELECT /*+ RESULT_CACHE */ groupid FROM operationgroups START WITH groupid = lnGroupID CONNECT BY groupid = PRIOR parentgroupid;
BEGIN
  -- not allow for branch
  SELECT /*+ RESULT_CACHE */
         DISTINCT doctype, iifattr, iifattr2, iifattr3
    INTO ovDocType, ovIIFAttr, ovIIFAttr2, ovIIFAttr3
    FROM DTDStore
   WHERE ((inDocType IS NULL AND DocType NOT IN ('CTPDATA','EVENTTYPES','ALERTS')) OR (DocType = inDocType))
     AND OperID = inOperID;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DECLARE
      lvDocType DTDStore.DocType%TYPE;
    BEGIN -- pretizeni pro GroupID
      SELECT /*+ RESULT_CACHE */
             groupid
        INTO lnGroupID
        FROM OperationTypes
       WHERE OperID = inOperID;
      FOR r2 IN c2 LOOP
        BEGIN
          SELECT /*+ RESULT_CACHE */
                 DISTINCT doctype, iifattr, iifattr2, iifattr3
            INTO lvDocType, ovIIFAttr, ovIIFAttr2, ovIIFAttr3
            FROM DTDStore
           WHERE GroupID=r2.GroupID
             AND ((inDocType IS NULL AND DocType NOT IN ('CTPDATA','EVENTTYPES','ALERTS')) OR (DocType = inDocType))
             AND OperId IS NULL;
          EXIT;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN NULL;
        END;
      END LOOP;
      IF lvDocType IS NULL THEN
        RAISE NO_DATA_FOUND;
      ELSE
         ovDocType := lvDocType;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN NULL;
    END;
END GetDTDAttributes;
-- =================================== SaveSPACEs ======================================
FUNCTION SaveSpaces(ixInputData IN XMLTYPE) RETURN XMLTYPE IS
  vData     VARCHAR2(4000 CHAR) := null;
  vMask     VARCHAR2(4000 CHAR) := null;
  vMaxChars NUMBER(5) := 100;
BEGIN
  vData := ixInputData.GetStringVal();
  -- Free Spaces:
  FOR i IN 1..vMaxChars LOOP
    vMask := '>'||LPAD('</',i+2,' ');
    vData := Replace(vData,vMask,' SPACES="'||i||'"></');
  END LOOP;
  -- Free zeros 000000 for ValueDate:
  vMask := '<valuedate>000000</valuedate>';
  vData := Replace(vData,vMask,'<valuedate ZEROS="6"></valuedate>');
  RETURN XMLTYPE(vData);
END SaveSpaces;
-- =================================== ReturnSPACEs ======================================
FUNCTION ReturnSpaces(ixInputData IN XMLTYPE) RETURN XMLTYPE IS
  vData      VARCHAR2(4000 CHAR) := null;
  vMask      VARCHAR2(4000 CHAR) := null;
  vReplace   VARCHAR2(4000 CHAR) := null;
  vTmpString VARCHAR2(4000 CHAR)  := null;
  vSpacePos  NUMBER(5)      := 0;
  vTagBgn    NUMBER(5)      := 0;
  vTagEnd    NUMBER(5)      := 0;
  vEndPos    NUMBER(5)      := 0;
  vSpaces    NUMBER(5)      := 0;
  vTagName   VARCHAR2(500 CHAR)  := null;
BEGIN
  vData := ixInputData.GetStringVal();
  -- Free spaces:
  LOOP
    vSpacePos := NVL(INSTR(vData,' SPACES="',1,1),0);
    EXIT WHEN vSpacePos = 0;
    vTmpString := SUBSTR(vData,1,vSpacePos);
    vTagBgn := NVL(INSTR(vTmpString,'<',-1,1),0);
    vEndPos := NVL(INSTR(vData,' ',vTagBgn,1),0);
    vTagName := SUBSTR(vData,vTagBgn+1,vEndPos-vTagBgn-1);
    vEndPos := NVL(INSTR(vData,'"',vSpacePos,2),0);
    vSpaces := to_number(SUBSTR(vData,vSpacePos+9, vEndPos-(vSpacePos+9)));
    if SUBSTR(vData,vEndPos+1,1) = '/' then
      vTagEnd := NVL(INSTR(vData,'/>',vSpacePos,1)+1,0);
      vMask := SUBSTR(vData,vTagBgn,vTagEnd-vTagBgn+1);
      vReplace := Replace(vMask,' SPACES="'||vSpaces||'"');
      vReplace := Replace(vReplace,'/>','>');
      vReplace := vReplace||LPAD(' ',vSpaces,' ')||'</'||vTagName||'>';
      vData := Replace(vData,vMask,vReplace);
    else
      vTagEnd := NVL(INSTR(vData,'<',vSpacePos,1)+1,0);
      vMask := SUBSTR(vData,vTagBgn,vTagEnd-vTagBgn-1);
      vReplace := Replace(vMask,' SPACES="'||vSpaces||'"');
      vReplace := vReplace||LPAD(' ',vSpaces,' ');
      vData := Replace(vData,vMask,vReplace);
    end if;
  END LOOP;
  -- Free zeros 000000 for ValueDate:
  vMask := '<valuedate ZEROS="6"></valuedate>';
  vData := Replace(vData,vMask,'<valuedate>000000</valuedate>');
  vMask := '<valuedate ZEROS="6"/>';
  vData := Replace(vData,vMask,'<valuedate>000000</valuedate>');
  RETURN XMLTYPE(vData);
END ReturnSpaces;
-- =================================================================================================
BEGIN
    gvBankCode := Reftab_GSP('LOCALBANKCODEALPHA');
END GDM_PARSER001;
/

PROMPT CREATE OR REPLACE PACKAGE IIF_CCFG_005
CREATE OR REPLACE 
PACKAGE IIF_CCFG_005
AUTHID DEFINER
IS
------------------
-- $release: 6673 $
-- $version: 8.0.50.3 $
------------------
--
-- Purpose: Podpisova pravidla 2.generace
-- Posledni patch z UDEBS: 2598
-- $Header: /Oracle/UDEBS_XPE/UDEBSXPE.IIF_CCFG_005.PSK 3     2.07.12 17:16 Pataky $
-- MODIFICATION HISTORY
-- Person       Date        Comments
-- ----------   ----------  ------------------------------------------
-- Ilenin       07.07.2014  UDEBS RS
-- Ilenin       23.11.2014  20009183-1042
-- Bocak        26.02.2015  PutSDClientSignRules - chenged erro message for -20001 (20009183-1719)
-- Pataky       04.03.2015  VerifySufficiencySigning - corrected sign.rule evaluation (20009183-1705,20009183-1845)
-- Pataky       04.03.2015  ExistValidSignatureRules - corrected sign.rule selecting (20009183-1705,20009183-1755,20009183-1845)
-- Pataky       05.03.2015  GetValidSignatureRules, SetOrChangeSignatureRule, VerifySignatureRule - exceptions for operId 781 (20009183-1853)
-- Pataky       09.03.2015  PutSDClientSignRules - changed error message Err01
-- Pataky       19.03.2015  ExistValidSignatureRules - corrected sign.rule selecting for ordered signers - 20009183-2047
-- Pataky       20.03.2015  VerifySufficiencySigning - corrected sign.rule evaluation (20009183-2047)
-- Pataky       26.03.2015  VerifySufficiencySigning,GetValidSignatureRules,VerifySignatureRule - unified limit rounding (20009183-2126, 20009183-2136)
-- Pataky       30.04.2015  ExistValidSignatureRules - exception for exchange rates problem added (20009183-2607)
-- Pataky       04.05.2015  ClientSignRules.AmountLimit can be null, evaluations fixed - 20009183-2477
-- Bocak        13.05.2015  VerifySufficiencySigning - set CTP.SigneValDate to SYSDATE when transaction is PARTSIG (20009183-2631)
-- Pataky       22.05.2015  Reviewed using of RESULT CACHE hint in select statements - 20009183-2902
-- Snederfler   25.05.2015  PutSDClientSignRulesDef - optimization
-- Bocak        28.07.2015  ExistValidSignatureRules, GetValidSignatureRules - new input parameter inCheckLimit (20009183-1703)
-- Bocak        26.08.2015  GetValidSignatureRules, SetOrChangeSignatureRule, VerifySignatureRule - exceptions for operId 786 (20009183-4361)
-- Pataky       28.08.2015  GetSumTotal - added exception for RB for own accounts operations, excluded from limit - 20009203-1329
-- Pataky       09.10.2015  VerifySignatureRule, GetValidSignatureRules - added exception for RB for own accounts operations, excluded from limit - 20009203-2317
-- Pataky       09.11.2015  Package amount can be recounted into local currency, based on parameter (because of multicurrency accounts)
-- Pataky       11.12.2015  VerifySufficiencySigning - keep status PARTSIG after multiple rules checking, if signature is attached
-- Emmer        16.12.2015  Removal of links to obsolete journal attributes 20009203-1354
-- Pataky       15.04.2016  Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-- Pataky       13.06.2016  VerifySufficiencySigning - keep status PARTSIG after multiple rules checking fix - 20009203-5429
-- Pataky       21.07.2016  New procedure GetValidSignRules to get sign.rules for external systems - 20009203-5706
-- Bocak        16.08.2016  GetValidSignatureRules - select data from ctp_tmp if package status is 33 - 20009238-282
-- Bocak        24.08.2016  GetValidSignatureRules, SetOrChangeSignatureRule, VerifySignatureRule - exceptions for operId 780 - 20009248-584
-- Pataky       16.09.2016  Removed LastLoginTime from Users table, fixed GetValidSignRules
-- Pataky       13.10.2016  Added OperationTypeChannels support into getOperationsForSignRule, ExistValidSignatureRules, GetValidSignatureRules, VerifySignatureRule procedures - 20009213-2049
-- Furdik       18.11.2016  New procedure GetValidSignRulesUsers - returns users for valid signature rules - 20009239-783
-- Pataky       07.12.2016  GetValidSignRules procedure fixed (MCHGIT-1052) - 20009203-6748
-- Pataky       09.12.2016  GetValidSignRules procedure updated - added clients attributes (MCHGIT-1052) - 20009203-6748
-- Snederfler   15.12.2016  GetValidSignRulesForClient and GetValidSignRulesForUser procedures updated - added ValidFrom and ValidUntil attributes - 20009203-6612
-- Furdik       18.05.2017  SetOrChangeSignatureRule add inJournal, VerifySufficiencySigning add inReport - P20009245-4389
-- Emmer        17.07.2017  GetValidSignRules hotfix optimalization (MYG-5227)
-- Emmer        17.07.2017  VerifySignatureRule, GetSumTotal optimalization (MYG-5262)
-- Emmer        20.07.2017  GetValidSignRules optimalization
-- Emmer        12.12.2017  Add new function ExistValidSignatureRules2 - P20009299-199
-- Emmer        15.01.2018  Add new function ExistValidSignatureRules3 (MYG-7042) - P20009299-265
-- ----------   ----------  ------------------------------------------
-- typy pro GUS procedury
type gus_signaturerule is record (
    SignRuleID      number,
    RuleDescription VARCHAR2(50),
    CurrencyID      VARCHAR2(3),
    limit_immediate number,
    limit_day       NUMBER,
    limit_month     number,
    limit_year      number,
    Status          number,
    OrderImportant  NUMBER
);
type gus_signaturestatus is record (
    PackageID       number,
    UserID          number,
    UserOrder       number,
    SignatureSeq    number
);
-- typy pre jednotlive tabulky
-----------------------
--TABLE CLIENTSIGNRULES
-----------------------
type t_rec_clientsignrules is record
 (
  clientsignruleseq          CLIENTSIGNRULES.clientsignruleseq%type,
  clientid                   CLIENTSIGNRULES.clientid%type,
  statusid                   CLIENTSIGNRULES.statusid%type,
  amountlimit                CLIENTSIGNRULES.amountlimit%type,
  amountlimitcurrid          CLIENTSIGNRULES.amountlimitcurrid%type,
  addnewaccount              CLIENTSIGNRULES.addnewaccount%type,
  orderimportant             CLIENTSIGNRULES.orderimportant%type,
  validfrom                  CLIENTSIGNRULES.validfrom%type,
  validuntil                 CLIENTSIGNRULES.validuntil%type,
  ruledescription            CLIENTSIGNRULES.ruledescription%TYPE,
  overrunpermitted           CLIENTSIGNRULES.OVERRUNPERMITTED%TYPE
 );
type t_arr_clientsignrules is table of t_rec_clientsignrules index by binary_integer;
---------------------------
-- TABLE CLIENTSIGNRULESDEF
---------------------------
type t_rec_clientsignrulesdef is record
 (
  clientsignruleseq          CLIENTSIGNRULESDEF.clientsignruleseq%type,
  userid                     CLIENTSIGNRULESDEF.clientsignruleseq%type,
  signruleroleid             CLIENTSIGNRULESDEF.signruleroleid%type,
  userorder                  CLIENTSIGNRULESDEF.clientsignruleseq%type,
  RequiredSignaturesCount    CLIENTSIGNRULESDEF.requiredsignaturescount%type
 );
type t_arr_clientsignrulesdef is table of t_rec_clientsignrulesdef index by binary_integer;
---------------------------
-- TABLE CLIENTSIGNRULEOPER
---------------------------
type t_rec_clientsignrulesoper is record
 (
  clientsignruleseq          CLIENTSIGNRULEOPER.clientsignruleseq%type,
  operid                     CLIENTSIGNRULEOPER.operid%type
 );
type t_arr_clientsignrulesoper is table of t_rec_clientsignrulesoper index by binary_integer;
---------------------------
-- TABLE CLIENTSIGNRULEACCOUNT
---------------------------
type t_rec_clientsignrulesaccount is record
 (
  accountaccessid            CLIENTSIGNRULEACCOUNT.accountaccessid%type,
  clientsignruleseq          CLIENTSIGNRULEACCOUNT.clientsignruleseq%type
 );
type t_arr_clientsignrulesaccount is table of t_rec_clientsignrulesaccount index by binary_integer;
---------------------------
-- TABLE clientsignrulelimits
---------------------------
type t_rec_ClientSignRuleslimit is record
 (
 clientsignruleseq      clientsignrulelimits.clientsignruleseq%type,
 period         clientsignrulelimits.period%type,
 amountlimit        clientsignrulelimits.amountlimit%type
 );
type t_arr_clientsignruleslimit is table of t_rec_clientsignruleslimit index by binary_integer;
---------------------------
-- TABLE SignRuleRoles
---------------------------
type t_rec_ClientSignRuleRole is record
 (
  signruleroleid    signruleroles.signruleroleid%type,
  role              signruleroles.role%type,
  clientid          signruleroles.clientid%type,
  systemflag        signruleroles.systemflag%type,
  description       signruleroles.description%type
 );
type t_arr_ClientSignRuleRole is table of t_rec_ClientSignRuleRole index by binary_integer;
--------------------------------------------------------------------
-- PRO APLIKACNI SERVER (GUS)
--------------------------------------------------------------------
procedure GetValidSignRulesForClient (  -- vraci vsechna podpisova pravidla klienta pro jejich spravu
    inClientID      in  Clients.ClientID%type,       -- Klient prihlaseneho Usera
    orVSRtab        out sys_refcursor                -- seznam pravidel
);
--------------------------------------------------------------------
procedure GetValidSignatureRules (  -- vraci vsechna prave platna podpisova pravidla, ktera mohou byt pouzita
    inClientID      in  Clients.ClientID%type,      -- Klient prihlaseneho Usera
    inPackageID     in  ClientTransactionPackages.PackageID%type, -- Package, ktere se priradi jedno pravidlo ze seznamu
    inCertMethodID  in  pls_integer,
    orVSRtab        out sys_refcursor,              -- seznam pravidel
    inAddNullPK     in  pls_integer default 1,       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
    inUserID   in  Users.userid%TYPE default NULL,
    inCheckLimit    IN  PLS_INTEGER DEFAULT 1,        -- 0/1 - nekontrolovat/kontrolovat limit
    inChannelID     in  OperationTypeChannels.ChannelID%TYPE DEFAULT NULL
);
--------------------------------------------------------------------
procedure GetValidSignatureRule (                               -- single varianta pro PK
    inSignRuleSeq   in  ClientSignRules.ClientSignRuleSeq%type, -- PK
    orVSRtab        out sys_refcursor,
    inAddNullPK     in  pls_integer default 1       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
);
--------------------------------------------------------------------
procedure GetSignatureRuleForPackage (                          -- vraci pravidlo prirazene package
    onReturnCode    out Number,
    orErrors        out sys_refcursor,
    orSignRule      out sys_refcursor,
    inUserID   in  Users.userid%TYPE,
    inChannelID     in  Channels.ChannelID%type,
    inPackageID     in  ClientTransactionPackages.PackageID%type,
    idCheckoutDate  IN  DATE,
    inOpenError     in  Pls_integer default 1
);
--------------------------------------------------------------------
procedure SetOrChangeSignatureRule(                             -- nastavuje package zvolene podpisove pravidlo
    onReturnCode    out number,
    orErrors        out sys_refcursor,
    inUserID   in  Users.userid%TYPE,
    inChannelID     in  Channels.ChannelID%type,
    inPackageID     in  ClientTransactionPackages.PackageID%type,
    idCheckoutDate  in  date,                                   -- vyplnen => pracuje nad CTP_TMP (pri editaci)
    ivSignRuleSeq   in  varchar2,
    inOpenError     in  Pls_integer default 1,
    inJournal       IN  PLS_INTEGER DEFAULT 1
); -- s kontrolou prav
--------------------------------------------------------------------
procedure VerifySignatureRule( -- testuje, zda prirazene pravidlo je stale platne
    inPackageID   in  ClientTransactionPackages.PackageID%type,
    inSignRuleSeq in  ClientTransactionPackages.SignRuleSeq%type,
    onReturnCode  out number,
    inChannelID   in  OperationTypeChannels.ChannelID%TYPE DEFAULT NULL
);
--------------------------------------------------------------------
procedure VerifySufficiencySigning( -- testuje podpisy podle pravidla
    inPackageID   in  ClientTransactionPackages.PackageID%type,
    inSignRuleSeq in  ClientTransactionPackages.SignRuleSeq%type,
    onReturnCode  out number,
    orErrors      out sys_refcursor,
    inOpenError   in  Pls_integer default 1,
    inReport      IN  PLS_INTEGER DEFAULT 1
);
--------------------------------------------------------------------
procedure getSgnStatusForRuleInPackage( -- vraci seznam pozadovanych podpisu users podle pravidla a ID ulozenych podpisu
    inUserID in  NUMBER,
    inChannelID   in  NUMBER,
    inPackageID   in  NUMBER,
    orSignStatus  out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getSgnStatusForUsersInPackage( -- vraci seznam podpisu package a pripadne poradi z definovaneho pravidla pro package
    inUserID in  NUMBER,
    inChannelID   in  NUMBER,
    inPackageID   in  NUMBER,
    orSignStatus  out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getSgnStatus( -- vraci podpis podle PK
    inPackageID   in  NUMBER,
    inUserID      in  NUMBER,
    orSignature   out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getAccountsForSignRule( -- vraci ucty spojene s podpisovym pravidlem
    inUserID in  NUMBER,            -- uzivatel, optional
    inClientID    in  NUMBER,       -- klient, nepouziva se, NULL
    inChannelID   in  NUMBER,       -- kanal, optional
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    orAccounts    out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getUsersForSignRule(      -- vraci uzivatele spojene s podpisovym pravidlem
    inUserID in  NUMBER,            -- uzivatel, nepouziva se, NULL
    inChannelID   in  NUMBER,       -- kanal, nepouziva se, NULL
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    orUsers       out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getOperationsForSignRule( -- vraci operace spojene s podpisovym pravidlem
    inUserID in  NUMBER,            -- uzivatel, optional
    inClientID    in  NUMBER,       -- klient, nepouziva se, NULL
    inChannelID   in  NUMBER,       -- kanal, optional
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    ivLangID      in  VARCHAR2,     -- jazyk pro textovy popis operace, optional
    orOperations  out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getLimitsForSignRule( -- vraci obdobove limity spojene s podpisovym pravidlem
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    orLimits      out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getAvailableLimits(       -- vraci castky, ktere zbyvaji do prekroceni limitu
    inSignRuleSeq in  Number,
    onDayLimit    out Number,
    onMonthLimit  out Number,
    onYearLimit   out Number
);
--
--------------------------------------------------------------------
procedure AutoGenerateSignRules(           -- automatic generation of signing rules for client - deletes old rules !
    inClientID    in  ClientSignRules.clientid%type
);
 -- in case of error, exception is thrown
--------------------------------------------------------------------
/*
procedure AutoAddSignRulesForUser(           -- automatic generation of signing rules for user - adds new rules for user
    inUserID      in  ClientSignRulesDef.userid%type
);
 -- in case of error, exception is thrown
*/
--------------------------------------------------------------------
procedure GetValidSignRulesForUser (  -- vraci vsechna podpisova pravidla uzivatele pro jejich spravu
    inUserID        in  ClientUsers.UserID%type,    -- Prihlaseny User
    inClientID      in  ClientUsers.ClientID%type,  -- Aktivny klient
    orVSRtab        out sys_refcursor               -- seznam pravidel
);
--------------------------------------------------------------------
procedure GetValidSignRules (  -- vraci vsechna podpisova pravidla pouzitelna pro kombinaci vstupnich parametru
    inChannelID           in  clientsignrulechannel.channelid%TYPE,  -- kanal pozadovany v pravidlu, mandatory
    ivHostUserID          in  Users.HostUserID%type,                 -- Prihlaseny User (PartyID), optional
    ivHostClientID        in  ClientHostAllocation.HostClientID%type,-- klient pozadovany v pravidlu, optional
    inOperID              in  clientsignruleoper.operid%type,        -- pozadovana operace v pravidlu, optional
    ivAccountNumberPrefix in  varchar2,                              -- prefix uctu pozadovaneho v pravidlu, optional
    ivAccountNumber       in  varchar2,                              -- cislo uctu pozadovaneho v pravidlu, optional
    ivBankCode            in  varchar2,                              -- banka uctu pozadovaneho v pravidlu, optional
    inAmount              in  number,                                -- castka operace pro kontrolu limitu v pravidlu, optional
    ivCurrencyId          in  varchar2,                              -- mena castky pro kontrolu limitu v pravidlu, optional
    orVSRtab              out sys_refcursor                          -- seznam pravidel vybranych podle podminek
);
--------------------------------------------------------------------
function getCertSgnStatusForUser( -- vracia 0 alebo 1 podla toho ci uzivatel prilozil k package cert.podpis
    inPackageID   in  NUMBER,
    inUserID      in  NUMBER
) return number;
--------------------------------------------------------------------
--
-- vracia userov pre vsetky prave platne podpisove pravidla, ktore mozu byt pouzite
Procedure GetValidSignRulesUsers (inClientID      in  Clients.ClientID%type,      -- Klient prihlaseneho Usera
                                  inPackageID     in  ClientTransactionPackages.PackageID%type, -- Package, ktere se priradi jedno pravidlo ze seznamu
                                  inCertMethodID  in  pls_integer,
                                  inAddNullPK     in  pls_integer default 1,       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
                                  orUserIDs       out SYS_REFCURSOR,
                                  inCheckLimit    IN  PLS_INTEGER DEFAULT 1        -- 0/1 - nekontrolovat/kontrolovat limit
);
--
-- vraci vsechna prave platna podpisova pravidla, ktera mohou byt pouzita
function ExistValidSignatureRules (inClientID      in  Clients.ClientID%type,      -- Klient prihlaseneho Usera
                                   inPackageID     in  ClientTransactionPackages.PackageID%type, -- Package, ktere se priradi jedno pravidlo ze seznamu
                                   inCertMethodID  in  pls_integer,
                                   inAddNullPK     in  pls_integer default 1,       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
                                   inUserID   in  Users.userid%TYPE default null,
                                   inCheckLimit    IN  PLS_INTEGER DEFAULT 1,        -- 0/1 - nekontrolovat/kontrolovat limit
                                   inChannelID   in  OperationTypeChannels.ChannelID%TYPE DEFAULT NULL
)
return pls_integer;
--
END IIF_CCFG_005;
/

CREATE OR REPLACE 
PACKAGE BODY IIF_CCFG_005 IS
------------------
-- $release: 6673 $
-- $version: 8.0.50.3 $
------------------
--
-- Purpose: Podpisova pravidla 2.generace
-- $Header: /Oracle/UDEBS_RS/UDEBSRS.IIF_CCFG_005.PBK 3     2.07.12 17:16 Pataky $
-- MODIFICATION HISTORY
-- Person       Date       Comments
-- ----------   ---------- ------------------------------------------
-- Ilenin       07.07.2014  UDEBS RS
-- Ilenin       23.11.2014  20009183-1042
-- Bocak        26.02.2015  PutSDClientSignRules - chenged erro message for -20001 (20009183-1719)
-- Pataky       04.03.2015  VerifySufficiencySigning - corrected sign.rule evaluation (20009183-1705,20009183-1845)
-- Pataky       04.03.2015  ExistValidSignatureRules - corrected sign.rule selecting (20009183-1705,20009183-1755,20009183-1845)
-- Pataky       05.03.2015  GetValidSignatureRules, SetOrChangeSignatureRule, VerifySignatureRule - exceptions for operId 781 (20009183-1853)
-- Pataky       09.03.2015  PutSDClientSignRules - changed error message Err01
-- Pataky       19.03.2015  ExistValidSignatureRules - corrected sign.rule selecting for ordered signers - 20009183-2047
-- Pataky       20.03.2015  VerifySufficiencySigning - corrected sign.rule evaluation (20009183-2047)
-- Pataky       26.03.2015  VerifySufficiencySigning,GetValidSignatureRules,VerifySignatureRule - unified limit rounding (20009183-2126, 20009183-2136)
-- Pataky       30.04.2015  ExistValidSignatureRules - exception for exchange rates problem added (20009183-2607)
-- Pataky       04.05.2015  ClientSignRules.AmountLimit can be null, evaluations fixed - 20009183-2477
-- Bocak        13.05.2015  VerifySufficiencySigning - set CTP.SigneValDate to SYSDATE when transaction is PARTSIG (20009183-2631)
-- Pataky       22.05.2015  Reviewed using of RESULT CACHE hint in select statements - 20009183-2902
-- Snederfler   25.05.2015  PutSDClientSignRulesDef - optimization
-- Bocak        28.07.2015  ExistValidSignatureRules, GetValidSignatureRules - new input parameter inCheckLimit (20009183-1703)
-- Bocak        26.08.2015  GetValidSignatureRules, SetOrChangeSignatureRule, VerifySignatureRule - exceptions for operId 786 (20009183-4361)
-- Pataky       28.08.2015  GetSumTotal - added exception for RB for own accounts operations, excluded from limit - 20009203-1329
-- Pataky       09.10.2015  VerifySignatureRule, GetValidSignatureRules - added exception for RB for own accounts operations, excluded from limit - 20009203-2317
-- Pataky       09.11.2015  Package amount can be recounted into local currency, based on parameter (because of multicurrency accounts)
-- Pataky       11.12.2015  VerifySufficiencySigning - keep status PARTSIG after multiple rules checking, if signature is attached
-- Emmer        16.12.2015  Removal of links to obsolete journal attributes 20009203-1354
-- Pataky       15.04.2016  Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-- Pataky       13.06.2016  VerifySufficiencySigning - keep status PARTSIG after multiple rules checking fix - 20009203-5429
-- Pataky       21.07.2016  New procedure GetValidSignRules to get sign.rules for external systems - 20009203-5706
-- Bocak        16.08.2016  GetValidSignatureRules - select data from ctp_tmp if package status is 33 - 20009238-282
-- Bocak        24.08.2016  GetValidSignatureRules, SetOrChangeSignatureRule, VerifySignatureRule - exceptions for operId 780 - 20009248-584
-- Pataky       16.09.2016  Removed LastLoginTime from Users table, fixed GetValidSignRules
-- Pataky       13.10.2016  Added OperationTypeChannels support into getOperationsForSignRule, ExistValidSignatureRules, GetValidSignatureRules, VerifySignatureRule procedures - 20009213-2049
-- Furdik       18.11.2016  GetValidSignRulesUsers - returns users for valid signature rules - 20009239-783
-- Pataky       07.12.2016  GetValidSignRules procedure fixed (MCHGIT-1052) - 20009203-6748
-- Pataky       09.12.2016  GetValidSignRules procedure updated - added clients attributes (MCHGIT-1052) - 20009203-6748
-- Snederfler   15.12.2016  GetValidSignRulesForClient and GetValidSignRulesForUser procedures updated - added ValidFrom and ValidUntil attributes - 20009203-6612
-- Furdik       16.12.2016  VerifySufficiencySigning - add TATR option to SLSP - 20009239-2128
-- Pataky       21.12.2016  GetValidSignRules procedure updated - updated search by account (MCHGIT-1135) - 20009203-6748
-- Pataky       06.02.2017  GetValidSignRules procedure updated - added condition on StatusID (MYG-2496) - 20009203-6741
-- Pataky       27.02.2017  GetValidSignRules, GetValidSignatureRules, VerifySignatureRule and VerifySufficiencySigning procedures updated - updated searching rules according to minimal signatures, limits and channels associations
-- Bocak        27.02.2017  GetSumTotal,GetValidSignatureRules,VerifySignatureRule - moved reftab_gsp from select to variables
-- Furdik       14.03.2017  GetSumTotal - add TATR code for tmpDate - 20009239-3117
-- Pataky       14.03.2017  ExistValidSignatureRules procedure updated - signature rule is not available, if user already attached signature - 20009239-3264
-- Pataky       15.03.2017  GetValidSignatureRules procedure fixed - added condition for signature rule channels (MYG-3636) - 20009246-337
-- Pataky       28.03.2017  GetValidSignRules procedure optimized (MYG-3700) - 20009246-337
-- Pataky       31.03.2017  VerifySufficiencySigning procedure updated - added MinimalSignatures check (MYG-3899) - 20009246-337
-- Pataky       10.04.2017  getUsersForSignRule procedure updated - added ClientUsers.StatusID check (MYG-4105) - 20009246-337
-- Pataky       19.04.2017  VerifySufficiencySigning procedure updated - fixed status setting for PARTSIG (not back to FORSIG), removed TATR from conditions - 20009239-4185
-- Furdik       24.04.2017  GetValidSignatureRules - declare lnChangeLimAmount, lnSumTotal as NUMBER - 20009239-4288
-- Pataky       25.04.2017  VerifySufficiencySigning procedure updated - fixed status setting for PARTSIG (not back to FORSIG) even if no signature for checked rule was found - 20009239-4185
-- Furdik       18.05.2017  SetOrChangeSignatureRule add inJournal, VerifySufficiencySigning add inReport - P20009245-4389
-- Emmer        17.07.2017  GetValidSignRules hotfix optimalization (MYG-5227)
-- Emmer        17.07.2017  VerifySignatureRule, GetSumTotal optimalization (MYG-5262)
-- Emmer        20.07.2017  GetValidSignRules optimalization
-- Pataky       08.08.2017  GetValidSignRules further optimalization, removed account number unnecessary conversions
-- Emmer        06.09.2017  transactionstrancache into EXECUTE IMMEDIATE for PRODUCT (MYG-5262)
-- Emmer        27.09.2017  GetSumTotal bugfix (MYG-5964)
-- Emmer        12.12.2017  Add new function ExistValidSignatureRules2 - P20009299-199
-- Emmer        15.01.2018  Add new function ExistValidSignatureRules3 (MYG-7042) - P20009299-265
-- Pataky       05.02.2018  Changes to support backward compatibility after removing SignRuleRoleID from ClientUsers - 20009311-240
-- Pataky       06.04.2018  Fixed mask of start day of week selector in GetSumTotal function to IW
-- Emmer        09.05.2018  Fixed getvalidsignaturerules  with 1000+ of PP (MYG-8203)
-- Emmer        23.05.2018  Fixed getvalidsignaturerules with only one PP (MYG-8414)
-- ----------   ---------- ------------------------------------------
--
UKV         exception;--unique constraint violation
pragma      exception_INIT (UKV,-1);
IKV         exception;--Integrity constraint violation
pragma      exception_INIT (IKV,-2291);
SEC         constant pls_integer := 1/(24*60*60);
--
--=================================================================================================
-- PRO APLIKACNI SERVER (GUS)
--=================================================================================================
-- help functions:
-- 1) vypocet sumy pro kontrolu limitu bezneho obdobi podpisoveho pravidla
function GetSumTotal(
    inSignRuleSeq   in  number,                  -- podpisove pravidlo
    inPeriod        in  pls_integer,                  -- id obdobi podpisoveho pravidla
    ivCurrencyId    IN  VARCHAR2,                     -- id meny, na kterou se vsechny castky prepoctou (mena podpisoveho pravidla)
    inPackageId     in  number default null      -- package, ktera ma byt z vypoctu vyrazena
) return number
is
  ldFrom        date;
  ldTo          date;
  lnSumTotal    ClientTransactionPackages.totalamount%TYPE;
  lDateCheck    varchar2(50):=reftab_gsp('DATEFORINTERVALLIMIT',0,'SIGNEVALDATE');
  tmpDate       date := sysdate;
  --
  lvLocalCurrCTP  VARCHAR2(1):=COALESCE(reftab_gsp('REQUESTS.USE_LOCAL_CURRENCY_IN_CTP',0,'0'),'0');
  lvLocalCurr     VARCHAR2(3):=Reftab_GSP('LOCALCURRENCY',0,'X');
  isRBCZ          NUMBER := CASE WHEN RefTab_GSP('LocalBankCodeAlpha',0,'X') = 'RBCZ' THEN 1 ELSE 0 END;
  v_LocalBankCode VARCHAR2(20) := RefTab_GSP('LocalBankCode',0,'X');
  v_sql           VARCHAR2(32000);
begin
  if lDateCheck = 'VALUEDATE' then
    begin
      select nvl(Valuedate,sysdate) into tmpDate
        from ClientTransactionPackages CTP
       where CTP.PackageID=inPackageId;
    exception when others then null;
    end;
  else
    tmpDate := sysdate;
  end if;
  IF Reftab_GSP('LOCALBANKCODEALPHA') in ('TATR') THEN
    if tmpDate < sysdate then
        tmpDate := sysdate;
    end if;
  END IF;
  if floor(inPeriod/100)=1 then -- na den
    ldFrom:=trunc(tmpDate);
    ldTo  :=trunc(tmpDate+1)-1/24/60/60;
  elsif floor(inPeriod/100)=2 then -- na tyden
    ldFrom:=trunc(tmpDate,'IW');
    ldTo  :=ldFrom+7-1/24/60/60;
  elsif floor(inPeriod/100)=3 then -- na mesic
    ldFrom:=trunc(tmpDate,'mm');
    ldTo  :=trunc(last_day(tmpDate)+1)-1/24/60/60;
  end if;
  if lDateCheck = 'VALUEDATE' then
    IF isRBCZ = 1 THEN
      -- vypocet sumy za podepsane a castecne podepsane packages (prepocet se nedela, byl jiz proveden v ReportClientTransaction.Put)
      v_sql := 'select nvl(sum(GetChangedAmount(CTP.TotalAmount,decode(:lvLocalCurrCTP,''1'',:lvLocalCurr,A.CurrencyID),:ivCurrencyID,A.BranchID)),0)
        from ClientTransactionPackages CTP, Accounts A,
             (select /*+FIRST_ROWS ORDERED*/ distinct CTP1.PackageID
               from ClientTransactionPackages CTP1
       LEFT JOIN Transactions TRN ON CTP1.PackageID = TRN.PackageID
       LEFT JOIN ClientAccounts CA ON CTP1.ClientId = CA.ClientId
       LEFT JOIN Accounts A1 ON CA.AccountID = A1.AccountID
       LEFT JOIN transactionstrancache trnc_acc on trnc_acc.transactionid = trn.transactionid AND trnc_acc.attrid = decode(337,304,12,311,12,5)
       LEFT JOIN transactionstrancache trnc_bc on trnc_bc.transactionid = trn.transactionid AND trnc_bc.attrid = decode(337,304,14,311,14,7)
               where reftab_ctst.FinalStatus(CTP1.StatusID) in (0,1) and CTP1.SignRuleSeq=:inSignRuleSeq
                 and reftab_ot.MakesDebit(CTP1.OperID,0)=1 and CTP1.Packageid<>nvl(:inPackageId,-1)
                 and nvl(CTP1.Valuedate,nvl(CTP1.Signevaldate,CTP1.Borndate)) between TO_DATE('''||TO_CHAR(ldFrom,'DD.MM.YYYY HH24:MI:SS')||''',''DD.MM.YYYY HH24:MI:SS'')
                       and TO_DATE('''||TO_CHAR(ldTo,'DD.MM.YYYY HH24:MI:SS')||''',''DD.MM.YYYY HH24:MI:SS'')
                 and CTP1.StatusID>0
               AND NOT(nvl(trnc_acc.val,''X'') = NVL(A1.accnum,''Y'') AND nvl(trnc_bc.val,''X'') = :v_LocalBankCode)
             and ( CTP1.SigneValDate is null and CTP1.StatusId > 0
                   or (CTP1.SigneValDate between TO_DATE('''||TO_CHAR(ldFrom,'DD.MM.YYYY HH24:MI:SS')||''',''DD.MM.YYYY HH24:MI:SS'')
                       and TO_DATE('''||TO_CHAR(ldTo,'DD.MM.YYYY HH24:MI:SS')||''',''DD.MM.YYYY HH24:MI:SS'') and CTP1.StatusID>=reftab_ot.RecStatusID(CTP1.OperID)))
           ) P
       where CTP.PackageID=P.PackageID and CTP.AccountID=A.AccountID';
       EXECUTE IMMEDIATE v_sql INTO lnSumTotal USING lvLocalCurrCTP, lvLocalCurr,ivCurrencyID,inSignRuleSeq,inPackageId,v_LocalBankCode;
    ELSE
      select nvl(sum(GetChangedAmount(CTP.TotalAmount,decode(lvLocalCurrCTP,'1',lvLocalCurr,A.CurrencyID),ivCurrencyID,A.BranchID)),0) into lnSumTotal
        from ClientTransactionPackages CTP, Accounts A,
             (select PackageID from ClientTransactionPackages CTP1
               where reftab_ctst.FinalStatus(StatusID) in (0,1) and SignRuleSeq=inSignRuleSeq
                 and reftab_ot.MakesDebit(OperID,0)=1 and Packageid<>nvl(inPackageId,-1)
                 and nvl(Valuedate,nvl(Signevaldate,Borndate)) between ldFrom and ldTo
                 and StatusID>0
               AND ( SigneValDate is null and StatusId > 0
               or (SigneValDate between ldFrom and ldTo and StatusID>=reftab_ot.RecStatusID(OperID)))
           ) P
       where CTP.PackageID=P.PackageID and CTP.AccountID=A.AccountID;
    END IF; -- IF isRBCZ = 1
  else
    IF isRBCZ = 1 THEN
      -- vypocet sumy za podepsane a castecne podepsane packages (prepocet se nedela, byl jiz proveden v ReportClientTransaction.Put)
      v_sql := 'select nvl(sum(GetChangedAmount(CTP.TotalAmount,decode(:lvLocalCurrCTP,''1'',:lvLocalCurr,A.CurrencyID),:ivCurrencyID,A.BranchID)),0)
        from ClientTransactionPackages CTP, Accounts A,
             (select /*+FIRST_ROWS ORDERED*/ distinct CTP1.PackageID from ClientTransactionPackages CTP1
       LEFT JOIN Transactions TRN ON CTP1.PackageID = TRN.PackageID
       LEFT JOIN ClientAccounts CA ON CTP1.ClientId = CA.ClientId
       LEFT JOIN Accounts A1 ON CA.AccountID = A1.AccountID
       LEFT JOIN transactionstrancache trnc_acc on trnc_acc.transactionid = trn.transactionid AND trnc_acc.attrid = decode(337,304,12,311,12,5)
       LEFT JOIN transactionstrancache trnc_bc on trnc_bc.transactionid = trn.transactionid AND trnc_bc.attrid = decode(337,304,14,311,14,7)
               where reftab_ctst.FinalStatus(CTP1.StatusID) in (0,1) and CTP1.SignRuleSeq=:inSignRuleSeq
                 and reftab_ot.MakesDebit(CTP1.OperID,0)=1 and CTP1.Packageid<>nvl(:inPackageId,-1)
               AND NOT(nvl(trnc_acc.val,''X'') = NVL(A1.accnum,''Y'') AND nvl(trnc_bc.val,''X'') = :v_LocalBankCode)
                 and ( CTP1.SigneValDate is null and CTP1.StatusId > 0
                       or (CTP1.SigneValDate between TO_DATE('''||TO_CHAR(ldFrom,'DD.MM.YYYY HH24:MI:SS')||''',''DD.MM.YYYY HH24:MI:SS'')
                       and TO_DATE('''||TO_CHAR(ldTo,'DD.MM.YYYY HH24:MI:SS')||''',''DD.MM.YYYY HH24:MI:SS'') and CTP1.StatusID>=reftab_ot.RecStatusID(CTP1.OperID)))
             ) P
       where CTP.PackageID=P.PackageID and CTP.AccountID=A.AccountID';
       EXECUTE IMMEDIATE v_sql INTO lnSumTotal USING lvLocalCurrCTP, lvLocalCurr,ivCurrencyID,inSignRuleSeq,inPackageId,v_LocalBankCode;
    ELSE
      select nvl(sum(GetChangedAmount(CTP.TotalAmount,decode(lvLocalCurrCTP,'1',lvLocalCurr,A.CurrencyID),ivCurrencyID,A.BranchID)),0) into lnSumTotal
        from ClientTransactionPackages CTP, Accounts A,
             (select PackageID from ClientTransactionPackages CTP1
               where reftab_ctst.FinalStatus(StatusID) in (0,1) and SignRuleSeq=inSignRuleSeq
                 and reftab_ot.MakesDebit(OperID,0)=1 and Packageid<>nvl(inPackageId,-1)
               AND ( SigneValDate is null and StatusId > 0
                   or (SigneValDate between ldFrom and ldTo and StatusID>=reftab_ot.RecStatusID(OperID)))
             ) P
       where CTP.PackageID=P.PackageID and CTP.AccountID=A.AccountID;
    END IF; -- IF isRBCZ = 1
  end if; -- if lDateCheck = 'VALUEDATE'
  return lnSumTotal;
End GetSumTotal;
-- end of help functions
-----------------------------------------------------------------------------------------------------
procedure GetValidSignRulesForClient (  -- vraci vsechna podpisova pravidla klienta pro jejich spravu
    inClientID      in  Clients.ClientID%type,       -- Klient prihlaseneho Usera
    orVSRtab        out sys_refcursor                -- seznam pravidel
)
is
begin
  open orVSRtab for
      select
             ClientSignRuleSeq SignRuleID, RuleDescription, AmountLimitCurrID CurrencyID,
             AmountLimit limit_immediate,
             Get_clientsignrulelimit(ClientSignRuleSeq,101) limit_day,
             Get_clientsignrulelimit(ClientSignRuleSeq,201) limit_month,
             Get_clientsignrulelimit(ClientSignRuleSeq,301) limit_year,
             StatusID Status, OrderImportant, ValidFrom, ValidUntil
        from ClientSignRules
       where ClientID=inClientID
         and sysdate between nvl(ValidFrom,trunc(sysdate)) and nvl(ValidUntil,sysdate)
    order by ClientSignRuleSeq;
end GetValidSignRulesForClient;
--------------------------------------------------------------------
procedure GetValidSignRulesForUser (  -- vraci vsechna podpisova pravidla uzivatele pro jejich spravu
    inUserID        in  ClientUsers.UserID%type,    -- Prihlaseny User
    inClientID      in  ClientUsers.ClientID%type,  -- Aktivny klient
    orVSRtab        out sys_refcursor               -- seznam pravidel
)
is
begin
  open orVSRtab for
      select
             CSR.ClientSignRuleSeq SignRuleID,
             CSR.RuleDescription,
             CSR.AmountLimitCurrID CurrencyID,
             CSR.AmountLimit limit_immediate,
             Get_clientsignrulelimit(CSR.ClientSignRuleSeq,101) limit_day,
             Get_clientsignrulelimit(CSR.ClientSignRuleSeq,201) limit_month,
             Get_clientsignrulelimit(CSR.ClientSignRuleSeq,301) limit_year,
             CSR.StatusID Status,
             CSR.OrderImportant,
             CSR.ValidFrom,
             CSR.ValidUntil
        from ClientSignRules CSR,
             clientsignrulesdef CSRD
       where CSR.ClientID=inClientID
         and CSR.ClientSignRuleSeq = CSRD.ClientSignRuleSeq
         and (CSRD.Userid = inUserID or CSRD.signruleroleid = (select cusrr.signruleroleid
                                                                 from clientusers cu, clientusersignrulerole cusrr
                                                                where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = CSR.ClientID
                                                                  and cu.userid = inUserID
                                                              )
             )
         and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
       order by CSR.ClientSignRuleSeq;
end GetValidSignRulesForUser;
--------------------------------------------------------------------
-- vracia userov pre vsetky prave platne podpisove pravidla, ktore mozu byt pouzite
PROCEDURE GetValidSignRulesUsers (inClientID      in  Clients.ClientID%type,      -- Klient prihlaseneho Usera
                                  inPackageID     in  ClientTransactionPackages.PackageID%type, -- Package, ktere se priradi jedno pravidlo ze seznamu
                                  inCertMethodID  in  pls_integer,
                                  inAddNullPK     in  pls_integer default 1,       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
                                  orUserIDs       out SYS_REFCURSOR,
                                  inCheckLimit    IN  PLS_INTEGER DEFAULT 1        -- 0/1 - nekontrolovat/kontrolovat limit
)
IS
lvListSeq      varchar2(32767);
lnUsersCnt     pls_integer;
BEGIN
  lnUsersCnt := 0;
  lvListSeq:='in (';
  for x in (select UserID from ClientUsers where ClientID=inClientID) loop
    if ExistValidSignatureRules(inClientID,inPackageID,inCertMethodID,inAddNullPK,x.UserID,inCheckLimit) = 1 then
      lvListSeq:=lvListSeq||x.UserID||',';
      lnUsersCnt := lnUsersCnt+1;
    end if;
  end loop;
  if lnUsersCnt>0 then
    lvListSeq:=rtrim(lvListSeq,',')||')';
  else
    lvListSeq:=' = -99 and 1=0';  -- nesplnitelna podmienka
  end if;
  open orUserIDs for
    'select UserID from ClientUsers'||
     ' where ClientID='||inClientID||' and UserID '||lvListSeq||
    ' order by 1';
END GetValidSignRulesUsers;
--------------------------------------------------------------------
-- vraci vsechna prave platna podpisova pravidla, ktera mohou byt pouzita
function ExistValidSignatureRules (inClientID      in  Clients.ClientID%type,      -- Klient prihlaseneho Usera
                                   inPackageID     in  ClientTransactionPackages.PackageID%type, -- Package, ktere se priradi jedno pravidlo ze seznamu
                                   inCertMethodID  in  pls_integer,
                                   inAddNullPK     in  pls_integer default 1,       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
                                   inUserID   in  Users.userid%TYPE default null,
                                   inCheckLimit    IN  PLS_INTEGER DEFAULT 1,        -- 0/1 - nekontrolovat/kontrolovat limit
                                   inChannelID     in  OperationTypeChannels.ChannelID%TYPE DEFAULT NULL
)
return pls_integer
is
  lnRetVal      pls_integer := 0;
  orVSRtab      sys_refcursor;
  lnUserCnt     pls_integer;
  lnRoleCnt     pls_integer;
  lnSAUserCnt   pls_integer;
  lnSARoleCnt   pls_integer;
  lnCnt         pls_integer;
  i             pls_integer;
  --
  cursor c is
    select ClientSignRuleSeq SignRuleID, RuleDescription, AmountLimitCurrID CurrencyID,
           AmountLimit limit_immediate,
           Get_clientsignrulelimit(ClientSignRuleSeq,101) limit_day,
           Get_clientsignrulelimit(ClientSignRuleSeq,201) limit_month,
           Get_clientsignrulelimit(ClientSignRuleSeq,301) limit_year,
           StatusID Status,OrderImportant
      from ClientSignRules
     where ClientSignRuleSeq = 0;
  --
  r c%rowtype;
  TYPE rec_SignRulesDef   IS record (UserOrder number, userid number, valid boolean, signruleroleid number);
  TYPE t_SignRulesDef     IS TABLE OF rec_SignRulesDef INDEX BY BINARY_INTEGER;
  ltSignRulesDef          t_SignRulesDef;
  CURSOR cur_SignatureRulesDef(inSignRuleSeq IN NUMBER) IS
    SELECT csr.UserId,csr.UserOrder,csr.signruleroleid,csr.requiredsignaturescount
      FROM ClientSignRulesDef csr
     WHERE csr.ClientSignRuleSeq = inSignRuleSeq
  ORDER BY csr.UserOrder;
begin
  declare
     problem_s_kurzom exception; -- UDEBS::GETCHANGEDAMOUNT Cannot find exchange rate for exchange from currency
     PRAGMA EXCEPTION_INIT (problem_s_kurzom, -20102);
  begin
     GetValidSignatureRules(inClientID,
                            inPackageID,
                            inCertMethodID,
                            orVSRtab,
                            inAddNullPK,
                            inUserID,
                            inCheckLimit,
                            inChannelID
                           );
  exception
     when problem_s_kurzom then
        lnRetVal := 0;
        return lnRetVal;
  end;
  if orVSRtab%isopen then
    loop
      fetch orVSRtab into r;
      exit when orVSRtab%notfound;
      if nvl(r.orderimportant,0) = 1 then -- order is important, get only upcoming signature to check
        -- check if signature is already not attached, and if next one is user or his role
        SELECT count(*)
          into lnCnt
          from SignatureArchive SA,
               (select /*+ RESULT_CACHE */ cu.userid, cusrr.signruleroleid
                                       from clientusers cu, clientusersignrulerole cusrr
                                      where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = inClientID
                                        and cu.userid = inUserID
                                      ) csr
         WHERE PackageId = inPackageID
           AND ContentType = '2'
           AND CertifSig = 1
           AND SA.userid = csr.userid;
        if lnCnt > 0 then
          lnRetVal := 0;
        else
          FOR rec_SignatureRulesDef IN cur_SignatureRulesDef(r.SignRuleId) LOOP
            ltSignRulesDef(cur_SignatureRulesDef%rowcount).UserOrder      := rec_SignatureRulesDef.UserOrder;
            ltSignRulesDef(cur_SignatureRulesDef%rowcount).UserID         := rec_SignatureRulesDef.UserID;
            ltSignRulesDef(cur_SignatureRulesDef%rowcount).SignRuleRoleID := rec_SignatureRulesDef.SignRuleRoleID;
            ltSignRulesDef(cur_SignatureRulesDef%rowcount).Valid := false;
          END LOOP;
          i:=ltSignRulesDef.First;
          for rec in (SELECT SA.UserId, SA.SignRuleRoleID
                        from SignatureArchive SA,
                             (select UserID,
                                     max(SignatureSeq) SignatureSeq
                                FROM SignatureArchive
                               where PackageId = inPackageID
                                 and ContentType='2'
                                 and CertifSig=1
                            group by UserID) B
                       WHERE SA.userid=B.userid
                         AND SA.signatureseq=B.signatureseq
                         AND PackageId = inPackageID
                         AND ContentType='2'
                         AND CertifSig=1
                    ORDER BY SA.signatureseq) loop
            if rec.UserID = nvl(ltSignRulesDef(i).UserId,0) or rec.SignRuleRoleID = nvl(ltSignRulesDef(i).SignRuleRoleID,0) then
              ltSignRulesDef(i).Valid := true; -- next valid signature in correct order
              i:=ltSignRulesDef.Next(i);
              if i is null then
                exit;
              end if;
            else
              -- check if such unused signature is not required in rule later, if so, fail, as it can not be added later - CPA, 14.3.2017 - 20009239-3264
              select count(*) into lnUserCnt from ClientSignRulesDef where ClientSignRuleSeq = r.SignRuleId and UserID is not null and UserID=rec.UserId;
              if lnUserCnt > 0 then
                lnRetVal := 0;
                i:=null;
                exit;
              end if;
            end if;
          end loop;
          if i is null then
            lnRetVal := 0;
          else
            select nvl(sum(case when cu.userid = nvl(ltSignRulesDef(i).UserId,0) then 1 else 0 end),0),
                   nvl(sum(case when cusrr.SignRuleRoleID = nvl(ltSignRulesDef(i).SignRuleRoleID,0) then 1 else 0 end),0)
             into lnUserCnt, lnRoleCnt
            from  clientusers cu, clientusersignrulerole cusrr
            where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = inClientID
              and cu.userid = inUserID
              and (cu.userid = nvl(ltSignRulesDef(i).UserId,0) or cusrr.SignRuleRoleID = nvl(ltSignRulesDef(i).SignRuleRoleID,0));
            if lnUserCnt > 0 then --  user signature is expected in order
              lnRetVal := 1;
            elsif lnRoleCnt > 0  then  --  role signature is expected in order, test if users single signature is not expected later
              select count(*) into lnCnt from ClientSignRulesDef csr
                where csr.ClientSignRuleSeq = r.SignRuleId and csr.UserId in (
                    Select cu.userid from clientusers cu
                    where cu.clientid = inClientID
                      and cu.userid = inUserID)
                   and csr.UserOrder > ltSignRulesDef(i).UserOrder;
              if lnCnt = 0 then -- users single signature is not expected later in rule, so it is expected as rule now
                lnRetVal := 1;
              else              -- users single signature is expected later in rule, so dont allow signature now
                lnRetVal := 0;
              end if;
            else
              lnRetVal := 0;
            end if;
          end if;
        end if;
      else -- order is not important
        -- a) current user is in that signature rule (either with UserID or RoleID)
        select nvl(sum(case when d.userid = csr.userid then 1 else 0 end),0), nvl(sum(case when d.signruleroleid = csr.signruleroleid then 1 else 0 end),0)
          into lnUserCnt, lnRoleCnt
          from ClientSignRulesDef d, (select /*+ RESULT_CACHE */ cu.userid, cusrr.signruleroleid
                                         from clientusers cu, clientusersignrulerole cusrr
                                        where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = inClientID
                                           and cu.userid = inUserID
                                        ) csr
         where d.clientsignruleseq = r.SignRuleId
           and (d.userid = csr.userid or d.signruleroleid = csr.signruleroleid);
        if lnUserCnt > 0 or lnRoleCnt > 0 then
          -- b) current user has not already attached signature
          SELECT nvl(sum(case when SA.userid = csr.userid then 1 else 0 end),0), nvl(sum(case when SA.signruleroleid = csr.signruleroleid then 1 else 0 end),0)
            into lnSAUserCnt, lnSARoleCnt
            from SignatureArchive SA,
                 (select /*+ RESULT_CACHE */ cu.userid, cusrr.signruleroleid
                                         from clientusers cu, clientusersignrulerole cusrr
                                        where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = inClientID
                                           and cu.userid = inUserID
                                        ) csr
           WHERE PackageId = inPackageID
             AND ContentType = '2'
             AND CertifSig = 1
             AND (SA.userid = csr.userid or SA.signruleroleid = csr.signruleroleid);
          -- if user has already used this signature rule, sign rule is not valid
          if lnSAUserCnt > 0 or (lnUserCnt = 0 and lnRoleCnt = 0) then
            lnRetVal := 0;
          elsif (lnUserCnt = 0 and lnRoleCnt > 0 and lnSARoleCnt > 0) then
            -- omit signatures for other users directly (not through role)
            select count(*) into lnCnt from SignatureArchive SA,
                 (select /*+ RESULT_CACHE */ cu.userid, cusrr.signruleroleid
                                         from clientusers cu, clientusersignrulerole cusrr
                                        where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = inClientID
                                          and cu.userid = inUserID
                                        ) csr,
                 (select csrd.UserId from ClientSignRulesDef csrd
                         where csrd.clientsignruleseq = r.SignRuleId) csrdx
            WHERE PackageId = inPackageID
              AND ContentType = '2'
              AND CertifSig = 1
              AND SA.signruleroleid = csr.signruleroleid
              AND SA.UserId = nvl(csrdx.UserId,0);
            if lnRoleCnt <= lnSARoleCnt-lnCnt then
              lnRetVal := 0;
            else
              lnRetVal := 1;
            end if;
          else
            lnRetVal := 1;
          end if;
        else
          lnRetVal := 0;
        end if;
      end if;
      if lnRetVal = 1 then
        exit;
      end if;
    end loop;
    --
    close orVSRtab;
  else
    lnRetVal := 0;
  end if;
  --
  return lnRetVal;
end ExistValidSignatureRules;
--------------------------------------------------------------------
procedure GetValidSignatureRules (  -- vraci vsechna prave platna podpisova pravidla, ktera mohou byt pouzita
    inClientID      in  Clients.ClientID%type,      -- Klient prihlaseneho Usera
    inPackageID     in  ClientTransactionPackages.PackageID%type, -- Package, ktere se priradi jedno pravidlo ze seznamu
    inCertMethodID  in  pls_integer,
    orVSRtab        out sys_refcursor,              -- seznam pravidel
    inAddNullPK     in  pls_integer default 1,       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
    inUserID   in  Users.userid%TYPE default null,
    inCheckLimit    IN  PLS_INTEGER DEFAULT 1,        -- 0/1 - nekontrolovat/kontrolovat limit
    inChannelID     in  OperationTypeChannels.ChannelID%TYPE DEFAULT NULL
)
is
  lnOperID              OperationTypes.OperID%type;  -- operace transakce
  lnAccountID           Accounts.AccountID%type;     -- vlastni ucet
  lnAmount              ClientTransactionPackages.TotalAmount%type;  -- castka package
  lvAmountCurr          Currencies.CurrencyID%type;  -- mena platby
  lnAccountAccessID     ClientAccounts.AccountAccessID%type;
  lnAccSens             Pls_Integer;
  lnMakesDebit          Pls_Integer;
  ltSignRuleSeq         dbms_sql.number_table;
  lvListSeq             CLOB;
  lnChangeLimAmount     NUMBER;
  lnSumTotal            NUMBER;
  ldFrom                date;
  ldTo                  date;
  lnCntErrorLimit       Pls_Integer;
  lnBranchID            Branches.BranchID%type;
  j                     Number;
  lnUserID              Users.userid%TYPE := null;
  lnCancPackageid       Transactions.cancelledpackageid%TYPE:=null;
  lnCntTmp              pls_integer;
  lnCheckLimit          boolean := true;
  lnOwnAccountsTrnCount pls_integer:=0;
  lnCertifReq           pls_integer:=0;
  --
  lvLocalCurrCTP        VARCHAR2(1):=COALESCE(reftab_gsp('REQUESTS.USE_LOCAL_CURRENCY_IN_CTP',0,'0'),'0');
  lvLocalCurr           VARCHAR2(3):=Reftab_GSP('LOCALCURRENCY',0,'X');
  v_sql                 CLOB;
  v_clob_part           CLOB;
  v_c     NUMBER := 0;
begin
  begin
    select OperID,CTP.AccountID,TotalAmount,nvl(CTP.CurrencyID,decode(lvLocalCurrCTP,'1',lvLocalCurr,A.CurrencyID)),nvl(BranchID,0),reftab_ot.AccountSensitive(OperID,0),reftab_ot.MakesDebit(OperID,0),
     reftab_ot.CertifReq(Operid,0,inChannelID)
      into lnOperID,lnAccountID,lnAmount,lvAmountCurr,lnBranchID,lnAccSens,lnMakesDebit, lnCertifReq
      from ClientTransactionPackages CTP,
           Accounts A
     where A.AccountID(+)=CTP.AccountID
      and CTP.StatusId!=33
      and PackageID=inPackageID;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      select OperID,CTP.AccountID,TotalAmount,nvl(CTP.CurrencyID,decode(lvLocalCurrCTP,'1',lvLocalCurr,A.CurrencyID)),nvl(BranchID,0),reftab_ot.AccountSensitive(OperID,0),reftab_ot.MakesDebit(OperID,0),
       reftab_ot.CertifReq(Operid,0,inChannelID)
        into lnOperID,lnAccountID,lnAmount,lvAmountCurr,lnBranchID,lnAccSens,lnMakesDebit, lnCertifReq
        from CTP_TMP CTP,
             Accounts A
       where A.AccountID(+)=CTP.AccountID
       and PackageID=inPackageID;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      im_error.AddError(2113559,'/unspecified/getvalidsignaturerules/packageid',inPackageID);
    END;
  end;
  begin
   if lnOperID NOT IN (781,786,780) then
    select
           AccountAccessID
      into lnAccountAccessID
      from ClientAccounts
     where ClientID=inClientID
       and AccountID=lnAccountID;
   else -- exception for file transfer operation (781,786,780)
    lnAccountAccessID:= null;
    lvAmountCurr := reftab_gsp('LocalCurrency',0,'EUR');   -- more accounts, so local currency is selected
    select nvl(sum(GetChangedAmount(nvl(amount,0),nvl(currencyId,lvAmountCurr),lvAmountCurr,lnBranchID)),0)
     into lnAmount
     from transactions where packageId = inPackageID;
   end if;
  exception
    when no_data_found then
      im_error.AddError(2113559,'/unspecified/getvalidsignaturerules/clientid',inClientID,lnAccountID);
  end;
  if inUserID is not null then
    BEGIN
      SELECT
             U.userid
        INTO lnUserID
        FROM Users U
       WHERE U.userid=inUserID
         and rownum <= 1;
    EXCEPTION
     when others then
       lnUserID := null;
    end;
  end if;
  for i in (select
                   CSR.ClientSignRuleSeq, CSR.AmountLimit, nvl(CSR.AmountLimitCurrID,lvAmountCurr) AmountLimitCurrID
              from ClientSignRules CSR,
                   ClientSignRuleOper CSRO
             where CSR.ClientSignRuleSeq=CSRO.ClientSignRuleSeq
               and CSR.ClientID=inClientID
               and CSR.StatusID=1
               and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
               and CSRO.OperID=lnOperID
               and lnCertifReq=1 -- only certified operations makes sense
               and (inChannelID is null or exists (select 1 from ClientSignRuleChannel CSRCH where CSR.ClientSignRuleSeq=CSRCH.ClientSignRuleSeq and CSRCH.channelid=inChannelID))
               and (lnUserID is null or exists (select 1
                                                  from ClientSignRulesDef d
                                                 where d.clientsignruleseq = CSR.ClientSignRuleSeq
                                                   and (  d.userid = lnUserID
                                                       or d.signruleroleid = (select cusrr.signruleroleid
                                                                                from clientusers cu, clientusersignrulerole cusrr
                                                                               where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = CSR.ClientID
                                                                                 and cu.userid = lnUserID
                                                                             )
                                                       )
                                               )
                    )
           ) loop
    if lnAccSens = 1 then -- musi byt svazano pravidlo s uctem
     if lnOperID NOT IN (781,786,780) then
      begin
        select
               ClientSignRuleSeq
          into ltSignRuleSeq(i.ClientSignRuleSeq)
          from ClientSignRuleAccount
         where ClientSignRuleSeq=i.ClientSignRuleSeq
           and AccountAccessID=lnAccountAccessID;
      exception
        when no_data_found then
          goto NEXT_FETCH;
      end;
     else  -- exception for file transfer operation (781,786,780)
      select count(*) into lnCntTmp
          from ClientSignRuleAccount
         where ClientSignRuleSeq=i.ClientSignRuleSeq
           and AccountAccessID in (
               select CA.accountaccessid from ClientAccounts CA, Transactions T
                where CA.ClientID = inClientID
                   and T.packageid = nvl(inPackageID,0)
                   and nvl(T.debitAccountID,0) = CA.AccountID
               );
      if lnCntTmp > 0 then
        ltSignRuleSeq(i.ClientSignRuleSeq):=i.ClientSignRuleSeq;
      else
        goto NEXT_FETCH;
      end if;
     end if;
    else
      ltSignRuleSeq(i.ClientSignRuleSeq):=i.ClientSignRuleSeq;
    end if;
    if lnMakesDebit = 1 AND inCheckLimit=1 then -- kontrola jednorazovych a obdobovych limitu
     IF i.AmountLimit is not null then -- CPA 4.5.2015, AmountLimit can be null - 20009183-2477
      IF lvAmountCurr <> i.AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
        lnChangeLimAmount:=GetChangedAmount(i.AmountLimit,i.AmountLimitCurrID,lvAmountCurr,lnBranchID);
      ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
        lnChangeLimAmount:=i.AmountLimit;
      END IF;
      -- prevody na ucty klienta se vylucuji z kontroly limitu pro RB
      lnCheckLimit := true;
      IF RefTab_GSP('LocalBankCodeAlpha',0,'X') IN ('RBCZ') THEN
       select count(*) into lnOwnAccountsTrnCount
        from Transactions Trn, Accounts A, ClientAccounts CA
        where Trn.Packageid=inPackageID AND A.AccountID = CA.AccountID AND reftab_ctst.FinalStatus(Trn.statusid,0) !=2 and CA.clientid = inClientID
          AND nvl(extractvalue(TRN.trandata,decode(lnOperID,304,'/TranData/DebitAccountNumber/text()',311,'/TranData/DebitAccountNumber/text()','/TranData/CreditAccountNumber/text()')),'X') = A.accnum
          AND nvl(extractvalue(TRN.trandata,decode(lnOperID,304,'/TranData/DebitBankCode/text()',311,'/TranData/DebitBankCode/text()','/TranData/CreditBankCode/text()')),'X') = RefTab_GSP('LocalBankCode',0,'X')
          and rownum<=1;
       IF lnOwnAccountsTrnCount>0 then
         lnCheckLimit := false;
       END IF;
      END IF;
      if lnCheckLimit and lnChangeLimAmount < lnAmount then -- prepoctena castka jednorazoveho limitu je mensi nez castka z package
        ltSignRuleSeq.delete(i.ClientSignRuleSeq);
        goto NEXT_FETCH;
      end if;
     END IF;
     IF lnCheckLimit then
      IF lvAmountCurr <> i.AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
        select count(*)
          into lnCntErrorLimit
          from clientsignrulelimits
         where ClientSignRuleSeq=i.ClientSignRuleSeq
           and GetChangedAmount(AmountLimit,i.AmountLimitCurrID,lvAmountCurr,lnBranchID)<lnAmount;
      ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
        select
               count(*)
          into lnCntErrorLimit
          from clientsignrulelimits
         where ClientSignRuleSeq=i.ClientSignRuleSeq
           and AmountLimit<lnAmount;
      END IF;
      if lnCntErrorLimit > 0 then -- alespon jeden limit pravidla je mensi nez zadana castka
        ltSignRuleSeq.delete(i.ClientSignRuleSeq);
        goto NEXT_FETCH;
      end if;
      for lim in (select AmountLimit,Period
                    from clientsignrulelimits
                   where ClientSignRuleSeq=i.ClientSignRuleSeq
                 ) loop
        lnSumTotal := GetSumTotal(i.ClientSignRuleSeq,lim.Period,lvAmountCurr,inPackageID);
        IF lvAmountCurr <> i.AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
          lnChangelimAmount:=GetChangedAmount(lim.AmountLimit,i.AmountLimitCurrID,lvAmountCurr,lnBranchID);
        ELSE  -- mena uctu a pravidla je stejna, lnAmount je jiz prepoctena castka v mene uctu => zadny prepocet
          lnChangeLimAmount:=lim.AmountLimit;
        END IF;
        if lnSumTotal + lnAmount > lnChangeLimAmount THEN -- suma castek je vetsi nez prepocteny limit
          ltSignRuleSeq.delete(i.ClientSignRuleSeq);
          exit;
        end if;
      end loop;
     end if;
    end if;
    <<NEXT_FETCH>>
    null;
  end loop;
  if ltSignRuleSeq.count>0 then
    lvListSeq:='in (';
    j:=ltSignRuleSeq.first;
    while j is not null loop
      v_c := v_c + 1;
      IF v_c = 900 THEN
        v_c := 0;
        DBMS_LOB.trim(lob_loc => lvListSeq, newlen => DBMS_LOB.getlength(lvListSeq)-1);
        v_clob_part := ') OR ClientSignRuleSeq IN (';
        lvListSeq:= lvListSeq||v_clob_part;
      END IF;
      v_clob_part := ltSignRuleSeq(j)||',';
      lvListSeq:=lvListSeq||v_clob_part;
      j:=ltSignRuleSeq.next(j);
    end loop;
    DBMS_LOB.trim(lob_loc => lvListSeq, newlen => DBMS_LOB.getlength(lvListSeq)-1);
    lvListSeq:=lvListSeq||')';
  else
    lvListSeq:='= -1';  -- nesplnitelna podminka
  end if;
  v_sql := 'select ClientSignRuleSeq SignRuleID, RuleDescription, AmountLimitCurrID CurrencyID,'||
          ' AmountLimit limit_immediate,'||
          ' Get_clientsignrulelimit(ClientSignRuleSeq,101) limit_day,'||
          ' Get_clientsignrulelimit(ClientSignRuleSeq,201) limit_month,'||
          ' Get_clientsignrulelimit(ClientSignRuleSeq,301) limit_year,'||
          ' StatusID Status,OrderImportant'||
     ' from ClientSignRules where ClientSignRuleSeq '||lvListSeq||' UNION '||
    'select 0 SignRuleID, ''<none>'' RuleDescription, reftab_gsp(''LocalCurrency'',0,''EUR'') CurrencyID,'||
          ' 0 limit_immediate, null limit_day, null limit_month, null limit_year,'||
          ' 1 Status, 0 OrderImportant from dual where 1='||to_char(inAddNullPK)||
    ' order by 1';
  open orVSRtab for v_sql;
end GetValidSignatureRules;
--------------------------------------------------------------------
procedure GetValidSignatureRule (                               -- single varianta pro PK
    inSignRuleSeq   in  ClientSignRules.ClientSignRuleSeq%type, -- PK
    orVSRtab        out sys_refcursor,
    inAddNullPK     in  pls_integer default 1       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
)
is
begin
  open orVSRtab for
      select ClientSignRuleSeq SignRuleID, RuleDescription, AmountLimitCurrID CurrencyID,
             AmountLimit limit_immediate,
             Get_clientsignrulelimit(ClientSignRuleSeq,101) limit_day,
             Get_clientsignrulelimit(ClientSignRuleSeq,201) limit_month,
             Get_clientsignrulelimit(ClientSignRuleSeq,301) limit_year,
             StatusID Status,OrderImportant
        from ClientSignRules
       where ClientSignRuleSeq=inSignRuleSeq
       UNION
      select 0 SignRuleID, '<none>' RuleDescription, reftab_gsp('LocalCurrency',0,'EUR') CurrencyID,
             0 limit_immediate, null limit_day, null limit_month, null limit_year,
             1 Status, 0 OrderImportant
        from dual
       where 0=inSignRuleSeq
         and 1=inAddNullPK;
end GetValidSignatureRule;
--------------------------------------------------------------------
procedure GetSignatureRuleForPackage (                          -- vraci pravidlo prirazene package
    onReturnCode    out Number,
    orErrors        out sys_refcursor,
    orSignRule      out sys_refcursor,
    inUserID   in  Users.userid%TYPE,
    inChannelID     in  Channels.ChannelID%type,
    inPackageID     in  ClientTransactionPackages.PackageID%type,
    idCheckoutDate  IN  DATE,
    inOpenError     in  Pls_integer default 1
)
is
begin
  onReturnCode:=0;
  if im_error.ExistErrType(im_error.ET_SUCCESSFUL)=0 then -- neexistuje warning z predchoziho prubehu
    im_error.init;
  end if;
  IF idCheckoutDate IS NOT NULL THEN  -- CTP_TMP
    select decode(Grants(inChannelID, ClientID, inUserID, 1, OperID, AccountID),1,0,1) into onReturnCode
      from CTP_TMP where PackageID=inPackageID;
  ELSE
    select decode(Grants(inChannelID, ClientID, inUserID, 1, OperID, AccountID),1,0,1) into onReturnCode
      from ClientTransactionPackages where PackageID=inPackageID;
  END IF;
  if onReturnCode=0 THEN
    IF idCheckoutDate IS NOT NULL THEN  -- CTP_TMP
      open orSignRule for
          select ClientSignRuleSeq SignRuleID, RuleDescription, AmountLimitCurrID CurrencyID,
                 AmountLimit limit_immediate,
                 Get_clientsignrulelimit(ClientSignRuleSeq,101) limit_day,
                 Get_clientsignrulelimit(ClientSignRuleSeq,201) limit_month,
                 Get_clientsignrulelimit(ClientSignRuleSeq,301) limit_year,
                 CSR.StatusID Status,OrderImportant
            from ClientSignRules CSR,
                 CTP_TMP CTP
           where CSR.ClientSignRuleSeq=CTP.SignRuleSeq
             and CTP.PackageID=inPackageID;
    ELSE
      open orSignRule for
          select ClientSignRuleSeq SignRuleID, RuleDescription, AmountLimitCurrID CurrencyID,
                 AmountLimit limit_immediate,
                 Get_clientsignrulelimit(ClientSignRuleSeq,101) limit_day,
                 Get_clientsignrulelimit(ClientSignRuleSeq,201) limit_month,
                 Get_clientsignrulelimit(ClientSignRuleSeq,301) limit_year,
                 CSR.StatusID Status,OrderImportant
            from ClientSignRules CSR,
                 ClientTransactionPackages CTP
           where CSR.ClientSignRuleSeq=CTP.SignRuleSeq
             and CTP.PackageID=inPackageID;
    END IF;
  else
    onReturnCode:=1;
    im_error.adderror(2113540,'You have no privileges for this operation.');
  end if;
  if inOpenError=1 then im_error.geterrors(orErrors); end if;
exception
  when no_data_found then
    onReturnCode:=1;
    im_error.AddError(2113559,null,'/unspecified/getsignatureruleforpackage/packageid',inPackageID);
    if inOpenError=1 then im_error.geterrors(orErrors); end if;
end GetSignatureRuleForPackage;
--------------------------------------------------------------------
procedure SetOrChangeSignatureRule(
    onReturnCode    out number,
    orErrors        out sys_refcursor,
    inUserID   in  Users.userid%TYPE,
    inChannelID     in  Channels.ChannelID%type,
    inPackageID     in  ClientTransactionPackages.PackageID%type,
    idCheckoutDate  in  date,
    ivSignRuleSeq   in  varchar2,
    inOpenError     in  Pls_integer default 1,
    inJournal       IN  PLS_INTEGER DEFAULT 1
) -- s kontrolou prav
is
  lnAccountID     ClientTransactionPackages.AccountID%type;
  lnOperID        ClientTransactionPackages.OperID%type;
  lnSignRuleSeq   ClientTransactionPackages.SignRuleSeq%type;
  lnOldSignRule   ClientTransactionPackages.SignRuleSeq%type;
  lnClientID      ClientTransactionPackages.ClientID%type;
  lnStatusID      ClientTransactionPackages.StatusID%type;
  lnAccSens       pls_integer;
  lnAccountAccessID number;
  lnCheckoutUserID  pls_integer;
  lnCheckoutChID  pls_integer;
  ldCheckoutDate  date;
  procedure TestSignRule is
  begin
    if lnSignRuleSeq > 0 then   -- 0 = vynulovat pravidlo
      select
             0
        into onReturnCode
        from ClientSignRules CSR,
             ClientSignRuleOper CSRO
       where CSR.ClientSignRuleSeq=CSRO.ClientSignRuleSeq
         and CSR.ClientID=lnClientID
         and CSR.StatusID=1
         and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
         and CSR.ClientSignRuleSeq=lnSignRuleSeq
         and CSRO.OperID=lnOperID;
      if lnAccSens = 1 then -- musi byt svazano pravidlo s uctem
       if nvl(lnOperID,0) NOT IN (781,786,780) then
        select
               0
          into onReturnCode
          from ClientSignRuleAccount
         where ClientSignRuleSeq=lnSignRuleSeq and AccountAccessID=lnAccountAccessID;
       else -- specific functionality for file transfer operation (781,786,780)
        select decode(count(*),0,1,0)
          into onReturnCode
          from ClientSignRuleAccount
         where ClientSignRuleSeq=lnSignRuleSeq and AccountAccessID in (
              select CA.accountaccessid from ClientAccounts CA, Transactions T
                where CA.ClientID = lnClientID
                   and T.packageid = nvl(inPackageID,0)
                   and nvl(T.debitAccountID,0) = CA.AccountID
            );
       end if;
      end if;
    end if;
  exception when no_data_found then
    onReturnCode:=1;
    im_error.AddError(2113581,null,'/unspecified/signruleseq',ivSignRuleSeq);
  end TestSignRule;
  ---------------
  PROCEDURE journaling IS
    lvDesc        VARCHAR2(1024 CHAR);
  BEGIN
    IF inJournal!=1 THEN
      RETURN;
    END IF;
    --
    IF lnSignRuleSeq <> NVL(lnOldSignRule,0) THEN
      IF lnSignRuleSeq = 0 THEN
        lvDesc:='none';
      ELSE
        SELECT RuleDescription INTO lvDesc FROM ClientSignRules
        WHERE CLIENTSIGNRULESEQ=lnSignRuleSeq;
      END IF;
      JOURNAL001.setparevseq(null,inPackageid);
      JOURNAL001.logchildevent(1700008,inUserID,systimestamp,null,lvDesc,'GUSRS',inchannelid,'UserID',inUserID,lnclientid);
    END IF;
  END journaling;
begin
  onReturnCode:=0;
  if im_error.ExistErrType(im_error.ET_SUCCESSFUL)=0 then -- neexistuje warning z predchoziho prubehu
    im_error.init;
  end if;
  begin
    lnSignRuleSeq:=char2num(nvl(ivSignRuleSeq,'0'));
  exception when others then
    onReturnCode:=1;
    im_error.AddError(2113575,null,'/unspecified/signruleseq',ivSignRuleSeq);
  end;
  if onReturnCode = 0 then
    if idCheckoutDate is not null then -- edit
      select CA.AccountID,OperID,UserID,ChannelID,BornDate,NVL(CA.ClientID, CTP_TMP.clientId),reftab_ot.AccountSensitive(OperID,0),AccountAccessID,SignRuleSeq
        into lnAccountID,lnOperID,lnCheckoutUserID,lnCheckoutChID,ldCheckoutDate,lnClientId,lnAccSens,lnAccountAccessID,lnOldSignRule
        from CTP_TMP,
             ClientAccounts CA
       where PackageID=inPackageID
         and CTP_TMP.ClientID=CA.ClientID(+)
         and CTP_TMP.AccountID=CA.AccountID(+);
      TestSignRule;
      if onReturnCode = 0 then
        if Grants(inChannelID,lnClientId, inUserID, 1, lnOperID, lnAccountID) = 1 and
           inUserID=lnCheckoutUserID and inChannelID=lnCheckoutChID and idCheckoutDate=ldCheckoutDate
        then
          update CTP_TMP
             set SignRuleSeq=decode(lnSignRuleSeq,0,null,lnSignRuleSeq)
           where PackageID=inPackageID
             and get_ctst_values('AllowSign',StatusID,0)<>0; -- TEMP,EDIT,PARSED,FORSIG,PARTSIG
          if sql%rowcount=1 then
            onReturnCode:=0;
            journaling;
          else
            onReturnCode:=1;
            im_error.AddError(2113614,null,'/unspecified/setorchangesignaturerule/packageid',inPackageID);
          end if;
        else
          onReturnCode:=1;
          im_error.adderror(2113540,'You have no privileges for this operation.');
        end if;
      end if;
    ELSE  -- no edit
      select CA.AccountID,OperID,NVL(CA.ClientID, CTP.ClientId),reftab_ot.AccountSensitive(OperID,0),AccountAccessID,SignRuleSeq,CTP.statusid
        into lnAccountID,lnOperID,lnClientId,lnAccSens,lnAccountAccessID,lnOldSignRule,lnStatusID
        from ClientTransactionPackages CTP,
             ClientAccounts CA
       where PackageID=inPackageID
         and CTP.ClientID=CA.ClientID(+)
         and CTP.AccountID=CA.AccountID(+);
      TestSignRule;
      if onReturnCode = 0 then
        if Grants(inChannelID, lnClientId, inUserID, 1, lnOperID, lnAccountID) = 1 then
          update ClientTransactionPackages
             set SignRuleSeq=decode(lnSignRuleSeq,0,null,lnSignRuleSeq)
           where PackageID=inPackageID
             and get_ctst_values('AllowSign',StatusID,0)<>0; -- TEMP,EDIT,PARSED,FORSIG,PARTSIG
          if sql%rowcount=1 then
            onReturnCode:=0;
            IF lnStatusID<>-1 THEN  -- neprirazuje se pravidlo tmp package
              journaling;
            END IF;
          else
            onReturnCode:=1;
            im_error.AddError(2113614,null,'/uspecified/setorchangesignaturerule/packageid',inPackageID);
          end if;
        else
          onReturnCode:=1;
          im_error.adderror(2113540,'You have no privileges for this operation.');
        end if;
      end if;
    end if;
  end if; -- onReturnCode = 0
  if inOpenError=1 then im_error.geterrors(orErrors); end if;
exception when no_data_found then
  onReturnCode:=1;
  im_error.AddError(2113559,null,'/uspecified/setorchangesignaturerule/packageid',inPackageID);
  if inOpenError=1 then im_error.geterrors(orErrors); end if;
end SetOrChangeSignatureRule;
--------------------------------------------------------------------
procedure VerifySignatureRule(
    inPackageID   in  ClientTransactionPackages.PackageID%type,
    inSignRuleSeq in  ClientTransactionPackages.SignRuleSeq%type,
    onReturnCode  out number,
    inChannelID   in  OperationTypeChannels.ChannelID%TYPE DEFAULT NULL
)
is
  lnAccountAccessID     ClientAccounts.AccountAccessID%type;
  lnAccSens             Pls_Integer;
  lnMakesDebit          Pls_Integer;
  lnClientID            Pls_Integer;
  lnAmount              ClientTransactionPackages.TotalAmount%type := 0;
  lnTotalAmount         ClientTransactionPackages.TotalAmount%type := 0;
  lnOperID              Number;
  lvAmountCurr          Varchar2(3);
  lnBranchID            Branches.BranchID%type;
  lnAmountLimit         ClientTransactionPackages.TotalAmount%type := 0;
  lvAmountLimitCurrID   Varchar2(3);
  lnChangeLimAmount     ClientTransactionPackages.TotalAmount%type := 0;
  lnSumTotal            ClientTransactionPackages.TotalAmount%type := 0;
  ldFrom                date;
  ldTo                  date;
  lnCntErrorLimit       Pls_Integer;
  lnStatusID            ClientTransactionPackages.StatusID%type;
  lnChannelID           ClientTransactionPackages.ChannelID%type;
  otSubtotals           Subtotal_tab;
  lnCheckLimit          boolean := true;
  lnOwnAccountsTrnCount pls_integer:=0;
  lnCertifReq           pls_integer:=1;
  --
  lvLocalCurrCTP        VARCHAR2(1):=COALESCE(reftab_gsp('REQUESTS.USE_LOCAL_CURRENCY_IN_CTP',0,'0'),'0');
  lvLocalCurr           VARCHAR2(3):=Reftab_GSP('LOCALCURRENCY',0,'X');
  isRBCZ                NUMBER := CASE WHEN RefTab_GSP('LocalBankCodeAlpha',0,'X') = 'RBCZ' THEN 1 ELSE 0 END;
  v_sql                 VARCHAR2(32000);
begin
  onReturnCode:=0;
  -- zistenie vlastnosti AccountSensitive a CertifReq
  select reftab_ot.AccountSensitive(OperID,0), StatusID, ChannelID, OperID, reftab_ot.CertifReq(Operid,0,inChannelID)
    into lnAccSens, lnStatusID, lnChannelID, lnOperID, lnCertifReq
    from ClientTransactionPackages
   where packageId = inPackageID;
  if lnCertifReq=1 then -- makes sense to check only for certified operations
    if lnAccSens = 1 then -- AccountSensitive = 1
     if lnOperID NOT IN (781,786,780) then
      select CTP.ClientID, TotalAmount, decode(lvLocalCurrCTP,'1',lvLocalCurr,A.CurrencyID), A.BranchID, AccountAccessID,
             reftab_ot.AccountSensitive(OperID,0), reftab_ot.MakesDebit(OperID,0)
        into lnClientID, lnTotalAmount, lvAmountCurr, lnBranchID, lnAccountAccessID, lnAccSens, lnMakesDebit
        from ClientAccounts CA,
             ClientTransactionPackages CTP,
             Accounts A
       where CTP.PackageID=inPackageID
         and CTP.SignRuleSeq=inSignRuleSeq
         and CA.ClientID=CTP.ClientID
         and CA.AccountID=CTP.AccountID
         and A.AccountID=CA.AccountID;
     else -- exception for file transfer operation (781,786,780) - not connected to account
      select ClientID, 0, reftab_gsp('LocalCurrency',0,'EUR'), 0, NULL,
             reftab_ot.AccountSensitive(OperID,0), reftab_ot.MakesDebit(OperID,0)
        into lnClientID, lnAmount, lvAmountCurr, lnBranchID, lnAccountAccessID, lnAccSens, lnMakesDebit
        from ClientTransactionPackages
       where PackageID=inPackageID
         and SignRuleSeq=inSignRuleSeq;
      select nvl(sum(GetChangedAmount(nvl(amount,0),nvl(currencyId,lvAmountCurr),lvAmountCurr,lnBranchID)),0)
       into lnAmount
       from transactions where packageId = inPackageID;
     end if;
    else -- AccountSensitive = 0
      select ClientID, TotalAmount, CurrencyID, NULL, NULL,
             reftab_ot.AccountSensitive(OperID,0), reftab_ot.MakesDebit(OperID,0)
        into lnClientID, lnAmount, lvAmountCurr, lnBranchID, lnAccountAccessID, lnAccSens, lnMakesDebit
        from ClientTransactionPackages
       where PackageID=inPackageID
         and SignRuleSeq=inSignRuleSeq;
    end if;
    select 0, CSR.AmountLimit, nvl(CSR.AmountLimitCurrID,lvAmountCurr) AmountLimitCurrID
      into onReturnCode, lnAmountLimit,lvAmountLimitCurrID
      from ClientSignRules CSR,
           ClientSignRuleOper CSRO
     where CSR.ClientSignRuleSeq=CSRO.ClientSignRuleSeq
       and CSR.ClientID=lnClientID
       and CSR.StatusID=1
       and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
       and CSR.ClientSignRuleSeq=inSignRuleSeq
       and CSRO.OperID=lnOperID;
    if lnAccSens = 1 then -- musi byt svazano pravidlo s uctem
     if lnOperID NOT IN (781,786,780) then
      select 0
        into onReturnCode
        from ClientSignRuleAccount
       where ClientSignRuleSeq=inSignRuleSeq
         and AccountAccessID=lnAccountAccessID;
     else -- exception for file transfer operation (781,786,780) - not connected to account
      select decode(count(*),0,1,0)
        into onReturnCode
        from ClientSignRuleAccount
       where ClientSignRuleSeq=inSignRuleSeq
            and AccountAccessID in (
                 select CA.accountaccessid from ClientAccounts CA, Transactions T
                  where CA.ClientID = lnClientID
                     and T.packageid = nvl(inPackageID,0)
                     and nvl(T.debitAccountID,0) = CA.AccountID
                 );
     end if;
    end if;
  else
    onReturnCode := 1;
  end if;
  if onReturnCode = 0 and lnMakesDebit = 1 then -- kontrola jednorazovych a obdobovych limitu
    -- prevody na ucty klienta se vylucuji z kontroly limitu pro RB
    lnCheckLimit := true;
    IF isRBCZ = 1 THEN
     v_sql := 'select count(*)
      from Transactions Trn
       JOIN ClientAccounts CA ON CA.clientid = :lnClientID
       JOIN Accounts A ON A.AccountID = CA.AccountID
       LEFT JOIN transactionstrancache trnc_acc on trnc_acc.transactionid = trn.transactionid AND trnc_acc.attrid = decode(:lnOperID,304,12,311,12,5)
       LEFT JOIN transactionstrancache trnc_bc on trnc_bc.transactionid = trn.transactionid AND trnc_bc.attrid = decode(:lnOperID,304,14,311,14,7)
      where Trn.Packageid=:inPackageID
       AND reftab_ctst.FinalStatus(Trn.statusid,0) !=2
        AND nvl(trnc_acc.val,''X'') = A.accnum
        AND nvl(trnc_bc.val,''X'') = RefTab_GSP(''LocalBankCode'',0,''X'')
        and rownum<=1';
     EXECUTE IMMEDIATE v_sql INTO lnOwnAccountsTrnCount USING lnClientID, lnOperID, lnOperID, inPackageID;
     IF lnOwnAccountsTrnCount>0 then
       lnCheckLimit := false;
     END IF;
    END IF; -- IF isRBCZ = 1
   IF lnCheckLimit and lnAmountLimit is not null then -- CPA 4.5.2015 , limitAmount not necessary - 20009183-2477
    IF lvAmountCurr <> lvAmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
      lnChangeLimAmount:=GetChangedAmount(lnAmountLimit,lvAmountLimitCurrID,lvAmountCurr,lnBranchID);
    ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
      lnChangeLimAmount:=lnAmountLimit;
    END IF;
   end if;
   if lnCheckLimit then
    begin
      -- vypocitame aktualny total amount pre vsetky transakcie package
      lnAmount := GetPackageAmount (otSubtotals,
                                    inPackageId,
                                    lnOperID,
                                    inStatusID => NULL,
                                    inSeqNum => NULL);
      journal001.logevent(1700012,'VerifySignatureRule.CurrentTotalAmount',systimestamp,systimestamp,'<PackageID>'||inPackageId||'</PackageID><TotalAmount>'||to_char(lnTotalAmount)||'</TotalAmount><SignRuleSeq>'||to_char(inSignRuleSeq)||'</SignRuleSeq><TotalAmount>'||to_char(lnAmount)||'</TotalAmount><AmountCurrency>'||lvAmountCurr||'</AmountCurrency><AmountLimit>'||to_char(lnAmountLimit)||'</AmountLimit><AmountLimitCurrID>'||lvAmountLimitCurrID||'</AmountLimitCurrID>',
                         'IIF_CCFG_005',lnChannelID,null,null,lnClientID);
    exception
      when others then
        journal001.logevent(1700012,'VerifySignatureRule.Error',systimestamp,systimestamp,'<PackageID>'||inPackageId||'</PackageID><TotalAmount>'||to_char(lnTotalAmount)||'</TotalAmount><SignRuleSeq>'||to_char(inSignRuleSeq)||'</SignRuleSeq><TotalAmount>'||to_char(lnAmount)||'</TotalAmount><AmountCurrency>'||lvAmountCurr||'</AmountCurrency><AmountLimit>'||to_char(lnAmountLimit)||'</AmountLimit><AmountLimitCurrID>'||lvAmountLimitCurrID||'</AmountLimitCurrID>',
                           'IIF_CCFG_005', lnChannelID,null,null,lnClientID, aExtension => dbms_utility.format_error_stack);
    end;
   IF lnAmountLimit is not null then -- CPA 4.5.2015 , limitAmount not necessary - 20009183-2477
    if lnChangeLimAmount < lnAmount then -- prepoctena castka jednorazoveho limitu je mensi nez castka z package
      onReturnCode:=1;
    end if;
   end if;
    IF lvAmountCurr <> lvAmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
      select count(*)
        into lnCntErrorLimit
        from clientsignrulelimits
       where ClientSignRuleSeq=inSignRuleSeq
         and GetChangedAmount(AmountLimit,lvAmountLimitCurrID,lvAmountCurr,lnBranchID)<lnAmount;
    ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
      select count(*)
        into lnCntErrorLimit
        from clientsignrulelimits
       where ClientSignRuleSeq=inSignRuleSeq
         and AmountLimit<lnAmount;
    END IF;
    if lnCntErrorLimit > 0 then -- alespon jeden limit pravidla je mensi nez zadana castka
      onReturnCode:=1;
    end if;
    if onReturnCode = 0 then -- dosud bez chyb
      for lim in (select AmountLimit,Period
                    from clientsignrulelimits
                   where ClientSignRuleSeq=inSignRuleSeq
                 ) loop
        lnSumTotal := GetSumTotal(inSignRuleSeq,lim.Period,lvAmountCurr,inPackageID);
        IF lvAmountCurr <> lvAmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
          lnChangelimAmount:=GetChangedAmount(lim.AmountLimit,lvAmountLimitCurrID,lvAmountCurr,lnBranchID);
        ELSE  -- mena uctu a pravidla je stejna, lnAmount je jiz prepoctena castka v mene uctu => zadny prepocet
          lnChangeLimAmount:=lim.AmountLimit;
        END IF;
        if lnSumTotal + lnAmount > lnChangeLimAmount THEN -- suma castek je vetsi nez prepocteny limit
          onReturnCode:=1;
          exit;
        end if;
      end loop;
    end if;
   end if;
  end if;
exception when others then
  onReturnCode:=1;
end VerifySignatureRule;
--
procedure VerifySufficiencySigning( -- testuje podpisy podle pravidla
    inPackageID   in  ClientTransactionPackages.PackageID%type,
    inSignRuleSeq in  ClientTransactionPackages.SignRuleSeq%type,
    onReturnCode  out number,
    orErrors      out sys_refcursor,
    inOpenError   in  Pls_integer default 1,
    inReport      IN  PLS_INTEGER DEFAULT 1
)
is
  lnSignRuleSeq           ClientTransactionPackages.SignRuleSeq%type;
  TYPE rec_Signatures     IS record (SignatureOrder number, userid number, used boolean, signruleroleid number);
  TYPE t_Signatures       IS TABLE OF rec_Signatures INDEX BY BINARY_INTEGER;
  TYPE rec_SignRulesDef   IS record (UserOrder number, userid number, valid boolean, signruleroleid number);
  TYPE t_SignRulesDef     IS TABLE OF rec_SignRulesDef INDEX BY BINARY_INTEGER;
  ltSignatures            t_Signatures;
  ltSignRulesDef          t_SignRulesDef;
  lnOrderImportant        Pls_integer;
  lnMinimalSignatures     ClientSignRules.minimalsignatures%TYPE;
  lnCountOfUsedSignatures pls_integer:=0;
  lnSignatureOrder        Pls_integer := 0;
  lnOperID                Pls_integer;
  lnChannelID             Pls_integer;
  lnStatusID              Pls_integer;
  lnOldStatusID           Pls_integer;
  lbPartiallySigned       Boolean;
  lbSufficientlySigned    Boolean := TRUE;
  i                       Pls_integer;
  j                       Pls_integer;
  lnSignatureFound        Pls_integer;
  k                       Pls_integer;
  ldSigneValDate          DATE;
  CURSOR cur_Signatures(inPckID in number) IS
    SELECT SA.UserId, SA.SignRuleRoleID
      from SignatureArchive SA,
           (select UserID,
                   max(SignatureSeq) SignatureSeq
              FROM SignatureArchive
             where PackageId = inPckId
               and ContentType='2'
               and CertifSig=1
          group by UserID) B
     WHERE SA.userid=B.userid
       AND SA.signatureseq=B.signatureseq
       AND PackageId = inPckId
       AND ContentType='2'
       AND CertifSig=1
  ORDER BY SA.signatureseq;
  -- uzivatele, kteri musi podepsat dokument platny podle daneho podpisoveho pravidla, serazeni podle poradi
  CURSOR cur_SignatureRulesDef(inSignRuleSeq IN NUMBER) IS
    SELECT csr.UserId,csr.UserOrder,csr.signruleroleid,csr.requiredsignaturescount
      FROM ClientSignRulesDef csr
     WHERE csr.ClientSignRuleSeq = inSignRuleSeq
  ORDER BY csr.UserOrder;
begin
  onReturnCode:=0;
  if im_error.ExistErrType(im_error.ET_SUCCESSFUL)=0 then -- neexistuje warning z predchoziho prubehu
    im_error.init;
  end if;
  begin
    select SignRuleSeq,OperId,ChannelID,StatusID
      into lnSignRuleSeq,lnOperID,lnChannelID, lnOldStatusID
      from ClientTransactionPackages
     where PackageID=inPackageID
       and nvl(SignRuleSeq,0)=nvl(inSignRuleSeq,nvl(SignRuleSeq,0));
  exception
    when no_data_found then
      onreturnCode:=1;
      im_error.AddError(2113559,null,'/unspecified/verifysufficiencysigning/packageid',inPackageID,inSignRuleSeq);
  end;
  if lnSignRuleSeq is not null then -- jiz prirazene podpisove pravidlo
    VerifySignatureRule(inPackageID, lnSignRuleSeq, onReturnCode);
  elsif reftab_ot.SignRulesReq(lnOperID,0)=1 then -- podpis musi byt podle pravidla, ktere neni dosud nastaveno
    onReturnCode:=1;
  end if;
  if onReturnCode = 1 then
    IF Reftab_GSP('LOCALBANKCODEALPHA') in ('SLSP') THEN
      lnOldStatusID := null;
    END IF;
    lbPartiallySigned := nvl(lbPartiallySigned,FALSE);
    lbSufficientlySigned:=FALSE;
    onReturnCode:=0;
    if lnSignRuleSeq is not null then
      im_error.AddError(2105346,'Signature rule was set to null during verification of sufficienty of signing because rule was recognized as unsufficient or as invalid.',
                                '/unspecified/verifysufficiencysigning/packageid',inPackageID,inSignRuleSeq);-- nastavene pravidlo je jiz neplatne a bylo smazano
    end if;
  else -- platne pravidlo nebo operace nevyzaduje pravidlo, ale jenom podpis
    -- podpisy do pomocne tabulky
    FOR rec_Signatures IN cur_Signatures(inPackageID) LOOP
      ltSignatures(cur_Signatures%rowcount).SignatureOrder := cur_Signatures%rowcount;
      ltSignatures(cur_Signatures%rowcount).UserID         := rec_Signatures.UserID;
      ltSignatures(cur_Signatures%rowcount).SignRuleRoleID := rec_Signatures.SignRuleRoleID;
      ltSignatures(cur_Signatures%rowcount).Used           := false;
    END LOOP;
    if reftab_ot.SignRulesReq(lnOperID,0)=1 then -- musi byt podpis podle pravidla
      -- users do pomocne tabulky
      FOR rec_SignatureRulesDef IN cur_SignatureRulesDef(lnSignRuleSeq) LOOP
        ltSignRulesDef(cur_SignatureRulesDef%rowcount).UserOrder      := rec_SignatureRulesDef.UserOrder;
        ltSignRulesDef(cur_SignatureRulesDef%rowcount).UserID         := rec_SignatureRulesDef.UserID;
        ltSignRulesDef(cur_SignatureRulesDef%rowcount).SignRuleRoleID := rec_SignatureRulesDef.SignRuleRoleID;
        ltSignRulesDef(cur_SignatureRulesDef%rowcount).Valid := false;
      END LOOP;
      select OrderImportant, MinimalSignatures
        into lnOrderImportant, lnMinimalSignatures
        from ClientSignRules
       where ClientSignRuleSeq=lnSignRuleSeq;
      i:=ltSignRulesDef.First;
      while i is not null LOOP
        j:=ltSignatures.First;
        lnSignatureFound := null;
        while j is not null LOOP
          if ltSignatures(j).Used = false then  -- skip used signatures
            if ltSignatures(j).UserID = nvl(ltSignRulesDef(i).UserId,0) then
              -- userid found, use it
              lnSignatureFound := j;
              exit;
            end if;
            if ltSignatures(j).SignRuleRoleID = nvl(ltSignRulesDef(i).SignRuleRoleID,0) then
              -- signruleroleid found, check rest of definitions if direct userid in can be found
              k:=ltSignRulesDef.First;
              while k is not null LOOP
                if ltSignRulesDef(k).Valid = false then  -- skip used signatures
                  if ltSignatures(j).UserID = nvl(ltSignRulesDef(k).UserId,0) then
                    exit;
                  end if;
                end if;
                k:=ltSignRulesDef.Next(k);
              end loop;
              if k is null then -- no direct user definition found, using signature as role
                lnSignatureFound := j;
                exit;
              end if;
            end if;
          end if;
          j:=ltSignatures.Next(j);
        end loop;
        IF lnSignatureFound is not null THEN
          IF lnOrderImportant = 1 THEN
            IF ( ((ltSignatures(lnSignatureFound).UserID = nvl(ltSignRulesDef(i).UserId,0))  AND lnSignatureOrder < ltSignatures(lnSignatureFound).SignatureOrder)
                OR ((ltSignatures(lnSignatureFound).SignRuleRoleID = nvl(ltSignRulesDef(i).SignRuleRoleID,0))  AND lnSignatureOrder < ltSignatures(lnSignatureFound).SignatureOrder)
               )
            THEN -- naposledy overeny podpis package musi predchazet stavajicimu podpisu
              IF i = 1 THEN -- prvni uzivatel podle pravidla
                ltSignRulesDef(i).Valid:=True;
                -- zapamatujeme si poradi overeneho podpisu
                lnSignatureOrder:=ltSignatures(lnSignatureFound).SignatureOrder;
                lbPartiallySigned := TRUE;
                ltSignatures(lnSignatureFound).Used := true;
              ELSIF ltSignRulesDef(i-1).Valid then -- predchozi podpis je platny => stavajici bude taky platny
                ltSignRulesDef(i).Valid:=True;
                -- zapamatujeme si poradi overeneho podpisu
                lnSignatureOrder:=ltSignatures(lnSignatureFound).SignatureOrder;
                lbPartiallySigned := TRUE;
                ltSignatures(lnSignatureFound).Used := true;
              ELSE -- podpis vlozen mimo poradi stanovene pravidlem
                ltSignRulesDef(i).Valid:=False;
                lbPartiallySigned := nvl(lbPartiallySigned,FALSE);
                lbSufficientlySigned:=FALSE;
              END IF;
            ELSE -- posledni overeny podpis nepredchazi stavajicimu
              ltSignRulesDef(i).Valid:=False;
              lbPartiallySigned := nvl(lbPartiallySigned,FALSE);
              lbSufficientlySigned:=FALSE;
            END IF;
          ELSE -- nezalezi na poradi => podpis je platny
            ltSignRulesDef(i).Valid:=True;
            lbPartiallySigned := TRUE;
            ltSignatures(lnSignatureFound).Used := true;
          END IF;
          IF ltSignatures(lnSignatureFound).Used and lnMinimalSignatures is not null then -- check count of used signatures, if it is not sufficient
            lnCountOfUsedSignatures:=lnCountOfUsedSignatures+1;
            if lnCountOfUsedSignatures >= lnMinimalSignatures then
              lbSufficientlySigned:=TRUE;
              i := null;
              EXIT;
            end if;
          END IF;
        ELSE -- dosud neexistuje podpis
          -- lnOldStatusID:=null;  -- commented due to 20009239-4185 by CPA, 25.4.2017, old status always remains, because of cyclic signature rules tests
          ltSignRulesDef(i).Valid:=False;
          lbPartiallySigned := nvl(lbPartiallySigned,FALSE);
          lbSufficientlySigned:=FALSE;
        END IF;
        i:=ltSignRulesDef.Next(i);
      END LOOP;
    elsif reftab_ot.CertifReq(lnOperID,0)=1 then -- musi byt platny podpis
      if ltSignatures.count=0 then
        lbSufficientlySigned:=False;
        lbPartiallySigned:=False;
      end if;
    end if;
  end if;   -- onReturnCode=1
  if lbSufficientlySigned then
    update ClientTransactionPackages
       set SigneValDate=sysdate
     where PackageID=inPackageID;
    lnStatusID:=get_ot_values('RECSTATUSID',lnOperID);
  elsif lbPartiallySigned or nvl(lnOldStatusID,31) = 32 THEN
    IF Reftab_GSP('LOCALBANKCODEALPHA') in ('SLSP') THEN
      ldSigneValDate:=SYSDATE;
    ELSE
      ldSigneValDate:=NULL;
    END IF;
    update ClientTransactionPackages
       set SigneValDate=ldSigneValDate
     where PackageID=inPackageID;
    lnStatusID:=32;
  else
    update ClientTransactionPackages
       set SigneValDate=null
     where PackageID=inPackageID;
    lnStatusID:=31;
  end if;
  ReportClientTransaction.Init;
--  ReportClientTransaction.Put(lnOperID, lnChannelID, inPackageID, 0, lnStatusID, 'VERIFYSUFFSIGN');
  ReportClientTransaction.Put(aOperId     => lnOperID,
                              aChannelID  => lnChannelID,
                              aPackageId  => inPackageID,
                              aSeqNum     => 0,
                              aStatusId   => lnStatusID,
                              aOriginator => 'VERIFYSUFFSIGN',
--                              aRelatedData VARCHAR2:=NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0,aBankRef VARCHAR2 := NULL, aBankEnsurePackageReport VARCHAR2 DEFAULT NULL,
--                              aCalculateAmount  NUMBER DEFAULT 1,aChangePackageStatus  NUMBER DEFAULT 1,
                              inReport    => inReport);
  ReportClientTransaction.Done;
  if inOpenError=1 then im_error.geterrors(orErrors); end if;
end VerifySufficiencySigning;
--
procedure getSgnStatusForRuleInPackage( -- vraci seznam pozadovanych podpisu users podle pravidla a ID ulozenych podpisu
    inUserID in  NUMBER,
    inChannelID   in  NUMBER,
    inPackageID   in  NUMBER,
    orSignStatus  out SYS_REFCURSOR
)
is
  lnAccountID         ClientTransactionPackages.AccountID%type;
  lnOperID            ClientTransactionPackages.OperID%type;
  lnOrderImportant    ClientSignRules.OrderImportant%type;
  lnSignRuleSeq       ClientTransactionPackages.SignRuleSeq%type;
  lnClientId          ClientSignRules.ClientId%type;
begin
  select OrderImportant,AccountID,OperID,SignRuleSeq,CSR.clientid
    into lnOrderImportant,lnAccountID,lnOperID,lnSignRuleSeq,lnClientId
    from ClientSignRules CSR, ClientTransactionPackages CTP
   where CSR.ClientSignRuleSeq=CTP.SignRuleSeq
     and PackageID=inPackageID;
  if Grants(inChannelID, lnClientId, inUserID, 1, lnOperID, lnAccountID) = 1 then
    open orSignStatus for
      select inPackageID PackageID,CSRD.UserID,decode(lnOrderImportant,1,UserOrder,0,0) UserOrder,
             SA.SignatureSeq, SA.MethodID, SA.CertSerialNumber DeviceID, SA.SignRuleRoleID
        from ClientSignRulesDef CSRD,
             SignatureArchive SA,
             (select UserID,max(SignatureSeq) SignatureSeq FROM SignatureArchive
               where PackageId = inPackageId and ContentType='2' and CertifSig=1 group by UserID) MSA
       where CSRD.ClientSignRuleSeq = lnSignRuleSeq
         and CSRD.UserID=MSA.UserID(+)
         and MSA.SignatureSeq=SA.SignatureSeq(+);
  else
    open orSignStatus for
      select 0 PackageID, 0 UserID, 0 UserOrder, 0 SignatureSeq, 0 MethodID, 0 DeviceID, 0 SignRuleRoleID
        from dual
       where 1=2;
  end if;
exception
  when no_data_found then -- neprirazeno pravidlo
    open orSignStatus for
      select 0 PackageID, 0 UserID, 0 UserOrder, 0 SignatureSeq, 0 MethodID, 0 DeviceID, 0 SignRuleRoleID
        from dual
       where 1=2;
end getSgnStatusForRuleInPackage;
--------------------------------------------------------------------
procedure getSgnStatusForUsersInPackage(-- vraci seznam podpisu package a pripadne poradi z definovaneho pravidla pro package
    inUserID in  NUMBER,
    inChannelID   in  NUMBER,
    inPackageID   in  NUMBER,
    orSignStatus  out SYS_REFCURSOR
)
is
  lnAccountID         ClientTransactionPackages.AccountID%type;
  lnOperID            ClientTransactionPackages.OperID%type;
  lnOrderImportant    ClientSignRules.OrderImportant%type;
  lnSignRuleSeq       ClientTransactionPackages.SignRuleSeq%type;
  lnClientId          ClientSignRules.ClientId%type;
  lnCount             number;
begin
  select OrderImportant,AccountID,OperID,SignRuleSeq,CSR.ClientId
    into lnOrderImportant,lnAccountID,lnOperID,lnSignRuleSeq,lnClientId
    from ClientSignRules CSR,
         ClientTransactionPackages CTP
   where CSR.ClientSignRuleSeq(+)=CTP.SignRuleSeq
     and PackageID=inPackageID;
  if Grants(inChannelID, lnClientId, inUserID, 1, lnOperID, lnAccountID) = 1 then
    if lnSignRuleSeq is not null then -- pravidlo prirazeno, ale muze byt prazdne
      select count(*)
        into lnCount
        from ClientSignRulesDef
       where ClientSignRuleSeq=lnSignRuleSeq; -- 0=pravidlo prazdne=>UserOrder=null
      open orSignStatus for
        select inPackageID PackageID,SA.UserID,decode(lnOrderImportant,1,UserOrder,0,decode(lnCount,0,null,0)) UserOrder,
               SA.SignatureSeq, SA.MethodID, SA.CertSerialNumber DeviceID, SA.SignRuleRoleID
          from ClientSignRulesDef CSRD,
               SignatureArchive SA,
               (select UserID,max(SignatureSeq) SignatureSeq FROM SignatureArchive
                 where PackageId = inPackageId and ContentType='2' and CertifSig=1 group by UserID) MSA
         where CSRD.UserID(+)=MSA.UserID
           and CSRD.ClientSignRuleSeq(+)=lnSignRuleSeq
           and MSA.SignatureSeq=SA.SignatureSeq;
    else -- existuji podpisy pro package bez pravidla
      open orSignStatus for
        select SA.PackageID,SA.UserID,null UserOrder,SA.SignatureSeq,SA.MethodID,SA.CertSerialNumber DeviceID, SA.SignRuleRoleID
          from SignatureArchive SA,
               (select max(SignatureSeq) SignatureSeq,PackageID,UserID from SignatureArchive
                 where PackageId = inPackageId and ContentType='2' and CertifSig=1
                 group by PackageID,UserID) MSA
         where SA.SignatureSeq=MSA.SignatureSeq;
    end if;
  else
    open orSignStatus for
      select 0 PackageID, 0 UserID, 0 UserOrder, 0 SignatureSeq, 0 MethodID, 0 DeviceID, 0 SignRuleRoleID
        from dual
       where 1=2;
  end if;
exception
  when no_data_found then
    im_error.AddError(-20102,'Entity does not exist','getSgnStatusForUsersInPackage:PackageID',inPackageID);
end getSgnStatusForUsersInPackage;
--
procedure getSgnStatus( -- vraci podpis podle PK
    inPackageID   in  NUMBER,
    inUserID      in  NUMBER,
    orSignature   out SYS_REFCURSOR
)
is
  lnOrderImportant    ClientSignRules.OrderImportant%type;
  lnSignRuleSeq       ClientTransactionPackages.SignRuleSeq%type;
  lnCount             number;
begin
  select OrderImportant,SignRuleSeq into lnOrderImportant,lnSignRuleSeq
    from ClientSignRules CSR,
         ClientTransactionPackages CTP
   where CSR.ClientSignRuleSeq(+)=CTP.SignRuleSeq
     and PackageID=inPackageID;
  if lnSignRuleSeq is not null then -- pravidlo prirazeno, ale muze byt prazdne
    select count(*)
      into lnCount
      from ClientSignRulesDef
     where ClientSignRuleSeq=lnSignRuleSeq; -- 0=pravidlo prazdne=>UserOrder=null
    open orSignature for
    -- existuje podpis
      select inPackageID PackageID,inUserID UserID,
             decode(lnOrderImportant,1,UserOrder,0,decode(lnCount,0,null,0)) UserOrder,
             SA.SignatureSeq,SA.MethodID,SA.CertSerialNumber DeviceID, SA.SignRuleRoleID
        from ClientSignRulesDef CSRD,
             SignatureArchive SA,
             (select UserID,max(SignatureSeq) SignatureSeq FROM SignatureArchive
               where PackageId = inPackageId and ContentType='2' and CertifSig=1 and UserID=inUserID
               group by UserID) MSA
       where MSA.SignatureSeq=SA.SignatureSeq
         and CSRD.UserID(+)=MSA.UserID
         and CSRD.ClientSignRuleSeq(+) = lnSignRuleSeq
      UNION
    -- existuje pravidlo
      select inPackageID PackageID,inUserID UserID,
             decode(lnOrderImportant,1,UserOrder,0,decode(lnCount,0,null,0)) UserOrder,
             SA.SignatureSeq,SA.MethodID,SA.CertSerialNumber DeviceID, SA.SignRuleRoleID
        from ClientSignRulesDef CSRD,
             SignatureArchive SA,
             (select UserID,max(SignatureSeq) SignatureSeq FROM SignatureArchive
               where PackageId = inPackageId and ContentType='2' and CertifSig=1 and UserID=inUserID
               group by UserID) MSA
       where CSRD.UserID=inUserID
         and CSRD.ClientSignRuleSeq = lnSignRuleSeq
         and CSRD.UserID=MSA.UserID(+)
         and MSA.SignatureSeq=SA.SignatureSeq(+);
  else -- mozna existuji podpisy pro package bez pravidla
    open orSignature for
      select inPackageID PackageID,inUserID UserID,null UserOrder,
             SA.SignatureSeq,SA.MethodID,SA.CertSerialNumber DeviceID, SA.SignRuleRoleID
        from SignatureArchive SA,
             (select max(SignatureSeq) SignatureSeq from SignatureArchive
               where PackageId = inPackageId and ContentType='2' and CertifSig=1 and UserID=inUserID) MSA
       where SA.SignatureSeq=MSA.SignatureSeq;
  end if;
exception
  when no_data_found then
    im_error.AddError(-20102,'Entity does not exist','getSgnStatus:PackageID',inPackageID);
end getSgnStatus;
--------------------------------------------------------------------
procedure getAccountsForSignRule( -- vraci ucty spojene s podpisovym pravidlem
    inUserID in  NUMBER,            -- uzivatel, optional
    inClientID    in  NUMBER,       -- klient, nepouziva se, NULL
    inChannelID   in  NUMBER,       -- kanal, optional
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    orAccounts    out SYS_REFCURSOR
)
is
begin
  open orAccounts for
    select
           distinct
           A.BRANCHID,A.ACCOUNTID,A.ACCTYPEID,A.CURRENCYID,A.NAME,A.ACCNUM,A.BISACCOUNTID,
           A.BISBRANCHID,A.CCACCOUNTID,A.SOURCETYPEID,A.PROCESSINGTYPE,A.TAXID,nvl(A.ADJUSTEDBALANCE,0) AdjustedBalance
      from RightTypes rt,
           ClientRights cr,
           ClientUserRightProfiles curp,
           ClientUsers cu,
           ClientAccounts ca,
           RightAssignClientAccounts raca,
           ClientSignRuleAccount csa,
           ClientSignRules CSR,
           Accounts a
     where ca.clientid = csr.clientid
       and ca.clientid = cu.clientid
       and cu.clientusersid = curp.clientusersid
       and rt.ChannelId = inChannelID
       and rt.AccessTypeId>=1
       and rt.RightId = cr.RightId
       and cu.UserId = nvl(inUserID,cu.UserId)
       and ca.AccountId = a.accountid
       and csa.accountaccessid=ca.accountaccessid
       and raca.ClientRightId = cr.ClientRightId
       and raca.RightProfileId = curp.RightProfileId
       and raca.AccountAccessId = ca.AccountAccessId
       and ca.StatusId=1
       and csa.clientsignruleseq=inSignRuleID and csa.clientsignruleseq = csr.clientsignruleseq;
end getAccountsForSignRule;
--------------------------------------------------------------------
procedure getUsersForSignRule(      -- vraci uzivatele spojene s podpisovym pravidlem
    inUserID in  NUMBER,            -- uzivatel, nepouziva se, NULL
    inChannelID   in  NUMBER,       -- kanal, nepouziva se, NULL
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    orUsers       out SYS_REFCURSOR
)
is
begin
  open orUsers for
    select /*+ INDEX (CSR idx_clientsignrules_statuscli) */ U.userid, U.statusid, U.borndate, U.langid, U.title, U.position,
       U.firstname, U.lastname, U.personalnum, U.identitycardnum,
       U.contactphone, U.contactfax, U.contactgsm, U.contactemail,
       U.address1, U.address2, U.address3, U.address4, U.address5,
       U.address6, U.serviceunblockkey, U.externalcauserid,
       U.ppd_restrict, U.ppd_block, U.ppd_wipe, U.user_options,
       U.middlename, U.xmlext, U.dmtimestamp, U.usertype, U.hostuserid,
       null lastlogintime, U.entitytimestamp, U.birthdate, CSRD.signruleroleid, CSRD.userorder, CSRD.requiredsignaturescount,
       SRR.role, SRR.description, SRR.systemflag
      from ClientUsers CU, Users U, clientusersignrulerole cusrr,
           ClientSignRulesDef CSRD,
           ClientSignRules CSR,
           signruleroles SRR
     where (U.UserID=CSRD.UserID or CSRD.signruleroleid = CUsrr.signruleroleid)
       and CSRD.signruleroleid = SRR.signruleroleid  (+)
       and CU.clientusersid = cusrr.clientusersid (+)
       and CSR.clientsignruleseq=inSignRuleID
       and CSRD.clientsignruleseq=CSR.clientsignruleseq
       and CSR.ClientID = CU.ClientID and U.UserID = CU.UserID and CU.StatusID = 1
  order by CSRD.UserOrder,U.LastName;
end getUsersForSignRule;
--------------------------------------------------------------------
procedure getOperationsForSignRule( -- vraci operace spojene s podpisovym pravidlem
    inUserID in  NUMBER,
    inClientID    in  NUMBER,
    inChannelID   in  NUMBER,
    inSignRuleID  in  NUMBER,
    ivLangID      in  VARCHAR2,
    orOperations  out SYS_REFCURSOR
)
is
begin
  open orOperations for
    select
           distinct
           ot.OperID,gettext(ot.textid,ivLangID) text,ot.groupid,ot.batch,ot.structcode, nvl(otc.signrulesreq,ot.signrulesreq) signrulesreq, nvl(otc.certifreq,ot.certifreq) certifreq
      from RightTypes rt,
           ClientRights cr,
           ClientUserRightProfiles curp,
           ClientUsers cu,
           RightProfileDefinitions rpd,
           ClientSignRuleOper cso,
           ClientSignRules CSR,
           OperationTypes ot,
           OperationTypeChannels otc
     where cr.clientid = csr.ClientID
       and cu.clientusersid = curp.clientusersid
       and cu.clientid = cr.clientid
       and rt.ChannelId = inChannelID
       and rt.AccessTypeId>=1
       and rt.RightId = cr.RightId
       and cu.UserId = nvl(inUserID,cu.UserId)
       and rpd.ClientRightId = cr.ClientRightId
       and rpd.RightProfileId = curp.RightProfileId
       and rt.operid=cso.operid
       and cso.operid=ot.operid
       and ot.operid = otc.operid (+) and nvl(inChannelID,0)=otc.channelid (+)
       and cso.clientsignruleseq=inSignRuleID
       and cso.clientsignruleseq = CSR.clientsignruleseq;
end getOperationsForSignRule;
--------------------------------------------------------------------
procedure getAvailableLimits(
    inSignRuleSeq in  Number,
    onDayLimit    out Number,
    onMonthLimit  out Number,
    onYearLimit   out Number
)
is
  lnSumTotal            ClientTransactionPackages.totalamount%type;
  ldFrom                date;
  ldTo                  date;
  lvAmountLimitCurrID   Varchar2(3);
begin
  select AmountLimitCurrID into lvAmountLimitCurrID from ClientSignRules
   where ClientSignRuleSeq=inSignRuleSeq;
  for lim in (select AmountLimit,Period from clientsignrulelimits
               where ClientSignRuleSeq=inSignRuleSeq
             ) loop
    lnSumTotal := GetSumTotal(inSignRuleSeq,lim.Period,lvAmountLimitCurrID);
    if floor(lim.Period/100)=1 then -- na den
      onDayLimit:= lim.AmountLimit - lnSumTotal;
    elsif floor(lim.Period/100)=2 then -- na tyden
      onMonthLimit:= lim.AmountLimit - lnSumTotal;
    elsif floor(lim.Period/100)=3 then -- na mesic
      onYearLimit:= lim.AmountLimit - lnSumTotal;
    end if;
  end loop;
end getAvailableLimits;
--------------------------------------------------------------------
procedure AutoGenerateSignRules(           -- automatic generation of signing rules for client - deletes old rules !
    inClientID    in  ClientSignRules.clientid%type
) IS
 -- in case of error, exception is thrown
   ldValidUntil date;
BEGIN
  -- invalidate all old signature rules for client
  for rec in (select clientsignruleseq,validuntil
                from clientsignrules
               where clientid = inClientID
                 and statusid = 1
                 and sysdate between validfrom and nvl(validuntil,sysdate+1) ) loop
    if nvl(rec.validuntil,sysdate+1) > sysdate then
      ldValidUntil:=sysdate;
    else
      ldValidUntil:=rec.validuntil;
    end if;
    update clientsignrules
        set validuntil=ldValidUntil,
            statusid=0
      where clientsignruleseq=rec.clientsignruleseq;
  end loop;
  -- loop through client's active users and add rules for them
  for rec in (select userid from clientusers where clientid = inClientID and statusid = 1) loop
    -- AutoAddSignRulesForUser(rec.userid);
    null;
  end loop;
END;
--------------------------------------------------------------------
function getCertSgnStatusForUser( -- vracia 0 alebo 1 podla toho ci uzivatel prilozil k package cert.podpis
    inPackageID   in  NUMBER,
    inUserID      in  NUMBER
) return number is
  lnReturn pls_integer:=0;
BEGIN
  select decode(count(*),0,0,1)
    into lnReturn
    FROM SignatureArchive
   where PackageId = inPackageId
     and ContentType='2'
     and CertifSig=1
     and UserID=inUserID; -- 24.11.2014, MIL: podmienka je, ze sa plni UserID aj v pripade ked UserID vystupuje v podpisovej roli
  return lnReturn;
END getCertSgnStatusForUser;
--------------------------------------------------------------------
--
-- Client-User sign rule roles
--
Procedure PutSDClientSignRuleRole(itClientSignRuleRole IN OUT NOCOPY t_rec_ClientSignRuleRole)
is
  lnSignRuleRoleId  clientusersignrulerole.SignruleRoleId%TYPE;
  lnTextId          SignRuleRoles.textId%TYPE;
  lnClientId        SignRuleRoles.ClientID%TYPE;
begin
  if itClientSignRuleRole.Role is null Or itClientSignRuleRole.Description is null then
    raise_application_error(-20110,'Role And/Or Description must be not empty');
  else
    begin
      select c.ClientId
        into lnClientId
        from clients c
       where c.ClientId = itClientSignRuleRole.ClientId;
    exception
      when no_data_found then
        raise_application_error(-20138,'Client not found [ClientId='||to_char(itClientSignRuleRole.ClientId)||']');
    end;
    -- novy zaznam
    if itClientSignRuleRole.SignRuleRoleId is null then
      begin
        insert into SignRuleRoles (SignRuleRoleId, ClientId, Role, SystemFlag, Description)
                           values (seq_signruleroles.nextval, itClientSignRuleRole.ClientId, upper(itClientSignRuleRole.Role), 0, itClientSignRuleRole.Description)
        returning SignRuleRoleId, Role into itClientSignRuleRole.SignRuleRoleId, itClientSignRuleRole.Role;
      exception
        when dup_val_on_index then
          raise_application_error(-20137,'Sign rule role name should be unique within client [SignRuleRoleId='||upper(itClientSignRuleRole.Role)||']');
      end;
    else
      -- update
      update SignRuleRoles
         set ClientId = NVL(itClientSignRuleRole.ClientId, ClientId),
             Role = NVL(upper(itClientSignRuleRole.Role), Role),
             Description = NVL(itClientSignRuleRole.Description, Description)
       where SignRuleRoleId = itClientSignRuleRole.SignRuleRoleId;
      if sql%rowcount = 0 then
        raise_application_error(-20136,'Sign rule role not found [SignRuleRoleId='||to_char(itClientSignRuleRole.SignRuleRoleId)||']');
      end if;
    end if;
  end if;
end PutSDClientSignRuleRole;
--
--------------------------------------------------------------------
procedure GetValidSignRules (  -- vraci vsechna podpisova pravidla pouzitelna pro kombinaci vstupnich parametru
    inChannelID           in  clientsignrulechannel.channelid%TYPE,  -- kanal pozadovany v pravidlu, mandatory
    ivHostUserID          in  Users.HostUserID%type,                 -- Prihlaseny User (PartyID), optional
    ivHostClientID        in  ClientHostAllocation.HostClientID%type,-- klient pozadovany v pravidlu, optional
    inOperID              in  clientsignruleoper.operid%type,        -- pozadovana operace v pravidlu, optional
    ivAccountNumberPrefix in  varchar2,                              -- prefix uctu pozadovaneho v pravidlu, optional
    ivAccountNumber       in  varchar2,                              -- cislo uctu pozadovaneho v pravidlu, optional
    ivBankCode            in  varchar2,                              -- banka uctu pozadovaneho v pravidlu, optional
    inAmount              in  number,                                -- castka operace pro kontrolu limitu v pravidlu, optional
    ivCurrencyId          in  varchar2,                              -- mena castky pro kontrolu limitu v pravidlu, optional
    orVSRtab              out sys_refcursor                          -- seznam pravidel vybranych podle podminek
) IS
  lnMakesDebit          Pls_Integer := 1;
  lnAccSensitive        Pls_Integer := 1;
  ltSignRuleSeq         dbms_sql.number_table;
  lvListSeq             varchar2(32767);
  lnChangeLimAmount     ClientTransactionPackages.TotalAmount%type;
  lnSumTotal            ClientTransactionPackages.TotalAmount%type;
  lnCntErrorLimit       Pls_Integer;
  j                     Number;
  lvBankCode            varchar2(20):=reftab_gsp('LOCALBANKCODE',0,'5500');
  lvAccountNumber       varchar2(100):=b24_format_accnum(ivAccountNumberPrefix||'-'||ivAccountNumber)||'/'||nvl(ivBankCode,reftab_gsp('LOCALBANKCODE',0,'5500'));
  lvAccountId           ACCOUNTS.ACCOUNTID%TYPE;
  TYPE cur_typ IS REF CURSOR;
  c cur_typ;
  v_query               VARCHAR2(32767);
  i_ClientSignRuleSeq   CLIENTSIGNRULES.CLIENTSIGNRULESEQ%TYPE;
  i_AmountLimitCurrID     CLIENTSIGNRULES.AMOUNTLIMITCURRID%TYPE;
  i_AmountLimit           CLIENTSIGNRULES.AMOUNTLIMIT%TYPE;
  i_minimalsignatures     CLIENTSIGNRULES.MINIMALSIGNATURES%TYPE;
  i_ClientID              CLIENTSIGNRULES.CLIENTID%TYPE;
begin
  if inOperID is not null then
    select nvl(MakesDebit,0),nvl(reftab_ot.AccountSensitive(inOperID,1),1) into lnMakesDebit,lnAccSensitive from operationTypes where operid = inOperID;
  end if;
  if ivAccountNumber is null or lnAccSensitive=0 then
    for i in (select CSR.ClientSignRuleSeq, CSR.AmountLimitCurrID, CSR.AmountLimit, CSR.minimalsignatures, CHA.ClientID
          from ClientSignRules CSR, Clients C, ClientHostAllocation CHA
         where CHA.ClientID = CSR.CLientID and CHA.ClientID = C.ClientID and CHA.hostclientid = nvl(ivHostClientID,CHA.hostclientid)
           and exists (select 1 from ClientSignRulesDef CSRD, Users U, ClientUsers CU, clientusersignrulerole CUSRR
                   where CSRD.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CU.ClientID = CSR.ClientID and CU.Userid = U.UserID and U.HostUserID = nvl(ivHostUserID,U.HostUserID)
                        and CU.clientusersid = CUSRR.clientusersid (+)
                        and (CSRD.UserId = CU.UserId or CSRD.signruleroleid = CUSRR.signruleroleid)
               )
           and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
           and nvl(CSR.statusID,1) = 1
           and exists (select 1 from ClientSignRuleOper CSRO where CSRO.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CSRO.OperID = nvl(inOperID,CSRO.OperID))
           and exists (select 1 from ClientSignRuleChannel CSRC where CSRC.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CSRC.ChannelID = nvl(inChannelID,CSRC.ChannelID))
         order by CSR.ClientSignRuleSeq) loop
      ltSignRuleSeq(i.ClientSignRuleSeq):=i.ClientSignRuleSeq;
      if lnMakesDebit = 1 and inAmount is not null and ivCurrencyId is not null then -- kontrola jednorazovych a obdobovych limitu
       IF i.AmountLimit is not null then -- CPA 4.5.2015, AmountLimit can be null - 20009183-2477
        IF ivCurrencyId <> i.AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
          lnChangeLimAmount:=GetChangedAmount(i.AmountLimit,i.AmountLimitCurrID,ivCurrencyId,0);
        ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
          lnChangeLimAmount:=i.AmountLimit;
        END IF;
        if lnChangeLimAmount < inAmount then -- prepoctena castka jednorazoveho limitu je mensi nez castka z package
          ltSignRuleSeq.delete(i.ClientSignRuleSeq);
          goto NEXT_FETCH;
        end if;
       END IF;
       IF ivCurrencyId <> i.AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
          select count(*) into lnCntErrorLimit from clientsignrulelimits
           where ClientSignRuleSeq=i.ClientSignRuleSeq
             and GetChangedAmount(AmountLimit,i.AmountLimitCurrID,ivCurrencyId,0)<inAmount;
       ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
          select count(*) into lnCntErrorLimit from clientsignrulelimits
           where ClientSignRuleSeq=i.ClientSignRuleSeq
             and AmountLimit<inAmount;
       END IF;
       if lnCntErrorLimit > 0 then -- alespon jeden limit pravidla je mensi nez zadana castka
          ltSignRuleSeq.delete(i.ClientSignRuleSeq);
          goto NEXT_FETCH;
       end if;
       for lim in (select AmountLimit,Period  from clientsignrulelimits where ClientSignRuleSeq=i.ClientSignRuleSeq
                   ) loop
          lnSumTotal := GetSumTotal(i.ClientSignRuleSeq,lim.Period,ivCurrencyId,null);
          IF ivCurrencyId <> i.AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
            lnChangelimAmount:=GetChangedAmount(lim.AmountLimit,i.AmountLimitCurrID,ivCurrencyId,0);
          ELSE  -- mena uctu a pravidla je stejna, lnAmount je jiz prepoctena castka v mene uctu => zadny prepocet
            lnChangeLimAmount:=lim.AmountLimit;
          END IF;
          if lnSumTotal + inAmount > lnChangeLimAmount THEN -- suma castek je vetsi nez prepocteny limit
            ltSignRuleSeq.delete(i.ClientSignRuleSeq);
            exit;
          end if;
       end loop;
      end if;
      <<NEXT_FETCH>>
      null;
    end loop;
  else
    IF nvl(ivBankCode,lvBankCode) = lvBankCode THEN
       SELECT AccountId INTO lvAccountId FROM Accounts A WHERE A.Accnum = lvAccountNumber;
    ELSE
       lvAccountId:=0;
    END IF;
    IF ivAccountNumber IS NOT NULL AND inOperID IS NOT NULL AND ivHostClientID IS NULL AND ivHostUserID IS NULL AND inChannelID IS NOT NULL THEN
     v_query := 'select /*+ORDERED FIRST_ROWS*/ distinct CSR.ClientSignRuleSeq, CSR.AmountLimitCurrID, CSR.AmountLimit, CSR.minimalsignatures, CSR.ClientID
          from ClientAccounts CA /*:ivHostClientID*/
          JOIN ClientSignRuleAccount CSRA ON CSRA.AccountAccessId = CA.AccountAccessID AND NVL(1,:ivHostClientID) = 1 AND NVL(1,:ivHostUserID) = 1
          JOIN ClientSignRules CSR ON CSRA.ClientSignRuleSeq=CSR.ClientSignRuleSeq AND CA.ClientID = CSR.ClientID
          JOIN ClientSignRuleOper CSRO ON CSRO.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CSRO.OperID = :inOperID
          JOIN ClientSignRuleChannel CSRC ON CSRC.ChannelID = :inChannelID AND CSRC.ClientSignRuleSeq=CSR.ClientSignRuleSeq
          JOIN ClientSignRulesDef CSRD ON CSRD.ClientSignRuleSeq=CSR.ClientSignRuleSeq
          JOIN ClientUsers CU ON CU.ClientID = CSR.ClientID and (CSRD.UserId = CU.UserId or CSRD.signruleroleid in (SELECT CUSRR.signruleroleid from ClientUserSignRuleRole CUSRR where CUSRR.clientusersid=CU.clientusersid))
         where CA.AccountID = :lvAccountId
           and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
           and nvl(CSR.statusID,1) = 1
         order by CSR.ClientSignRuleSeq';
    ELSE
     v_query := 'select CSR.ClientSignRuleSeq, CSR.AmountLimitCurrID, CSR.AmountLimit, CSR.minimalsignatures, CHA.ClientID
          from ClientSignRules CSR, Clients C, ClientHostAllocation CHA
         where CHA.ClientID = CSR.CLientID and CHA.ClientID = C.ClientID and CHA.hostclientid = nvl(:ivHostClientID,CHA.hostclientid)
           and exists (select 1 from ClientSignRulesDef CSRD, Users U, ClientUsers CU
                   where CSRD.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CU.ClientID = CSR.ClientID and CU.Userid = U.UserID and U.HostUserID = nvl(:ivHostUserID,U.HostUserID)
                        and (CSRD.UserId = CU.UserId or CSRD.signruleroleid in (SELECT CUSRR.signruleroleid from ClientUserSignRuleRole CUSRR where CUSRR.clientusersid=CU.clientusersid))
               )
           and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
           and nvl(CSR.statusID,1) = 1
           and exists (select 1 from ClientSignRuleOper CSRO where CSRO.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CSRO.OperID = nvl(:inOperID,CSRO.OperID))
           and exists (select 1 from ClientSignRuleChannel CSRC where CSRC.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CSRC.ChannelID = nvl(:inChannelID,CSRC.ChannelID))
           and exists (select 1 from ClientSignRuleAccount CSRA, ClientAccounts CA
                where CSRA.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CSRA.AccountAccessId = CA.AccountAccessID and CA.ClientID = CSR.ClientID and CA.AccountID = :lvAccountId)
         order by CSR.ClientSignRuleSeq';
    END IF;
    OPEN c FOR v_query USING ivHostClientID,ivHostUserID,inOperID,inChannelID,lvAccountId;
    LOOP
      FETCH c INTO i_ClientSignRuleSeq, i_AmountLimitCurrID, i_AmountLimit, i_minimalsignatures, i_ClientID;
      EXIT WHEN c%NOTFOUND;
      ltSignRuleSeq(i_ClientSignRuleSeq):=i_ClientSignRuleSeq;
      if lnMakesDebit = 1 and inAmount is not null and ivCurrencyId is not null then -- kontrola jednorazovych a obdobovych limitu
       IF i_AmountLimit is not null then -- CPA 4.5.2015, AmountLimit can be null - 20009183-2477
        IF ivCurrencyId <> i_AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
          lnChangeLimAmount:=GetChangedAmount(i_AmountLimit,i_AmountLimitCurrID,ivCurrencyId,0);
        ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
          lnChangeLimAmount:=i_AmountLimit;
        END IF;
        if lnChangeLimAmount < inAmount then -- prepoctena castka jednorazoveho limitu je mensi nez castka z package
          ltSignRuleSeq.delete(i_ClientSignRuleSeq);
          goto NEXT_FETCH2;
        end if;
       END IF;
       IF ivCurrencyId <> i_AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
          select count(*) into lnCntErrorLimit from clientsignrulelimits
           where ClientSignRuleSeq=i_ClientSignRuleSeq
             and GetChangedAmount(AmountLimit,i_AmountLimitCurrID,ivCurrencyId,0)<inAmount;
       ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
          select count(*) into lnCntErrorLimit from clientsignrulelimits
           where ClientSignRuleSeq=i_ClientSignRuleSeq
             and AmountLimit<inAmount;
       END IF;
       if lnCntErrorLimit > 0 then -- alespon jeden limit pravidla je mensi nez zadana castka
          ltSignRuleSeq.delete(i_ClientSignRuleSeq);
          goto NEXT_FETCH2;
       end if;
       for lim in (select AmountLimit,Period  from clientsignrulelimits where ClientSignRuleSeq=i_ClientSignRuleSeq
                   ) loop
          lnSumTotal := GetSumTotal(i_ClientSignRuleSeq,lim.Period,ivCurrencyId,null);
          IF ivCurrencyId <> i_AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
            lnChangelimAmount:=GetChangedAmount(lim.AmountLimit,i_AmountLimitCurrID,ivCurrencyId,0);
          ELSE  -- mena uctu a pravidla je stejna, lnAmount je jiz prepoctena castka v mene uctu => zadny prepocet
            lnChangeLimAmount:=lim.AmountLimit;
          END IF;
          if lnSumTotal + inAmount > lnChangeLimAmount THEN -- suma castek je vetsi nez prepocteny limit
            ltSignRuleSeq.delete(i_ClientSignRuleSeq);
            exit;
          end if;
       end loop;
      end if;
      <<NEXT_FETCH2>>
      null;
    END LOOP;
  end if;
  if ltSignRuleSeq.count>0 then
    lvListSeq:='in (';
    j:=ltSignRuleSeq.first;
    while j is not null loop
      lvListSeq:=lvListSeq||ltSignRuleSeq(j)||',';
      j:=ltSignRuleSeq.next(j);
    end loop;
    lvListSeq:=rtrim(lvListSeq,',')||')';
  else
    lvListSeq:='= -1';  -- nesplnitelna podminka
  end if;
  open orVSRtab for
      'select
             CSR.ClientSignRuleSeq SignRuleID,
             CSR.RuleDescription,
             CSR.AmountLimitCurrID OneTimeLimitCurrencyID,
             CSR.AmountLimit       OneTimeLimitAmount,
             CSR.StatusID Status,
             CSR.OrderImportant,
             CSR.minimalsignatures,
             C.Description ClientDescription,
             CHA.ClientID,
             CHA.HostClientID
        from ClientSignRules CSR, Clients C, ClientHostAllocation CHA
       where CHA.ClientID = CSR.CLientID and CHA.ClientID = C.ClientID and CSR.ClientSignRuleSeq '||lvListSeq;
END GetValidSignRules;
--------------------------------------------------------------------
procedure getLimitsForSignRule( -- vraci obdobove limity spojene s podpisovym pravidlem
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    orLimits      out SYS_REFCURSOR
) is
BEGIN
  open orLimits for
    select CSRL.period, CSRL.amountlimit, CSR.amountlimitcurrid amountlimitcurrencyid
      from ClientSignRuleLimits CSRL,
           ClientSignRules CSR
     where CSRL.clientsignruleseq=inSignRuleID
       and CSRL.clientsignruleseq=CSR.clientsignruleseq
  order by CSRL.period;
END getLimitsForSignRule;
--------------------------------------------------------------------
END IIF_CCFG_005;
/

PROMPT CREATE OR REPLACE PACKAGE IM_ERROR
CREATE OR REPLACE 
PACKAGE IM_ERROR
IS
------------------
-- $release: 078 $
-- $version: 8.0.0.0 $
------------------
-- $Header: /Oracle/UDEBS_XPE/UDEBSXPE.IM_ERROR.PSK 4     6. 11. 12 16:55 Ilenin $
------------------
-- Posledni patch z UDEBS: 2854
--
-- MODIFICATION HISTORY
--
-- Person      Date       Comments
-- ----------- ---------- ---------------------------------------------------------
-- Ilenin      21.07.2014 UDEBS RS
-- --------------------------------------------------------------------------------
--
-- PURPOSE :
-- Internal Module
-- registrovanie chyb pocas spracovania v nejakom pl/sql kode, na zaver vyber vsetkych registrovanych chyb
--
-- DOC :
-- Z:\BSC_Praha\Banky\GEMINI\Docs\rules\Exceptions_Classification.doc
-- Z:\BSC_Praha\Banky\GEMINI\Docs\rules\GeminiExceptions.xls
--
-- Aplikacne chyby, ktore vzniknu v UDEBSe sa zatial neprezentuju standardnym sposobom (koli aplikaciam, ktore by ich nevedeli
-- zatial odchytit), ale vyhadzuju sa uzivatelske exceptions, tak ako je to zvykom v doterajsich interfaces.
-- V buducnosti, ked budu vsetky aplikacie citat nazretovy cursor chyb, tak sa budu most aj aplikacne chyby odovzdavat takisto
-- ako businnes.
--
  --
  -- ref cursor
  TYPE tRefCursor IS REF CURSOR;
  --
  gvDbmsOutput NUMBER  := 0;
  gvDelete     BOOLEAN := true;
  --
  -- typ record, koli jednoduchemu fetchovaniu v ostatnych PL/SQL kodoch, kopiruje objekt ERR_OBJ (do objektu sa neda fetchnut), MUSI typovo sediet s resultsetom GetErrors !
  TYPE tRecError IS RECORD (
      ErrorId   ClientTransactionErrorTypes.ExternalErrorId%TYPE,
      Text      ClientTransactionErrorTypes.Description%TYPE,
      Param0    ClientTransactionErrorTypes.Param0%TYPE,
      Param1    ClientTransactionErrorTypes.Param1%TYPE,
      Param2    ClientTransactionErrorTypes.Param2%TYPE,
      Param3    ClientTransactionErrorTypes.Param3%TYPE,
      Param4    ClientTransactionErrorTypes.Param4%TYPE,
      Param5    ClientTransactionErrorTypes.Param5%TYPE,
      Param6    ClientTransactionErrorTypes.Param6%TYPE,
      Param7    ClientTransactionErrorTypes.Param7%TYPE,
      Param8    ClientTransactionErrorTypes.Param8%TYPE,
      Param9    ClientTransactionErrorTypes.Param9%TYPE,
      OccurDate DATE);
  --
  -- typy chyb (podla dokumentu hore), ktore sa mozu generovat z PL/SQL, konsanty - nasobky 2
  ET_USER_EXC        CONSTANT NUMBER := 1;
  ET_BUSINESS_EXC    CONSTANT NUMBER := 2;
  ET_SUCCESSFUL      CONSTANT NUMBER := 4;
  ET_APPLICATION_EXC CONSTANT NUMBER := 8; -- tieto typy su vyhadzovane ako uzivatelske chyby ORA-20000 .. ORA--20999, konstant je definovana iba pre pripad, ze to niekto zle zavola ...
  -- ET_SYSTEM_EXC -- tieto su vyhadzovane ako ORA chyby, vsetky okrem aplikacnych
  -- ET_SYSTEM_ERR -- tieto su vyhadzovane ako ORA chyby, vsetky okrem aplikacnych
  --
  -- exception, ktora je vyhodena ak nastane akakolvek z chyb, okrem ET_SUCCESSFUL
  REG_ANY_ERROR EXCEPTION;
  PRAGMA EXCEPTION_INIT(REG_ANY_ERROR,-20000);
  --
  -- zmaze glob. premennu zoznam chyb.
  PROCEDURE Init;
  --
  -- parametre spoji do jedneho textu
  FUNCTION CombineParams (
      ivParam0  IN ClientTransactionErrorTypes.Param0%TYPE DEFAULT NULL,
      ivParam1  IN ClientTransactionErrorTypes.Param1%TYPE DEFAULT NULL,
      ivParam2  IN ClientTransactionErrorTypes.Param2%TYPE DEFAULT NULL,
      ivParam3  IN ClientTransactionErrorTypes.Param3%TYPE DEFAULT NULL,
      ivParam4  IN ClientTransactionErrorTypes.Param4%TYPE DEFAULT NULL,
      ivParam5  IN ClientTransactionErrorTypes.Param5%TYPE DEFAULT NULL,
      ivParam6  IN ClientTransactionErrorTypes.Param6%TYPE DEFAULT NULL,
      ivParam7  IN ClientTransactionErrorTypes.Param7%TYPE DEFAULT NULL,
      ivParam8  IN ClientTransactionErrorTypes.Param8%TYPE DEFAULT NULL,
      ivParam9  IN ClientTransactionErrorTypes.Param9%TYPE DEFAULT NULL)
  RETURN VARCHAR2;
  --
  -- prida chybu, ErrorId moze byt standardne ako je definovane v XLS (vsetky, aj aplikacne), alebo aj -20999..-20000 co su oraclovske aplikacne chyby
  PROCEDURE AddError (
      inErrorId IN ClientTransactionErrorTypes.ExternalErrorId%TYPE,
      ivText    IN ClientTransactionErrorTypes.Description%TYPE DEFAULT NULL,
      ivParam0  IN ClientTransactionErrorTypes.Param0%TYPE DEFAULT NULL,
      ivParam1  IN ClientTransactionErrorTypes.Param1%TYPE DEFAULT NULL,
      ivParam2  IN ClientTransactionErrorTypes.Param2%TYPE DEFAULT NULL,
      ivParam3  IN ClientTransactionErrorTypes.Param3%TYPE DEFAULT NULL,
      ivParam4  IN ClientTransactionErrorTypes.Param4%TYPE DEFAULT NULL,
      ivParam5  IN ClientTransactionErrorTypes.Param5%TYPE DEFAULT NULL,
      ivParam6  IN ClientTransactionErrorTypes.Param6%TYPE DEFAULT NULL,
      ivParam7  IN ClientTransactionErrorTypes.Param7%TYPE DEFAULT NULL,
      ivParam8  IN ClientTransactionErrorTypes.Param8%TYPE DEFAULT NULL,
      ivParam9  IN ClientTransactionErrorTypes.Param9%TYPE DEFAULT NULL,
      inRaiseAnyError IN NUMBER DEFAULT 0); -- ak je registrovana akakolvek chyba vyhodi sa geRegAnyError
  --
  -- vrati ref cursor chyb
  PROCEDURE GetErrors (
      orErrors  IN OUT tRefCursor);
  --
  -- zistenie ci od volania Init a seriou volani AddError vznikla chyba daneho typu
  FUNCTION ExistErrType (
      inErrType IN NUMBER) -- jedna z konstant (alebo viacero zANDovanych z hlavicky tohoto package)
  RETURN NUMBER; -- 0 chyba daneho typu neexistuje, 1 v serii volani sa vyskytla
  --
END IM_ERROR;
/

CREATE OR REPLACE 
PACKAGE BODY IM_ERROR
IS
------------------
-- $release: 078 $
-- $version: 8.0.0.0 $
------------------
-- $Header: /Oracle/UDEBS_XPE/UDEBSXPE.IM_ERROR.PBK 4     6. 11. 12 16:55 Ilenin $
------------------
--
--
-- MODIFICATION HISTORY
--
-- Person      Date       Comments
-- ----------- ---------- ---------------------------------------------------------
-- Ilenin      21.07.2014 UDEBS RS
-- --------------------------------------------------------------------------------
--
  --
  gtErrTab     err_tab; -- zoznam chyb
  gnErrTypes   NUMBER;  -- binarne ANDovane vsetky typy chyb, je to koli tomu, aby sa nemusel prechadzat zoznam na to, aby sa vedelo, ci v zozname je napr BUSSINESS chyba - nemoze sa pokracovat v spracovani dalej ...
  gnActErrType NUMBER;  -- aktualne pridany typ chyby
  gvLangId     SupportedLang.LangId%TYPE := 'en'; -- zatial neesituje procka, ktora by jazyk textu nastavovala, preto bude default 'en'
  --
  -- doplnenie textu (ak je prazdny)
  PROCEDURE GetDefaults (
      inErrorId IN ClientTransactionErrorTypes.ExternalErrorId%TYPE,
      pvText    IN OUT NOCOPY ClientTransactionErrorTypes.Description%TYPE,
      pvParam0  IN OUT NOCOPY ClientTransactionErrorTypes.Param0%TYPE,
      pvParam1  IN OUT NOCOPY ClientTransactionErrorTypes.Param1%TYPE,
      pvParam2  IN OUT NOCOPY ClientTransactionErrorTypes.Param2%TYPE,
      pvParam3  IN OUT NOCOPY ClientTransactionErrorTypes.Param3%TYPE,
      pvParam4  IN OUT NOCOPY ClientTransactionErrorTypes.Param4%TYPE,
      pvParam5  IN OUT NOCOPY ClientTransactionErrorTypes.Param5%TYPE,
      pvParam6  IN OUT NOCOPY ClientTransactionErrorTypes.Param6%TYPE,
      pvParam7  IN OUT NOCOPY ClientTransactionErrorTypes.Param7%TYPE,
      pvParam8  IN OUT NOCOPY ClientTransactionErrorTypes.Param8%TYPE,
      pvParam9  IN OUT NOCOPY ClientTransactionErrorTypes.Param9%TYPE)
      -- tu som umiestnil texty chyb, aby neboli rozhadzane po roznych kodoch, tj. neboli pre tu istu chybu rozne
      -- az sa na to zavedie ciselnik (zatial to je iba v XLS), tak tu tieto texty nebudu ale budu sa v tejto funkcii
      -- vyberat z ciselniku ...
  IS
  BEGIN
    SELECT NVL(pvText,Description), NVL(pvParam0,Param0), NVL(pvParam1,Param1), NVL(pvParam2,Param2), NVL(pvParam3,Param3),
           NVL(pvParam4,Param4), NVL(pvParam5,Param5), NVL(pvParam6,Param6), NVL(pvParam7,Param7), NVL(pvParam8,Param8),
           NVL(pvParam9, NVL(Param9, GetText(TextId,gvLangId)))
      INTO pvText, pvParam0, pvParam1, pvParam2, pvParam3, pvParam4, pvParam5, pvParam6, pvParam7, pvParam8, pvParam9
      FROM ClientTransactionErrorTypes WHERE ExternalErrorId = inErrorId;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
  END GetDefaults;
  --
  -- zmaze glob. premennu zoznam chyb.
  PROCEDURE Init
      -- treba zabezpecit aby bola vzdy zavolana minimalne na zaciatku PIF - tj. na zaciatku volania z nejakej aplikacie
      -- v opacnom pripade by sa mohli stat, ze volanie vrati chybu, ktora v nom nikde nenastala
  IS
  BEGIN
    gnErrTypes := 0;
    IF gtErrTab IS NULL THEN
      gtErrTab := err_tab();
    ELSE
      IF gvDelete THEN
        gtErrTab.delete;
      END IF;
    END IF;
  END Init;
  --
  PROCEDURE AddType (
      inErrorId IN NUMBER)
      -- registruje v glob premennej, ci v priebehu volani vznikla spon jedna chyba daneho typu
      -- podla excelu ma na 16^3 pozicii cislo chyby zakodovany jej typ pre :
      -- ET_USER_EXC : 3, ET_BUSINESS_EXC : 4, ET_SUCCESSFUL : 0, ET_APPLICATION_EXC : 5
      -- ET_SYSTEM_EXC : 6, ET_SYSTEM_ERR : 7
  IS
    x1 RAW(10) := utl_raw.cast_to_raw(chr(gnErrTypes));
    x2 RAW(10);
  BEGIN
    IF inErrorId BETWEEN -20999 AND -20000 THEN
      x2 := utl_raw.cast_to_raw(chr(ET_APPLICATION_EXC));
    ELSE
      -- vyberiem len 4. poziciu zprava, vysledok bude 0..16 ~ 0..F
      SELECT DECODE(16 * (trunc(inErrorId/4096)/16 - trunc(trunc(inErrorId/4096)/16)),
                    3, ET_USER_EXC,
                    4, ET_BUSINESS_EXC,
                    0, ET_SUCCESSFUL,
                    5, ET_APPLICATION_EXC,
                       0)
          INTO gnActErrType FROM dual;
       x2 := utl_raw.cast_to_raw(chr(gnActErrType));
    END IF;
  gnErrTypes := ascii(utl_raw.cast_to_varchar2(utl_raw.bit_or(x1,x2)));
  END AddType;
  --
  -- parametre spoji do jedneho textu
  FUNCTION CombineParams (
      ivParam0  IN ClientTransactionErrorTypes.Param0%TYPE DEFAULT NULL,
      ivParam1  IN ClientTransactionErrorTypes.Param1%TYPE DEFAULT NULL,
      ivParam2  IN ClientTransactionErrorTypes.Param2%TYPE DEFAULT NULL,
      ivParam3  IN ClientTransactionErrorTypes.Param3%TYPE DEFAULT NULL,
      ivParam4  IN ClientTransactionErrorTypes.Param4%TYPE DEFAULT NULL,
      ivParam5  IN ClientTransactionErrorTypes.Param5%TYPE DEFAULT NULL,
      ivParam6  IN ClientTransactionErrorTypes.Param6%TYPE DEFAULT NULL,
      ivParam7  IN ClientTransactionErrorTypes.Param7%TYPE DEFAULT NULL,
      ivParam8  IN ClientTransactionErrorTypes.Param8%TYPE DEFAULT NULL,
      ivParam9  IN ClientTransactionErrorTypes.Param9%TYPE DEFAULT NULL)
  RETURN VARCHAR2
  IS
    lvStr VARCHAR2(1847 CHAR); -- raise_application_error dokaze pouzit 2048, text je obmedzeny na 200 znakov + 1 medzera
  BEGIN
    IF ivParam0 IS NOT NULL THEN lvStr := lvStr||'P0="'||substr(ivParam0,1,174)||'" ';  END IF;
    IF ivParam1 IS NOT NULL THEN lvStr := lvStr||'P1="'||substr(ivParam1,1,174)||'" ';  END IF;
    IF ivParam2 IS NOT NULL THEN lvStr := lvStr||'P2="'||substr(ivParam2,1,174)||'" ';  END IF;
    IF ivParam3 IS NOT NULL THEN lvStr := lvStr||'P3="'||substr(ivParam3,1,174)||'" ';  END IF;
    IF ivParam4 IS NOT NULL THEN lvStr := lvStr||'P4="'||substr(ivParam4,1,174)||'" ';  END IF;
    IF ivParam5 IS NOT NULL THEN lvStr := lvStr||'P5="'||substr(ivParam5,1,174)||'" ';  END IF;
    IF ivParam6 IS NOT NULL THEN lvStr := lvStr||'P6="'||substr(ivParam6,1,174)||'" ';  END IF;
    IF ivParam7 IS NOT NULL THEN lvStr := lvStr||'P7="'||substr(ivParam7,1,174)||'" ';  END IF;
    IF ivParam8 IS NOT NULL THEN lvStr := lvStr||'P8="'||substr(ivParam8,1,174)||'" ';  END IF;
    IF ivParam9 IS NOT NULL THEN lvStr := lvStr||'P9="'||substr(ivParam9,1,174)||'" ';  END IF;
    RETURN lvStr;
  END CombineParams;
  --
  -- prida chybu
  PROCEDURE AddError (
      inErrorId IN ClientTransactionErrorTypes.ExternalErrorId%TYPE,
      ivText    IN ClientTransactionErrorTypes.Description%TYPE DEFAULT NULL,
      ivParam0  IN ClientTransactionErrorTypes.Param0%TYPE DEFAULT NULL,
      ivParam1  IN ClientTransactionErrorTypes.Param1%TYPE DEFAULT NULL,
      ivParam2  IN ClientTransactionErrorTypes.Param2%TYPE DEFAULT NULL,
      ivParam3  IN ClientTransactionErrorTypes.Param3%TYPE DEFAULT NULL,
      ivParam4  IN ClientTransactionErrorTypes.Param4%TYPE DEFAULT NULL,
      ivParam5  IN ClientTransactionErrorTypes.Param5%TYPE DEFAULT NULL,
      ivParam6  IN ClientTransactionErrorTypes.Param6%TYPE DEFAULT NULL,
      ivParam7  IN ClientTransactionErrorTypes.Param7%TYPE DEFAULT NULL,
      ivParam8  IN ClientTransactionErrorTypes.Param8%TYPE DEFAULT NULL,
      ivParam9  IN ClientTransactionErrorTypes.Param9%TYPE DEFAULT NULL,
      inRaiseAnyError IN NUMBER DEFAULT 0) -- ak je registrovana akakolvek chyba vyhodi sa geRegAnyError
      -- registruje vyskytnute chyby do globalnej premennej
      -- posledny parameter ma nasledujuce vyuzitie :
      -- ak nastane chyba typu ineho ako ET_SUCCESSFUL, vyhodi sa chyba REG_ANY_ERROR ORA-20000
      -- je to uzitocne najma v pripade, ze v jednom PL/SQL sa vola viacero procedur a kazda z nich moze
      -- skoncit nejakou chybou, bolo by programatorsky neefektivne za kazdym volanim zistovat ci dana chyba
      -- nastala a nasledne povolit/zakazat dalsie volania, takto staci dat do hlavneho bloku jej odchytenie
      -- a zoznam chyb pre toto elementarne volanie bude aj zoznamom chyb pre cely PL/SQL bolk
  IS
    lvText    ClientTransactionErrorTypes.Description%TYPE := ivText;
    lvParam0  ClientTransactionErrorTypes.Param0%TYPE := ivParam0;
    lvParam1  ClientTransactionErrorTypes.Param1%TYPE := ivParam1;
    lvParam2  ClientTransactionErrorTypes.Param2%TYPE := ivParam2;
    lvParam3  ClientTransactionErrorTypes.Param3%TYPE := ivParam3;
    lvParam4  ClientTransactionErrorTypes.Param4%TYPE := ivParam4;
    lvParam5  ClientTransactionErrorTypes.Param5%TYPE := ivParam5;
    lvParam6  ClientTransactionErrorTypes.Param6%TYPE := ivParam6;
    lvParam7  ClientTransactionErrorTypes.Param7%TYPE := ivParam7;
    lvParam8  ClientTransactionErrorTypes.Param8%TYPE := ivParam8;
    lvParam9  ClientTransactionErrorTypes.Param9%TYPE := ivParam9;
    lnRaised NUMBER;
    i NUMBER;
    FUNCTION CombParams RETURN VARCHAR2 IS
    BEGIN
      RETURN CombineParams(lvParam0,lvParam1,lvParam2,lvParam3,lvParam4,lvParam5,lvParam6,lvParam7,lvParam8,lvParam9);
    END;
  BEGIN
    IF gtErrTab IS NULL THEN
      Init;
    END IF;
    GetDefaults(inErrorId, lvText, lvParam0, lvParam1, lvParam2, lvParam3, lvParam4, lvParam5, lvParam6, lvParam7, lvParam8, lvParam9);
    AddType(inErrorId);
    IF gnActErrType = ET_APPLICATION_EXC OR inErrorId BETWEEN -20999 AND -20000 THEN -- aplikacne chyby vyhadzujeme ako exceptions
      IF inErrorId BETWEEN -20999 AND -20000 THEN
        lnRaised := inErrorId;
      ELSE
        SELECT -mod(inErrorId,4096) - 19999 INTO lnRaised FROM dual;
      END IF;
      IF gvDbmsOutput = 1 THEN
        DBMS_OUTPUT.PUT_LINE('ErrorId='||inErrorId||' Text="'||substr(lvText,1,50)||'" '||substr(CombParams,1,170));
      END IF;
      raise_application_error(lnRaised, substr(lvText,1,200)||' '||CombParams);
    ELSE
      i := NVL(gtErrTab.last,0)+1;
      gtErrTab.extend;
      gtErrTab(i) := err_obj.Init(inErrorId, lvText, lvParam0, lvParam1, lvParam2, lvParam3, lvParam4, lvParam5, lvParam6, lvParam7, lvParam8, lvParam9, sysdate);
      IF gvDbmsOutput = 1  THEN
        DBMS_OUTPUT.PUT_LINE('ErrorId='||inErrorId||' Text="'||substr(lvText,1,50)||' '||substr(CombParams,1,170));
      END IF;
    END IF;
    IF inRaiseAnyError = 1 AND gnActErrType IN (ET_USER_EXC, ET_BUSINESS_EXC, ET_APPLICATION_EXC) THEN
      raise_application_error( -20000, inErrorId||' '||substr(lvText,1,293)||' '||CombParams);
    END IF;
  END AddError;
  --
  -- vrati ref cursor chyb
  PROCEDURE GetErrors (
      orErrors  IN OUT tRefCursor)
  IS
  BEGIN
    IF orErrors%ISOPEN THEN
      CLOSE orErrors;
    END IF;
    -- we'll open cursor only if there are errors
    IF gtErrTab.COUNT > 0 THEN
      OPEN orErrors FOR
          SELECT ErrorId, Text, Param0, Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8, Param9, OccurDate
            FROM the (SELECT CAST(gtErrTab AS err_tab) FROM dual) x;
    END IF;
  END GetErrors;
  --
  -- zisti ci v serii volani existuje dany typ (dane typy) chyb
  FUNCTION ExistErrType (
      inErrType IN NUMBER) -- jedna z konstant (alebo viacero zANDovanych z hlavicky tohoto package)
  RETURN NUMBER -- 0 chyba daneho typu neexistuje, 1 v serii volani sa vyskytla
  IS
    x1 RAW(10) := utl_raw.cast_to_raw(chr(gnErrTypes));
    x2 RAW(10) := utl_raw.cast_to_raw(chr(inErrType));
  BEGIN
    IF ascii(utl_raw.cast_to_varchar2(utl_raw.bit_and(x1,x2))) > 0 THEN
      RETURN 1;
    ELSE
      RETURN 0;
    END IF;
  END ExistErrType;
END IM_ERROR;
/

PROMPT CREATE OR REPLACE PACKAGE JOURNAL001
CREATE OR REPLACE 
PACKAGE JOURNAL001 IS
  ------------------
  -- $release: 5751 $
  -- $version: 8.0.19.0 $
  ------------------
  -- BSC Praha spol. s r.o.
  -- package umoznujuci manipulaciu s tabulkou Journal
  -- MODIFICATION HISTORY
  -- Person         Date        Comments
  -- ------------   ----------  -------------------------------------------
  -- Ilenin         01.07.2014  UDEBS RS
  -- Bocak          10.02.2015  LogEventPSI
  -- Emmer          07.09.2015  Rewrite Journaling
  -- Pataky         13.10.2015  Details parameter type functions overloaded with CLOB versions, Details in table Journal still remains XMLType - 20009203-1354
  -- Emmer          16.11.2015  Log events only into new structures
  -- Emmer          09.12.2015  Remove obsolete interface
  -- Bocak          04.01.2016  Get back procedures
  -- Pataky         28.04.2017  Modifications to support extension journal tables - 20009203-7142
  -- Emmer          07.09.2017  skipJournal: if EventTypes.loglevel = 0 then nothing is journaled for this event. Event -9 is always journaled.
  -- Emmer          28.02.2018  ERROR_LOG_AUTONOMOUS
  --
  TYPE Rseq IS RECORD(
    seq NUMBER,
    RID ROWID);
  TYPE TRseq IS TABLE OF Rseq INDEX BY PLS_INTEGER;
  ATRseq TRseq;
  Lvl PLS_INTEGER;
  ParEvSeq Journal.Seq%TYPE := NULL;
  --
  -- Check whether EventType exists in table EventTypes
  --   return value - EventID, if not found - then NULL
  FUNCTION CheckEvent(p_Event EventTypes.eventid%TYPE) RETURN NUMBER RESULT_CACHE DETERMINISTIC;
  --
  -- If EventTypes.LogLevel = 0 then journaling is completely skipped
  FUNCTION skipJournal(p_Event EventTypes.eventid%TYPE) RETURN NUMBER RESULT_CACHE DETERMINISTIC;
  --
  -- get xml from clob, if clob is null then function returns null
  FUNCTION get_xml_from_clob(p_clob CLOB) RETURN xmltype;
  --
  -- New overloaded function LogEvent returns new primary journal key (Seq: Journal ID) without commit.
  -- Returned value can be used for USERSESSION.ParentSeq
  FUNCTION LogEventClob(p_Event        IN Journal.eventid%TYPE,
                        p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                        p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                        p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                        p_Details      IN CLOB DEFAULT NULL,
                        p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                        p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION LogEvent(p_Event        IN Journal.eventid%TYPE,
                    p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                    p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                    p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                    p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                    p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                    p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  -- New overloaded procedure LogEvent without commit
  PROCEDURE LogEventClob(p_Event        IN Journal.eventid%TYPE,
                         p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                         p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                         p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                         p_Details      IN CLOB DEFAULT NULL,
                         p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                         p_Duration     IN Journal.Duration%TYPE DEFAULT NULL);
  --
  PROCEDURE LogEvent(p_Event        IN Journal.eventid%TYPE,
                     p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                     p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                     p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                     p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                     p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                     p_Duration     IN Journal.Duration%TYPE DEFAULT NULL);
  --
  -- Obsolete procedure
  PROCEDURE LogEventClob(Event         IN Journal.eventid%TYPE,
                         Fea           IN VARCHAR2,
                         Dt1           IN TIMESTAMP := NULL,
                         Dt2           IN TIMESTAMP := NULL,
                         Ds            IN VARCHAR2 := NULL,
                         aOriginatorID IN Journal.originatorid%TYPE := NULL,
                         aChannelID    IN NUMBER := NULL,
                         LDTYPE        IN VARCHAR2 := NULL,
                         LDID          IN VARCHAR2 := NULL,
                         aClientID     IN Journal.clientid%TYPE := NULL,
                         aExtension    IN CLOB DEFAULT NULL,
                         aUserID       IN Journal.UserID%TYPE DEFAULT NULL,
                         aDetails      IN CLOB DEFAULT NULL,
                         aDuration     IN Journal.Duration%TYPE DEFAULT NULL);
  -- Obsolete procedure
  PROCEDURE LogEvent(Event         IN Journal.eventid%TYPE,
                     Fea           IN VARCHAR2,
                     Dt1           IN TIMESTAMP := NULL,
                     Dt2           IN TIMESTAMP := NULL,
                     Ds            IN VARCHAR2 := NULL,
                     aOriginatorID IN Journal.originatorid%TYPE := NULL,
                     aChannelID    IN NUMBER := NULL,
                     LDTYPE        IN VARCHAR2 := NULL,
                     LDID          IN VARCHAR2 := NULL,
                     aClientID     IN Journal.clientid%TYPE := NULL,
                     aExtension    IN CLOB DEFAULT NULL,
                     aUserID       IN Journal.UserID%TYPE DEFAULT NULL,
                     aDetails      IN Journal.Details%TYPE DEFAULT NULL,
                     aDuration     IN Journal.Duration%TYPE DEFAULT NULL);
  -- New overloaded LogChildEvent without commit
  PROCEDURE LogChildEventClob(p_Event        IN Journal.eventid%TYPE,
                              p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                              p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                              p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                              p_Details      IN CLOB DEFAULT NULL,
                              p_Duration     IN Journal.Duration%TYPE DEFAULT NULL);
  --
  PROCEDURE LogChildEvent(p_Event        IN Journal.eventid%TYPE,
                          p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                          p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                          p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                          p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                          p_Duration     IN Journal.Duration%TYPE DEFAULT NULL);
  --
  -- Obsolete procedure
  PROCEDURE LogChildEvent(Event         IN Journal.eventid%TYPE,
                          Fea           IN VARCHAR2,
                          Dt1           IN TIMESTAMP := NULL,
                          Dt2           IN TIMESTAMP := NULL,
                          Ds            IN VARCHAR2 := NULL,
                          aOriginatorID IN Journal.originatorid%TYPE := NULL,
                          aChannelID    IN NUMBER := NULL,
                          LDTYPE        IN VARCHAR2 := NULL,
                          LDID          IN VARCHAR2 := NULL,
                          aClientID     IN Journal.clientid%TYPE := NULL,
                          aExtension    IN CLOB DEFAULT NULL,
                          aUserID       IN Journal.UserID%TYPE DEFAULT NULL);
  --
  PROCEDURE ToParent;
  --
  PROCEDURE ToChild;
  --
  -- Obsolete procedure
  PROCEDURE PutStatus(St IN NUMBER);
  --
  -- Obsolete function
  FUNCTION GetStatus RETURN NUMBER;
  --
  -- Obsolete procedure
  PROCEDURE PutProgress(St IN NUMBER);
  --
  -- Obsolete function
  FUNCTION GetProgress RETURN NUMBER;
  --
  -- Obsolete procedure
  PROCEDURE PutEndDate(Dt2 IN TIMESTAMP);
  --
  -- Obsolete procedure
  PROCEDURE PutOriginator(Ori IN VARCHAR2);
  --
  -- Obsolete procedure
  PROCEDURE PutChannel(Chann IN VARCHAR2);
  --
  -- Obsolete procedure
  PROCEDURE PutClient(Cli IN VARCHAR2);
  --
  -- Obsolete procedure
  PROCEDURE PutLoginDev(LDTYPE IN VARCHAR2,
                        LDID   IN VARCHAR2);
  --
  -- Obsolete procedure
  PROCEDURE PutDescription(Ds IN VARCHAR2);
  --
  -- Obsolete procedure
  PROCEDURE PutFeature(Fea IN VARCHAR2);
  --
  FUNCTION GetSeq(inAvoidError IN NUMBER DEFAULT NULL) RETURN NUMBER;
  --
  PROCEDURE ToSeq(Seq IN NUMBER);
  --
  PROCEDURE SetParEvSeq(pSeq    IN Journal.Seq%TYPE,
                        pPackId IN ClientTransactionPackages.PackageId%TYPE DEFAULT NULL);
  --
  PROCEDURE ERROR_LOG_AUTONOMOUS(p_OriginatorID VARCHAR2,
                                 p_Details      CLOB,
                                 p_ParEvSeq     NUMBER DEFAULT NULL,
                                 p_EventID      NUMBER DEFAULT -500);
  --
END JOURNAL001;
/

CREATE OR REPLACE 
PACKAGE BODY JOURNAL001 IS
  ------------------
  -- $release: 5751 $
  -- $version: 8.0.19.0 $
  ------------------
  -- BSC Praha spol. s r.o.
  -- package umoznujuci manipulaciu s tabulkou Journal
  -- MODIFICATION HISTORY
  -- Person         Date        Comments
  -- ------------   ----------  -------------------------------------------
  -- Ilenin         01.07.2014  UDEBS RS
  -- Bocak          10.02.2015  LogEventPSI
  -- Emmer          07.09.2015  Rewrite Journaling
  -- Bocak          05.10.2015  I2Journal - added exception fk_exception
  -- Bocak          08.10.2015  LogEvent,I2Journal - added aDetails - 20009183-4757
  -- Pataky         13.10.2015  Details parameter type functions overloaded with CLOB versions, Details in table Journal still remains XMLType - 20009203-1354
  -- Bocak          28.10.2015  I2Journal - added another exception fk_exception
  -- Emmer          16.11.2015  Log events only into new structures
  -- Emmer          14.12.2015  Third journaling phase - removal of obsolete functions and procedures + leaving LogEvent/LogChildEvent for backwards compatibility 20009203-1354
  -- Bocak          04.01.2016  Get back procedures
  -- Bocak          13.01.2016  don't use ESCAPE_TEXT_FOR_XML for decription - 20009183-5403
  -- Emmer          15.01.2016  Error journaling while journaling in autonomous transaction
  -- Bocak          22.01.2016  ESCAPE_TEXT_FOR_DES - 20009183-5425
  -- Pataky         15.04.2016  Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
  -- Snederfler     03.05.2016  20009203-4818 - I2Journal - replace_xml_start_tag applied on p_Details when p_Event is NULL
  -- Pataky         28.04.2017  Modifications to support extension journal tables - 20009203-7142
  -- Pataky         18.05.2017  Fixed support of extension journal tables (MYG-4787) - 20009203-7142
  -- Emmer          07.09.2017  skipJournal: if EventTypes.loglevel = 0 then nothing is journaled for this event. Event -9 is always journaled.
  -- Emmer          28.02.2018  ERROR_LOG_AUTONOMOUS
  --
  -----private
  --
  RIDBuf ROWID;
  TYPE tTExtendedJournalingCode IS TABLE OF EventTypes.extendedjournalingcode%TYPE INDEX BY PLS_INTEGER;
  tExtendedJournalingCode tTExtendedJournalingCode;
  --
  -- remove special characters only when string is not in XML format
  FUNCTION ESCAPE_TEXT_FOR_DES(ivText VARCHAR2) RETURN CLOB IS
    lvText VARCHAR2(32000);
    lxTmp XMLTYPE;
    leXMLError EXCEPTION;
    PRAGMA EXCEPTION_INIT(leXMLError, -31011);
  BEGIN
    lvText := REPLACE(ivText, '&', '&amp;');
    lvText := REPLACE(REPLACE(REPLACE(REPLACE(lvText, '<', '&lt;'), '>', '&gt;'), '''', '&apos;'),
                      '"',
                      '&quot;');
    /* optimizations
    BEGIN
      SELECT XMLTYPE(ivText) INTO lxTmp FROM dual;
      lvText:=ivText;
    EXCEPTION
      WHEN leXMLError THEN
          lvText := REPLACE(ivText, '&', '&amp;');
        lvText := REPLACE(REPLACE(REPLACE(REPLACE(lvText, '<', '&lt;'), '>', '&gt;'), '''', '&apos;'),
                          '"',
                          '&quot;');
      WHEN OTHERS THEN
        lvText:=ivText;
    END;
    */
    RETURN lvText;
  END ESCAPE_TEXT_FOR_DES;
  --
  -- used for 1 element extraction from Journal.Details XML
  FUNCTION get_elem_from_xml(p_xml       xmltype,
                             p_elem_name VARCHAR2) RETURN VARCHAR2 IS
    v_return VARCHAR2(4000);
  BEGIN
    SELECT extractvalue(p_xml, '//' || p_elem_name)
      INTO v_return
      FROM dual;
    RETURN v_return;
  END;
  --
  FUNCTION get_xml_from_clob(p_clob CLOB) RETURN xmltype IS
  BEGIN
    IF p_clob IS NULL THEN
      RETURN NULL;
    ELSE
      RETURN xmltype(p_clob);
    END IF;
  END;
  --
  FUNCTION get_clob_from_xml(p_xml xmltype) RETURN CLOB IS
  BEGIN
    IF p_xml IS NULL THEN
      RETURN NULL;
    ELSE
      RETURN p_xml.getClobval();
    END IF;
  END;
  --
  -- replace_xml_start_tag in CLOB, for example <?xml version="1.0" encoding="UTF-8" standalone=''yes''?>
  FUNCTION replace_xml_start_tag(p_CLOB_text CLOB) RETURN CLOB IS
    v_xml_start NUMBER;
    v_xml_end NUMBER;
    v_buffer VARCHAR2(32000);
    v_temp_clob CLOB;
  BEGIN
    v_xml_start := dbms_lob.instr(lob_loc => p_CLOB_text, pattern => '<?xml');
    v_xml_end := dbms_lob.instr(lob_loc => p_CLOB_text, pattern => '?>', offset => v_xml_start);
    v_temp_clob := TO_CLOB(dbms_lob.substr(p_CLOB_text, v_xml_start - 1));
    v_temp_clob := v_temp_clob ||
                   TO_CLOB(dbms_lob.substr(p_CLOB_text,
                                           dbms_lob.getlength(p_CLOB_text) - v_xml_end - 1,
                                           v_xml_end + 2));
    RETURN v_temp_clob;
  END;
  --
  FUNCTION ESCAPE_TEXT_FOR_XML(p_text VARCHAR2) RETURN CLOB IS
    v_text VARCHAR2(32000);
  BEGIN
    v_text := REPLACE(p_text, '&', '&amp;');
    v_text := REPLACE(REPLACE(REPLACE(REPLACE(v_text, '<', '&lt;'), '>', '&gt;'), '''', '&apos;'),
                      '"',
                      '&quot;');
    RETURN v_text;
  END;
  --
  FUNCTION skipJournal(p_Event EventTypes.eventid%TYPE) RETURN NUMBER RESULT_CACHE DETERMINISTIC IS
    v_LogLevel EventTypes.Loglevel%TYPE;
  BEGIN
    SELECT loglevel
      INTO v_LogLevel
      FROM EventTypes
     WHERE EventID = p_Event;
    IF v_LogLevel = 0 THEN
      RETURN 1;
    ELSE
      RETURN 0;
    END IF;
  EXCEPTION
    WHEN no_data_found THEN
      RETURN 0;
  END skipJournal;
  --
  FUNCTION CheckEvent(p_Event EventTypes.eventid%TYPE) RETURN NUMBER RESULT_CACHE DETERMINISTIC IS
    v_EventID EventTypes.eventid%TYPE;
  BEGIN
    SELECT EventID
      INTO v_EventID
      FROM EventTypes
     WHERE EventID = p_Event;
    RETURN v_EventID;
  EXCEPTION
    WHEN no_data_found THEN
      RETURN NULL;
  END CheckEvent;
  --
  PROCEDURE I2Journal_Error(p_Par          IN Journal.parentseq%TYPE,
                            p_Event        IN Journal.eventid%TYPE,
                            p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                            p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                            p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                            p_ErrMsg       IN CLOB DEFAULT NULL) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    IF tExtendedJournalingCode.exists(p_Event)
       AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT1' THEN
      INSERT INTO JOURNAL_EXT1
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         p_ErrMsg,
         systimestamp);
    ELSIF tExtendedJournalingCode.exists(p_Event)
          AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT2' THEN
      INSERT INTO JOURNAL_EXT2
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         p_ErrMsg,
         systimestamp);
    ELSIF tExtendedJournalingCode.exists(p_Event)
          AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT3' THEN
      INSERT INTO JOURNAL_EXT3
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         p_ErrMsg,
         systimestamp);
    ELSE
      INSERT INTO Journal
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         xmltype(p_ErrMsg));
    END IF;
    COMMIT;
  END;
  --
  -- New overloaded I2Journal with direct XMLType input
  FUNCTION I2Journal(p_Par          IN Journal.parentseq%TYPE,
                     p_Event        IN Journal.eventid%TYPE,
                     p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                     p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                     p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                     p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                     p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) RETURN NUMBER IS
    xcERROR EXCEPTION;
    N Journal.seq%TYPE;
    lnEventID EventTypes.EventID%TYPE := NULL;
    lvErrMsg CLOB := NULL;
    v_ChannelID NUMBER;
    v_xmldiff CLOB;
    v_err_repeat BOOLEAN := TRUE;
    fk_exception EXCEPTION;
    tmpClob CLOB := NULL;
    PRAGMA EXCEPTION_INIT(fk_exception, -02291);
    tmp_cnt PLS_INTEGER;
  BEGIN
    -- validacia existencie EventID
    IF p_Event IS NULL THEN
      lvErrMsg := '<Journal><Error>JOURNAL001: Error - mandatory EventID in call is null.</Error>' ||
                  '<OriginatorID>' || ESCAPE_TEXT_FOR_XML(p_OriginatorID) ||
                  '</OriginatorID><Details>' || replace_xml_start_tag(get_clob_from_xml(p_Details)) ||
                  '</Details></Journal>';
      RAISE xcERROR;
    ELSIF p_Event IS NOT NULL THEN
      IF NOT tExtendedJournalingCode.exists(p_Event) THEN
        -- if present in collection, then it exists
        lnEventID := CheckEvent(p_Event);
        IF lnEventID IS NULL THEN
          -- EventID nenalezen - zalogovani chyby (autonomni transakci)
          lvErrMsg := '<Journal><Error>JOURNAL001: Error - mandatory EventID in call not found in EventTypes.</Error>' ||
                      '<EventID>' || p_Event || '</EventID><OriginatorID>' ||
                      ESCAPE_TEXT_FOR_XML(p_OriginatorID) || '</OriginatorID><Details>' ||
                      replace_xml_start_tag(get_clob_from_xml(p_Details)) || '</Details></Journal>';
          RAISE xcERROR;
        END IF;
      ELSE
        lnEventID := p_Event;
      END IF;
    END IF;
    --
    -- insert into Journal (or extension table) and also convert pDetails into XMLType (and thus check its validity)
    --
    IF tExtendedJournalingCode.exists(p_Event)
       AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT1' THEN
      tmpClob := get_clob_from_xml(p_Details);
      INSERT INTO JOURNAL_EXT1
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         tmpClob,
         p_Duration,
         systimestamp)
      RETURNING ROWID, SEQ INTO RidBuf, N;
    ELSIF tExtendedJournalingCode.exists(p_Event)
          AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT2' THEN
      tmpClob := get_clob_from_xml(p_Details);
      INSERT INTO JOURNAL_EXT2
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         tmpClob,
         p_Duration,
         systimestamp)
      RETURNING ROWID, SEQ INTO RidBuf, N;
    ELSIF tExtendedJournalingCode.exists(p_Event)
          AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT3' THEN
      tmpClob := get_clob_from_xml(p_Details);
      INSERT INTO JOURNAL_EXT3
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         tmpClob,
         p_Duration,
         systimestamp)
      RETURNING ROWID, SEQ INTO RidBuf, N;
    ELSE
      BEGIN
        INSERT INTO Journal
          (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration)
        VALUES
          (SEQ_JOURNAL.nextval,
           p_Par,
           p_Event,
           p_OriginatorID,
           p_ClientID,
           p_UserID,
           p_Details,
           p_Duration)
        RETURNING ROWID, SEQ INTO RidBuf, N;
      EXCEPTION
        WHEN fk_exception THEN
          -- check if parentSeq is not present in extension tables
          SELECT COUNT(1)
            INTO tmp_cnt
            FROM JOURNAL_EXT1
           WHERE rownum = 1
             AND seq = p_Par;
          IF tmp_cnt > 0 THEN
            tmpClob := get_clob_from_xml(p_Details);
            INSERT INTO JOURNAL_EXT1
              (SEQ,
               ParentSeq,
               EventID,
               OriginatorID,
               ClientID,
               UserID,
               Details,
               Duration,
               EventTime)
            VALUES
              (SEQ_JOURNAL.nextval,
               p_Par,
               p_Event,
               p_OriginatorID,
               p_ClientID,
               p_UserID,
               tmpClob,
               p_Duration,
               systimestamp)
            RETURNING ROWID, SEQ INTO RidBuf, N;
          ELSE
            SELECT COUNT(1)
              INTO tmp_cnt
              FROM JOURNAL_EXT2
             WHERE rownum = 1
               AND seq = p_Par;
            IF tmp_cnt > 0 THEN
              tmpClob := get_clob_from_xml(p_Details);
              INSERT INTO JOURNAL_EXT2
                (SEQ,
                 ParentSeq,
                 EventID,
                 OriginatorID,
                 ClientID,
                 UserID,
                 Details,
                 Duration,
                 EventTime)
              VALUES
                (SEQ_JOURNAL.nextval,
                 p_Par,
                 p_Event,
                 p_OriginatorID,
                 p_ClientID,
                 p_UserID,
                 tmpClob,
                 p_Duration,
                 systimestamp)
              RETURNING ROWID, SEQ INTO RidBuf, N;
            ELSE
              SELECT COUNT(1)
                INTO tmp_cnt
                FROM JOURNAL_EXT3
               WHERE rownum = 1
                 AND seq = p_Par;
              IF tmp_cnt > 0 THEN
                tmpClob := get_clob_from_xml(p_Details);
                INSERT INTO JOURNAL_EXT3
                  (SEQ,
                   ParentSeq,
                   EventID,
                   OriginatorID,
                   ClientID,
                   UserID,
                   Details,
                   Duration,
                   EventTime)
                VALUES
                  (SEQ_JOURNAL.nextval,
                   p_Par,
                   p_Event,
                   p_OriginatorID,
                   p_ClientID,
                   p_UserID,
                   tmpClob,
                   p_Duration,
                   systimestamp)
                RETURNING ROWID, SEQ INTO RidBuf, N;
              ELSE
                INSERT INTO Journal
                  (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration)
                VALUES
                  (SEQ_JOURNAL.nextval,
                   NULL,
                   p_Event,
                   p_OriginatorID,
                   p_ClientID,
                   p_UserID,
                   p_Details,
                   p_Duration)
                RETURNING ROWID, SEQ INTO RidBuf, N;
              END IF;
            END IF;
          END IF;
      END;
    END IF;
    --
    RETURN N;
  EXCEPTION
    WHEN xcERROR THEN
      ROLLBACK;
      -- kontrola, zda neni chyba jiz zazurnalovana, aby se stejna chyba pro kombinaci EVENT/ORIGINATOR/USER/CLIENT/SESSION/DETAIL neopakovala
      --lvErrMsg := xmltype(lvErrMsg).getclobval();
      FOR d IN (SELECT Details, seq
                  FROM Journal
                 WHERE EventID = -500
                   AND NVL(ParentSeq, -1) = NVL(p_Par, -1)
                   AND NVL(UserID, -1) = NVL(p_UserID, -1)
                   AND NVL(ClientID, -1) = NVL(p_ClientID, -1)
                   AND Details IS NOT NULL
                   AND TRUNC(eventtime) = TRUNC(SYSDATE)) LOOP
        SELECT REPLACE(REPLACE(XMLDIFF(xmltype(lvErrMsg), d.Details),
                               '<xd:xdiff xsi:schemaLocation="http://xmlns.oracle.com/xdb/xdiff.xsd http://xmlns.oracle.com/xdb/xdiff.xsd" xmlns:xd="http://xmlns.oracle.com/xdb/xdiff.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><?oracle-xmldiff operations-in-docorder="true" output-model="snapshot" diff-algorithm="global"?>'),
                       '</xd:xdiff>')
          INTO v_xmldiff
          FROM dual;
        IF v_xmldiff IS NULL THEN
          v_err_repeat := FALSE;
        END IF;
        EXIT WHEN NOT(v_err_repeat);
      END LOOP;
      -- zazurnalovani chyby, pokud se neopakuje
      IF v_err_repeat THEN
        BEGIN
          I2Journal_Error(p_Par          => p_Par,
                          p_Event        => -500,
                          p_OriginatorID => 'I2Journal',
                          p_ClientID     => p_ClientID,
                          p_UserID       => p_UserID,
                          p_ErrMsg       => lvErrMsg);
        EXCEPTION
          WHEN fk_exception THEN
            I2Journal_Error(p_Par          => NULL,
                            p_Event        => -500,
                            p_OriginatorID => 'I2Journal',
                            p_ClientID     => p_ClientID,
                            p_UserID       => p_UserID,
                            p_ErrMsg       => lvErrMsg);
        END;
      END IF;
      --
      im_error.Init;
      im_error.AddError(2117939, -- z clienttransactionerrortypes, kody chyb by mely byt spolecne mezi bankami, parametry nejsou povinne
                        dbms_lob.substr(lvErrMsg, amount => 1000),
                        'EventID',
                        p_Event);
  END I2Journal;
  -- New overloaded I2Journal without commit, CLOB version, target implementation
  FUNCTION I2Journal(p_Par          IN Journal.parentseq%TYPE,
                     p_Event        IN Journal.eventid%TYPE,
                     p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                     p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                     p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                     p_Details      IN CLOB DEFAULT NULL, --Journal.Details%TYPE DEFAULT NULL,
                     p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) RETURN NUMBER IS
    xcERROR EXCEPTION;
    N Journal.seq%TYPE;
    lnEventID EventTypes.EventID%TYPE := NULL;
    lvErrMsg CLOB := NULL;
    v_ChannelID NUMBER;
    v_xmldiff CLOB;
    v_err_repeat BOOLEAN := TRUE;
    fk_exception EXCEPTION;
    PRAGMA EXCEPTION_INIT(fk_exception, -02291);
    tmp_cnt PLS_INTEGER;
  BEGIN
    -- validacia existencie EventID
    IF p_Event IS NULL THEN
      lvErrMsg := '<Journal><Error>JOURNAL001: Error - mandatory EventID in call is null.</Error>' ||
                  '<OriginatorID>' || ESCAPE_TEXT_FOR_XML(p_OriginatorID) ||
                  '</OriginatorID><Details>' || replace_xml_start_tag(p_Details) ||
                  '</Details></Journal>';
      RAISE xcERROR;
    ELSIF p_Event IS NOT NULL THEN
      IF NOT tExtendedJournalingCode.exists(p_Event) THEN
        -- if present in collection, then it exists
        lnEventID := CheckEvent(p_Event);
        IF lnEventID IS NULL THEN
          -- EventID nenalezen - zalogovani chyby (autonomni transakci)
          lvErrMsg := '<Journal><Error>JOURNAL001: Error - mandatory EventID in call not found in EventTypes.</Error>' ||
                      '<EventID>' || p_Event || '</EventID><OriginatorID>' ||
                      ESCAPE_TEXT_FOR_XML(p_OriginatorID) || '</OriginatorID><Details>' ||
                      replace_xml_start_tag(p_Details) || '</Details></Journal>';
          RAISE xcERROR;
        END IF;
      ELSE
        lnEventID := p_Event;
      END IF;
    END IF;
    --
    -- insert into Journal (or extension table) and also convert pDetails into XMLType (and thus check its validity)
    --
    IF tExtendedJournalingCode.exists(p_Event)
       AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT1' THEN
      INSERT INTO JOURNAL_EXT1
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         p_Details,
         p_Duration,
         systimestamp)
      RETURNING ROWID, SEQ INTO RidBuf, N;
    ELSIF tExtendedJournalingCode.exists(p_Event)
          AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT2' THEN
      INSERT INTO JOURNAL_EXT2
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         p_Details,
         p_Duration,
         systimestamp)
      RETURNING ROWID, SEQ INTO RidBuf, N;
    ELSIF tExtendedJournalingCode.exists(p_Event)
          AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT3' THEN
      INSERT INTO JOURNAL_EXT3
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         p_Details,
         p_Duration,
         systimestamp)
      RETURNING ROWID, SEQ INTO RidBuf, N;
    ELSE
      BEGIN
        INSERT INTO Journal
          (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration)
        VALUES
          (SEQ_JOURNAL.nextval,
           p_Par,
           p_Event,
           p_OriginatorID,
           p_ClientID,
           p_UserID,
           get_xml_from_clob(p_Details),
           p_Duration)
        RETURNING ROWID, SEQ INTO RidBuf, N;
      EXCEPTION
        WHEN fk_exception THEN
          -- check if parentSeq is not present in extension tables
          SELECT COUNT(1)
            INTO tmp_cnt
            FROM JOURNAL_EXT1
           WHERE rownum = 1
             AND seq = p_Par;
          IF tmp_cnt > 0 THEN
            INSERT INTO JOURNAL_EXT1
              (SEQ,
               ParentSeq,
               EventID,
               OriginatorID,
               ClientID,
               UserID,
               Details,
               Duration,
               EventTime)
            VALUES
              (SEQ_JOURNAL.nextval,
               p_Par,
               p_Event,
               p_OriginatorID,
               p_ClientID,
               p_UserID,
               p_Details,
               p_Duration,
               systimestamp)
            RETURNING ROWID, SEQ INTO RidBuf, N;
          ELSE
            SELECT COUNT(1)
              INTO tmp_cnt
              FROM JOURNAL_EXT2
             WHERE rownum = 1
               AND seq = p_Par;
            IF tmp_cnt > 0 THEN
              INSERT INTO JOURNAL_EXT2
                (SEQ,
                 ParentSeq,
                 EventID,
                 OriginatorID,
                 ClientID,
                 UserID,
                 Details,
                 Duration,
                 EventTime)
              VALUES
                (SEQ_JOURNAL.nextval,
                 p_Par,
                 p_Event,
                 p_OriginatorID,
                 p_ClientID,
                 p_UserID,
                 p_Details,
                 p_Duration,
                 systimestamp)
              RETURNING ROWID, SEQ INTO RidBuf, N;
            ELSE
              SELECT COUNT(1)
                INTO tmp_cnt
                FROM JOURNAL_EXT3
               WHERE rownum = 1
                 AND seq = p_Par;
              IF tmp_cnt > 0 THEN
                INSERT INTO JOURNAL_EXT3
                  (SEQ,
                   ParentSeq,
                   EventID,
                   OriginatorID,
                   ClientID,
                   UserID,
                   Details,
                   Duration,
                   EventTime)
                VALUES
                  (SEQ_JOURNAL.nextval,
                   p_Par,
                   p_Event,
                   p_OriginatorID,
                   p_ClientID,
                   p_UserID,
                   p_Details,
                   p_Duration,
                   systimestamp)
                RETURNING ROWID, SEQ INTO RidBuf, N;
              ELSE
                INSERT INTO Journal
                  (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration)
                VALUES
                  (SEQ_JOURNAL.nextval,
                   NULL,
                   p_Event,
                   p_OriginatorID,
                   p_ClientID,
                   p_UserID,
                   get_xml_from_clob(p_Details),
                   p_Duration)
                RETURNING ROWID, SEQ INTO RidBuf, N;
              END IF;
            END IF;
          END IF;
      END;
    END IF;
    --
    RETURN N;
  EXCEPTION
    WHEN xcERROR THEN
      ROLLBACK;
      -- kontrola, zda neni chyba jiz zazurnalovana, aby se stejna chyba pro kombinaci EVENT/ORIGINATOR/USER/CLIENT/SESSION/DETAIL neopakovala
      lvErrMsg := xmltype(lvErrMsg).getclobval();
      FOR d IN (SELECT Details, seq
                  FROM Journal
                 WHERE EventID = -500
                   AND NVL(ParentSeq, -1) = NVL(p_Par, -1)
                   AND NVL(UserID, -1) = NVL(p_UserID, -1)
                   AND NVL(ClientID, -1) = NVL(p_ClientID, -1)
                   AND Details IS NOT NULL
                   AND TRUNC(eventtime) = TRUNC(SYSDATE)) LOOP
        SELECT REPLACE(REPLACE(XMLDIFF(xmltype(lvErrMsg), d.Details),
                               '<xd:xdiff xsi:schemaLocation="http://xmlns.oracle.com/xdb/xdiff.xsd http://xmlns.oracle.com/xdb/xdiff.xsd" xmlns:xd="http://xmlns.oracle.com/xdb/xdiff.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><?oracle-xmldiff operations-in-docorder="true" output-model="snapshot" diff-algorithm="global"?>'),
                       '</xd:xdiff>')
          INTO v_xmldiff
          FROM dual;
        IF v_xmldiff IS NULL THEN
          v_err_repeat := FALSE;
        END IF;
        EXIT WHEN NOT(v_err_repeat);
      END LOOP;
      -- zazurnalovani chyby, pokud se neopakuje
      IF v_err_repeat THEN
        BEGIN
          I2Journal_Error(p_Par          => p_Par,
                          p_Event        => -500,
                          p_OriginatorID => 'I2Journal',
                          p_ClientID     => p_ClientID,
                          p_UserID       => p_UserID,
                          p_ErrMsg       => lvErrMsg);
        EXCEPTION
          WHEN fk_exception THEN
            I2Journal_Error(p_Par          => NULL,
                            p_Event        => -500,
                            p_OriginatorID => 'I2Journal',
                            p_ClientID     => p_ClientID,
                            p_UserID       => p_UserID,
                            p_ErrMsg       => lvErrMsg);
        END;
      END IF;
      --
      im_error.Init;
      im_error.AddError(2117939, -- z clienttransactionerrortypes, kody chyb by mely byt spolecne mezi bankami, parametry nejsou povinne
                        dbms_lob.substr(lvErrMsg, amount => 1000),
                        'EventID',
                        p_Event);
  END I2Journal;
  --
  ----- PUBLIC
  --
  -- New overloaded function LogEvent returns new primary journal key (Seq: Journal ID) without commit. Primary version used for journaling - target implementation.
  -- Returned value can be used for USERSESSION.ParentSeq
  FUNCTION LogEventClob(p_Event        IN Journal.eventid%TYPE,
                        p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                        p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                        p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                        p_Details      IN CLOB DEFAULT NULL,
                        p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                        p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) RETURN NUMBER IS
    N NUMBER := 0;
  BEGIN
    ATRSeq.delete;
    IF skipJournal(p_Event) = 0 THEN
      -- dohledani parent seq z USERSESSION nelze, proto se pouzije bud predana promenna, nebo promenna journal001
      N := I2Journal(p_ParentSeq, --nvl(p_ParentSeq, ParEvSeq),
                     p_Event,
                     p_OriginatorID,
                     p_ClientID,
                     p_UserID,
                     p_Details,
                     p_Duration);
      ATRseq(1).SEQ := N;
      ATRseq(1).RID := RIDBuf;
      Lvl := 1;
    END IF;
    RETURN N;
  END LogEventClob;
  --
  FUNCTION LogEvent(p_Event        IN Journal.eventid%TYPE,
                    p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                    p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                    p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                    p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                    p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                    p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) RETURN NUMBER IS
    N NUMBER := 0;
  BEGIN
    ATRSeq.delete;
    IF skipJournal(p_Event) = 0 THEN
      -- dohledani parent seq z USERSESSION nelze, proto se pouzije bud predana promenna, nebo promenna journal001
      N := I2Journal(p_ParentSeq, --nvl(p_ParentSeq, ParEvSeq),
                     p_Event,
                     p_OriginatorID,
                     p_ClientID,
                     p_UserID,
                     p_Details,
                     p_Duration);
      ATRseq(1).SEQ := N;
      ATRseq(1).RID := RIDBuf;
      Lvl := 1;
    END IF;
    RETURN N;
  END LogEvent;
  --
  -- New overloaded procedure LogEvent without commit
  PROCEDURE LogEventClob(p_Event        IN Journal.eventid%TYPE,
                         p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                         p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                         p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                         p_Details      IN CLOB DEFAULT NULL,
                         p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                         p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) IS
    N NUMBER;
  BEGIN
    ATRSeq.delete;
    IF skipJournal(p_Event) = 0 THEN
      -- dohledani parent seq z USERSESSION nelze, proto se pouzije bud predana promenna, nebo promenna journal001
      N := I2Journal(p_ParentSeq, --nvl(p_ParentSeq, ParEvSeq),
                     p_Event,
                     p_OriginatorID,
                     p_ClientID,
                     p_UserID,
                     p_Details,
                     p_Duration);
      ATRseq(1).SEQ := N;
      ATRseq(1).RID := RIDBuf;
      Lvl := 1;
    END IF;
  END LogEventClob;
  --
  PROCEDURE LogEvent(p_Event        IN Journal.eventid%TYPE,
                     p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                     p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                     p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                     p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                     p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                     p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) IS
    N NUMBER;
  BEGIN
    IF skipJournal(p_Event) = 0 THEN
      N := LogEvent(p_Event,
                    p_OriginatorID,
                    p_ClientID,
                    p_UserID,
                    p_Details,
                    p_ParentSeq,
                    p_Duration);
    END IF;
  END LogEvent;
  --
  PROCEDURE LogEventClob(Event         IN Journal.eventid%TYPE,
                         Fea           IN VARCHAR2,
                         Dt1           IN TIMESTAMP := NULL,
                         Dt2           IN TIMESTAMP := NULL,
                         Ds            IN VARCHAR2 := NULL,
                         aOriginatorID IN Journal.originatorid%TYPE := NULL,
                         aChannelID    IN NUMBER := NULL,
                         LDTYPE        IN VARCHAR2 := NULL,
                         LDID          IN VARCHAR2 := NULL,
                         aClientID     IN Journal.clientid%TYPE := NULL,
                         aExtension    IN CLOB DEFAULT NULL,
                         aUserID       IN Journal.UserID%TYPE DEFAULT NULL,
                         aDetails      IN CLOB DEFAULT NULL,
                         aDuration     IN Journal.Duration%TYPE DEFAULT NULL) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    ATRSeq.delete;
    IF skipJournal(Event) = 0 THEN
      v_temp_clob := '<Trandata>' || aDetails;
      v_temp_clob := v_temp_clob || '<Feature>' || ESCAPE_TEXT_FOR_XML(Fea) ||
                     '</Feature><Description>' || ESCAPE_TEXT_FOR_DES(Ds) ||
                     '</Description><StartDate>' || TO_CHAR(Dt1, 'DD.MM.YYYY HH24:MI:SS') ||
                     '</StartDate><EndDate>' || TO_CHAR(Dt2, 'DD.MM.YYYY HH24:MI:SS') ||
                     '</EndDate><ChannelID>' || aChannelID || '</ChannelID><LoginDevID>' ||
                     ESCAPE_TEXT_FOR_XML(LDID) || '</LoginDevID><LoginDevType>' ||
                     ESCAPE_TEXT_FOR_XML(LDTYPE) || '</LoginDevType><Status>0</Status><Extension>';
      v_temp_clob := v_temp_clob || replace_xml_start_tag(aExtension);
      v_temp_clob := v_temp_clob || '</Extension></Trandata>';
      N := I2Journal(ParEvSeq, Event, aOriginatorID, aClientID, aUserID, v_temp_clob, aDuration);
      ATRseq(1).SEQ := N;
      ATRseq(1).RID := RIDBuf;
      Lvl := 1;
    END IF;
  END LogEventClob;
  --
  PROCEDURE LogEvent(Event         IN Journal.eventid%TYPE,
                     Fea           IN VARCHAR2,
                     Dt1           IN TIMESTAMP := NULL,
                     Dt2           IN TIMESTAMP := NULL,
                     Ds            IN VARCHAR2 := NULL,
                     aOriginatorID IN Journal.originatorid%TYPE := NULL,
                     aChannelID    IN NUMBER := NULL,
                     LDTYPE        IN VARCHAR2 := NULL,
                     LDID          IN VARCHAR2 := NULL,
                     aClientID     IN Journal.clientid%TYPE := NULL,
                     aExtension    IN CLOB DEFAULT NULL,
                     aUserID       IN Journal.UserID%TYPE DEFAULT NULL,
                     aDetails      IN Journal.Details%TYPE DEFAULT NULL,
                     aDuration     IN Journal.Duration%TYPE DEFAULT NULL) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    ATRSeq.delete;
    IF skipJournal(Event) = 0 THEN
      v_temp_clob := '<Trandata>' || get_clob_from_xml(aDetails);
      v_temp_clob := v_temp_clob || '<Feature>' || ESCAPE_TEXT_FOR_XML(Fea) ||
                     '</Feature><Description>' || ESCAPE_TEXT_FOR_DES(Ds) ||
                     '</Description><StartDate>' || TO_CHAR(Dt1, 'DD.MM.YYYY HH24:MI:SS') ||
                     '</StartDate><EndDate>' || TO_CHAR(Dt2, 'DD.MM.YYYY HH24:MI:SS') ||
                     '</EndDate><ChannelID>' || aChannelID || '</ChannelID><LoginDevID>' ||
                     ESCAPE_TEXT_FOR_XML(LDID) || '</LoginDevID><LoginDevType>' ||
                     ESCAPE_TEXT_FOR_XML(LDTYPE) || '</LoginDevType><Status>0</Status><Extension>';
      v_temp_clob := v_temp_clob || replace_xml_start_tag(aExtension);
      v_temp_clob := v_temp_clob || '</Extension></Trandata>';
      N := I2Journal(ParEvSeq, Event, aOriginatorID, aClientID, aUserID, v_temp_clob, aDuration);
      ATRseq(1).SEQ := N;
      ATRseq(1).RID := RIDBuf;
      Lvl := 1;
    END IF;
  END LogEvent;
  --
  -- New overloaded LogChildEvent without commit
  PROCEDURE LogChildEventClob(p_Event        IN Journal.eventid%TYPE,
                              p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                              p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                              p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                              p_Details      IN CLOB DEFAULT NULL,
                              p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) IS
    N NUMBER;
    M NUMBER;
  BEGIN
    IF skipJournal(p_Event) = 0 THEN
      M := ATRSeq(Lvl).SEQ;
      N := I2Journal(M, p_Event, p_OriginatorID, p_ClientID, p_UserID, p_Details, p_Duration);
      Lvl := Lvl + 1;
      ATRseq(Lvl).SEQ := N;
      ATRseq(Lvl).RID := RIDBuf;
    END IF;
  END LogChildEventClob;
  --
  PROCEDURE LogChildEvent(p_Event        IN Journal.eventid%TYPE,
                          p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                          p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                          p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                          p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                          p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) IS
    N NUMBER;
    M NUMBER;
  BEGIN
    IF skipJournal(p_Event) = 0 THEN
      M := ATRSeq(Lvl).SEQ;
      N := I2Journal(M, p_Event, p_OriginatorID, p_ClientID, p_UserID, p_Details, p_Duration);
      Lvl := Lvl + 1;
      ATRseq(Lvl).SEQ := N;
      ATRseq(Lvl).RID := RIDBuf;
    END IF;
  END LogChildEvent;
  --
  PROCEDURE LogChildEvent(Event         IN Journal.eventid%TYPE,
                          Fea           IN VARCHAR2,
                          Dt1           IN TIMESTAMP := NULL,
                          Dt2           IN TIMESTAMP := NULL,
                          Ds            IN VARCHAR2,
                          aOriginatorID IN Journal.originatorid%TYPE := NULL,
                          aChannelID    IN NUMBER := NULL,
                          LDTYPE        IN VARCHAR2 := NULL,
                          LDID          IN VARCHAR2 := NULL,
                          aClientID     IN Journal.clientid%TYPE := NULL,
                          aExtension    IN CLOB DEFAULT NULL,
                          aUserID       IN Journal.UserID%TYPE DEFAULT NULL) IS
    N NUMBER;
    M NUMBER;
    v_temp_clob CLOB;
  BEGIN
    IF skipJournal(Event) = 0 THEN
      M := ATRSeq(Lvl).SEQ;
      v_temp_clob := '<Trandata><Feature>' || ESCAPE_TEXT_FOR_XML(Fea) || '</Feature><Description>' ||
                     ESCAPE_TEXT_FOR_DES(Ds) || '</Description><StartDate>' ||
                     TO_CHAR(Dt1, 'DD.MM.YYYY HH24:MI:SS') || '</StartDate><EndDate>' ||
                     TO_CHAR(Dt2, 'DD.MM.YYYY HH24:MI:SS') || '</EndDate><ChannelID>' || aChannelID ||
                     '</ChannelID><LoginDevID>' || ESCAPE_TEXT_FOR_XML(LDID) ||
                     '</LoginDevID><LoginDevType>' || ESCAPE_TEXT_FOR_XML(LDTYPE) ||
                     '</LoginDevType><Status>0</Status><Extension>';
      v_temp_clob := v_temp_clob || replace_xml_start_tag(aExtension);
      v_temp_clob := v_temp_clob || '</Extension></Trandata>';
      N := I2Journal(M, Event, aOriginatorID, aClientID, aUserID, v_temp_clob);
      Lvl := Lvl + 1;
      ATRseq(Lvl).SEQ := N;
      ATRseq(Lvl).RID := RIDBuf;
    END IF;
  END LogChildEvent;
  --
  PROCEDURE ToParent IS
  BEGIN
    IF LVL > 1 THEN
      Lvl := Lvl - 1;
    END IF;
  END ToParent;
  --
  PROCEDURE ToChild IS
  BEGIN
    IF ATRseq.Exists(Lvl + 1) THEN
      Lvl := Lvl + 1;
    ELSE
      RAISE NO_DATA_FOUND;
    END IF;
  END ToChild;
  --
  PROCEDURE PutStatus(St IN NUMBER) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutStatus</Procedure><Status>' || St ||
                   '</Status></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutStatus;
  --
  FUNCTION GetStatus RETURN NUMBER IS
    st NUMBER;
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>GetStatus</Procedure></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
    RETURN NULL;
  END GetStatus;
  --
  PROCEDURE PutProgress(St IN NUMBER) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutProgress</Procedure><Status>' || St ||
                   '</Status></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutProgress;
  --
  FUNCTION GetProgress RETURN NUMBER IS
    st NUMBER;
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>GetProgress</Procedure></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
    RETURN NULL;
  END GetProgress;
  --
  PROCEDURE PutEndDate(Dt2 IN TIMESTAMP) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutEndDate</Procedure><EndDate>' ||
                   TO_CHAR(Dt2, 'DD.MM.YYYY HH24:MI:SS') || '</EndDate></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutEndDate;
  --
  PROCEDURE PutOriginator(Ori IN VARCHAR2) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutOriginator</Procedure><Originator>' ||
                   ESCAPE_TEXT_FOR_XML(Ori) || '</Originator></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutOriginator;
  --
  PROCEDURE PutChannel(Chann IN VARCHAR2) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutChannel</Procedure><Channel>' || Chann ||
                   '</Channel></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutChannel;
  --
  PROCEDURE PutClient(Cli IN VARCHAR2) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutClient</Procedure><Client>' || Cli ||
                   '</Client></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutClient;
  --
  PROCEDURE PutLoginDev(LDTYPE IN VARCHAR2,
                        LDID   IN VARCHAR2) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutLoginDev</Procedure><LoginDevType>' ||
                   ESCAPE_TEXT_FOR_XML(LDTYPE) || '</LoginDevType><LoginDevID>' ||
                   ESCAPE_TEXT_FOR_XML(LDID) || '</LoginDevID></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutLoginDev;
  --
  PROCEDURE PutDescription(Ds IN VARCHAR2) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutDescription</Procedure><Description>' ||
                   ESCAPE_TEXT_FOR_DES(Ds) || '</Description></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutDescription;
  --
  PROCEDURE PutFeature(Fea IN VARCHAR2) IS
    v_temp_clob CLOB;
    N NUMBER;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutFeature</Procedure><Feature>' ||
                   ESCAPE_TEXT_FOR_XML(Fea) || '</Feature></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutFeature;
  --
  FUNCTION GetSeq(inAvoidError IN NUMBER DEFAULT NULL) RETURN NUMBER IS
    lnReturn NUMBER := NULL;
  BEGIN
    IF inAvoidError = 1 THEN
      BEGIN
        lnReturn := ATRseq(Lvl).SEQ;
      EXCEPTION
        WHEN OTHERS THEN
          lnReturn := NULL;
      END;
    ELSE
      lnReturn := ATRseq(Lvl).SEQ;
    END IF;
    RETURN lnReturn;
  END GetSeq;
  --
  PROCEDURE ToSeq(Seq IN NUMBER) IS
  BEGIN
    ATRSeq.delete;
    IF Seq IS NOT NULL
       AND Seq <> 0 THEN
      ATRseq(1).SEQ := Seq;
      LVL := 1;
    END IF;
  END ToSeq;
  --
  PROCEDURE Temporary_ERROR_LOG(p_call      VARCHAR2,
                                p_ParEvSeq  NUMBER,
                                p_temp_clob CLOB) IS
    N NUMBER;
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    N := I2Journal(p_Par          => p_ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => p_call || ' ERROR',
                   p_Details      => p_temp_clob);
    /*
    INSERT INTO JOURNALEXTENSION
    VALUES
      (N, p_temp_clob);
    */
    COMMIT;
  END;
  --
  PROCEDURE ERROR_LOG_AUTONOMOUS(p_OriginatorID VARCHAR2,
                                 p_Details      CLOB,
                                 p_ParEvSeq     NUMBER DEFAULT NULL,
                                 p_EventID      NUMBER DEFAULT -500) IS
    N NUMBER;
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    N := I2Journal(p_Par          => p_ParEvSeq,
                   p_Event        => p_EventID,
                   p_OriginatorID => 'ERROR ' || p_OriginatorID,
                   p_Details      => p_Details);
    COMMIT;
  END;
  --
  PROCEDURE SetParEvSeq(pSeq    IN Journal.Seq%TYPE,
                        pPackId IN ClientTransactionPackages.PackageId%TYPE DEFAULT NULL) IS
  BEGIN
    IF pPackId IS NOT NULL
       AND (pSeq IS NULL OR pSeq = 0) THEN
      SELECT ParentEventSeq
        INTO ParEvSeq
        FROM ClientTransactionPackages
       WHERE PackageId = pPackId;
    ELSE
      ParEvSeq := pSeq;
    END IF;
    IF ParEvSeq IS NOT NULL
       AND ParEvSeq <> 0 THEN
      SELECT MAX(ROWID)
        INTO RIDBuf
        FROM Journal
       WHERE Seq = ParEvSeq; -- max je koli NO_DATA_FOUND
      ATRSeq.delete;
      ATRseq(1).SEQ := ParEvSeq;
      ATRseq(1).RID := RIDBuf;
      Lvl := 1;
    END IF;
  END SetParEvSeq;
  --
BEGIN
  FOR x IN (SELECT /*+ RESULT_CACHE */
             e.EventID, e.ExtendedJournalingCode
              FROM EventTypes e
             WHERE e.ExtendedJournalingCode IS NOT NULL) LOOP
    tExtendedJournalingCode(x.EventID) := x.ExtendedJournalingCode;
  END LOOP;
END JOURNAL001;
/

PROMPT CREATE OR REPLACE PACKAGE REFTAB_CTET
CREATE OR REPLACE 
PACKAGE REFTAB_CTET IS
------------------
-- $release: 078 $
-- $version: 8.0.0.0 $
------------------
-- $Header: /Oracle/UDEBS_EE/UDEBS0105.REFTAB_CTET.PSK 4     22.07.03 21:53 Jaros $
-- ---------   ---------- -------------------------------------------------
-- Ilenin      07.07.2014 UDEBS RS
-- ---------   ---------- -------------------------------------------------
  FUNCTION Existing      (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE) RETURN BOOLEAN;
  --
  FUNCTION TextId        (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          inDefaultVal    IN ClientTransactionErrorTypes.TextId%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION BankErrorCode (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          inDefaultVal    IN ClientTransactionErrorTypes.BankErrorCode%TYPE DEFAULT NULL) RETURN VARCHAR2;
  --
  FUNCTION FatalError    (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          inDefaultVal    IN NUMBER DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION Text          (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          ivLangId        IN SupportedLang.LangId%TYPE) RETURN VARCHAR2;
  --
  FUNCTION ErrorId       (inBankErrorCode IN ClientTransactionErrorTypes.BankErrorCode%TYPE,
                          inDefaultVal    IN ClientTransactionErrorTypes.ErrorId%TYPE DEFAULT NULL) RETURN NUMBER;
END reftab_ctet;
/

CREATE OR REPLACE 
PACKAGE BODY REFTAB_CTET IS
------------------
-- $release: 078 $
-- $version: 8.0.0.0 $
------------------
-- $Header: /Oracle/UDEBS_EE/UDEBS0105.REFTAB_CTET.PBK 4     22.07.03 21:53 Jaros $
-- ---------   ---------- -------------------------------------------------
-- Ilenin      07.07.2014 UDEBS RS
-- ---------   ---------- -------------------------------------------------
  TYPE t_ErrorTypes IS RECORD (
      TextId         ClientTransactionErrorTypes.TextId%TYPE,
      BankErrorCode  ClientTransactionErrorTypes.BankErrorCode%TYPE,
      FatalError     NUMBER(1)
  );
  TYPE t_ErrorTypesTable IS TABLE OF t_ErrorTypes INDEX BY BINARY_INTEGER;
  gtErrorTypes t_ErrorTypesTable;
  gbInitialized BOOLEAN := FALSE;
  --
  FUNCTION Existing      (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE) RETURN BOOLEAN IS
  BEGIN
    IF gtErrorTypes.Exists(inErrorId) THEN RETURN TRUE;
    ELSE RETURN FALSE;
    END IF;
  END;
  --
  FUNCTION TextId        (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          inDefaultVal    IN ClientTransactionErrorTypes.TextId%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtErrorTypes(inErrorId).TextId;
  EXCEPTION
    WHEN OTHERS THEN RETURN inDefaultVal;
  END;
  --
  FUNCTION BankErrorCode (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          inDefaultVal    IN ClientTransactionErrorTypes.BankErrorCode%TYPE DEFAULT NULL) RETURN VARCHAR2 IS
  BEGIN
    RETURN gtErrorTypes(inErrorId).BankErrorCode;
  EXCEPTION
    WHEN OTHERS THEN RETURN inDefaultVal;
  END;
  --
  FUNCTION FatalError    (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          inDefaultVal    IN NUMBER DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtErrorTypes(inErrorId).FatalError;
  EXCEPTION
    WHEN OTHERS THEN RETURN inDefaultVal;
  END;
  --
  FUNCTION Text          (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          ivLangId        IN SupportedLang.LangId%TYPE) RETURN VARCHAR2 IS
  BEGIN
    RETURN GetText(gtErrorTypes(inErrorId).TextID,ivLangID);
  END;
  --
  FUNCTION ErrorId       (inBankErrorCode IN ClientTransactionErrorTypes.BankErrorCode%TYPE,
                          inDefaultVal    IN ClientTransactionErrorTypes.ErrorId%TYPE DEFAULT NULL) RETURN NUMBER IS
    v_Ret ClientTransactionErrorTypes.ErrorId%TYPE;
  BEGIN
    BEGIN
      SELECT ErrorId INTO v_Ret FROM ClientTransactionErrorTypes
       WHERE BankErrorCode = inBankErrorCode;
    EXCEPTION WHEN OTHERS THEN
       NULL;
    END;
    RETURN NVL(v_Ret,inDefaultVal);
  END;
  --
BEGIN
  DECLARE
    CURSOR c_ctet IS
      SELECT ErrorId, TextId, BankErrorCode, DECODE(CategoryCode,'OKWITHINFOTOLOG', 0, 1) FatalError
      FROM ClientTransactionErrorTypes
      WHERE ErrorId IS NOT NULL;
  BEGIN
    FOR ctet IN c_ctet LOOP
      gtErrorTypes(ctet.ErrorId).TextId        := ctet.TextId;
      gtErrorTypes(ctet.ErrorId).BankErrorCode := ctet.BankErrorCode;
      gtErrorTypes(ctet.ErrorId).FatalError    := ctet.FatalError;
    END LOOP;
    gbInitialized := TRUE;
  END;
END reftab_ctet;
/

PROMPT CREATE OR REPLACE PACKAGE REFTAB_CTST
CREATE OR REPLACE 
PACKAGE REFTAB_CTST
IS
------------------
-- $release: 1666 $
-- $version: 8.0.2.0 $
------------------
-- $Header: /Oracle/UDEBS_RU/UDEBSRU.REFTAB_CTST.PSK 2     15.12.04 16:55 Matis $
--------------------------------------------------------------------------------
-- Ilenin       07.07.2014  UDEBS RS
-- Ilenin       02.12.2014  Override AllowSign
-- Pataky       02.11.2015  Added Visible function
--------------------------------------------------------------------------------
  FUNCTION Existing       (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE) RETURN BOOLEAN;
  --
  FUNCTION PackageStatus  (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.PackageStatus%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION AllowDel       (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION AllowEdit      (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION AllowCancel    (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION AllowSign      (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.AllowSign%TYPE DEFAULT NULL
                          ) RETURN PLS_INTEGER;
  --
  FUNCTION FinalStatus    (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.FinalStatus%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION ReportToClient (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION ShortText      (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.ShortText%TYPE DEFAULT NULL) RETURN VARCHAR2;
  --
  FUNCTION TextId         (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.TextId%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION Visible        (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.Visible%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
END reftab_ctst;
/

CREATE OR REPLACE 
PACKAGE BODY REFTAB_CTST
IS
------------------
-- $release: 1666 $
-- $version: 8.0.2.0 $
------------------
-- $Header: /Oracle/UDEBS_RU/UDEBSRU.REFTAB_CTST.PBK 2     15.12.04 16:56 Matis $
--------------------------------------------------------------------------------
-- Ilenin       07.07.2014  UDEBS RS
-- Ilenin       02.12.2014  Override AllowSign
-- Pataky       02.11.2015  Added Visible function
--------------------------------------------------------------------------------
  TYPE t_CliTrStatTypesTable   IS TABLE OF ClientTransactionStatusTypes%ROWTYPE INDEX BY BINARY_INTEGER;
  gtCliTrStatTypes             t_CliTrStatTypesTable;
  --
  FUNCTION Existing       (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE) RETURN BOOLEAN IS
  BEGIN
    IF gtCliTrStatTypes.Exists(inStatusId) THEN RETURN TRUE;
    ELSE RETURN FALSE;
    END IF;
  END Existing;
  --------------------------------------------------------------------------------------------------
  FUNCTION PackageStatus  (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.PackageStatus%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
  BEGIN
    RETURN gtCliTrStatTypes(inStatusId).PackageStatus;
  EXCEPTION
    WHEN OTHERS THEN RETURN inDefaultVal;
  END PackageStatus;
  --------------------------------------------------------------------------------------------------
  FUNCTION AllowDel       (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
    vRet StatusReporting.AllowDel%TYPE;
    CURSOR c_sr IS
      SELECT /*+ RESULT_CACHE */
             AllowDel
        FROM StatusReporting
       WHERE StatusId = inStatusId AND
             (NVL(ChannelId,inChannelId) = inChannelId OR (ChannelId IS NULL AND inChannelId IS NULL)) AND
             (NVL(OperId,inOperId) = inOperId OR (OperId IS NULL AND inOperId IS NULL))
       ORDER BY OperId, ChannelId;
  BEGIN
    OPEN c_sr;
    FETCH c_sr INTO vRet;
    IF c_sr%NOTFOUND THEN
      BEGIN
        vRet:=gtCliTrStatTypes(inStatusId).AllowDel;
      EXCEPTION
        WHEN OTHERS THEN
          vRet:=inDefaultVal;
      END;
    END IF;
    CLOSE c_sr;
    RETURN vRet;
  END AllowDel;
  --------------------------------------------------------------------------------------------------
  FUNCTION AllowEdit      (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
    vRet StatusReporting.AllowEdit%TYPE;
    CURSOR c_sr IS
      SELECT /*+ RESULT_CACHE */
             AllowEdit
        FROM StatusReporting
       WHERE StatusId = inStatusId AND
             (NVL(ChannelId,inChannelId) = inChannelId OR (ChannelId IS NULL AND inChannelId IS NULL)) AND
             (NVL(OperId,inOperId) = inOperId OR (OperId IS NULL AND inOperId IS NULL))
       ORDER BY OperId, ChannelId;
  BEGIN
    OPEN c_sr;
    FETCH c_sr INTO vRet;
    IF c_sr%NOTFOUND THEN
      BEGIN
        vRet:=gtCliTrStatTypes(inStatusId).AllowEdit;
      EXCEPTION
        WHEN OTHERS THEN
          vRet:=inDefaultVal;
      END;
    END IF;
    CLOSE c_sr;
    RETURN vRet;
  END AllowEdit;
  --------------------------------------------------------------------------------------------------
  FUNCTION AllowCancel    (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
    vRet StatusReporting.AllowCancel%TYPE;
    CURSOR c_sr IS
      SELECT /*+ RESULT_CACHE */
             AllowCancel
        FROM StatusReporting
       WHERE StatusId = inStatusId AND
             (NVL(ChannelId,inChannelId) = inChannelId OR (ChannelId IS NULL AND inChannelId IS NULL)) AND
             (NVL(OperId,inOperId) = inOperId OR (OperId IS NULL AND inOperId IS NULL))
       ORDER BY OperId, ChannelId;
  BEGIN
    OPEN c_sr;
    FETCH c_sr INTO vRet;
    IF c_sr%NOTFOUND THEN
      BEGIN
        vRet:=gtCliTrStatTypes(inStatusId).AllowCancel;
      EXCEPTION
        WHEN OTHERS THEN
          vRet:=inDefaultVal;
      END;
    END IF;
    CLOSE c_sr;
    RETURN vRet;
  END AllowCancel;
  --------------------------------------------------------------------------------------------------
  FUNCTION AllowSign      (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.AllowSign%TYPE DEFAULT NULL
                          ) RETURN PLS_INTEGER IS
    vRet StatusReporting.AllowSign%TYPE;
    CURSOR c_sr IS
      SELECT /*+ RESULT_CACHE */
             AllowSign
        FROM StatusReporting
       WHERE StatusId = inStatusId AND
             (NVL(ChannelId,inChannelId) = inChannelId OR (ChannelId IS NULL AND inChannelId IS NULL)) AND
             (NVL(OperId,inOperId) = inOperId OR (OperId IS NULL AND inOperId IS NULL))
       ORDER BY OperId, ChannelId;
  BEGIN
    OPEN c_sr;
    FETCH c_sr INTO vRet;
    IF c_sr%NOTFOUND THEN
      BEGIN
        vRet:=gtCliTrStatTypes(inStatusId).AllowSign;
      EXCEPTION
        WHEN OTHERS THEN
          vRet:=inDefaultVal;
      END;
    END IF;
    CLOSE c_sr;
    RETURN vRet;
  END AllowSign;
  --------------------------------------------------------------------------------------------------
  FUNCTION FinalStatus    (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.FinalStatus%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
  BEGIN
    RETURN gtCliTrStatTypes(inStatusId).FinalStatus;
  EXCEPTION
    WHEN OTHERS THEN RETURN inDefaultVal;
  END FinalStatus;
  --------------------------------------------------------------------------------------------------
  FUNCTION ShortText      (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.ShortText%TYPE DEFAULT NULL) RETURN VARCHAR2 IS
  BEGIN
    RETURN gtCliTrStatTypes(inStatusId).ShortText;
  EXCEPTION
    WHEN OTHERS THEN RETURN inDefaultVal;
  END ShortText;
  --------------------------------------------------------------------------------------------------
  FUNCTION TextId         (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.TextId%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
  BEGIN
    RETURN gtCliTrStatTypes(inStatusId).TextId;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END TextId;
  --------------------------------------------------------------------------------------------------
  FUNCTION ReportToClient (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
    vRet StatusReporting.ReportToClient%TYPE;
    CURSOR c_sr IS
      SELECT /*+ RESULT_CACHE */
             ReportToClient
        FROM StatusReporting
       WHERE StatusId = inStatusId AND
             (NVL(ChannelId,inChannelId) = inChannelId OR (ChannelId IS NULL AND inChannelId IS NULL)) AND
             (NVL(OperId,inOperId) = inOperId OR (OperId IS NULL AND inOperId IS NULL))
       ORDER BY OperId, ChannelId;
  BEGIN
    OPEN c_sr;
    FETCH c_sr INTO vRet;
    IF c_sr%NOTFOUND THEN
      BEGIN
        vRet:=gtCliTrStatTypes(inStatusId).ReportToClient;
      EXCEPTION
        WHEN OTHERS THEN
          vRet:=inDefaultVal;
      END;
    END IF;
    CLOSE c_sr;
    RETURN vRet;
  END ReportToClient;
  --
  FUNCTION Visible        (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.Visible%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
    vRet StatusReporting.Visible%TYPE;
    CURSOR c_sr IS
      SELECT /*+ RESULT_CACHE */
             Visible
        FROM StatusReporting
       WHERE StatusId = inStatusId AND
             (NVL(ChannelId,inChannelId) = inChannelId OR (ChannelId IS NULL AND inChannelId IS NULL)) AND
             (NVL(OperId,inOperId) = inOperId OR (OperId IS NULL AND inOperId IS NULL))
       ORDER BY OperId, ChannelId;
  BEGIN
    OPEN c_sr;
    FETCH c_sr INTO vRet;
    IF c_sr%NOTFOUND THEN
      BEGIN
        vRet:=gtCliTrStatTypes(inStatusId).Visible;
      EXCEPTION
        WHEN OTHERS THEN
          vRet:=inDefaultVal;
      END;
    END IF;
    CLOSE c_sr;
    RETURN vRet;
  END Visible;
  --------------------------------------------------------------------------------------------------
BEGIN
  DECLARE
    CURSOR c_ctst IS
      SELECT /*+ RESULT_CACHE */
             *
        FROM ClientTransactionStatusTypes;
  BEGIN
    FOR ctst IN c_ctst LOOP
      gtCliTrStatTypes(ctst.StatusId) := ctst;
    END LOOP;
  END;
END reftab_ctst;
/

PROMPT CREATE OR REPLACE PACKAGE REFTAB_OT
CREATE OR REPLACE 
PACKAGE REFTAB_OT AUTHID DEFINER IS
  --
  ------------------
  -- $release: 4619 $
  -- $version: 8.0.5.0 $
  ------------------
  -- $Header: /Oracle/UDEBS_EE/UDEBS0105.REFTAB_OT.PSK 2     1.04.03 15:12 Jaros $
  -- MODIFICATION HISTORY
  -- Person       Date       Comments
  -- ----------   ---------- ------------------------------------------
  -- Bocak        27.02.2017 all functions 
  -- Emmer        17.01.2018 all functions RESULT_CACHE removed, hints stays
  -- ----------   ---------- ------------------------------------------
  --
  FUNCTION Existing(inOperId IN OperationTypes.OperId%TYPE) RETURN BOOLEAN;
  --
  FUNCTION GroupId(inOperId     IN OperationTypes.OperId%TYPE,
                   inDefaultVal IN OperationTypes.GroupId%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION AccountSensitive(inOperId     IN OperationTypes.OperId%TYPE,
                            inDefaultVal IN OperationTypes.AccountSensitive%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION AuthenReq(inOperId     IN OperationTypes.OperId%TYPE,
                     inDefaultVal IN OperationTypes.AuthenReq%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION CertifReq(inOperId     IN OperationTypes.OperId%TYPE,
                     inDefaultVal IN OperationTypes.CertifReq%TYPE DEFAULT NULL,
                     inChannelID  IN OperationTypeChannels.ChannelID%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION TextId(inOperId     IN OperationTypes.OperId%TYPE,
                  inDefaultVal IN OperationTypes.TextId%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION GroupOrder(inOperId     IN OperationTypes.OperId%TYPE,
                      inDefaultVal IN OperationTypes.GroupOrder%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION SignRulesReq(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.SignRulesReq%TYPE DEFAULT NULL,
                        inChannelID  IN OperationTypeChannels.ChannelID%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION SysTabIndicator(inOperId     IN OperationTypes.OperId%TYPE,
                           inDefaultVal IN OperationTypes.SysTabIndicator%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION Priority(inOperId     IN OperationTypes.OperId%TYPE,
                    inDefaultVal IN OperationTypes.Priority%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION OperTypeCode(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.OperTypeCode%TYPE DEFAULT NULL)
    RETURN VARCHAR2;
  --
  FUNCTION BankToClient(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.BankToClient%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION ClientToBank(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.ClientToBank%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION AuthorReq(inOperId     IN OperationTypes.OperId%TYPE,
                     inDefaultVal IN OperationTypes.AuthorReq%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION HistoryKept(inOperId     IN OperationTypes.OperId%TYPE,
                       inDefaultVal IN OperationTypes.HistoryKept%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION Hidden(inOperId     IN OperationTypes.OperId%TYPE,
                  inDefaultVal IN OperationTypes.Hidden%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION CtpRetention(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.CtpRetention%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION RecStatusId(inOperId     IN OperationTypes.OperId%TYPE,
                       inDefaultVal IN OperationTypes.RecStatusId%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION HomeTableName(inOperId     IN OperationTypes.OperId%TYPE,
                         inDefaultVal IN OperationTypes.HomeTableName%TYPE DEFAULT NULL)
    RETURN VARCHAR2;
  --
  FUNCTION PayLimitCum(inOperId     IN OperationTypes.OperId%TYPE,
                       inDefaultVal IN OperationTypes.PayLimitCum%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION PayLimitSingle(inOperId     IN OperationTypes.OperId%TYPE,
                          inDefaultVal IN OperationTypes.PayLimitSingle%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION CopyStatusId(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.CopyStatusId%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION PerformAuthorisation(inOperId     IN OperationTypes.OperId%TYPE,
                                inDefaultVal IN OperationTypes.PerformAuthorisation%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION MakesDebit(inOperId     IN OperationTypes.OperId%TYPE,
                      inDefaultVal IN OperationTypes.MakesDebit%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION NotificationType(inOperId     IN OperationTypes.OperId%TYPE,
                            inDefaultVal IN OperationTypes.NotificationType%TYPE DEFAULT NULL)
    RETURN VARCHAR2;
  --
  FUNCTION Batch(inOperId     IN OperationTypes.OperId%TYPE,
                 inDefaultVal IN OperationTypes.Batch%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION PaymentSystemID(inOperId     IN OperationTypes.OperId%TYPE,
                           inBranchID   IN Branches.BranchID%TYPE,
                           inDefaultVal IN OperPaymentSystems.PaymentSystemID%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION AvailableForClient(inOperId     IN OperationTypes.OperId%TYPE,
                              inDefaultVal IN OperationTypes.AvailableForClient%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION AvailableForOperator(inOperId     IN OperationTypes.OperId%TYPE,
                                inDefaultVal IN OperationTypes.AvailableForOperator%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
END REFTAB_OT;
/

CREATE OR REPLACE 
PACKAGE BODY REFTAB_OT IS
  ------------------
  -- $release: 4619 $
  -- $version: 8.0.5.0 $
  ------------------
  -- $Header: /Oracle/UDEBS_EE/UDEBS0105.REFTAB_OT.PBK 2     1.04.03 15:12 Jaros $
  -- MODIFICATION HISTORY
  -- Person       Date       Comments
  -- ----------   ---------- ------------------------------------------
  -- Bocak        27.02.2017 all functions RESULT_CACHE
  -- Emmer        17.01.2018 all functions RESULT_CACHE removed, hints stays
  -- ----------   ---------- ------------------------------------------
  TYPE t_OperTypesTable IS TABLE OF OperationTypes%ROWTYPE INDEX BY BINARY_INTEGER;
  TYPE t_OperPayTable IS TABLE OF OperPaymentSystems%ROWTYPE INDEX BY BINARY_INTEGER;
  TYPE t_t_OperPayTable IS TABLE OF t_OperPayTable INDEX BY BINARY_INTEGER;
  gtOperTypes t_OperTypesTable;
  gtOperPayments t_t_OperPayTable;
  --
  FUNCTION Existing(inOperId IN OperationTypes.OperId%TYPE) RETURN BOOLEAN IS
  BEGIN
    IF gtOperTypes.Exists(inOperID) THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  END;
  --
  FUNCTION GroupId(inOperId     IN OperationTypes.OperId%TYPE,
                   inDefaultVal IN OperationTypes.GroupId%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).GroupId;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION AccountSensitive(inOperId     IN OperationTypes.OperId%TYPE,
                            inDefaultVal IN OperationTypes.AccountSensitive%TYPE DEFAULT NULL)
    RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).AccountSensitive;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION AuthenReq(inOperId     IN OperationTypes.OperId%TYPE,
                     inDefaultVal IN OperationTypes.AuthenReq%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).AuthenReq;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION CertifReq(inOperId     IN OperationTypes.OperId%TYPE,
                     inDefaultVal IN OperationTypes.CertifReq%TYPE DEFAULT NULL,
                     inChannelID  IN OperationTypeChannels.ChannelID%TYPE DEFAULT NULL) RETURN NUMBER IS
    lnCertifReq OperationTypeChannels.CertifReq%TYPE;
  BEGIN
    IF inChannelID IS NOT NULL THEN
      BEGIN
        SELECT /*+ RESULT_CACHE */
         certifreq
          INTO lnCertifReq
          FROM OperationTypeChannels
         WHERE operid = inOperId
           AND channelid = inChannelID;
      EXCEPTION
        WHEN no_data_found THEN
          RETURN gtOperTypes(inOperID).CertifReq;
      END;
    ELSE
      RETURN gtOperTypes(inOperID).CertifReq;
    END IF;
    RETURN lnCertifReq;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION TextId(inOperId     IN OperationTypes.OperId%TYPE,
                  inDefaultVal IN OperationTypes.TextId%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).TextId;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION GroupOrder(inOperId     IN OperationTypes.OperId%TYPE,
                      inDefaultVal IN OperationTypes.GroupOrder%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).GroupOrder;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION SignRulesReq(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.SignRulesReq%TYPE DEFAULT NULL,
                        inChannelID  IN OperationTypeChannels.ChannelID%TYPE DEFAULT NULL)
    RETURN NUMBER IS
    lnSignRulesReq OperationTypeChannels.SignRulesReq%TYPE;
  BEGIN
    IF inChannelID IS NOT NULL THEN
      BEGIN
        SELECT /*+ RESULT_CACHE */
         SignRulesReq
          INTO lnSignRulesReq
          FROM OperationTypeChannels
         WHERE operid = inOperId
           AND channelid = inChannelID;
      EXCEPTION
        WHEN no_data_found THEN
          RETURN gtOperTypes(inOperID).SignRulesReq;
      END;
    ELSE
      RETURN gtOperTypes(inOperID).SignRulesReq;
    END IF;
    RETURN lnSignRulesReq;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION SysTabIndicator(inOperId     IN OperationTypes.OperId%TYPE,
                           inDefaultVal IN OperationTypes.SysTabIndicator%TYPE DEFAULT NULL)
    RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).SysTabIndicator;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION Priority(inOperId     IN OperationTypes.OperId%TYPE,
                    inDefaultVal IN OperationTypes.Priority%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).Priority;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION OperTypeCode(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.OperTypeCode%TYPE DEFAULT NULL)
    RETURN VARCHAR2 IS
  BEGIN
    RETURN gtOperTypes(inOperID).OperTypeCode;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION BankToClient(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.BankToClient%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).BankToClient;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION ClientToBank(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.ClientToBank%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).ClientToBank;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION AuthorReq(inOperId     IN OperationTypes.OperId%TYPE,
                     inDefaultVal IN OperationTypes.AuthorReq%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).AuthorReq;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION HistoryKept(inOperId     IN OperationTypes.OperId%TYPE,
                       inDefaultVal IN OperationTypes.HistoryKept%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).HistoryKept;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION Hidden(inOperId     IN OperationTypes.OperId%TYPE,
                  inDefaultVal IN OperationTypes.Hidden%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).Hidden;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION CtpRetention(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.CtpRetention%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).CtpRetention;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION RecStatusId(inOperId     IN OperationTypes.OperId%TYPE,
                       inDefaultVal IN OperationTypes.RecStatusId%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).RecStatusId;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION HomeTableName(inOperId     IN OperationTypes.OperId%TYPE,
                         inDefaultVal IN OperationTypes.HomeTableName%TYPE DEFAULT NULL)
    RETURN VARCHAR2 IS
  BEGIN
    RETURN gtOperTypes(inOperID).HomeTableName;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION PayLimitCum(inOperId     IN OperationTypes.OperId%TYPE,
                       inDefaultVal IN OperationTypes.PayLimitCum%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).PayLimitCum;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION PayLimitSingle(inOperId     IN OperationTypes.OperId%TYPE,
                          inDefaultVal IN OperationTypes.PayLimitSingle%TYPE DEFAULT NULL)
    RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).PayLimitSingle;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION CopyStatusId(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.CopyStatusId%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).CopyStatusId;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION PerformAuthorisation(inOperId     IN OperationTypes.OperId%TYPE,
                                inDefaultVal IN OperationTypes.PerformAuthorisation%TYPE DEFAULT NULL)
    RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).PerformAuthorisation;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION MakesDebit(inOperId     IN OperationTypes.OperId%TYPE,
                      inDefaultVal IN OperationTypes.MakesDebit%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).MakesDebit;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION NotificationType(inOperId     IN OperationTypes.OperId%TYPE,
                            inDefaultVal IN OperationTypes.NotificationType%TYPE DEFAULT NULL)
    RETURN VARCHAR2 IS
  BEGIN
    RETURN NVL(gtOperTypes(inOperID).NotificationType, inDefaultVal);
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION Batch(inOperId     IN OperationTypes.OperId%TYPE,
                 inDefaultVal IN OperationTypes.Batch%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(InOperId).Batch;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION PaymentSystemID(inOperId     IN OperationTypes.OperId%TYPE,
                           inBranchID   IN Branches.BranchID%TYPE,
                           inDefaultVal IN OperPaymentSystems.PaymentSystemID%TYPE DEFAULT NULL)
    RETURN NUMBER IS
    lnPBID Branches.BranchID%TYPE := inBranchID;
  BEGIN
    RETURN gtOperPayments(lnPBID)(InOperId).PaymentSystemID;
  EXCEPTION
    WHEN OTHERS THEN
      LOOP
        BEGIN
          SELECT /*+ RESULT_CACHE */
           ParentBranchID
            INTO lnPBID
            FROM Branches
           WHERE BranchID = lnPBID;
          RETURN gtOperPayments(lnPBID)(InOperId).PaymentSystemID;
        EXCEPTION
          WHEN OTHERS THEN
            IF lnPBID IS NULL THEN
              RETURN inDefaultVal; -- kdyz se nenajde BranchID v Branches;
            ELSE
              NULL;
            END IF;
        END;
      END LOOP;
  END;
  --
  FUNCTION StructCode(inOperId     IN OperationTypes.OperId%TYPE,
                      inDefaultVal IN OperationTypes.StructCode%TYPE DEFAULT NULL) RETURN VARCHAR2 IS
  BEGIN
    RETURN gtOperTypes(inOperID).StructCode;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION AvailableForClient(inOperId     IN OperationTypes.OperId%TYPE,
                              inDefaultVal IN OperationTypes.AvailableForClient%TYPE DEFAULT NULL)
    RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).AvailableForClient;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION AvailableForOperator(inOperId     IN OperationTypes.OperId%TYPE,
                                inDefaultVal IN OperationTypes.AvailableForOperator%TYPE DEFAULT NULL)
    RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).AvailableForOperator;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
BEGIN
  DECLARE
    CURSOR c_ot IS
      SELECT /*+ RESULT_CACHE */
       *
        FROM OperationTypes;
    CURSOR c_ops IS
      SELECT /*+ RESULT_CACHE */
       *
        FROM OperPaymentSystems;
  BEGIN
    FOR ot IN c_ot LOOP
      gtOperTypes(ot.OperId) := ot;
    END LOOP;
    FOR ops IN c_ops LOOP
      gtOperPayments(ops.BranchID)(ops.OperID) := ops;
    END LOOP;
  END;
END REFTAB_OT;
/

PROMPT CREATE OR REPLACE PACKAGE REPORTCLIENTTRANSACTION
CREATE OR REPLACE 
PACKAGE REPORTCLIENTTRANSACTION
IS
------------------
-- $release: 6627 $
-- $version: 8.0.35.0 $
------------------
--
-- $Header: /Oracle/UDEBS_RS/UDEBSRS.REPORTCLIENTTRANSACTION.PBK 4     10. 12. 14 13:26 Ilenin $
--
-- MODIFICATION HISTORY
-- Person               Date       Comments
-- -------------------  ---------- ---------------------------------------------------
-- Ilenin               11.07.2014 UDEBS RS
-- Bocak                17.03.2015 GetHistoryReport - input parameter inSeqNum
-- Bocak                24.08.2015 Put - aCalculateAmount
-- Bocak                08.09.2015 Put - aChangePackageStatus
-- Pataky               27.10.2015 GetPackageHistoryReport- overloaded GetHistoryReport procedure for RB transactions
-- Pataky               15.04.2016 Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-------------------------------------------------------------------------------------------------------------------------------
  TYPE TrnCurTyp IS REF CURSOR;
  --
  PROCEDURE Init;
  -- Nulovanie premenych, nastavenie prostredia pre vygenerovanie reportu zmien statusov transakcii a package
  --
  PROCEDURE Report(aOperId NUMBER,aChannelID NUMBER,aPackageID NUMBER,aSeqNum NUMBER,aStatusID NUMBER,
                   aOriginator VARCHAR2,aRelatedData VARCHAR2 := NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0);
  -- Ak je Status pre dany kanal a operaciu reportovatelny (*1) a status este nebol reportovany v aktualne davke
  -- zmeny statuov (medzi volanim Init a Done), tak za vytvori report do tabulky ClientTransactionStatusReports a
  -- ClientTransactionErrors. (v pripade viacerych chyb, je len jeden zaznam o chybe - o prvej z nich)
  -- (*1) O tom ci je status reportovatelny prvotne rozhoduje stlpec ReportToClient z ClientTransactionStatusTypes.
  -- Je to ale len definicia reportovania pre status bez ohladu na kanal a/alebo operaciu. Tato definicia sa da
  -- predefinovat v tabulke StatusReporting a to tak, ze sa do tabulky vlozi zaznam s konkretnou hodnotou OperId a
  -- ChannelId a nastavi sa hodnota ReportToClient na zelanu hodnotu. Stlpec OperId v tabulke StatusReporting
  -- akceptuje NULL hodnotu a ta znamena ze status sa bude reportovat podla ChanelId, StatusId a ReportToClient
  -- pre vsetky mozne operacie.
  -- PR1 : ak sa ma status 90 reportovat iba pr operaciu 20 v kanali 5 a v ostatnych pripadoch nie :
  --       do ClientTransactionStatusTypes sa nastavi ReportToClient = 0 pre StatusId = 90
  --       do StatusReporting sa vlozi zaznam s OperId = 20, ChannelId = 5, StatusId = 90, ReportToClient = 1
  -- PR2 : ak sa NEMA StatusId 190 reportovat iba pre kanal 5 bez ohladu na operaciu, pre ostatne kanaly ano :
  --       do ClientTransactionStatusTypes sa nastavi ReportToClient = 1 pre StatusId = 190
  --       do StatusReporting sa vlozi zaznam s OperId = NULL, ChannelId = 5, StatusId = 190, ReportToClient = 0
  --
  PROCEDURE Put(aOperId NUMBER,aChannelID NUMBER,aPackageID NUMBER,aSeqNum NUMBER,aStatusID NUMBER,
                aOriginator VARCHAR2,aRelatedData VARCHAR2 := NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0,
                aBankRef VARCHAR2 := NULL, aBankEnsurePackageReport VARCHAR2 DEFAULT NULL,
                aCalculateAmount  NUMBER DEFAULT 1,aChangePackageStatus  NUMBER DEFAULT 1,
                inReport      IN  PLS_INTEGER DEFAULT 1);
  -- Zmeni status package, alebo transakcie a zavola report.
  -- Dovoli zmenit status iba vtedy ak nie je momentalny Status konecny a to len na vyssi status. Ak niekto proceduru
  -- zavola tak, ze bude chciet zmenit status na nizsi, alebo bude chciet modifikovat konecny status, volanie sa
  -- ignoruje, status sa nemeni.
  -- Pri jednotransakcnych packages zmeni aj status transakcie a to v pripade ked sa meni status package na final a
  -- tento status je vacsi ako 46 (zmena prisla z bankoveho systemu) - je to len bezpecnostna poistka pre pripad,
  -- ze bankovy system by nedodal report o zmene stavu transakcie.
  -- Pri zmene statusu transakcie na "final" (konecny spravny alebo konecny chybny) modifikuje aj stlpce CTP
  -- CountTransaction_OK a CountTransaction_E a ak je parameter GSP 'BankEnsurePackageReport' nastaveny na 1, tak
  -- pri zmene stavu poslednej transakcie na "final", zmeni aj status package (podla toho kolko je chybnych
  -- a kolko spravnych transakcii)
  --
  PROCEDURE Put(aTransactionID VARCHAR2,aStatusID VARCHAR2,aOriginator VARCHAR2,aRelatedData VARCHAR2 := NULL,
                aErrorID VARCHAR2 := '0',aReportSeq NUMBER := 0, aBankRef VARCHAR2 := NULL);
  -- Pretazenie prvej Put funkcie. TransactionId sa rozlozi do OperId, ChannelId, PackageId, SeqNum
  --
  PROCEDURE Put(aOperId NUMBER,aChannelID NUMBER,aPackageID NUMBER,aSeqNum NUMBER,aStatusID NUMBER,
                aOriginator VARCHAR2,aRelatedData VARCHAR2 := NULL,aErrorID NUMBER :=0,aReport Varchar2,aReportSeq IN OUT NUMBER);
  -- pretizeni prvni funkce za ucelem ziskat jako vystupni parametr pridelene RepoertId
  -- parametr aRepoert ma pouze smysl pro pretizeni, jeho hodnota muze byt jakakoli a nema zadny vyznam
  --
  PROCEDURE PutTrn(aTransactionID VARCHAR2,aStatusID VARCHAR2,aOriginator VARCHAR2,aRelatedData VARCHAR2 := NULL,
                aErrorID NUMBER := 0,aReportSeq IN OUT NUMBER, aBankRef VARCHAR2 := NULL);
  -- pretazenie druhej procedury - ucel = ziskanie ReportSeq (nazov je iny koli kolizii volani)
  --=============================================================================================================
  PROCEDURE PutForce(aOperId NUMBER,aChannelID NUMBER,aPackageId NUMBER,aSeqNum NUMBER,aStatusId NUMBER,
                     aOriginator VARCHAR2,aRelatedData VARCHAR2:=NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0,
                     aBankRef VARCHAR2 := NULL, aBankEnsurePackageReport VARCHAR2 DEFAULT NULL);
  -- Nastavuje packages a transakci v prubeznych i finalnich statusech na jakykoliv novy status.
  -- Procedura je volana napr. z formulare PACKAGEADMIN pro manualni nastaveni statusu.
  -- Package a transakce jiz byly nastavovany volanim standardni Put().
  --=============================================================================================================
  PROCEDURE Done(aReportseq NUMBER := null,aPackageid NUMBER :=null,aLockAttr varchar2:=0);
  -- Cistenie premennych a ak medzi init a done je aspon 1 transakcia, alebo package reportovatelny, zapise sa
  -- udalost pre GMO 3, ktore vygeneruje report.
  -- Pri viacnasobnom volani bez toho aby medzi 1. a 2. volanim bol Init a Put, sa vykona iba raz.
  --
  PROCEDURE GetTransactions (
      pPackageId IN ClientTransactionPackages.PackageId%TYPE,
      pOperId    IN ClientTransactionPackages.OperId%TYPE,
      pTrnCur    OUT TrnCurTyp);
  -- procedura ziska zoznam transakcii daneho package
  --
  PROCEDURE ChangePcgTrnStatus (
      pOperId        IN ClientTransactionPackages.OperId%TYPE,
      pChannelId     IN ClientTransactionPackages.ChannelId%TYPE,
      pPackageId     IN ClientTransactionPackages.PackageId%TYPE,
      pStatusId      IN ClientTransactionPackages.StatusId%TYPE,
      pOriginatorId  IN ClientTransactionStatusReports.Originator%TYPE);
  -- zmena stavu package + zmena stavu vsetkych jeho transakcii
------------------------------------------------------------------------
  PROCEDURE GetValidationReport (
      onReturnCode    OUT pls_integer,
      orErrors        OUT sys_refcursor,
      ocReport        OUT NOCOPY Clob,
      inUserID   IN  Number,
      inChannelID     IN  Number,
      inPackageID     IN  ClientTransactionPackages.PackageId%TYPE
  );
  -- Procedura vrati XML report o validaci a chybach
------------------------------------------------------------------------
END REPORTCLIENTTRANSACTION;
/

CREATE OR REPLACE 
PACKAGE BODY REPORTCLIENTTRANSACTION
IS
------------------
-- $release: 6627 $
-- $version: 8.0.35.0 $
------------------
--
-- $Header: /Oracle/UDEBS_RS/UDEBSRS.REPORTCLIENTTRANSACTION.PBK 4     10. 12. 14 13:26 Ilenin $
--
-- MODIFICATION HISTORY
-- Person               Date       Comments
-- -------------------  ---------- ---------------------------------------------------
-- Ilenin               11.07.2014 UDEBS RS
-- Ilenin               18.11.2014 20009183-1042: IIF_EVENT
-- Ilenin               26.01.2015 20009183-1481: Nastavenie statusu package pre status >= 40 podla transakcie
-- Bocak                13.03.2015 GetHistoryReport - changed select (20009183-1909)
-- Bocak                17.03.2015 GetHistoryReport - changed select (20009183-1909), input parameter inSeqNum
-- Bocak                15.04.2015 Put, PutForce - used new column SubTotals_All
-- Bocak                21.04.2015 GetHistoryReport - return RQC only for inSeqNum > 0
-- Pataky               09.06.2015 Put - for RBCZ no alerts are created
-- Pataky               27.07.2015 Put - for RBCZ no events for operid 11 are created
-- Bocak                28.07.2015 Put - prepocitanie sumy SUBTOTALS_ALL vsetkych tranzakcii v packagi (20009183-3839)
-- Bocak                29.07.2015 GetHistoryReport - extended select
-- Bocak                24.08.2015 Put - aCalculateAmount (20009183-4245)
-- Bocak                08.09.2015 Put - aChangePackageStatus
-- Bocak                21.09.2015 changed datatype subtotal_all
-- Bocak                28.09.2015 GetHistoryReport - fixed select
-- Pataky               27.10.2015 GetPackageHistoryReport- overloaded GetHistoryReport procedure for RB transactions - 20009203-2647
-- Bocak                04.11.2015 fix calculating of totalAmount
-- Pataky               25.11.2015 GetPackageHistoryReport- fixed GetHistoryReport procedure for RB transactions, selecting also from A_Journal table - 20009203-2647
-- Emmer                16.12.2015 Removal of links to obsolete journal attributes 20009203-1354
-- Bocak                13.11.2015 GetHistoryReport - fixed getting description from DETAILS column - 20009183-5403
-- Bocak                20.01.2016 GetHistoryReport - 20009183-5434
-- Emmer                01.04.2016 GetPackageHistoryReport - uprava razeni z ASC na DESC (MYG-40) 20009203-3373
-- Pataky               15.04.2016 Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-- Bocak                18.07.2016 ChangePackageStatus - call procedure 'put' with filled aCalculateAmount and aChangePackageStatus -  20009238-249
-- Bocak                25.07.2016 Put - recalculate amounts only for non final statuses - exception for SLSP - 20009238-249
-- Bocak                02.08.2016 Put - ChangePackageAmounts - don't set TotalAmount to 0 for final statuses - SLSP only
-- Bocak                04.10.2016 Put - fix CTP counters - 20009238-320
-- Bocak                17.10.2016 Put - fix CTP counters - 20009245-3179
-- Furdik               13.02.2017 add gStatusReportSeqNum global variable - 20009239-2856
-- Bocak                09.03.2017 Put.ChangePackageStatus - dont change to 89 while status is not over 40
-- Furdik               25.02.2017 Put - add reftab_ot.Batch condition for jednotransakcny package - 20009239-3530
-- Pataky               05.04.2017 Put - add possibility to change final package status for specified banks (RB Jira MYG-4023) - 20009246-337
-- Furdik               18.05.2017 Put, GetHistoryReport - add inReport, StatusReportID - P20009245-4389
-- Furdik               21.06.2017 GetHistoryReport - add RQE type for StatusID=122 - P20009239-3033
-- Bocak                28.08.2017 Put - fix changing to final status for SLSP only
-- Vagner               23.04.2018 GetPackageHistoryReport - copy batches (RBCZ MYG-8021)
-------------------------------------------------------------------------------------------------------------------------------
  gStatusReportId ClientTransactionStatusReports.StatusReportId%TYPE; -- identifikacia reportu
  gPackageId      ClientTransactionPackages.PackageId%TYPE; -- aktialne spracuvany package
  gOriginatorId   UpdateEvents.OriginatorId%TYPE; -- originator (staci ho nastavit v prvom volani)
  gBranchID       Branches.BranchID%TYPE := null; -- identifikace poboeky podle CTP.AccountID
  gStatusReportSeqNum ClientTransactionStatusReports.SeqNum%TYPE; -- seq num
  --
  PROCEDURE Init IS
  BEGIN
    gOriginatorId := NULL;
    gStatusReportId := NULL;
    gPackageId := NULL;
    gBranchID := NULL;
    gStatusReportSeqNum := NULL;
  END Init;
  --
  PROCEDURE Report(aOperId NUMBER,aChannelID NUMBER,aPackageId NUMBER,aSeqNum NUMBER,aStatusId NUMBER,
                   aOriginator VARCHAR2,aRelatedData VARCHAR2:=NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0) IS
    lnErrorSeqNum ClientTransactionErrors.ErrorSeqNum%TYPE := 1;
    vDummy Dual.Dummy%TYPE;
    failCond varchar2(50):='0';
  BEGIN
    IF gBranchID is null THEN
      gBranchID:=Get_BranchID(null,aPackageID);
    END IF;
    IF (reftab_ctst.ReportToClient(aStatusId, aChannelId, aOperId, 0) = 1) THEN -- ak je status reportovatelny
      BEGIN
         failCond:=reftab_gsp('EnableDuplicitReportsOnStatus','0');
         SELECT 'x' INTO vDummy -- kombinacia ctsr.PackageId, ctsr.SeqNum, ctsr.StatusId a cte.ErrorId ma byt UNIQUE (a nemusi to byt v 1 reporte)
          FROM ClientTransactionStatusReports ctsr, ClientTransactionErrors cte
         WHERE ctsr.PackageId = aPackageId
           AND ctsr.SeqNum = aSeqNum
           AND ctsr.StatusId = aStatusId -- koli HB musi esistovat index PackageId, SeqNum, StatusId
           AND failCond='0'
           AND ctsr.StatusReportId = cte.StatusReportId
           AND cte.SeqNum = ctsr.SeqNum
           AND cte.ErrorId = aErrorId
           AND NVL(cte.RelatedData,' ') = NVL(aRelatedData,' '); -- StatusReportId, SeqNum, ErrorSeqNum
      EXCEPTION
        WHEN no_data_found THEN
          IF NVL(aReportSeq,0) > 0 THEN -- ak je report zavolany tak, aby sa priviazal ku konkretnemu StatusReportId (nemaprilis velky vyznam)
            gStatusReportId:=aReportSeq;
          ELSIF gStatusReportId IS NULL THEN -- ak je reportovanie zavolane prvykrat po Init
            gStatusReportId := Seq_ClientTransactionStatusRep.NextVal;
          END IF;
          gStatusReportSeqNum := aSeqNum;
          BEGIN
            INSERT INTO ClientTransactionStatusReports(TransactionID,
                                                       StatusReportID, StatusId, PackageId, SeqNum, OperID, ChannelID, BornDate, Originator)
                                                VALUES(to_char(aOperID)||'.'||to_char(aChannelID)||'.'||to_char(aPackageId)||'.'||to_char(aSeqNum),
                                                       gStatusReportId, aStatusId, aPackageId, aSeqNum, aOperID, aChannelID, systimestamp, aOriginator);
          EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN -- koli vyberu ErrorSeqNum treba zaznam zamknut
              SELECT 'x' INTO vDummy FROM ClientTransactionStatusReports WHERE StatusReportId = gStatusReportId AND SeqNum = aSeqNum FOR UPDATE;
          END;
          BEGIN
            SELECT NVL(MAX(ErrorSeqNum),0)+1 INTO lnErrorSeqNum
              FROM ClientTransactionErrors
             WHERE StatusReportID = gStatusReportId
               AND SeqNum = aSeqNum;
            INSERT INTO ClientTransactionErrors(StatusReportID, SeqNum, ErrorSeqNum, ErrorID, RelatedData)
            VALUES (gStatusReportId, aSeqNum, lnErrorSeqNum, aErrorID, aRelatedData);
          EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
              NULL;
          END;
        WHEN OTHERS THEN NULL; -- pred nasadenim akt. release zalozene data mozu sposobit too_many_rows
      END;
    END IF; -- ak je status reportovatelny
  END Report;
  --========================================================================================================================
  -- procedura je volana napr. z formulare PACKAGEADMIN pro manualni nastaveni statusu, package a transakce jiz
  -- byly nastaveny volani standardni Put(). Akce teto procedury nejsou vazany na FinalStatus.
  --========================================================================================================================
  PROCEDURE PutForce(aOperId NUMBER,aChannelID NUMBER,aPackageId NUMBER,aSeqNum NUMBER,aStatusId NUMBER,
                     aOriginator VARCHAR2,aRelatedData VARCHAR2:=NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0,
                     aBankRef VARCHAR2 := NULL, aBankEnsurePackageReport VARCHAR2 DEFAULT NULL)
  IS
    vTRNStatusID  pls_integer;
    vDummy        varchar2(1 CHAR);
    vClientId     ClientTransactionPackages.ClientId%TYPE;
    vAccountId    ClientTransactionPackages.AccountId%TYPE;
    --
    PROCEDURE ChangePackageStatus(inOrigStatusID in pls_integer) IS -- volana, kdyz se meni status transakce
      vCount            ClientTransactionPackages.CountTransactions%TYPE;
      vCountOK          ClientTransactionPackages.CountTransactions_OK%TYPE;
      vCountErr         ClientTransactionPackages.CountTransactions_E%TYPE;
      vStatusId         ClientTransactionPackages.StatusId%TYPE := NULL;
      lnOrigFinal       pls_integer:=get_ctst_values('FinalStatus',inOrigStatusID);
      lnNewFinal        pls_integer:=get_ctst_values('FinalStatus',aStatusID);
      lnAddOk           NUMBER:=0;
      lnAddE            NUMBER:=0;
    BEGIN
      if lnNewFinal = 0 then -- na prubezny status
        if lnOrigFinal = 0 then
          lnAddE := 0;
          lnAddOK := 0;
        elsif lnOrigFinal = 1 then
          lnAddE := 0;
          lnAddOK := -1;
        elsif lnOrigFinal = 2 then
          lnAddE := -1;
          lnAddOK := 0;
        end if;
      elsif lnNewFinal = 1 then -- na final status OK
        if lnOrigFinal = 0 then
          lnAddE := 0;
          lnAddOK := 1;
        elsif lnOrigFinal = 1 then
          lnAddE := 0;
          lnAddOK := 0;
        elsif lnOrigFinal = 2 then
          lnAddE := -1;
          lnAddOK := 1;
        end if;
      elsif lnNewFinal = 2 then -- na final status Error
        if lnOrigFinal = 0 then
          lnAddE := 1;
          lnAddOK := 0;
        elsif lnOrigFinal = 1 then
          lnAddE := 1;
          lnAddOK := -1;
        elsif lnOrigFinal = 2 then
          lnAddE := 0;
          lnAddOK := 0;
        end if;
      end if;
      UPDATE ClientTransactionPackages
         SET CountTransactions_Ok = nvl(CountTransactions_Ok,0) + lnAddOK,
             CountTransactions_E = nvl(CountTransactions_e,0) + lnAddE
       WHERE PackageId = aPackageId
       RETURNING CountTransactions, CountTransactions_OK, CountTransactions_E INTO vCount, vCountOK, vCountErr;
      IF SQL%ROWCOUNT = 1 AND NVL(aBankEnsurePackageReport, reftab_gsp('BankEnsurePackageReport',gBranchID,'1')) = '0' THEN
        IF vCountOK = vCount THEN -- Status Done
          vStatusId := 90;
        ELSIF vCountErr = vCount THEN -- Status Error
          vStatusId := 51;
        ELSIF vCountOK + vCountErr = vCount THEN -- Status DONE_E
          IF reftab_gsp('FORMALCONTROLSPRINCIP', gBranchID, 'PACKAGE') = 'PACKAGE' THEN
            vStatusId := 89;
          ELSE
            vStatusId := 91;
          END IF;
        END IF;
        IF vStatusId is not NULL THEN
          PutForce(aOperId, aChannelId, aPackageId, 0, vStatusId, aOriginator, aReportSeq => aReportSeq);
        END IF;
      END IF;
    END ChangePackageStatus;
    --
    PROCEDURE ChangePackageAmounts(inOrigStatusID in pls_integer) IS
      vAmount         NUMBER;
      vAmount_All     NUMBER;
      vFinal          ClientTransactionStatusTypes.FinalStatus%TYPE := get_ctst_values('FinalStatus',aStatusId);
      vTotalAmount    ClientTransactionPackages.TotalAmount%TYPE;
      vReqCoverAmount ClientTransactionPackages.ReqCoverAmount%TYPE;
      ltSubtotals     Subtotal_tab:=Subtotal_tab();
      ltSubtotals_All Subtotal_all_tab:=Subtotal_all_tab();
      lnBatch         pls_integer:=get_OT_values('Batch',aOperID);
      lnOrigFinal     pls_integer:=get_ctst_values('FinalStatus',inOrigStatusID);
      lnNewFinal      pls_integer:=get_ctst_values('FinalStatus',aStatusID);
      lnMultipleTotal NUMBER:=0;
      lnMultipleCover NUMBER:=0;
    BEGIN
      IF aSeqNum > 0 THEN
        vAmount := nvl(GetPackageAmount(ltSubtotals, aPackageId, aOperId, NULL, aSeqNum),0);
        vAmount_All := GetPackageAmount_All(ltSubtotals_All, aPackageId, aOperId);
        vAmount:=GREATEST(NVL(vAmount,0),NVL(vAmount_All,0));
        if lnNewFinal = 0 then  -- na prubezny status
          if lnOrigFinal = 0 then
            lnMultipleTotal := 0;
            lnMultipleCover := 0;
          elsif lnOrigFinal = 1 then
            lnMultipleTotal := 0;
            lnMultipleCover := -1;
          elsif lnOrigFinal = 2 then
            lnMultipleTotal := -1;
            lnMultipleCover := -1;
          end if;
        elsif lnNewFinal = 1 then -- na final status OK
          if lnOrigFinal = 0 then
            lnMultipleTotal := 0;
            lnMultipleCover := 1;
          elsif lnOrigFinal = 1 then
            lnMultipleTotal := 0;
            lnMultipleCover := 0;
          elsif lnOrigFinal = 2 then
            lnMultipleTotal := -1;
            lnMultipleCover := 0;
          end if;
        elsif lnNewFinal = 2 then -- na final status Error
          if lnOrigFinal = 0 then
            lnMultipleTotal := 1;
            lnMultipleCover := 1;
          elsif lnOrigFinal = 1 then
            lnMultipleTotal := 1;
            lnMultipleCover := 0;
          elsif lnOrigFinal = 2 then
            lnMultipleTotal := 0;
            lnMultipleCover := 0;
          end if;
        end if;
        UPDATE ClientTransactionPackages
           SET TotalAmount = decode(CountTransactions-CountTransactions_E,0,0,TotalAmount - (lnMultipleTotal * vAmount)),
               ReqCoverAmount = ReqCoverAmount - (lnMultipleCover * vAmount)
         WHERE PackageId = aPackageId;
        if lnBatch=1 then
          begin
            select 'X' into vDummy from ClientTransactionPackages
             where PackageId = aPackageId and Subtotals is not null;  -- pojistka proti ORA-22908
            UPDATE TABLE(Select Subtotals from ClientTransactionPackages where PackageId = aPackageId)
               SET OrigCurrAmount = OrigCurrAmount - (lnMultipleTotal * ltSubtotals(1).OrigCurrAmount),
                   AccCurrAmount = AccCurrAmount - (lnMultipleTotal * ltSubtotals(1).AccCurrAmount)
             WHERE CurrencyID = ltSubtotals(1).CurrencyID;
            UPDATE TABLE(Select Subtotals_All from ClientTransactionPackages where PackageId = aPackageId)
               SET OrigCurrAmount = ltSubtotals_All(1).OrigCurrAmount,
                   AccCurrAmount = ltSubtotals_All(1).AccCurrAmount
             WHERE CurrencyID = ltSubtotals_All(1).CurrencyID;
          exception when no_data_found then
            null;
          end;
        end if;
      END IF;
    EXCEPTION
      WHEN INVALID_NUMBER OR VALUE_ERROR THEN
        -- odchytenie vynimky z volania fcie GetPackageAmounts, ak je problem v konverzii sumy
        NULL; -- nerob nic
    END ChangePackageAmounts;
    --
  BEGIN
    gBranchID:=Get_BranchID(null,aPackageID);
    IF reftab_ctet.FatalError(aErrorId,1) = 1 THEN
      gPackageId := aPackageId;
      gOriginatorId := NVL(aOriginator,gOriginatorId);
      IF aSeqNum = 0 THEN
        UPDATE ClientTransactionPackages
           SET StatusId = aStatusId
         WHERE PackageId = aPackageId
        RETURNING ClientId, AccountId INTO vClientId, vAccountId;
        --
      ELSE -- pro transakci (aSeqNum > 0)
        IF reftab_ot.Existing(aOperId) THEN
          begin
            SELECT T.StatusID INTO vTRNStatusID FROM ClientTransactionPackages CTP, Transactions T
             WHERE CTP.PackageID=T.PackageID AND CTP.PackageId = aPackageId AND SeqNum = aSeqNum
               FOR UPDATE OF CTP.StatusID;
          exception when no_data_found then
            raise_application_error(-20001,'PackageID '||aPackageId||' not found');
          end;
          IF reftab_ot.HomeTableName(aOperId) = 'TRANSACTIONS' THEN
            UPDATE Transactions SET StatusId = aStatusId WHERE PackageId = aPackageId AND SeqNum = aSeqNum;
            IF SQL%ROWCOUNT = 1 THEN
              ChangePackageStatus(vTRNStatusID);
              ChangePackageAmounts(vTRNStatusID);
            END IF;
          END IF;
        END IF;
      END IF; -- IF aSeqNum = 0 THEN
      Report(aOperID, aChannelID, aPackageId, aSeqNum, aStatusId, aOriginator, aRelatedData, aErrorID, aReportSeq);
    END IF; -- FatalError
  END PutForce;
  --
  PROCEDURE Put(aOperId NUMBER,aChannelID NUMBER,aPackageId NUMBER,aSeqNum NUMBER,aStatusId NUMBER,
                aOriginator VARCHAR2,aRelatedData VARCHAR2:=NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0,
                aBankRef VARCHAR2 := NULL, aBankEnsurePackageReport VARCHAR2 DEFAULT NULL,
                aCalculateAmount  NUMBER DEFAULT 1,aChangePackageStatus  NUMBER DEFAULT 1,
                inReport      IN  PLS_INTEGER DEFAULT 1)
  IS
    vDummy     Dual.Dummy%TYPE;
    vClientId  ClientTransactionPackages.ClientId%TYPE;
    vAccountId ClientTransactionPackages.AccountId%TYPE;
    --
    PROCEDURE ChangePackageStatus IS
      vCount            ClientTransactionPackages.CountTransactions%TYPE;
      vCountOK          ClientTransactionPackages.CountTransactions_OK%TYPE;
      vCountErr         ClientTransactionPackages.CountTransactions_E%TYPE;
      vStatusId         ClientTransactionPackages.StatusId%TYPE := NULL;
      lnCnt             PLS_INTEGER;
      lnTranStatusID    Transactions.StatusId%TYPE := NULL;
      lnOK              PLS_INTEGER;
      lnERR             PLS_INTEGER;
    BEGIN
      IF reftab_ctst.FinalStatus(aStatusId,0) in (1,2) THEN
        -- fix CTP counters
        SELECT SUM(DECODE(reftab_ctst.FinalStatus(StatusId,0),1,1,0)),
               SUM(DECODE(reftab_ctst.FinalStatus(StatusId,0),2,1,0))
        INTO lnOK,lnERR
        FROM Transactions
        WHERE PackageId=aPackageId;
        UPDATE ClientTransactionPackages
        SET CountTransactions_Ok = lnOK,
            CountTransactions_E = lnERR
        WHERE PackageId = aPackageId
        AND reftab_ctst.FinalStatus(StatusId,0) = 0
        RETURNING CountTransactions, CountTransactions_OK, CountTransactions_E INTO vCount, vCountOK, vCountErr;
        --
/* old version
        UPDATE ClientTransactionPackages
           SET CountTransactions_Ok = NVL(CountTransactions_Ok,0) + DECODE(reftab_ctst.FinalStatus(aStatusId,0),1,1,0),
               CountTransactions_E = NVL(CountTransactions_e,0) + DECODE(reftab_ctst.FinalStatus(aStatusId,0),2,1,0)
         WHERE PackageId = aPackageId
           AND reftab_ctst.FinalStatus(StatusId,0) = 0
         RETURNING CountTransactions, CountTransactions_OK, CountTransactions_E INTO vCount, vCountOK, vCountErr;
*/
        IF SQL%ROWCOUNT = 1 AND NVL(aBankEnsurePackageReport, reftab_gsp('BankEnsurePackageReport',gBranchID,'1')) = '0' THEN
          IF vCountOK = vCount THEN -- Status Done
            IF reftab_ctst.FinalStatus(aStatusId,0)=1 AND reftab_ctst.PackageStatus(aStatusId,0)=1 AND Reftab_GSP('LOCALBANKCODEALPHA')='SLSP' THEN
              vStatusId := aStatusId;
            ELSIF reftab_ctst.FinalStatus(aStatusId,0)=1 AND reftab_ctst.PackageStatus(aStatusId,0)=0 AND Reftab_GSP('LOCALBANKCODEALPHA')='SLSP' THEN
              vStatusId := aStatusId-100;
            ELSE
              vStatusId := 90;
            END IF;
          ELSIF vCountErr = vCount THEN -- Status Failed
            IF ABS(aStatusId-100)>40 THEN
              vStatusId := 89;
            ELSE
              vStatusId:=aStatusId-100;
            END IF;
          ELSIF vCountOK + vCountErr = vCount THEN -- Status DONE_E
            IF reftab_gsp('FORMALCONTROLSPRINCIP', gBranchID, 'PACKAGE') = 'PACKAGE' THEN
              vStatusId := 89;
            ELSE
              vStatusId := 91;
            END IF;
          END IF;
          IF vStatusId is not NULL THEN
            Put(aOperId, aChannelId, aPackageId, 0, vStatusId, aOriginator, aReportSeq => aReportSeq, aCalculateAmount=>aCalculateAmount, aChangePackageStatus=>aChangePackageStatus);
          END IF;
        END IF;
      ELSIF reftab_ctst.FinalStatus(aStatusId,0) in (0) THEN
       -- zmena statusu CTP podla najmensieho statusu TRN
       BEGIN
         SELECT MIN(T.StatusID)
           INTO lnTranStatusID
           FROM Transactions T
          WHERE T.PackageID = aPackageId;
         IF reftab_ctst.Existing(lnTranStatusID-100) THEN
           UPDATE ClientTransactionPackages CTP
              SET CTP.StatusID = lnTranStatusID-100
            WHERE CTP.PackageID = aPackageId
              AND CTP.StatusID < lnTranStatusID-100
              AND CTP.StatusID >= 40;
           IF SQL%ROWCOUNT = 1 THEN
             IF inReport=1 THEN
              Report(aOperId,aChannelID,aPackageId,0,lnTranStatusID-100,aOriginator);
             END IF;
           END IF;
         END IF;
       EXCEPTION
         WHEN no_data_found THEN
           NULL;
       END;
      END IF;
    END ChangePackageStatus;
    --
    PROCEDURE ChangePackageAmounts IS
      vAmount         NUMBER;
      vAmount_All     NUMBER;
      vFinal          ClientTransactionStatusTypes.FinalStatus%TYPE := reftab_ctst.FinalStatus(aStatusId, 0);
      vTotalAmount    ClientTransactionPackages.TotalAmount%TYPE;
      vReqCoverAmount ClientTransactionPackages.ReqCoverAmount%TYPE;
      ltSubtotals     Subtotal_tab:=Subtotal_tab();
      ltSubtotals_All Subtotal_all_tab:=Subtotal_all_tab();
      lnBatch         pls_integer:=get_OT_values('Batch',aOperID);
      lvStruct        OperationTypes.structcode%TYPE := get_OT_values('StructCode',aOperID);
    BEGIN
      IF lvStruct <> 'CANC' THEN  -- cancelrequest ma TotalAmount nastaven pri vytvoreni
        IF aSeqNum > 0 AND vFinal in (1,2) THEN
          vAmount := GetPackageAmount(ltSubtotals, aPackageId, aOperId, NULL, aSeqNum);
          vAmount_All := GetPackageAmount_All(ltSubtotals_All, aPackageId, aOperId); -- prepocitanie sumy vsetkych tranzakcii v packagi
          vAmount:=GREATEST(NVL(vAmount,0),NVL(vAmount_All,0));
          IF Reftab_GSP('LOCALBANKCODEALPHA')='SLSP' THEN
            UPDATE ClientTransactionPackages
               SET TotalAmount = TotalAmount,
                   ReqCoverAmount = ReqCoverAmount - nvl(vAmount,0)
             WHERE PackageId = aPackageId;
          ELSE
            UPDATE ClientTransactionPackages
               SET TotalAmount = decode(CountTransactions-CountTransactions_E,0,0,TotalAmount - DECODE(vFinal, 1, 0, 2, nvl(vAmount,0))),
                   ReqCoverAmount = ReqCoverAmount - nvl(vAmount,0)
             WHERE PackageId = aPackageId;
          END IF;
          if lnBatch=1 then
            begin
              select 'X' into vDummy from ClientTransactionPackages
               where PackageId = aPackageId and Subtotals is not null;  -- pojistka proti ORA-22908
              UPDATE TABLE(Select Subtotals from ClientTransactionPackages where PackageId = aPackageId)
                 SET OrigCurrAmount = OrigCurrAmount - DECODE(vFinal, 1, 0, 2, ltSubtotals(1).OrigCurrAmount),
                     AccCurrAmount = AccCurrAmount - DECODE(vFinal, 1, 0, 2, ltSubtotals(1).AccCurrAmount)
               WHERE CurrencyID = ltSubtotals(1).CurrencyID;
              UPDATE TABLE(Select Subtotals_All from ClientTransactionPackages where PackageId = aPackageId)
                 SET OrigCurrAmount = ltSubtotals_All(1).OrigCurrAmount,
                     AccCurrAmount = ltSubtotals_All(1).AccCurrAmount
               WHERE CurrencyID = ltSubtotals_All(1).CurrencyID;
            exception when no_data_found then
              null;
            end;
          end if;
        ELSIF aSeqNum = 0 THEN
          begin
            SELECT TotalAmount, ReqCoverAmount INTO vTotalAmount, vReqCoverAmount
              FROM ClientTransactionPackages WHERE PackageId = aPackageId;
          exception when no_data_found then
            raise_application_error(-20001,'PackageID '||aPackageId||' not found');
          end;
          --IF vTotalAmount IS NULL OR vReqCoverAmount IS NULL THEN
            vAmount := GetPackageAmount(ltSubtotals, aPackageId, aOperId);
            vAmount_All := GetPackageAmount_All(ltSubtotals_All, aPackageId, aOperId);
            vAmount:=GREATEST(NVL(vAmount,0),NVL(vAmount_All,0));
            if lnBatch = 0 then
              IF Reftab_GSP('LOCALBANKCODEALPHA')='SLSP' THEN
                UPDATE ClientTransactionPackages
                   SET TotalAmount = vAmount,
                       ReqCoverAmount = DECODE(vFinal, 0, vAmount, 0)
                 WHERE PackageId = aPackageId;
              ELSE
                UPDATE ClientTransactionPackages
                   SET TotalAmount = vAmount,
                       ReqCoverAmount = DECODE(vFinal, 0, vAmount, 0)
                 WHERE PackageId = aPackageId;
              END IF;
            ELSE
              IF Reftab_GSP('LOCALBANKCODEALPHA')='SLSP' THEN
                UPDATE ClientTransactionPackages
                   SET TotalAmount = nvl(vAmount,0),
                       ReqCoverAmount = DECODE(vFinal, 0, nvl(vAmount,0), 0),
                       Subtotals = ltSubtotals,
                       Subtotals_All = ltSubtotals_All
                 WHERE PackageId = aPackageId;
              ELSE
                UPDATE ClientTransactionPackages
                   SET TotalAmount = DECODE(vFinal, 2, 0, nvl(vAmount,0)),
                       ReqCoverAmount = DECODE(vFinal, 0, nvl(vAmount,0), 0),
                       Subtotals = ltSubtotals,
                       Subtotals_All = ltSubtotals_All
                 WHERE PackageId = aPackageId;
              END IF;
            end if;
          --END IF;
        END IF;
      END IF;
    EXCEPTION
      WHEN INVALID_NUMBER OR VALUE_ERROR THEN
        -- odchytenie vynimky z volania fcie GetPackageAmounts, ak je problem v konverzii sumy
        NULL; -- nerob nic
    END ChangePackageAmounts;
    --
  BEGIN
    gBranchID:=Get_BranchID(null,aPackageID);
    IF reftab_ctet.FatalError(aErrorId,1) = 1 THEN
      gPackageId := aPackageId;
      gOriginatorId := NVL(aOriginator,gOriginatorId);
      IF aSeqNum = 0 THEN
        UPDATE ClientTransactionPackages
           SET StatusId = aStatusId
         WHERE PackageId = aPackageId
           AND (reftab_ctst.FinalStatus(StatusId,0) = 0 or Reftab_GSP('LOCALBANKCODEALPHA') in ('RBCZ'))
         RETURNING ClientId, AccountId INTO vClientId, vAccountId;
        IF SQL%ROWCOUNT = 1 AND aStatusId >= 46 AND reftab_ctst.FinalStatus(aStatusId,0) != 0 AND reftab_ctst.Existing(aStatusId+100) THEN
          DECLARE -- bezpecnostna poistka pre jednotransakcne packages, keby bankovy system nezmenil status transakcii
            vNumOfTrans NUMBER := 0;
            vTrnCur     TrnCurTyp;
            vSeqNum     Transactions.SeqNum%TYPE;
            vStatusId   Transactions.StatusId%TYPE;
          BEGIN
/*
            GetTransactions (aPackageId, aOperId, vTrnCur);
            LOOP
              FETCH vTrnCur INTO vSeqNum, vStatusId;
              EXIT WHEN vTrnCur%NOTFOUND;
              vNumOfTrans := vNumOfTrans + 1;
            END LOOP;
            CLOSE vTrnCur;
*/
            SELECT COUNT(1) INTO vNumOfTrans FROM Transactions WHERE PackageId=aPackageId;
            IF vNumOfTrans = 1 THEN -- jednotransakcny package
              IF reftab_ot.HomeTableName(aOperId) = 'TRANSACTIONS' AND reftab_ot.Batch(aOperId) = 0 THEN
                UPDATE Transactions
                   SET StatusId = aStatusId + 100
                 WHERE PackageId = aPackageId;
              END IF; -- rozdelenie podla HomeTableName
            END IF; -- jednotransakcny package
          END; -- bezpecnostna poistka pre jednotransakcne packages, keby bankovy system nezmenil status transakcii
        END IF; -- zmena statusu transakcie jednotransakcneho package
        --
        IF aCalculateAmount=1 THEN
          ChangePackageAmounts;
        END IF;
      ELSE
        IF reftab_ot.Existing(aOperId) THEN
          begin
            SELECT 'x'
              INTO vDummy
              FROM ClientTransactionPackages
             WHERE PackageId = aPackageId FOR UPDATE;
          exception
            when no_data_found then
            raise_application_error(-20001,'PackageID '||aPackageId||' not found');
          end;
          IF reftab_ot.HomeTableName(aOperId) = 'TRANSACTIONS' THEN
            UPDATE Transactions
               SET StatusId = aStatusId --, BankRef = NVL(aBankRef, BankRef)
             WHERE PackageId = aPackageId
               AND SeqNum = aSeqNum
               AND ((aStatusId > NVL(StatusId,0)) or (aStatusId between 154 and 159 and NVL(StatusId,0) between 154 and 159));
            IF SQL%ROWCOUNT = 1 THEN
              IF aChangePackageStatus=1 THEN
                ChangePackageStatus;
              END IF;
              IF aCalculateAmount=1 THEN
                ChangePackageAmounts;
              END IF;
            END IF;
          END IF;
        END IF;
      END IF; -- IF aSeqNum = 0 THEN
      IF inReport=1 THEN
        Report(aOperID, aChannelID, aPackageId, aSeqNum, aStatusId, aOriginator, aRelatedData, aErrorID, aReportSeq);
      END IF;
    END IF; -- FatalError
  END Put;
  --
  PROCEDURE Put(aTransactionID VARCHAR2,aStatusId VARCHAR2,aOriginator VARCHAR2,aRelatedData VARCHAR2:=NULL,
                aErrorID VARCHAR2 := '0',aReportSeq NUMBER := 0, aBankRef VARCHAR2 := NULL) IS
    vZac       PLS_INTEGER := 1;
    vKon       PLS_INTEGER;
    vOperID    ClientTransactionStatusReports.OperId%TYPE;
    vChannelID ClientTransactionStatusReports.ChannelId%TYPE;
    vPackageId ClientTransactionStatusReports.PackageId%TYPE;
    vSeqNum    ClientTransactionStatusReports.SeqNum%TYPE;
    --
    PROCEDURE TransIdPiece (onPiece OUT NUMBER) IS
    BEGIN
      vKon:=instr(aTransactionID,'.',vZac);
      IF vKon=0 THEN
        onPiece:=TO_NUMBER(rtrim(substr(aTransactionID,vZac)));
        vZac:=LENGTH(aTransactionID)+1;
      ELSE
        onPiece:=TO_NUMBER(rtrim(substr(aTransactionID,vZac,vKon-vZac)));
        vZac:=vKon+1;
      END IF;
      IF onPiece IS NULL THEN
        RAISE_APPLICATION_ERROR(-20010,'Bad or mising TransactionID');
      END IF;
    EXCEPTION
      WHEN INVALID_NUMBER THEN
        RAISE_APPLICATION_ERROR(-20010,'Bad or mising TransactionID');
    END TransIdPiece;
    --
  BEGIN
     TransIdPiece(vOperID);
     TransIdPiece(vChannelID);
     TransIdPiece(vPackageId);
     TransIdPiece(vSeqNum);
     Put(vOperID,vChannelID,vPackageId,vSeqNum,to_number(aStatusId),aOriginator,aRelatedData,to_number(aErrorID),aReportSeq, aBankRef);
  END Put;
  --
  PROCEDURE Put(aOperId NUMBER,aChannelID NUMBER,aPackageID NUMBER,aSeqNum NUMBER,aStatusID NUMBER,
                aOriginator VARCHAR2,aRelatedData VARCHAR2:=NULL,aErrorID NUMBER := 0,aReport in Varchar2,aReportSeq IN OUT NUMBER) is
  begin
    Put(aOperId,aChannelID,aPackageId,aSeqNum,aStatusId,aOriginator,aRelatedData,aErrorID);
    aReportSeq:=gStatusReportId;
  end Put;
  --
  PROCEDURE PutTrn(aTransactionID VARCHAR2,aStatusID VARCHAR2,aOriginator VARCHAR2,aRelatedData VARCHAR2 := NULL,
                aErrorID NUMBER := 0,aReportSeq IN OUT NUMBER, aBankRef VARCHAR2 := NULL) IS
  BEGIN
    Put(aTransactionID,aStatusID,aOriginator,aRelatedData,to_char(aErrorID),aReportSeq,aBankRef);
    aReportSeq:=gStatusReportId;
  END PutTrn;
  --
  PROCEDURE Done(aReportseq NUMBER := null,aPackageId Number:=null,aLockAttr varchar2:=0) IS
    vClientId UpdateEvents.ClientId%TYPE;
  BEGIN
    IF (gStatusReportId is not null) or (aReportSeq is not null) THEN
      BEGIN
        SELECT ClientId INTO vClientId FROM ClientTransactionPackages WHERE PackageId = nvl(aPackageid,gPackageId);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN vClientId := NULL;
      END;
    END IF;
    if aReportseq is null then
       Init;
    end if;
  END Done;
  --
  PROCEDURE GetTransactions (
      pPackageId IN ClientTransactionPackages.PackageId%TYPE,
      pOperId    IN ClientTransactionPackages.OperId%TYPE,
      pTrnCur    OUT TrnCurTyp)
  IS
  BEGIN
    IF reftab_ot.HomeTableName(pOperId) = 'TRANSACTIONS' THEN
      OPEN pTrnCur FOR SELECT SeqNum, StatusId FROM Transactions WHERE PackageId = pPackageId;
    ELSE
      OPEN pTrnCur FOR SELECT 1 SeqNum, 1 StatusId FROM Dual WHERE 1=2;
    END IF;
  END GetTransactions;
  --
  PROCEDURE ChangePcgTrnStatus (
      pOperId        IN ClientTransactionPackages.OperId%TYPE,
      pChannelId     IN ClientTransactionPackages.ChannelId%TYPE,
      pPackageId     IN ClientTransactionPackages.PackageId%TYPE,
      pStatusId      IN ClientTransactionPackages.StatusId%TYPE,
      pOriginatorId  IN ClientTransactionStatusReports.Originator%TYPE)
  IS
    vTrnCur   TrnCurTyp;
    vSeqNum   Transactions.SeqNum%TYPE;
    vStatusId Transactions.StatusId%TYPE;
  BEGIN
    Init;
    IF reftab_ctst.Existing(pStatusId + 100) THEN
      GetTransactions (
          pPackageId => pPackageId,
          pOperId    => pOperId,
          pTrnCur    => vTrnCur);
      LOOP
        FETCH vTrnCur INTO vSeqNum, vStatusId;
        EXIT WHEN vTrnCur%NOTFOUND;
        Put(
            aOperId     => pOperId,
            aChannelId  => pChannelId,
            aPackageId  => pPackageId,
            aSeqNum     => vSeqNum,
            aStatusId   => pStatusId + 100,
            aOriginator => pOriginatorId);
      END LOOP;
      CLOSE vTrnCur;
    END IF;
    Put(
        aOperId     => pOperId,
        aChannelId  => pChannelId,
        aPackageId  => pPackageId,
        aSeqNum     => 0,
        aStatusId   => pStatusId,
        aOriginator => pOriginatorId);
    Done;
  END ChangePcgTrnStatus;
------------------------------------------------------------------------
 -- Procedura vrati XML report o validaci a chybach
  PROCEDURE GetValidationReport (
      onReturnCode    OUT pls_integer,
      orErrors        OUT sys_refcursor,
      ocReport        OUT NOCOPY Clob,
      inUserID   IN  Number,
      inChannelID     IN  Number,
      inPackageID     IN  ClientTransactionPackages.PackageId%TYPE
  )
  IS
    qryCtx    DBMS_XMLGEN.ctxHandle;
    lrMyCur   sys_refcursor;
    lnRight   pls_integer;
  BEGIN
    im_error.init;
    im_error.adderror(2113540,'You have no privileges for this operation.');
    onReturnCode:=1;
    im_error.geterrors(orErrors);
  END GetValidationReport;
------------------------------------------------------------------------
END REPORTCLIENTTRANSACTION;
/

PROMPT CREATE OR REPLACE PACKAGE WAC_CATALOGS
CREATE OR REPLACE 
PACKAGE WAC_CATALOGS
  IS
------------------
-- $release: 1964 $
-- $version: 8.0.0.1 $
------------------
-- $Header: $
------------------
--
--
-- MODIFICATION HISTORY
--
-- Person      Date       Comments
-- ----------- ---------- -----------------------------------------------------
-- Furdik      25.11.2015 Package creation, proc PutText and function ExistsTextInTab (20009213-108)
-- Furdik      02.12.2015 added procs RemText and RemNullTexts (20009213-108)
-- ----------------------------------------------------------------------------
--
  --
  TYPE tRefCursor IS REF CURSOR;
--
-- Procedure to remove all texts gor given TextID  - uses DELETE CASCADE constraint on TEXTS tab
--
PROCEDURE RemText (
      onReturnCode      OUT NUMBER,                 -- 0 - OK , 1 Error
      orErrors          OUT tRefCursor,
      inTextID          IN OUT LanguageTexts.textid%type,
      inRaiseAnyError   IN NUMBER DEFAULT 0);
--
-- Procedure to remove texts in Catalog - TextID set to null, delete from TEXTS tab
PROCEDURE RemNullTexts (
      onReturnCode      OUT NUMBER,                 -- 0 - OK , 1 Error
      orErrors          OUT tRefCursor,
      inTextID          IN OUT LanguageTexts.textid%type,
      ivListName        IN CATALOGATTRIBUTE.CATALOGCODE%type,
      ivColName         IN CATALOGATTRIBUTE.NAME%type,
      inRaiseAnyError   IN NUMBER DEFAULT 0);
--
-- Procedure to insert or update a text in the ListName table for given textid and langid
PROCEDURE PutText (
      onReturnCode      OUT NUMBER,                 -- 0 - OK , 1 Error
      orErrors          OUT tRefCursor,
      ivListName        IN CATALOG.CODE%type,
      inTextID          IN OUT LanguageTexts.textid%type,
      inLangID          IN LanguageTexts.langid%type,
      ivText            IN LanguageTexts.text%type,
      ivPurposeID       IN LanguageTexts.purposeid%type DEFAULT 'DEFAULT',  -- PurposeID  - parameter specifikuje ucel (DEFAULT - povodny text pre prislusne TID, LTD)
      inRaiseAnyError   IN NUMBER DEFAULT 0
);
--
-- test if the TextID column exists in a ListName table
FUNCTION ExistsTextInTab (
    ivListName        IN CATALOG.CODE%type
    ) RETURN NUMBER; -- 0/1
--
END WAC_CATALOGS;
/

CREATE OR REPLACE 
PACKAGE BODY WAC_CATALOGS
IS
------------------
-- $release: 1964 $
-- $version: 8.0.0.1 $
------------------
-- $Header: $
------------------
--
--
-- MODIFICATION HISTORY
--
-- Person      Date       Comments
-- ----------- ---------- -----------------------------------------------------
-- Furdik      25.11.2015 Package creation, proc PutText and function ExistsTextInTab (20009213-108)
-- Furdik      02.12.2015 added procs RemText and RemNullTexts (20009213-108)
-- ----------------------------------------------------------------------------
--
  --
--
-- Procedure to remove all texts gor given TextID  - uses DELETE CASCADE constraint on TEXTS tab
--
PROCEDURE RemText (
      onReturnCode      OUT NUMBER,                 -- 0 - OK , 1 Error
      orErrors          OUT tRefCursor,
      inTextID          IN OUT LanguageTexts.textid%type,
      inRaiseAnyError   IN NUMBER DEFAULT 0
)
IS
BEGIN
  im_error.Init;
  onReturnCode := 0;
  IF inTextID is not null THEN
     DELETE FROM TEXTS WHERE TextID = inTextID;
     if SQL%rowcount<1 then -- nothing to remove
        onReturnCode := 1;
        IF inRaiseAnyError = 1 THEN
          raise_application_error(-20103,'Text with TextID='||inTextID||' does not exist');
        ELSE
          im_error.AddError(inErrorId => 20103, ivText => 'Text with TextID does not exist',
          ivParam0 => 'TextID = '||to_char(inTextID), inRaiseAnyError => inRaiseAnyError);
          im_error.GetErrors (orErrors);
        END IF;
     end if;
  ELSE -- error - input TextID is null
     onReturnCode := 1;
        IF inRaiseAnyError = 1 THEN
          raise_application_error(-00001,'Error when removing text - input TextID is null');
        ELSE
          im_error.AddError(inErrorId => 00001, ivText => 'Error when removing text - input TextID is null',
          ivParam0 => 'TextID = '||to_char(inTextID), inRaiseAnyError => inRaiseAnyError);
          im_error.GetErrors (orErrors);
        END IF;
  END IF;
EXCEPTION
  WHEN im_error.REG_ANY_ERROR THEN
    onReturnCode := 1;
    IF inRaiseAnyError = 1 THEN RAISE; ELSE im_error.GetErrors (orErrors); END IF;
  WHEN OTHERS THEN
    onReturnCode := 1;
    IF inRaiseAnyError = 1 THEN RAISE; ELSE
       im_error.AddError(inErrorId => 00001, ivText => 'Error when removing text',
          ivParam0 => 'TextID = '||to_char(inTextID), inRaiseAnyError => inRaiseAnyError);
      im_error.GetErrors (orErrors);
    END IF;
END RemText;
--
-- Procedure to remove texts in Catalog - TextID set to null, delete from TEXTS tab
PROCEDURE RemNullTexts (
      onReturnCode      OUT NUMBER,                 -- 0 - OK , 1 Error
      orErrors          OUT tRefCursor,
      inTextID          IN OUT LanguageTexts.textid%type,
      ivListName        IN CATALOGATTRIBUTE.CATALOGCODE%type,
      ivColName         IN CATALOGATTRIBUTE.NAME%type,
      inRaiseAnyError   IN NUMBER DEFAULT 0
)
IS
  doProcessing pls_integer := 1;
  lnRemCheck pls_integer;
BEGIN
  im_error.Init;
  onReturnCode := 0;
  IF inTextID is null OR ivListName is null OR ivColName is null THEN
    doProcessing := 0;
  END IF;
  IF doProcessing = 1 THEN
     EXECUTE IMMEDIATE 'select count(*) from '||ivListName||' where '||ivColName||'='||inTextID INTO lnRemCheck;
     if lnRemCheck > 0 then
       EXECUTE IMMEDIATE 'UPDATE '||ivListName||' SET '||ivColName||'=null WHERE '||ivColName||'='||inTextID;
       DELETE FROM TEXTS WHERE TextID = inTextID;
     else
        onReturnCode := 1;
        IF inRaiseAnyError = 1 THEN
          raise_application_error(-20103,'Text with TextID='||inTextID||' does not exist');
        ELSE
          im_error.AddError(inErrorId => 20103, ivText => 'Text does not exist',
          ivParam0 => 'TextID = '||to_char(inTextID), ivParam1 => 'ListName = '||ivListName,
          ivParam2 => 'ColName = '||ivColName, inRaiseAnyError => inRaiseAnyError);
          im_error.GetErrors (orErrors);
        END IF;
     end if;
  ELSE -- error - TextID or ListName or ColName is null
     onReturnCode := 1;
        IF inRaiseAnyError = 1 THEN
          raise_application_error(-00001,'Error when removing text - some of required inputs is null');
        ELSE
          im_error.AddError(inErrorId => 00001, ivText => 'Error when removing text - some of required inputs is null',
          ivParam0 => 'TextID = '||to_char(inTextID), ivParam1 => 'ListName = '||ivListName,
          ivParam2 => 'ColName = '||ivColName, inRaiseAnyError => inRaiseAnyError);
          im_error.GetErrors (orErrors);
        END IF;
  END IF;
EXCEPTION
  WHEN im_error.REG_ANY_ERROR THEN
    onReturnCode := 1;
    IF inRaiseAnyError = 1 THEN RAISE; ELSE im_error.GetErrors (orErrors); END IF;
  WHEN OTHERS THEN
    onReturnCode := 1;
    IF inRaiseAnyError = 1 THEN RAISE; ELSE
       im_error.AddError(inErrorId => 00001, ivText => 'Error when removing text',
          ivParam0 => 'TextID = '||to_char(inTextID), ivParam1 => 'ListName = '||ivListName,
          ivParam2 => 'ColName = '||ivColName, inRaiseAnyError => inRaiseAnyError);
      im_error.GetErrors (orErrors);
    END IF;
END RemNullTexts;
--
-- Procedure to insert or update a text in the ListName table for given textid and langid
PROCEDURE PutText (
      onReturnCode      OUT NUMBER,                 -- 0 - OK , 1 Error
      orErrors          OUT tRefCursor,
      ivListName        IN CATALOG.CODE%type,
      inTextID          IN OUT LanguageTexts.textid%type,
      inLangID          IN LanguageTexts.langid%type,
      ivText            IN LanguageTexts.text%type,
      ivPurposeID       IN LanguageTexts.purposeid%type DEFAULT 'DEFAULT',  -- PurposeID  - parameter specifikuje ucel (DEFAULT - povodny text pre prislusne TID, LTD)
      inRaiseAnyError   IN NUMBER DEFAULT 0
)
IS
  lnUpdateTypeCheck pls_integer;
BEGIN
  im_error.Init;
  onReturnCode := 0;
  if inTextID is null then -- new record
    IF ivText is not null THEN
      inTextID := getnexttextid(ivListName);
      begin
        INSERT INTO LANGUAGETEXTS (LangID, TextID, Text, PurposeID)
          VALUES (inLangID, inTextID, ivText, ivPurposeID);
      exception when DUP_VAL_ON_INDEX THEN
        UPDATE LANGUAGETEXTS Set Text=ivText
          WHERE LangID=inLangID and TextID=inTextID and PurposeID=ivPurposeID;
      end;
    ELSE
      inTextID := null;
    END IF;
  else -- modify existing
      select count (*) into lnUpdateTypeCheck from LANGUAGETEXTS
      where LangID=inLangID and TextID=inTextID and PurposeID=ivPurposeID;
      if (lnUpdateTypeCheck = 0) then -- insert text for new language
        IF ivText is not null THEN
         INSERT INTO LANGUAGETEXTS (LangID, TextID, Text, PurposeID)
           VALUES (inLangID, inTextID, ivText, ivPurposeID);
        END IF;
      else --if (lnUpdateTypeCheck = 1) then -- update text for existing language
        IF ivText is not null THEN
         UPDATE LANGUAGETEXTS Set Text=ivText
           WHERE LangID=inLangID and TextID=inTextID and PurposeID=ivPurposeID;
        ELSE
         DELETE FROM LANGUAGETEXTS
           WHERE LangID=inLangID and TextID=inTextID and PurposeID=ivPurposeID;
         inTextID := null;
        END IF;
      end if;
  end if;
EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    onReturnCode := 1;
    IF inRaiseAnyError = 1 THEN RAISE;
    ELSE
      im_error.AddError(inErrorId => 00001, ivText => 'Unique constraint error',
        ivParam0 => 'TableName = '||ivListName, ivParam1 => 'LangID = '||inLangID,
        ivParam2 => 'TextID = '||inTextID, ivParam3 => 'PurposeID = '||ivPurposeID,
        inRaiseAnyError => inRaiseAnyError);
      im_error.GetErrors (orErrors);
    END IF;
  WHEN im_error.REG_ANY_ERROR THEN
    onReturnCode := 1;
    IF inRaiseAnyError = 1 THEN RAISE; ELSE im_error.GetErrors (orErrors); END IF;
END PutText;
--
-- test if the TextID column exists in a ListName table
FUNCTION ExistsTextInTab (
    ivListName        IN CATALOG.CODE%type
    )
RETURN NUMBER -- 0/1
IS
  v_column_exists number := 0;
BEGIN
  SELECT count(*) INTO v_column_exists FROM user_tab_cols
  WHERE column_name = 'TEXTID' AND table_name = ivListName;
  RETURN v_column_exists;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 0;
  WHEN OTHERS THEN
    RETURN 0;
END ExistsTextInTab;
--
END WAC_CATALOGS;
/

spool off

EXIT
