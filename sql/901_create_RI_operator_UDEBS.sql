set serveroutput on size 100000
spool 901_create_RI_operator_UDEBS.log

set define off

PROMPT Setup of new RI operators
declare
 lnBankClientID number;
 lnBankClientUserID number;
 lnBankClientRightProfileID number;
 procedure createRIOperator(ivFirstName in varchar2, ivLastName in varchar2, ivUserID in varchar2, ivPhoneNr in varchar2, ivEmail in varchar2) is
  tmp pls_integer;
 begin
  dbms_output.put_line('Create RI operator '||ivFirstName||' '||ivLastName);
  select count(*) into tmp from users where HOSTUSERID = ivUserID;
  if tmp=0 then
   INSERT INTO users (USERID,STATUSID,BORNDATE,LANGID,FIRSTNAME,LASTNAME,PERSONALNUM,CONTACTPHONE,CONTACTFAX,CONTACTGSM,CONTACTEMAIL,ADDRESS1,ADDRESS2,ADDRESS3,ADDRESS4,DMTIMESTAMP,HOSTUSERID,ENTITYTIMESTAMP) 
     VALUES (SEQ_USERS.nextval,1,sysdate,'en',ivFirstName,ivLastName,null,ivPhoneNr,null,ivPhoneNr,ivEmail,null,null,null,null,'0000000001 '||to_char(sysdate,'DD.MM.YYYY HH24:MI:SS'),ivUserID,TO_CHAR (SYSTIMESTAMP, 'YYYYMMDDHH24MISSFF9'));
   INSERT INTO clientusers (CLIENTUSERSID,CLIENTID,USERID,STATUSID,ENTITYTIMESTAMP) 
     VALUES (SEQ_CLIENTUSERS.nextval,lnBankClientID,SEQ_USERS.currval,1,TO_CHAR (SYSTIMESTAMP, 'YYYYMMDDHH24MISSFF9')) 
     returning CLIENTUSERSID into lnBankClientUserID;
   INSERT INTO clientuserchannelprofiles (CLIENTUSERSID,CHANNELID,CLIENTID,STATUSID,BORNDATE,DMTIMESTAMP,ENTITYTIMESTAMP) 
     VALUES (lnBankClientUserID,11,lnBankClientID,1,sysdate,'0000000001 '||to_char(sysdate,'DD.MM.YYYY HH24:MI:SS'),TO_CHAR (SYSTIMESTAMP, 'YYYYMMDDHH24MISSFF9'));
   INSERT INTO CLIENTUSERRIGHTPROFILES (clientuserrightprofileid, clientusersid, rightprofileid) 
     VALUES (SEQ_CLIENTUSERRIGHTPROFILES.nextval,lnBankClientUserID,lnBankClientRightProfileID);
   INSERT INTO RIGHTPROFILECLIENTCATEGORIES (rightprofileid, clientcategid) 
     SELECT lnBankClientRightProfileID, clientCategId from ClientCategories;
  end if;   
 end;
begin
 select to_number(value) into lnBankClientID from globalsystemparameter where paramname = 'TECHNICAL_BANK_CLIENT_ID';
 select rightprofileid into lnBankClientRightProfileID from clientrightprofiles where clientid = lnBankClientID and rightprofilename='MCH_ADMIN_FULL';
 -- predefined list of RI operators
 createRIOperator('Petr', 'Administrator', 'admin', '11111111', 'petradministrator@mailinator.com');
 --
 commit;
end;
/

spool off

EXIT