### Liquibase Migration for MCH (UDEBS)

This scripts are created from MCH RI (reference implementation) [sql scripts](https://bitbucket.org/bscideas/mch-ri-db-oracle/src/develop/sql/) 
where currently `102_upgrade_to_mch_9_0_0_UDEBS.sql` is the last script used.

   
## ORACLE   
```
java  -cp $HOME/.m2/repository/org/liquibase/liquibase-core/3.6.3/liquibase-core-3.6.3.jar:$HOME/.m2/repository/ch/qos/logback/logback-core/1.1.11/logback-core-1.1.11.jar:$HOME/.m2/repository/ch/qos/logback/logback-classic/1.1.11/logback-classic-1.1.11.jar:$HOME/.m2/repository/org/slf4j/slf4j-api/1.7.25/slf4j-api-1.7.25.jar \
      liquibase.integration.commandline.Main \
      --classpath=/Users/lukas/.m2/repository/com/oracle/ojdbc7/12.1.0.2/ojdbc7-12.1.0.2.jar:/Users/lukas/development/liquibase/target/liquibase.jar \
      --driver=oracle.jdbc.OracleDriver \
      --changeLogFile=db/changelog/mch-core/mch-core-liquibase-changelog-master.xml \
      --url="jdbc:oracle:thin:@//localhost:1521/LIQUIBASE" \
      --contexts=default,ddl,synonym,grant \
      --username=UDEBS \
      --password=udebspwd \
      updateSql
```
## H2
```
java  -cp $HOME/.m2/repository/org/liquibase/liquibase-core/3.6.3/liquibase-core-3.6.3.jar:$HOME/.m2/repository/ch/qos/logback/logback-core/1.1.11/logback-core-1.1.11.jar:$HOME/.m2/repository/ch/qos/logback/logback-classic/1.1.11/logback-classic-1.1.11.jar:$HOME/.m2/repository/org/slf4j/slf4j-api/1.7.25/slf4j-api-1.7.25.jar \
      liquibase.integration.commandline.Main \
      --classpath=/Users/lukas/.m2/repository/com/h2database/h2/1.4.197/h2-1.4.197.jar:/Users/lukas/development/liquibase/target/liquibase.jar \
      --driver=org.h2.Driver \
      --changeLogFile=db/changelog/mch-core/mch-core-liquibase-changelog-master.xml \
      --url="jdbc:h2:/tmp/testdb" \
      --contexts=default,ddl,synonym,grant \
      --username=sa \
      --password="" \
      updateSql
```
## PostgreSQL
```
java  -cp $HOME/.m2/repository/org/liquibase/liquibase-core/3.6.3/liquibase-core-3.6.3.jar:$HOME/.m2/repository/ch/qos/logback/logback-core/1.1.11/logback-core-1.1.11.jar:$HOME/.m2/repository/ch/qos/logback/logback-classic/1.1.11/logback-classic-1.1.11.jar:$HOME/.m2/repository/org/slf4j/slf4j-api/1.7.25/slf4j-api-1.7.25.jar \
      liquibase.integration.commandline.Main \
      --classpath=/Users/lukas/.m2/repository/org/postgresql/postgresql/42.2.5/postgresql-42.2.5.jar:/Users/lukas/development/liquibase/target/liquibase.jar \
      --driver=org.postgresql.Driver \
      --changeLogFile=db/changelog/mch-core/mch-core-liquibase-changelog-master.xml \
      --url="jdbc:postgresql://localhost:5432/udebs" \
      --contexts=default,ddl,synonym,grant \
      --username=udebs \
      --password=udebspwd \
      updateSql
```

## Diff
```
java  -cp $HOME/.m2/repository/org/liquibase/liquibase-core/3.6.3/liquibase-core-3.6.3.jar:$HOME/.m2/repository/ch/qos/logback/logback-core/1.1.11/logback-core-1.1.11.jar:$HOME/.m2/repository/ch/qos/logback/logback-classic/1.1.11/logback-classic-1.1.11.jar:$HOME/.m2/repository/org/slf4j/slf4j-api/1.7.25/slf4j-api-1.7.25.jar \
      liquibase.integration.commandline.Main \
        --classpath=/Users/lukas/.m2/repository/com/oracle/ojdbc7/12.1.0.2/ojdbc7-12.1.0.2.jar \
        --driver=oracle.jdbc.OracleDriver \
        --url=jdbc:oracle:thin:@//localhost:1521/UDEBS101 \
        --username=UDEBS \
        --password=udebspwd \
    diffChangeLog \
        --referenceUrl=jdbc:oracle:thin:@//localhost:1521/UDEBS102 \
        --referenceUsername=UDEBS \
        --referencePassword=udebspwd
```

## Helping queries
### check constraint
```
select '<changeSet id="changeme" author="system"><sql>ALTER TABLE '||table_name||' ADD CONSTRAINT '||constraint_name||' CHECK('||search_condition_vc||')</sql><rollback><sql>ALTER TABLE  '||table_name||' DROP CONSTRAINT '||constraint_name||' </sql></rollback></changeSet>'
 from all_constraints where owner = 'UDEBS' and constraint_type = 'C' and constraint_name not like 'SYS%' order by table_name;
```
### synonyms

### grants


### fix index names
```
set serveroutput on
declare
    cursor cur_idx is select i.index_name, replace(index_name, '$', '_') new_name from user_indexes i where index_name like '%$%' and index_name not like 'SYS%';
begin
    for idx in cur_idx 
    loop
        dbms_output.put_line('renaming index '||idx.index_name||' to '||idx.new_name);
        begin
            execute immediate('alter index '||idx.index_name||' rename to '||idx.new_name);
        exception when others then
            dbms_output.put_line('ERROR renaming index '||idx.index_name || ' '||SQLCODE||' -> '||SQLERRM );
        end;
    end loop;
end;
```

### fix constraint names
```
set serveroutput on
declare
    cursor cur_const is SELECT c.table_name, c.constraint_name, replace(constraint_name, '$', '_') new_name FROM user_constraints c where constraint_name like '%$%' and constraint_name not like 'SYS_%';
begin
    for constr in cur_const 
    loop
        dbms_output.put_line('renaming constraint '||constr.constraint_name||' to '||constr.new_name);
        begin
            execute immediate('alter table '||constr.table_name ||' rename constraint '||constr.constraint_name||'  to '||constr.new_name);
        exception when others then
            dbms_output.put_line('ERROR renaming constraint '||constr.constraint_name || ' '||SQLCODE||' -> '||SQLERRM );
        end;
    end loop;
end;
```

