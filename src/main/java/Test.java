import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


/**
 * @author Lukáš Vasek
 */
public class Test {


    public static void main(String[] args) throws IOException {
        Files.readAllLines(Paths.get("/Users/lukas/development/grants_gus.txt"))
                .stream()
                .forEach(line -> {
                    System.out.println("<changeSet author=\"system\" id=\"changeme\">");
                    System.out.println("<sql>GRANT " + String.format(line, "TO") + "</sql>");
                    System.out.println("<rollback>REVOKE " + String.format(line, "FROM") + "</rollback>");
                    System.out.println("</changeSet>");
                });
    }
}
