
  CREATE OR REPLACE EDITIONABLE TYPE "SUBTOTAL_ALL_OBJ" AS OBJECT(
    CurrencyID VARCHAR2(3), -- mena subtotalu
    Scheme VARCHAR2(10),    -- schema pre inkasa
    TranCount  NUMBER(10),  -- pocet transakci zahrnutych do subtotalu
    OrigCurrAmount NUMBER,  -- castka v mene subtotalu
    AccCurrAmount NUMBER    -- castka v mene uctu
)

