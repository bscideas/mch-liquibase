CREATE TYPE SUBTOTAL_ALL_OBJ AS (
    CurrencyID VARCHAR(3), -- mena subtotalu
    Scheme bigint,    -- schema pre inkasa
    TranCount  bigint,  -- pocet transakci zahrnutych do subtotalu
    OrigCurrAmount numeric,  -- castka v mene subtotalu
    AccCurrAmount numeric    -- castka v mene uctu
);