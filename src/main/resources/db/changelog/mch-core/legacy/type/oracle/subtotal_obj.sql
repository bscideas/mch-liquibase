
  CREATE OR REPLACE EDITIONABLE TYPE "SUBTOTAL_OBJ" AS OBJECT(
    CurrencyID VARCHAR2(3), -- mena subtotalu
    TranCount  NUMBER(10),  -- pocet transakci zahrnutych do subtotalu
    OrigCurrAmount NUMBER,  -- castka v mene subtotalu
    AccCurrAmount NUMBER    -- castka v mene uctu
)

