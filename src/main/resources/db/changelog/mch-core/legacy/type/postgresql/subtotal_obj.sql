CREATE TYPE SUBTOTAL_OBJ AS (
    CurrencyID VARCHAR(3), -- mena subtotalu
    TranCount  bigint,  -- pocet transakci zahrnutych do subtotalu
    OrigCurrAmount numeric,  -- castka v mene subtotalu
    AccCurrAmount numeric    -- castka v mene uctu
);