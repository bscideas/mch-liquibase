
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIU_MESSAGEATT$ENTITYTS"
 BEFORE
  INSERT OR UPDATE
 ON MESSAGEATTACHMENT
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
  SELECT to_char(systimestamp,'YYYYMMDDHH24MISSFF9')
  INTO :NEW.ENTITYTIMESTAMP
  FROM dual;
END;

