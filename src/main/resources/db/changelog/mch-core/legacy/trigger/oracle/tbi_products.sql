
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBI_PRODUCTS"
 BEFORE
  INSERT
 ON PRODUCTS
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
 WHEN (new.productid is null) Begin
  :new.productid := seq_products.nextval;
End;

