
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULELIMITS_D"
 INSTEAD OF
  DELETE
 ON VHF_CLIENTSIGNRULELIMITS
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :old.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with changeset table
    -- try to remove INSERT record
    DELETE FROM F_CLIENTSIGNRULELIMITS
    WHERE
      F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND F_ChangeType = 'INSERT'
      AND CLIENTSIGNRULELIMID=:old.CLIENTSIGNRULELIMID
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there was no INSERT record, so remove eventual UPDATE record and create DELETE record
      DELETE FROM F_CLIENTSIGNRULELIMITS
      WHERE
        F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND F_ChangeType = 'UPDATE'
        AND CLIENTSIGNRULELIMID=:old.CLIENTSIGNRULELIMID
      ;
      INSERT INTO F_CLIENTSIGNRULELIMITS (F_ExternalChangeSetId, F_ChangeType,
        CLIENTSIGNRULESEQ
        ,PERIOD
        ,AMOUNTLIMIT
        ,CLID
        ,CLIENTSIGNRULELIMID
      ) VALUES (:old.F_ExternalChangeSetId, 'DELETE',
        :old.CLIENTSIGNRULESEQ
        ,:old.PERIOD
        ,:old.AMOUNTLIMIT
        ,:old.CLID
        ,:old.CLIENTSIGNRULELIMID
      );
    END IF;
  ELSE
    IF :old.H_ValidTo IS NULL THEN
      -- work with live table
      DELETE FROM CLIENTSIGNRULELIMITS
      WHERE
        CLIENTSIGNRULELIMID=:old.CLIENTSIGNRULELIMID
      ;
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

