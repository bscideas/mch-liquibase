
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIU_USERS$ENTITYTIMESTAMP"
 BEFORE
  INSERT OR UPDATE
 ON USERS
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
  SELECT to_char(systimestamp,'YYYYMMDDHH24MISSFF9')
  INTO :NEW.ENTITYTIMESTAMP
  FROM dual;
END;

