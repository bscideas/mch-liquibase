
  CREATE OR REPLACE EDITIONABLE TRIGGER "TAIUD_SIGNRULEROLES$H"
 AFTER
  INSERT OR DELETE OR UPDATE
 ON SIGNRULEROLES
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE
lvChangeType varchar2(10);
BEGIN
IF INSERTING THEN lvChangeType :='INSERT';
insert into H_SIGNRULEROLES (
SIGNRULEROLEID
,h_CHANGETYPE
,CLIENTID
,ROLE
,SYSTEMFLAG
,DESCRIPTION
,h_VALIDFROM
,h_VALIDTO
) VALUES (
:NEW.SIGNRULEROLEID
, lvChangeType
,:NEW.CLIENTID
,:NEW.ROLE
,:NEW.SYSTEMFLAG
,:NEW.DESCRIPTION
,:NEW.h_VALIDFROM
, systimestamp
);
ELSIF UPDATING THEN lvChangeType :='UPDATE';
insert into H_SIGNRULEROLES (
SIGNRULEROLEID
,h_CHANGETYPE
,CLIENTID
,ROLE
,SYSTEMFLAG
,DESCRIPTION
,h_VALIDFROM
,h_VALIDTO
) VALUES (
:OLD.SIGNRULEROLEID
, lvChangeType
,:OLD.CLIENTID
,:OLD.ROLE
,:OLD.SYSTEMFLAG
,:OLD.DESCRIPTION
,:OLD.h_VALIDFROM
, systimestamp
);
ELSE lvChangeType :='DELETE';
insert into H_SIGNRULEROLES (
SIGNRULEROLEID
,h_CHANGETYPE
,CLIENTID
,ROLE
,SYSTEMFLAG
,DESCRIPTION
,h_VALIDFROM
,h_VALIDTO
) VALUES (
:OLD.SIGNRULEROLEID
, lvChangeType
,:OLD.CLIENTID
,:OLD.ROLE
,:OLD.SYSTEMFLAG
,:OLD.DESCRIPTION
,:OLD.h_VALIDFROM
, systimestamp
);
END IF;
END;

