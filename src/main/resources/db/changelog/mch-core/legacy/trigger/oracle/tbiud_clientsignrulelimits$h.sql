
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIUD_CLIENTSIGNRULELIMITS$H"
 BEFORE
  INSERT OR DELETE OR UPDATE
 ON CLIENTSIGNRULELIMITS
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE
  lvChangeType varchar2(10);
  ltCurrentTS  timestamp := systimestamp;
BEGIN
  IF INSERTING THEN
    lvChangeType     := 'INSERT';
    :NEW.H_validfrom := ltCurrentTS;
  ELSE
    IF UPDATING THEN
      lvChangeType     := 'UPDATE';
      :NEW.H_validfrom := ltCurrentTS;
    ELSE
      lvChangeType := 'DELETE';
    END IF;
    insert into h_ClientSignRuleLimits
      (H_CHANGETYPE,
       H_VALIDTO,
       CLIENTSIGNRULESEQ,
       PERIOD,
       AMOUNTLIMIT,
       CLID,
       CLIENTSIGNRULELIMID,
       H_VALIDFROM)
    VALUES
      (lvChangeType,
       ltCurrentTS,
       :OLD.CLIENTSIGNRULESEQ,
       :OLD.PERIOD,
       :OLD.AMOUNTLIMIT,
       :OLD.CLID,
       :OLD.CLIENTSIGNRULELIMID,
       :OLD.H_VALIDFROM);
  END IF;
END;

