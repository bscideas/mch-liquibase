
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIU_CCHP$ENTITYTIMESTAMP"
 BEFORE
  INSERT OR UPDATE
 ON CLIENTCHANNELPROFILES
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
  SELECT to_char(systimestamp,'YYYYMMDDHH24MISSFF9')
  INTO :NEW.ENTITYTIMESTAMP
  FROM dual;
END;

