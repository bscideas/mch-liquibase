
  CREATE OR REPLACE EDITIONABLE TRIGGER "TIB_BATCHFILES"
 BEFORE
  INSERT
 ON BATCHFILES
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
 WHEN (new.batchfileid is null) begin
  :new.batchfileid := seq_batchfiles.nextval;
end;

