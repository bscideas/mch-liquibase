
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIU_TRANSACTIONS_TMP$STATUS"
 BEFORE
   INSERT OR UPDATE OF STATUSID
 ON TRANSACTIONS_TMP
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin
  IF    (:NEW.STATUSID <> :OLD.STATUSID)
     OR (:NEW.STATUSID IS NOT NULL AND :OLD.STATUSID IS NULL)
     OR (:NEW.STATUSID IS NULL AND :OLD.STATUSID IS NOT NULL)
  THEN
    :NEW.LASTSTATUSUPDATE := systimestamp;
  END IF;
End;

