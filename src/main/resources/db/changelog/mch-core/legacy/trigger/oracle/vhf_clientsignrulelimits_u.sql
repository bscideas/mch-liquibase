
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULELIMITS_U"
 INSTEAD OF
  UPDATE
 ON VHF_CLIENTSIGNRULELIMITS
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :new.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with changeset table
    -- try to update existing UPDATE record
    UPDATE F_CLIENTSIGNRULELIMITS
    SET
      CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
      ,PERIOD=:new.PERIOD
      ,AMOUNTLIMIT=:new.AMOUNTLIMIT
      ,CLID=:new.CLID
      ,CLIENTSIGNRULELIMID=:new.CLIENTSIGNRULELIMID
    WHERE
      F_ExternalChangeSetId = :new.F_ExternalChangeSetId AND (F_ChangeType = 'UPDATE' OR F_ChangeType = 'INSERT')
      AND CLIENTSIGNRULELIMID=:new.CLIENTSIGNRULELIMID
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there is no existing UPDATE record, create new one
      INSERT INTO F_CLIENTSIGNRULELIMITS (F_ExternalChangeSetId, F_ChangeType,
        CLIENTSIGNRULESEQ
        ,PERIOD
        ,AMOUNTLIMIT
        ,CLID
        ,CLIENTSIGNRULELIMID
      ) VALUES (:new.F_ExternalChangeSetId, 'UPDATE',
        :new.CLIENTSIGNRULESEQ
        ,:new.PERIOD
        ,:new.AMOUNTLIMIT
        ,:new.CLID
        ,:new.CLIENTSIGNRULELIMID
      );
    END IF;
  ELSE
    IF :new.H_ValidTo IS NULL THEN
      -- work with live table
      UPDATE CLIENTSIGNRULELIMITS SET
        CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
        ,PERIOD=:new.PERIOD
        ,AMOUNTLIMIT=:new.AMOUNTLIMIT
        ,CLID=:new.CLID
        ,CLIENTSIGNRULELIMID=:new.CLIENTSIGNRULELIMID
      WHERE
        CLIENTSIGNRULELIMID=:new.CLIENTSIGNRULELIMID
      ;
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

