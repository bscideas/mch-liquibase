
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULESDEF_U"
 INSTEAD OF
  UPDATE
 ON VHF_CLIENTSIGNRULESDEF
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :new.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with changeset table
    -- try to update existing UPDATE record
    UPDATE F_CLIENTSIGNRULESDEF
    SET
      CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
      ,USERID=:new.USERID
      ,SIGNRULEROLEID=:new.SIGNRULEROLEID
      ,USERORDER=:new.USERORDER
      ,REQUIREDSIGNATURESCOUNT=:new.REQUIREDSIGNATURESCOUNT
    WHERE
      F_ExternalChangeSetId = :new.F_ExternalChangeSetId AND (F_ChangeType = 'UPDATE' OR F_ChangeType = 'INSERT')
      AND CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
      AND USERORDER=:new.USERORDER
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there is no existing UPDATE record, create new one
      INSERT INTO F_CLIENTSIGNRULESDEF (F_ExternalChangeSetId, F_ChangeType,
        CLIENTSIGNRULESEQ
        ,USERID
        ,SIGNRULEROLEID
        ,USERORDER
        ,REQUIREDSIGNATURESCOUNT
      ) VALUES (:new.F_ExternalChangeSetId, 'UPDATE',
        :new.CLIENTSIGNRULESEQ
        ,:new.USERID
        ,:new.SIGNRULEROLEID
        ,:new.USERORDER
        ,:new.REQUIREDSIGNATURESCOUNT
      );
    END IF;
  ELSE
    IF :new.H_ValidTo IS NULL THEN
      -- work with live table
      UPDATE CLIENTSIGNRULESDEF SET
        CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
        ,USERID=:new.USERID
        ,SIGNRULEROLEID=:new.SIGNRULEROLEID
        ,USERORDER=:new.USERORDER
        ,REQUIREDSIGNATURESCOUNT=:new.REQUIREDSIGNATURESCOUNT
      WHERE
        CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
        AND USERORDER=:new.USERORDER
      ;
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

