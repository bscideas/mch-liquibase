
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULEACCOUNT_I"
 INSTEAD OF
  INSERT
 ON VHF_CLIENTSIGNRULEACCOUNT
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  recCount INTEGER;
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :new.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with future data table
    -- try to remove DELETE record
    DELETE FROM F_CLIENTSIGNRULEACCOUNT
    WHERE
      F_ExternalChangeSetId = :new.F_ExternalChangeSetId AND F_ChangeType = 'DELETE'
      AND ACCOUNTACCESSID=:new.ACCOUNTACCESSID
      AND CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there was no DELETE record, thus create INSERT record
      -- at first, check uniqueness
      SELECT COUNT(*) INTO recCount FROM V_CLIENTSIGNRULEACCOUNT
      WHERE
        ACCOUNTACCESSID=:new.ACCOUNTACCESSID
        AND CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
        AND ((:new.H_ValidTo IS NULL AND H_ValidTo IS NULL) OR (:new.H_ValidTo >= H_ValidFrom AND :new.H_ValidTo < H_ValidTo));
      IF recCount > 0 THEN
        RAISE DUP_VAL_ON_INDEX;
      END IF;
      INSERT INTO F_CLIENTSIGNRULEACCOUNT (F_ExternalChangeSetId, F_ChangeType,
        ACCOUNTACCESSID
        ,CLIENTSIGNRULESEQ
      ) VALUES (:new.F_ExternalChangeSetId, 'INSERT',
        :new.ACCOUNTACCESSID
        ,:new.CLIENTSIGNRULESEQ
      );
    ELSE
      -- there was DELETE record... so create UPDATE one
      INSERT INTO F_CLIENTSIGNRULEACCOUNT (F_ExternalChangeSetId, F_ChangeType,
        ACCOUNTACCESSID
        ,CLIENTSIGNRULESEQ
      ) VALUES (:new.F_ExternalChangeSetId, 'UPDATE',
        :new.ACCOUNTACCESSID
        ,:new.CLIENTSIGNRULESEQ
      );
    END IF;
  ELSE
    IF :new.H_ValidTo IS NULL THEN
      -- work with live table
      INSERT INTO CLIENTSIGNRULEACCOUNT (
        ACCOUNTACCESSID
        ,CLIENTSIGNRULESEQ
      ) VALUES (
        :new.ACCOUNTACCESSID
        ,:new.CLIENTSIGNRULESEQ
      );
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

