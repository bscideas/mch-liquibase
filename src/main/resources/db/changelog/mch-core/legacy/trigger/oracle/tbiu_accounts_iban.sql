
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIU_ACCOUNTS_IBAN"
 BEFORE
  INSERT OR UPDATE
 ON ACCOUNTS
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
 WHEN (NEW.IBAN IS NULL) Declare
    lvBankId       VARCHAR2(34);
    lvCountryId    VARCHAR2(2);
    lvAccnum       Accounts.AccNum%TYPE;
Begin
 begin
    lvBankId := reftab_gsp('LocalBankCode',0,'0000');
    SELECT COUNTRYID
      INTO lvCountryId
      FROM BANKS A,
           MEMBERBANKS B
     WHERE a.BICCODE   = b.BICCODE
       AND b.BANKCODE = lvBankId;
 exception
  when others then
    if lvBankId = '0900' then
      lvCountryId:='SK';
    else
      lvCountryId:='CZ';
    end if;
 end;
 IF :NEW.ACCNUM IS NOT NULL THEN
   IF instr(:NEW.ACCNUM,'/') > 0 THEN
     lvAccnum:= substr(:NEW.ACCNUM, 1, instr(:NEW.ACCNUM,'/')-1);
   ELSE
     lvAccnum:= :NEW.ACCNUM;
   END IF;
   IF INSERTING THEN
      :NEW.IBAN := GetIBAN(substr(lvBankId,1,4),lvAccnum,lvCountryId);
   ELSIF UPDATING  THEN
    IF NVL(:NEW.ACCNUM,'0')<>NVL(:OLD.ACCNUM,'0') OR :NEW.IBAN IS NULL THEN
      :NEW.IBAN := GetIBAN(substr(lvBankId,1,4),lvAccnum,lvCountryId);
    END IF;
   END IF;
 END IF;
exception
 when OTHERS then
   null;
End;

