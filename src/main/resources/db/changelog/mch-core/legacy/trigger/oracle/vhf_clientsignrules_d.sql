
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULES_D"
 INSTEAD OF
  DELETE
 ON VHF_CLIENTSIGNRULES
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :old.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with changeset table
    -- try to remove INSERT record
    DELETE FROM F_CLIENTSIGNRULES
    WHERE
      F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND F_ChangeType = 'INSERT'
      AND CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there was no INSERT record, so remove eventual UPDATE record and create DELETE record
      DELETE FROM F_CLIENTSIGNRULES
      WHERE
        F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND F_ChangeType = 'UPDATE'
        AND CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
      ;
      INSERT INTO F_CLIENTSIGNRULES (F_ExternalChangeSetId, F_ChangeType,
        ATTR1
        ,ATTR2
        ,ATTR3
        ,ENTITYTIMESTAMP
        ,CLIENTSIGNRULESEQ
        ,CLIENTID
        ,STATUSID
        ,AMOUNTLIMIT
        ,AMOUNTLIMITCURRID
        ,ADDNEWACCOUNT
        ,ORDERIMPORTANT
        ,VALIDFROM
        ,VALIDUNTIL
        ,RULEDESCRIPTION
        ,OVERRUNPERMITTED
        ,MINIMALSIGNATURES
      ) VALUES (:old.F_ExternalChangeSetId, 'DELETE',
        :old.ATTR1
        ,:old.ATTR2
        ,:old.ATTR3
        ,TO_CHAR (SYSTIMESTAMP, 'YYYYMMDDHH24MISSFF9')
        ,:old.CLIENTSIGNRULESEQ
        ,:old.CLIENTID
        ,:old.STATUSID
        ,:old.AMOUNTLIMIT
        ,:old.AMOUNTLIMITCURRID
        ,:old.ADDNEWACCOUNT
        ,:old.ORDERIMPORTANT
        ,:old.VALIDFROM
        ,:old.VALIDUNTIL
        ,:old.RULEDESCRIPTION
        ,:old.OVERRUNPERMITTED
        ,:old.MINIMALSIGNATURES
      );
    END IF;
  ELSE
    IF :old.H_ValidTo IS NULL THEN
      -- work with live table
      DELETE FROM CLIENTSIGNRULES
      WHERE
        CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
      ;
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

