
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBI_CLIENTACCOUNTRESTR"
 BEFORE
  INSERT
 ON CLIENTACCOUNTRESTRICTIONS
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
 WHEN (new.clientaccountrestrictionid is null) begin
    :new.clientaccountrestrictionid := seq_clientaccountrestr.nextval;
end;

