
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIU_OPERID_WORKINGDAYS"
 BEFORE
   INSERT OR UPDATE OF OPERID, PAYMENTSYSTEMID
 ON WORKINGDAYS
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin
  if :new.operid is not null and :new.paymentsystemid is not null then
    Raise_Application_error(-20402,'You cannot fill OperID and PaymentSystemID');
  end if;
End;

