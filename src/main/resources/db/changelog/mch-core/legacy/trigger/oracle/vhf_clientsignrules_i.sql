
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULES_I"
 INSTEAD OF
  INSERT
 ON VHF_CLIENTSIGNRULES
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  recCount INTEGER;
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :new.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with future data table
    -- try to remove DELETE record
    DELETE FROM F_CLIENTSIGNRULES
    WHERE
      F_ExternalChangeSetId = :new.F_ExternalChangeSetId AND F_ChangeType = 'DELETE'
      AND CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there was no DELETE record, thus create INSERT record
      -- at first, check uniqueness
      SELECT COUNT(*) INTO recCount FROM V_CLIENTSIGNRULES
      WHERE
        CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
        AND ((:new.H_ValidTo IS NULL AND H_ValidTo IS NULL) OR (:new.H_ValidTo >= H_ValidFrom AND :new.H_ValidTo < H_ValidTo));
      IF recCount > 0 THEN
        RAISE DUP_VAL_ON_INDEX;
      END IF;
      INSERT INTO F_CLIENTSIGNRULES (F_ExternalChangeSetId, F_ChangeType,
        ATTR1
        ,ATTR2
        ,ATTR3
        ,ENTITYTIMESTAMP
        ,CLIENTSIGNRULESEQ
        ,CLIENTID
        ,STATUSID
        ,AMOUNTLIMIT
        ,AMOUNTLIMITCURRID
        ,ADDNEWACCOUNT
        ,ORDERIMPORTANT
        ,VALIDFROM
        ,VALIDUNTIL
        ,RULEDESCRIPTION
        ,OVERRUNPERMITTED
        ,MINIMALSIGNATURES
      ) VALUES (:new.F_ExternalChangeSetId, 'INSERT',
        :new.ATTR1
        ,:new.ATTR2
        ,:new.ATTR3
        ,TO_CHAR (SYSTIMESTAMP, 'YYYYMMDDHH24MISSFF9')
        ,:new.CLIENTSIGNRULESEQ
        ,:new.CLIENTID
        ,:new.STATUSID
        ,:new.AMOUNTLIMIT
        ,:new.AMOUNTLIMITCURRID
        ,:new.ADDNEWACCOUNT
        ,:new.ORDERIMPORTANT
        ,:new.VALIDFROM
        ,:new.VALIDUNTIL
        ,:new.RULEDESCRIPTION
        ,:new.OVERRUNPERMITTED
        ,:new.MINIMALSIGNATURES
      );
    ELSE
      -- there was DELETE record... so create UPDATE one
      INSERT INTO F_CLIENTSIGNRULES (F_ExternalChangeSetId, F_ChangeType,
        ATTR1
        ,ATTR2
        ,ATTR3
        ,ENTITYTIMESTAMP
        ,CLIENTSIGNRULESEQ
        ,CLIENTID
        ,STATUSID
        ,AMOUNTLIMIT
        ,AMOUNTLIMITCURRID
        ,ADDNEWACCOUNT
        ,ORDERIMPORTANT
        ,VALIDFROM
        ,VALIDUNTIL
        ,RULEDESCRIPTION
        ,OVERRUNPERMITTED
        ,MINIMALSIGNATURES
      ) VALUES (:new.F_ExternalChangeSetId, 'UPDATE',
        :new.ATTR1
        ,:new.ATTR2
        ,:new.ATTR3
        ,TO_CHAR (SYSTIMESTAMP, 'YYYYMMDDHH24MISSFF9')
        ,:new.CLIENTSIGNRULESEQ
        ,:new.CLIENTID
        ,:new.STATUSID
        ,:new.AMOUNTLIMIT
        ,:new.AMOUNTLIMITCURRID
        ,:new.ADDNEWACCOUNT
        ,:new.ORDERIMPORTANT
        ,:new.VALIDFROM
        ,:new.VALIDUNTIL
        ,:new.RULEDESCRIPTION
        ,:new.OVERRUNPERMITTED
        ,:new.MINIMALSIGNATURES
      );
    END IF;
  ELSE
    IF :new.H_ValidTo IS NULL THEN
      -- work with live table
      INSERT INTO CLIENTSIGNRULES (
        ATTR1
        ,ATTR2
        ,ATTR3
        ,ENTITYTIMESTAMP
        ,CLIENTSIGNRULESEQ
        ,CLIENTID
        ,STATUSID
        ,AMOUNTLIMIT
        ,AMOUNTLIMITCURRID
        ,ADDNEWACCOUNT
        ,ORDERIMPORTANT
        ,VALIDFROM
        ,VALIDUNTIL
        ,RULEDESCRIPTION
        ,OVERRUNPERMITTED
        ,MINIMALSIGNATURES
      ) VALUES (
        :new.ATTR1
        ,:new.ATTR2
        ,:new.ATTR3
        ,:new.ENTITYTIMESTAMP
        ,:new.CLIENTSIGNRULESEQ
        ,:new.CLIENTID
        ,:new.STATUSID
        ,:new.AMOUNTLIMIT
        ,:new.AMOUNTLIMITCURRID
        ,:new.ADDNEWACCOUNT
        ,:new.ORDERIMPORTANT
        ,:new.VALIDFROM
        ,:new.VALIDUNTIL
        ,:new.RULEDESCRIPTION
        ,:new.OVERRUNPERMITTED
        ,:new.MINIMALSIGNATURES
      );
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

