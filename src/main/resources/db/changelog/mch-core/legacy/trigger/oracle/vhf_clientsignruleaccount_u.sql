
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULEACCOUNT_U"
 INSTEAD OF
  UPDATE
 ON VHF_CLIENTSIGNRULEACCOUNT
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :new.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with changeset table
    -- try to update existing UPDATE record
    UPDATE F_CLIENTSIGNRULEACCOUNT
    SET
      ACCOUNTACCESSID=:new.ACCOUNTACCESSID
      ,CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
    WHERE
      F_ExternalChangeSetId = :new.F_ExternalChangeSetId AND (F_ChangeType = 'UPDATE' OR F_ChangeType = 'INSERT')
      AND ACCOUNTACCESSID=:new.ACCOUNTACCESSID
      AND CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there is no existing UPDATE record, create new one
      INSERT INTO F_CLIENTSIGNRULEACCOUNT (F_ExternalChangeSetId, F_ChangeType,
        ACCOUNTACCESSID
        ,CLIENTSIGNRULESEQ
      ) VALUES (:new.F_ExternalChangeSetId, 'UPDATE',
        :new.ACCOUNTACCESSID
        ,:new.CLIENTSIGNRULESEQ
      );
    END IF;
  ELSE
    IF :new.H_ValidTo IS NULL THEN
      -- work with live table
      UPDATE CLIENTSIGNRULEACCOUNT SET
        ACCOUNTACCESSID=:new.ACCOUNTACCESSID
        ,CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
      WHERE
        ACCOUNTACCESSID=:new.ACCOUNTACCESSID
        AND CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
      ;
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

