
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULECHANNEL_U"
 INSTEAD OF
  UPDATE
 ON VHF_CLIENTSIGNRULECHANNEL
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :new.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with changeset table
    -- try to update existing UPDATE record
    UPDATE F_CLIENTSIGNRULECHANNEL
    SET
      CHANNELID=:new.CHANNELID
      ,CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
    WHERE
      F_ExternalChangeSetId = :new.F_ExternalChangeSetId AND (F_ChangeType = 'UPDATE' OR F_ChangeType = 'INSERT')
      AND CHANNELID=:new.CHANNELID
      AND CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there is no existing UPDATE record, create new one
      INSERT INTO F_CLIENTSIGNRULECHANNEL (F_ExternalChangeSetId, F_ChangeType,
        CHANNELID
        ,CLIENTSIGNRULESEQ
      ) VALUES (:new.F_ExternalChangeSetId, 'UPDATE',
        :new.CHANNELID
        ,:new.CLIENTSIGNRULESEQ
      );
    END IF;
  ELSE
    IF :new.H_ValidTo IS NULL THEN
      -- work with live table
      UPDATE CLIENTSIGNRULECHANNEL SET
        CHANNELID=:new.CHANNELID
        ,CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
      WHERE
        CHANNELID=:new.CHANNELID
        AND CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
      ;
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

