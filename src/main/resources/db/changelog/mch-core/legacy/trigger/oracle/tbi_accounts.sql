
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBI_ACCOUNTS"
 BEFORE
  INSERT
 ON ACCOUNTS
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
 WHEN (new.accountid is null) Begin
  :new.accountid := seq_accounts.nextval;
End;

