
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIUD_CLIENTSIGNRULECHANNEL$H"
 BEFORE
  INSERT OR DELETE OR UPDATE
 ON CLIENTSIGNRULECHANNEL
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE
  lvChangeType varchar2(10);
  ltCurrentTS  timestamp := systimestamp;
BEGIN
  IF INSERTING THEN
    lvChangeType     := 'INSERT';
    :NEW.H_validfrom := ltCurrentTS;
  ELSE
    IF UPDATING THEN
      lvChangeType     := 'UPDATE';
      :NEW.H_validfrom := ltCurrentTS;
    ELSE
      lvChangeType := 'DELETE';
    END IF;
    insert into h_ClientSignRuleChannel
      (H_CHANGETYPE,
       H_VALIDTO,
       CHANNELID,
       CLIENTSIGNRULESEQ,
       H_VALIDFROM)
    VALUES
      (lvChangeType,
       ltCurrentTS,
       :OLD.CHANNELID,
       :OLD.CLIENTSIGNRULESEQ,
       :OLD.H_VALIDFROM);
  END IF;
END;

