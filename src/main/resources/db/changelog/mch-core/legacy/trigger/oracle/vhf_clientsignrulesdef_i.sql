
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULESDEF_I"
 INSTEAD OF
  INSERT
 ON VHF_CLIENTSIGNRULESDEF
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  recCount INTEGER;
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :new.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with future data table
    -- try to remove DELETE record
    DELETE FROM F_CLIENTSIGNRULESDEF
    WHERE
      F_ExternalChangeSetId = :new.F_ExternalChangeSetId AND F_ChangeType = 'DELETE'
      AND CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
      AND USERORDER=:new.USERORDER
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there was no DELETE record, thus create INSERT record
      -- at first, check uniqueness
      SELECT COUNT(*) INTO recCount FROM V_CLIENTSIGNRULESDEF
      WHERE
        CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
        AND USERORDER=:new.USERORDER
        AND ((:new.H_ValidTo IS NULL AND H_ValidTo IS NULL) OR (:new.H_ValidTo >= H_ValidFrom AND :new.H_ValidTo < H_ValidTo));
      IF recCount > 0 THEN
        RAISE DUP_VAL_ON_INDEX;
      END IF;
      INSERT INTO F_CLIENTSIGNRULESDEF (F_ExternalChangeSetId, F_ChangeType,
        CLIENTSIGNRULESEQ
        ,USERID
        ,SIGNRULEROLEID
        ,USERORDER
        ,REQUIREDSIGNATURESCOUNT
      ) VALUES (:new.F_ExternalChangeSetId, 'INSERT',
        :new.CLIENTSIGNRULESEQ
        ,:new.USERID
        ,:new.SIGNRULEROLEID
        ,:new.USERORDER
        ,:new.REQUIREDSIGNATURESCOUNT
      );
    ELSE
      -- there was DELETE record... so create UPDATE one
      INSERT INTO F_CLIENTSIGNRULESDEF (F_ExternalChangeSetId, F_ChangeType,
        CLIENTSIGNRULESEQ
        ,USERID
        ,SIGNRULEROLEID
        ,USERORDER
        ,REQUIREDSIGNATURESCOUNT
      ) VALUES (:new.F_ExternalChangeSetId, 'UPDATE',
        :new.CLIENTSIGNRULESEQ
        ,:new.USERID
        ,:new.SIGNRULEROLEID
        ,:new.USERORDER
        ,:new.REQUIREDSIGNATURESCOUNT
      );
    END IF;
  ELSE
    IF :new.H_ValidTo IS NULL THEN
      -- work with live table
      INSERT INTO CLIENTSIGNRULESDEF (
        CLIENTSIGNRULESEQ
        ,USERID
        ,SIGNRULEROLEID
        ,USERORDER
        ,REQUIREDSIGNATURESCOUNT
      ) VALUES (
        :new.CLIENTSIGNRULESEQ
        ,:new.USERID
        ,:new.SIGNRULEROLEID
        ,:new.USERORDER
        ,:new.REQUIREDSIGNATURESCOUNT
      );
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

