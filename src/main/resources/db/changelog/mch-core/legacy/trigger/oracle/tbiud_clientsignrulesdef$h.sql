
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIUD_CLIENTSIGNRULESDEF$H"
 BEFORE
  INSERT OR DELETE OR UPDATE
 ON CLIENTSIGNRULESDEF
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE
  lvChangeType varchar2(10);
  ltCurrentTS  timestamp := systimestamp;
BEGIN
  IF INSERTING THEN
    lvChangeType   := 'INSERT';
    :NEW.H_validfrom := ltCurrentTS;
  ELSE
    IF UPDATING THEN
      lvChangeType   := 'UPDATE';
      :NEW.H_validfrom := ltCurrentTS;
    ELSE
      lvChangeType := 'DELETE';
    END IF;
    insert into h_ClientSignRulesDef
      (H_CHANGETYPE,
       H_VALIDTO,
       CLIENTSIGNRULESEQ,
       USERID,
       SIGNRULEROLEID,
       USERORDER,
       REQUIREDSIGNATURESCOUNT,
       H_VALIDFROM)
    VALUES
      (lvChangeType,
       ltCurrentTS,
       :OLD.CLIENTSIGNRULESEQ,
       :OLD.USERID,
       :OLD.SIGNRULEROLEID,
       :OLD.USERORDER,
       :OLD.REQUIREDSIGNATURESCOUNT,
       :OLD.H_VALIDFROM);
  END IF;
END;

