
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIUD_CLIENTUSERSIGNRULEROLE$H"
 BEFORE
  INSERT OR DELETE OR UPDATE
 ON CLIENTUSERSIGNRULEROLE
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE
lvChangeType varchar2(10);
ltCurrentTS  timestamp:=systimestamp;
BEGIN
IF INSERTING THEN lvChangeType :='INSERT';
:NEW.h_validfrom := ltCurrentTS;
ELSIF UPDATING THEN lvChangeType :='UPDATE';
insert into h_ClientUserSignRuleRole (
h_CHANGETYPE
,CLIENTUSERSID
,ENTITYTIMESTAMP
,SIGNRULEROLEID
,h_VALIDFROM
,h_VALIDTO
) VALUES (
lvChangeType
,:OLD.CLIENTUSERSID
,null
,:OLD.SIGNRULEROLEID
,:OLD.h_VALIDFROM
, ltCurrentTS
);
:NEW.h_validfrom := ltCurrentTS;
ELSE lvChangeType :='DELETE';
insert into h_ClientUserSignRuleRole (
h_CHANGETYPE
,CLIENTUSERSID
,ENTITYTIMESTAMP
,SIGNRULEROLEID
,h_VALIDFROM
,h_VALIDTO
) VALUES (
lvChangeType
,:OLD.CLIENTUSERSID
,null
,:OLD.SIGNRULEROLEID
,:OLD.h_VALIDFROM
, ltCurrentTS
);
END IF;
END;

