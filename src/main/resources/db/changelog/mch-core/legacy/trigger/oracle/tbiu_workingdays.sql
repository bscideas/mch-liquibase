
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIU_WORKINGDAYS"
 BEFORE
   INSERT OR UPDATE OF MASK
 ON WORKINGDAYS
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
 WHEN (Length(new.mask)<>365) Declare
   Xx PLS_Integer:=to_number(to_char(to_date(:NEW.YEAR||'1231','YYYYMMDD'),'DDD'));
   Yy PLS_Integer:=length(:NEW.Mask);
Begin
   If yy=366 and xx=365 then
      :NEW.Mask:=Substr(:NEW.Mask,1,59)||Substr(:NEW.Mask,61);
   elsif not (yy in (365,366)) then
      Raise_Application_error(-20401,'Invalid Mask Length');
   end if;
End;

