
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIU_CLIENTTRANSACTIONPACK$ETS"
 BEFORE
  INSERT OR UPDATE
 ON CLIENTTRANSACTIONPACKAGES
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
  :new.entitytimestamp := TO_CHAR (SYSTIMESTAMP, 'YYYYMMDDHH24MISSFF9');
END;

