
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIU_CLIENTUSERSIGNRULEROLE$ET"
 BEFORE
  INSERT OR UPDATE
 ON CLIENTUSERSIGNRULEROLE
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
  if :new.entitytimestamp is not null and substr(:new.entitytimestamp,1,5)='KEEP:' then
    :new.entitytimestamp := substr(:new.entitytimestamp,6);
    if :new.entitytimestamp is null then
      :new.entitytimestamp := TO_CHAR (SYSTIMESTAMP, 'YYYYMMDDHH24MISSFF9');
    end if;
  else
    :new.entitytimestamp := TO_CHAR (SYSTIMESTAMP, 'YYYYMMDDHH24MISSFF9');
  end if;  
END;

