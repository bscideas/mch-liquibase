
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULESDEF_D"
 INSTEAD OF
  DELETE
 ON VHF_CLIENTSIGNRULESDEF
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :old.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with changeset table
    -- try to remove INSERT record
    DELETE FROM F_CLIENTSIGNRULESDEF
    WHERE
      F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND F_ChangeType = 'INSERT'
      AND CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
      AND USERORDER=:old.USERORDER
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there was no INSERT record, so remove eventual UPDATE record and create DELETE record
      DELETE FROM F_CLIENTSIGNRULESDEF
      WHERE
        F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND F_ChangeType = 'UPDATE'
        AND CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
        AND USERORDER=:old.USERORDER
      ;
      INSERT INTO F_CLIENTSIGNRULESDEF (F_ExternalChangeSetId, F_ChangeType,
        CLIENTSIGNRULESEQ
        ,USERID
        ,SIGNRULEROLEID
        ,USERORDER
        ,REQUIREDSIGNATURESCOUNT
      ) VALUES (:old.F_ExternalChangeSetId, 'DELETE',
        :old.CLIENTSIGNRULESEQ
        ,:old.USERID
        ,:old.SIGNRULEROLEID
        ,:old.USERORDER
        ,:old.REQUIREDSIGNATURESCOUNT
      );
    END IF;
  ELSE
    IF :old.H_ValidTo IS NULL THEN
      -- work with live table
      DELETE FROM CLIENTSIGNRULESDEF
      WHERE
        CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
        AND USERORDER=:old.USERORDER
      ;
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

