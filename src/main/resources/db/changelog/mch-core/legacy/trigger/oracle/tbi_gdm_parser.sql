
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBI_GDM_PARSER"
 BEFORE
  INSERT
 ON GDM_PARSER
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Begin
-- $release: 079 $
-- $version: 8.0.0.0 $
  :new.iifattr := upper(:new.iifattrname);
End;

