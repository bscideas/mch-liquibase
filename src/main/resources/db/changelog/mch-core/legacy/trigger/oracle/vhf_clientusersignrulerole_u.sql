
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTUSERSIGNRULEROLE_U"
 INSTEAD OF
  UPDATE
 ON VHF_CLIENTUSERSIGNRULEROLE
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :new.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with changeset table
    -- try to update existing UPDATE record
    UPDATE F_ClientUserSignRuleRole
    SET
       CLIENTUSERSID = :new.CLIENTUSERSID
      ,SIGNRULEROLEID = :new.SIGNRULEROLEID
      ,ENTITYTIMESTAMP = TO_CHAR (SYSTIMESTAMP, 'YYYYMMDDHH24MISSFF9')
    WHERE
      F_ExternalChangeSetId = :new.F_ExternalChangeSetId AND
      (F_ChangeType = 'UPDATE' OR F_ChangeType = 'INSERT') AND
      CLIENTUSERSID = :new.CLIENTUSERSID;
    IF SQL%ROWCOUNT = 0 THEN
      -- there is no existing UPDATE record, create new one
      INSERT INTO F_ClientUserSignRuleRole (F_ExternalChangeSetId, F_ChangeType
        ,CLIENTUSERSID
        ,SIGNRULEROLEID
        ,ENTITYTIMESTAMP
      ) VALUES (:new.F_ExternalChangeSetId, 'UPDATE'
        ,:new.CLIENTUSERSID
        ,:new.SIGNRULEROLEID
        ,TO_CHAR (SYSTIMESTAMP, 'YYYYMMDDHH24MISSFF9')
      );
    END IF;
  ELSE
    IF :new.H_ValidTo IS NULL THEN
      -- work with live table
      UPDATE ClientUserSignRuleRole
      SET
         CLIENTUSERSID = :new.CLIENTUSERSID
        ,SIGNRULEROLEID = :new.SIGNRULEROLEID
        ,ENTITYTIMESTAMP = :new.ENTITYTIMESTAMP
      WHERE
        CLIENTUSERSID= :new.CLIENTUSERSID;
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

