
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTUSERSIGNRULEROLE_D"
 INSTEAD OF
  DELETE
 ON VHF_CLIENTUSERSIGNRULEROLE
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :old.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with changeset table
    -- try to remove INSERT record
    DELETE FROM F_ClientUserSignRuleRole
    WHERE
      F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND
      F_ChangeType = 'INSERT' AND
      CLIENTUSERSID = :old.CLIENTUSERSID;
    IF SQL%ROWCOUNT = 0 THEN
      -- there was no INSERT record, so remove eventual UPDATE record and create DELETE record
      DELETE FROM F_ClientUserSignRuleRole
      WHERE
        F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND
        F_ChangeType = 'UPDATE' AND
        CLIENTUSERSID = :old.CLIENTUSERSID;
      INSERT INTO F_ClientUserSignRuleRole (F_ExternalChangeSetId, F_ChangeType
        ,CLIENTUSERSID
        ,SIGNRULEROLEID
        ,ENTITYTIMESTAMP
      ) VALUES (:old.F_ExternalChangeSetId, 'DELETE'
        ,:old.CLIENTUSERSID
        ,:old.SIGNRULEROLEID
        ,TO_CHAR (SYSTIMESTAMP, 'YYYYMMDDHH24MISSFF9')
      );
    END IF;
  ELSE
    IF :old.H_ValidTo IS NULL THEN
      -- work with live table
      DELETE FROM ClientUserSignRuleRole
      WHERE CLIENTUSERSID = :old.CLIENTUSERSID;
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

