
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBI_MESSAGE"
 BEFORE
  INSERT
 ON MESSAGE
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
 WHEN (new.MessageID is NULL) Begin
  :NEW.MessageID := SEQ_Message.nextval;
End;

