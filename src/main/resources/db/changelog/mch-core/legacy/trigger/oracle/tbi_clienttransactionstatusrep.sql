
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBI_CLIENTTRANSACTIONSTATUSREP"
 BEFORE
  INSERT
 ON CLIENTTRANSACTIONSTATUSREPORTS
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
 WHEN (NEW.STATUSREPORTID is null) Begin
   :NEW.STATUSREPORTID := SEQ_CLIENTTRANSACTIONSTATUSREP.NextVAL;
End;

