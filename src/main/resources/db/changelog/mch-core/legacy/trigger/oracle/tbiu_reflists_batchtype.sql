
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIU_REFLISTS_BATCHTYPE"
 BEFORE
   INSERT OR UPDATE OF LISTVALUE
 ON REFLISTS
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
 WHEN (new.ListName = 'BATCHTYPE') DECLARE
  lnOperID OperationTypes.OperID%TYPE;
BEGIN
  lnOperID:=to_number(substr(:new.listvalue,1,instr(:new.listvalue,'.')-1));
  SELECT OperID INTO lnOperID FROM OperationTypes
   WHERE OperID=lnOperID AND Batch=1;
EXCEPTION
  WHEN others THEN
    raise_application_error(-20001,'Invalid BatchID. It must start with OperID, which has the attribute batch=1, and next character must be ''.''');
END;

