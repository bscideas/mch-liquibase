
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBI_SIGNATUREARCHIVE"
 BEFORE
  INSERT
 ON SIGNATUREARCHIVE
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
 WHEN (new.SIGNATURESEQ is NULL) Begin
  :NEW.SIGNATURESEQ := SEQ_SIGNATUREARCHIVE.nextval;
End;

