
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULEOPER_U"
 INSTEAD OF
  UPDATE
 ON VHF_CLIENTSIGNRULEOPER
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :new.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with changeset table
    -- try to update existing UPDATE record
    UPDATE F_CLIENTSIGNRULEOPER
    SET
      CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
      ,OPERID=:new.OPERID
      ,AMOUNTLIMITCURRENCYID=:new.AMOUNTLIMITCURRENCYID
    WHERE
      F_ExternalChangeSetId = :new.F_ExternalChangeSetId AND (F_ChangeType = 'UPDATE' OR F_ChangeType = 'INSERT')
      AND CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
      AND OPERID=:new.OPERID
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there is no existing UPDATE record, create new one
      INSERT INTO F_CLIENTSIGNRULEOPER (F_ExternalChangeSetId, F_ChangeType,
        CLIENTSIGNRULESEQ
        ,OPERID
        ,AMOUNTLIMITCURRENCYID
      ) VALUES (:new.F_ExternalChangeSetId, 'UPDATE',
        :new.CLIENTSIGNRULESEQ
        ,:new.OPERID
        ,:new.AMOUNTLIMITCURRENCYID
      );
    END IF;
  ELSE
    IF :new.H_ValidTo IS NULL THEN
      -- work with live table
      UPDATE CLIENTSIGNRULEOPER SET
        CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
        ,OPERID=:new.OPERID
        ,AMOUNTLIMITCURRENCYID=:new.AMOUNTLIMITCURRENCYID
      WHERE
        CLIENTSIGNRULESEQ=:new.CLIENTSIGNRULESEQ
        AND OPERID=:new.OPERID
      ;
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

