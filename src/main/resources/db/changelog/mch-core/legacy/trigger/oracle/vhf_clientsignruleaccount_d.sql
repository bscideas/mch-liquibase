
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULEACCOUNT_D"
 INSTEAD OF
  DELETE
 ON VHF_CLIENTSIGNRULEACCOUNT
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :old.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with changeset table
    -- try to remove INSERT record
    DELETE FROM F_CLIENTSIGNRULEACCOUNT
    WHERE
      F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND F_ChangeType = 'INSERT'
      AND ACCOUNTACCESSID=:old.ACCOUNTACCESSID
      AND CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there was no INSERT record, so remove eventual UPDATE record and create DELETE record
      DELETE FROM F_CLIENTSIGNRULEACCOUNT
      WHERE
        F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND F_ChangeType = 'UPDATE'
        AND ACCOUNTACCESSID=:old.ACCOUNTACCESSID
        AND CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
      ;
      INSERT INTO F_CLIENTSIGNRULEACCOUNT (F_ExternalChangeSetId, F_ChangeType,
        ACCOUNTACCESSID
        ,CLIENTSIGNRULESEQ
      ) VALUES (:old.F_ExternalChangeSetId, 'DELETE',
        :old.ACCOUNTACCESSID
        ,:old.CLIENTSIGNRULESEQ
      );
    END IF;
  ELSE
    IF :old.H_ValidTo IS NULL THEN
      -- work with live table
      DELETE FROM CLIENTSIGNRULEACCOUNT
      WHERE
        ACCOUNTACCESSID=:old.ACCOUNTACCESSID
        AND CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
      ;
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

