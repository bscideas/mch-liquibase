
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULECHANNEL_D"
 INSTEAD OF
  DELETE
 ON VHF_CLIENTSIGNRULECHANNEL
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :old.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with changeset table
    -- try to remove INSERT record
    DELETE FROM F_CLIENTSIGNRULECHANNEL
    WHERE
      F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND F_ChangeType = 'INSERT'
      AND CHANNELID=:old.CHANNELID
      AND CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there was no INSERT record, so remove eventual UPDATE record and create DELETE record
      DELETE FROM F_CLIENTSIGNRULECHANNEL
      WHERE
        F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND F_ChangeType = 'UPDATE'
        AND CHANNELID=:old.CHANNELID
        AND CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
      ;
      INSERT INTO F_CLIENTSIGNRULECHANNEL (F_ExternalChangeSetId, F_ChangeType,
        CHANNELID
        ,CLIENTSIGNRULESEQ
      ) VALUES (:old.F_ExternalChangeSetId, 'DELETE',
        :old.CHANNELID
        ,:old.CLIENTSIGNRULESEQ
      );
    END IF;
  ELSE
    IF :old.H_ValidTo IS NULL THEN
      -- work with live table
      DELETE FROM CLIENTSIGNRULECHANNEL
      WHERE
        CHANNELID=:old.CHANNELID
        AND CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
      ;
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

