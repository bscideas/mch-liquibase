
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIU_TRANSACTIONS"
 BEFORE
  INSERT OR UPDATE
 ON TRANSACTIONS
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE
  v_clob CLOB;
  v_operid NUMBER;
BEGIN
  IF INSERTING THEN
    -- TransactionID ma format operID.ChannelID.PackageID.SeqNum
    IF (INSTR(:NEW.transactionid, '.', 1, 3) = 0) THEN
      :NEW.transactionid := :NEW.transactionid || '.' || TO_CHAR(:NEW.seqnum);
    END IF;
  END IF;
  -- LastUpdate should by always set by system!
  :NEW.lastupdate := SYSDATE;
  -- LastStatusUpdate should be set to a new value when StatusID is changed
  IF (:NEW.statusid != :OLD.statusid)
     OR (:NEW.statusid IS NOT NULL AND :OLD.statusid IS NULL)
     OR (:NEW.statusid IS NULL AND :OLD.statusid IS NOT NULL) THEN
    :NEW.laststatusupdate := SYSTIMESTAMP;
  END IF;
  -- additional indexed columns are always filled from Trandata appropriate elements
  IF :NEW.trandata IS NULL THEN
    :NEW.valuedate := NULL;
    :NEW.amount := NULL;
    :NEW.currencyid := NULL;
    :NEW.paymentpriority := NULL;
  ELSE
    SELECT operid
      INTO v_operid
      FROM CLIENTTRANSACTIONPACKAGES
     WHERE PACKAGEID = :NEW.PACKAGEID;
    IF :NEW.trandata.existsnode('/TranData/ValueDate') > 0 THEN
      DECLARE
        d VARCHAR2(10) := SUBSTR(LTRIM(:NEW.trandata.extract('/TranData/ValueDate/text()')
                                       .getStringVal()),
                                 1,
                                 10);
      BEGIN
        :NEW.valuedate := TO_DATE(d, 'YYYY-MM-DD');
      EXCEPTION
        WHEN OTHERS THEN
          :NEW.valuedate := TO_DATE(d, 'DD.MM.YYYY');
      END;
    ELSE
      :NEW.valuedate := NULL;
    END IF; -- IF :NEW.trandata.existsnode('/TranData/ValueDate') > 0
    --
    IF v_operid IN (333, 355, 370) THEN
      IF :NEW.trandata.existsnode('/TranData/MaxAmount') > 0 THEN
        DECLARE
          n VARCHAR2(50) := TRIM(:NEW.trandata.extract('/TranData/MaxAmount/text()').getStringVal());
        BEGIN
          :NEW.amount := TO_NUMBER(REPLACE(n, ',', '.'));
        EXCEPTION
          WHEN OTHERS THEN
            :NEW.amount := TO_NUMBER(REPLACE(n, '.', ','));
        END;
      ELSE
        :NEW.amount := NULL;
      END IF; -- IF :NEW.trandata.existsnode('/TranData/MaxAmount') > 0
    ELSE
      IF :NEW.trandata.existsnode('/TranData/Amount') > 0 THEN
        DECLARE
          n VARCHAR2(50) := TRIM(:NEW.trandata.extract('/TranData/Amount/text()').getStringVal());
        BEGIN
          :NEW.amount := TO_NUMBER(REPLACE(n, ',', '.'));
        EXCEPTION
          WHEN OTHERS THEN
            :NEW.amount := TO_NUMBER(REPLACE(n, '.', ','));
        END;
      ELSE
        :NEW.amount := NULL;
      END IF; -- IF :NEW.trandata.existsnode('/TranData/Amount') > 0
    END IF;
    --
    IF :NEW.trandata.existsnode('/TranData/CurrencyID') > 0 THEN
      :NEW.currencyid := :NEW.trandata.extract('/TranData/CurrencyID/text()').getStringVal();
    ELSE
      :NEW.currencyid := NULL;
    END IF;
    --
    IF :NEW.trandata.existsnode('/TranData/PaymentPriority') > 0 THEN
      :NEW.paymentpriority := TO_NUMBER(:NEW.trandata.extract('/TranData/PaymentPriority/text()')
                                        .getStringVal());
    ELSE
      :NEW.paymentpriority := NULL;
    END IF;

    IF v_operid IN (756) THEN
      IF :NEW.trandata.existsnode('/TranData/CurrencyId') > 0 THEN
        :NEW.currencyid := :NEW.trandata.extract('/TranData/CurrencyId/text()').getStringVal();
      ELSE
        :NEW.currencyid := NULL;
      END IF;
    END IF;
    --
  END IF; -- IF :NEW.trandata IS NULL
EXCEPTION
  WHEN OTHERS THEN
    v_clob := 'transactionid ' || :NEW.transactionid || ', NEW statusid ' || :NEW.statusid ||
              ', OLD transactionid ' || :OLD.statusid || ', seqnum ' || :NEW.seqnum ||
              ', packageid ' || :NEW.packageid || ', trandata ';
    v_clob := v_clob || :NEW.trandata.getclobval();
    journal001.ERROR_LOG_AUTONOMOUS(p_OriginatorID => 'TBIU_TRANSACTIONS', p_Details => v_clob);
END;

