
  CREATE OR REPLACE EDITIONABLE TRIGGER "VHF_CLIENTSIGNRULEOPER_D"
 INSTEAD OF
  DELETE
 ON VHF_CLIENTSIGNRULEOPER
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
  view_read_only EXCEPTION;
  PRAGMA EXCEPTION_INIT(view_read_only, -01732);
BEGIN
  IF :old.F_ExternalChangeSetId IS NOT NULL THEN
    -- work with changeset table
    -- try to remove INSERT record
    DELETE FROM F_CLIENTSIGNRULEOPER
    WHERE
      F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND F_ChangeType = 'INSERT'
      AND CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
      AND OPERID=:old.OPERID
    ;
    IF SQL%ROWCOUNT = 0 THEN
      -- there was no INSERT record, so remove eventual UPDATE record and create DELETE record
      DELETE FROM F_CLIENTSIGNRULEOPER
      WHERE
        F_ExternalChangeSetId = :old.F_ExternalChangeSetId AND F_ChangeType = 'UPDATE'
        AND CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
        AND OPERID=:old.OPERID
      ;
      INSERT INTO F_CLIENTSIGNRULEOPER (F_ExternalChangeSetId, F_ChangeType,
        CLIENTSIGNRULESEQ
        ,OPERID
        ,AMOUNTLIMITCURRENCYID
      ) VALUES (:old.F_ExternalChangeSetId, 'DELETE',
        :old.CLIENTSIGNRULESEQ
        ,:old.OPERID
        ,:old.AMOUNTLIMITCURRENCYID
      );
    END IF;
  ELSE
    IF :old.H_ValidTo IS NULL THEN
      -- work with live table
      DELETE FROM CLIENTSIGNRULEOPER
      WHERE
        CLIENTSIGNRULESEQ=:old.CLIENTSIGNRULESEQ
        AND OPERID=:old.OPERID
      ;
    ELSE
      -- it is not possible to really change historical data
      RAISE view_read_only;
    END IF;
  END IF;
END;

