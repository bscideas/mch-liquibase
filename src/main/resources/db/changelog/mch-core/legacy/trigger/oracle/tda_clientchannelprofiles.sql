
  CREATE OR REPLACE EDITIONABLE TRIGGER "TDA_CLIENTCHANNELPROFILES"
 AFTER
  DELETE
 ON CLIENTCHANNELPROFILES
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
Declare
   MUTATION EXCEPTION;
   PRAGMA EXCEPTION_INIT(MUTATION, -4091);
   cursor c1 is
     select cu.ClientUsersID
       from ClientUsers cu
      where cu.ClientID = :OLD.ClientID;
Begin
   for r1 in c1 LOOP
     Delete from ClientUserChannelProfiles
           where ChannelID = :OLD.ChannelID
             and ClientUsersID = r1.ClientUsersID;
   end LOOP;
   /*
   Delete from ClientRights
         where ClientID = :OLD.ClientID
           and RightID in (select RightID
                             from RightTypes
                            where ChannelID = :OLD.ChannelID);
    -- only right profiles will be cleaned from this rights
    */
    Delete from rightprofiledefinitions where clientrightid in (
       select clientrightid from ClientRights
         where ClientID = :OLD.ClientID and RightID in (select RightID from RightTypes  where ChannelID = :OLD.ChannelID));
Exception
   When MUTATION then null;
End;

