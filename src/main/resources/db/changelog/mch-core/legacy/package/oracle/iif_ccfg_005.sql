
  CREATE OR REPLACE EDITIONABLE PACKAGE "IIF_CCFG_005"
AUTHID DEFINER
IS
------------------
-- $release: 6673 $
-- $version: 8.0.50.3 $
------------------
--
-- Purpose: Podpisova pravidla 2.generace
-- Posledni patch z UDEBS: 2598
-- $Header: /Oracle/UDEBS_XPE/UDEBSXPE.IIF_CCFG_005.PSK 3     2.07.12 17:16 Pataky $
-- MODIFICATION HISTORY
-- Person       Date        Comments
-- ----------   ----------  ------------------------------------------
-- Ilenin       07.07.2014  UDEBS RS
-- Ilenin       23.11.2014  20009183-1042
-- Bocak        26.02.2015  PutSDClientSignRules - chenged erro message for -20001 (20009183-1719)
-- Pataky       04.03.2015  VerifySufficiencySigning - corrected sign.rule evaluation (20009183-1705,20009183-1845)
-- Pataky       04.03.2015  ExistValidSignatureRules - corrected sign.rule selecting (20009183-1705,20009183-1755,20009183-1845)
-- Pataky       05.03.2015  GetValidSignatureRules, SetOrChangeSignatureRule, VerifySignatureRule - exceptions for operId 781 (20009183-1853)
-- Pataky       09.03.2015  PutSDClientSignRules - changed error message Err01
-- Pataky       19.03.2015  ExistValidSignatureRules - corrected sign.rule selecting for ordered signers - 20009183-2047
-- Pataky       20.03.2015  VerifySufficiencySigning - corrected sign.rule evaluation (20009183-2047)
-- Pataky       26.03.2015  VerifySufficiencySigning,GetValidSignatureRules,VerifySignatureRule - unified limit rounding (20009183-2126, 20009183-2136)
-- Pataky       30.04.2015  ExistValidSignatureRules - exception for exchange rates problem added (20009183-2607)
-- Pataky       04.05.2015  ClientSignRules.AmountLimit can be null, evaluations fixed - 20009183-2477
-- Bocak        13.05.2015  VerifySufficiencySigning - set CTP.SigneValDate to SYSDATE when transaction is PARTSIG (20009183-2631)
-- Pataky       22.05.2015  Reviewed using of RESULT CACHE hint in select statements - 20009183-2902
-- Snederfler   25.05.2015  PutSDClientSignRulesDef - optimization
-- Bocak        28.07.2015  ExistValidSignatureRules, GetValidSignatureRules - new input parameter inCheckLimit (20009183-1703)
-- Bocak        26.08.2015  GetValidSignatureRules, SetOrChangeSignatureRule, VerifySignatureRule - exceptions for operId 786 (20009183-4361)
-- Pataky       28.08.2015  GetSumTotal - added exception for RB for own accounts operations, excluded from limit - 20009203-1329
-- Pataky       09.10.2015  VerifySignatureRule, GetValidSignatureRules - added exception for RB for own accounts operations, excluded from limit - 20009203-2317
-- Pataky       09.11.2015  Package amount can be recounted into local currency, based on parameter (because of multicurrency accounts)
-- Pataky       11.12.2015  VerifySufficiencySigning - keep status PARTSIG after multiple rules checking, if signature is attached
-- Emmer        16.12.2015  Removal of links to obsolete journal attributes 20009203-1354
-- Pataky       15.04.2016  Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-- Pataky       13.06.2016  VerifySufficiencySigning - keep status PARTSIG after multiple rules checking fix - 20009203-5429
-- Pataky       21.07.2016  New procedure GetValidSignRules to get sign.rules for external systems - 20009203-5706
-- Bocak        16.08.2016  GetValidSignatureRules - select data from ctp_tmp if package status is 33 - 20009238-282
-- Bocak        24.08.2016  GetValidSignatureRules, SetOrChangeSignatureRule, VerifySignatureRule - exceptions for operId 780 - 20009248-584
-- Pataky       16.09.2016  Removed LastLoginTime from Users table, fixed GetValidSignRules
-- Pataky       13.10.2016  Added OperationTypeChannels support into getOperationsForSignRule, ExistValidSignatureRules, GetValidSignatureRules, VerifySignatureRule procedures - 20009213-2049
-- Furdik       18.11.2016  New procedure GetValidSignRulesUsers - returns users for valid signature rules - 20009239-783
-- Pataky       07.12.2016  GetValidSignRules procedure fixed (MCHGIT-1052) - 20009203-6748
-- Pataky       09.12.2016  GetValidSignRules procedure updated - added clients attributes (MCHGIT-1052) - 20009203-6748
-- Snederfler   15.12.2016  GetValidSignRulesForClient and GetValidSignRulesForUser procedures updated - added ValidFrom and ValidUntil attributes - 20009203-6612
-- Furdik       18.05.2017  SetOrChangeSignatureRule add inJournal, VerifySufficiencySigning add inReport - P20009245-4389
-- Emmer        17.07.2017  GetValidSignRules hotfix optimalization (MYG-5227)
-- Emmer        17.07.2017  VerifySignatureRule, GetSumTotal optimalization (MYG-5262)
-- Emmer        20.07.2017  GetValidSignRules optimalization
-- Emmer        12.12.2017  Add new function ExistValidSignatureRules2 - P20009299-199
-- Emmer        15.01.2018  Add new function ExistValidSignatureRules3 (MYG-7042) - P20009299-265
-- ----------   ----------  ------------------------------------------
-- typy pro GUS procedury
type gus_signaturerule is record (
    SignRuleID      number,
    RuleDescription VARCHAR2(50),
    CurrencyID      VARCHAR2(3),
    limit_immediate number,
    limit_day       NUMBER,
    limit_month     number,
    limit_year      number,
    Status          number,
    OrderImportant  NUMBER
);
type gus_signaturestatus is record (
    PackageID       number,
    UserID          number,
    UserOrder       number,
    SignatureSeq    number
);
-- typy pre jednotlive tabulky
-----------------------
--TABLE CLIENTSIGNRULES
-----------------------
type t_rec_clientsignrules is record
 (
  clientsignruleseq          CLIENTSIGNRULES.clientsignruleseq%type,
  clientid                   CLIENTSIGNRULES.clientid%type,
  statusid                   CLIENTSIGNRULES.statusid%type,
  amountlimit                CLIENTSIGNRULES.amountlimit%type,
  amountlimitcurrid          CLIENTSIGNRULES.amountlimitcurrid%type,
  addnewaccount              CLIENTSIGNRULES.addnewaccount%type,
  orderimportant             CLIENTSIGNRULES.orderimportant%type,
  validfrom                  CLIENTSIGNRULES.validfrom%type,
  validuntil                 CLIENTSIGNRULES.validuntil%type,
  ruledescription            CLIENTSIGNRULES.ruledescription%TYPE,
  overrunpermitted           CLIENTSIGNRULES.OVERRUNPERMITTED%TYPE
 );
type t_arr_clientsignrules is table of t_rec_clientsignrules index by binary_integer;
---------------------------
-- TABLE CLIENTSIGNRULESDEF
---------------------------
type t_rec_clientsignrulesdef is record
 (
  clientsignruleseq          CLIENTSIGNRULESDEF.clientsignruleseq%type,
  userid                     CLIENTSIGNRULESDEF.clientsignruleseq%type,
  signruleroleid             CLIENTSIGNRULESDEF.signruleroleid%type,
  userorder                  CLIENTSIGNRULESDEF.clientsignruleseq%type,
  RequiredSignaturesCount    CLIENTSIGNRULESDEF.requiredsignaturescount%type
 );
type t_arr_clientsignrulesdef is table of t_rec_clientsignrulesdef index by binary_integer;
---------------------------
-- TABLE CLIENTSIGNRULEOPER
---------------------------
type t_rec_clientsignrulesoper is record
 (
  clientsignruleseq          CLIENTSIGNRULEOPER.clientsignruleseq%type,
  operid                     CLIENTSIGNRULEOPER.operid%type
 );
type t_arr_clientsignrulesoper is table of t_rec_clientsignrulesoper index by binary_integer;
---------------------------
-- TABLE CLIENTSIGNRULEACCOUNT
---------------------------
type t_rec_clientsignrulesaccount is record
 (
  accountaccessid            CLIENTSIGNRULEACCOUNT.accountaccessid%type,
  clientsignruleseq          CLIENTSIGNRULEACCOUNT.clientsignruleseq%type
 );
type t_arr_clientsignrulesaccount is table of t_rec_clientsignrulesaccount index by binary_integer;
---------------------------
-- TABLE clientsignrulelimits
---------------------------
type t_rec_ClientSignRuleslimit is record
 (
 clientsignruleseq      clientsignrulelimits.clientsignruleseq%type,
 period         clientsignrulelimits.period%type,
 amountlimit        clientsignrulelimits.amountlimit%type
 );
type t_arr_clientsignruleslimit is table of t_rec_clientsignruleslimit index by binary_integer;
---------------------------
-- TABLE SignRuleRoles
---------------------------
type t_rec_ClientSignRuleRole is record
 (
  signruleroleid    signruleroles.signruleroleid%type,
  role              signruleroles.role%type,
  clientid          signruleroles.clientid%type,
  systemflag        signruleroles.systemflag%type,
  description       signruleroles.description%type
 );
type t_arr_ClientSignRuleRole is table of t_rec_ClientSignRuleRole index by binary_integer;
--------------------------------------------------------------------
-- PRO APLIKACNI SERVER (GUS)
--------------------------------------------------------------------
procedure GetValidSignRulesForClient (  -- vraci vsechna podpisova pravidla klienta pro jejich spravu
    inClientID      in  Clients.ClientID%type,       -- Klient prihlaseneho Usera
    orVSRtab        out sys_refcursor                -- seznam pravidel
);
--------------------------------------------------------------------
procedure GetValidSignatureRules (  -- vraci vsechna prave platna podpisova pravidla, ktera mohou byt pouzita
    inClientID      in  Clients.ClientID%type,      -- Klient prihlaseneho Usera
    inPackageID     in  ClientTransactionPackages.PackageID%type, -- Package, ktere se priradi jedno pravidlo ze seznamu
    inCertMethodID  in  pls_integer,
    orVSRtab        out sys_refcursor,              -- seznam pravidel
    inAddNullPK     in  pls_integer default 1,       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
    inUserID   in  Users.userid%TYPE default NULL,
    inCheckLimit    IN  PLS_INTEGER DEFAULT 1,        -- 0/1 - nekontrolovat/kontrolovat limit
    inChannelID     in  OperationTypeChannels.ChannelID%TYPE DEFAULT NULL
);
--------------------------------------------------------------------
procedure GetValidSignatureRule (                               -- single varianta pro PK
    inSignRuleSeq   in  ClientSignRules.ClientSignRuleSeq%type, -- PK
    orVSRtab        out sys_refcursor,
    inAddNullPK     in  pls_integer default 1       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
);
--------------------------------------------------------------------
procedure GetSignatureRuleForPackage (                          -- vraci pravidlo prirazene package
    onReturnCode    out Number,
    orErrors        out sys_refcursor,
    orSignRule      out sys_refcursor,
    inUserID   in  Users.userid%TYPE,
    inChannelID     in  Channels.ChannelID%type,
    inPackageID     in  ClientTransactionPackages.PackageID%type,
    idCheckoutDate  IN  DATE,
    inOpenError     in  Pls_integer default 1
);
--------------------------------------------------------------------
procedure SetOrChangeSignatureRule(                             -- nastavuje package zvolene podpisove pravidlo
    onReturnCode    out number,
    orErrors        out sys_refcursor,
    inUserID   in  Users.userid%TYPE,
    inChannelID     in  Channels.ChannelID%type,
    inPackageID     in  ClientTransactionPackages.PackageID%type,
    idCheckoutDate  in  date,                                   -- vyplnen => pracuje nad CTP_TMP (pri editaci)
    ivSignRuleSeq   in  varchar2,
    inOpenError     in  Pls_integer default 1,
    inJournal       IN  PLS_INTEGER DEFAULT 1
); -- s kontrolou prav
--------------------------------------------------------------------
procedure VerifySignatureRule( -- testuje, zda prirazene pravidlo je stale platne
    inPackageID   in  ClientTransactionPackages.PackageID%type,
    inSignRuleSeq in  ClientTransactionPackages.SignRuleSeq%type,
    onReturnCode  out number,
    inChannelID   in  OperationTypeChannels.ChannelID%TYPE DEFAULT NULL
);
--------------------------------------------------------------------
procedure VerifySufficiencySigning( -- testuje podpisy podle pravidla
    inPackageID   in  ClientTransactionPackages.PackageID%type,
    inSignRuleSeq in  ClientTransactionPackages.SignRuleSeq%type,
    onReturnCode  out number,
    orErrors      out sys_refcursor,
    inOpenError   in  Pls_integer default 1,
    inReport      IN  PLS_INTEGER DEFAULT 1
);
--------------------------------------------------------------------
procedure getSgnStatusForRuleInPackage( -- vraci seznam pozadovanych podpisu users podle pravidla a ID ulozenych podpisu
    inUserID in  NUMBER,
    inChannelID   in  NUMBER,
    inPackageID   in  NUMBER,
    orSignStatus  out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getSgnStatusForUsersInPackage( -- vraci seznam podpisu package a pripadne poradi z definovaneho pravidla pro package
    inUserID in  NUMBER,
    inChannelID   in  NUMBER,
    inPackageID   in  NUMBER,
    orSignStatus  out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getSgnStatus( -- vraci podpis podle PK
    inPackageID   in  NUMBER,
    inUserID      in  NUMBER,
    orSignature   out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getAccountsForSignRule( -- vraci ucty spojene s podpisovym pravidlem
    inUserID in  NUMBER,            -- uzivatel, optional
    inClientID    in  NUMBER,       -- klient, nepouziva se, NULL
    inChannelID   in  NUMBER,       -- kanal, optional
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    orAccounts    out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getUsersForSignRule(      -- vraci uzivatele spojene s podpisovym pravidlem
    inUserID in  NUMBER,            -- uzivatel, nepouziva se, NULL
    inChannelID   in  NUMBER,       -- kanal, nepouziva se, NULL
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    orUsers       out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getOperationsForSignRule( -- vraci operace spojene s podpisovym pravidlem
    inUserID in  NUMBER,            -- uzivatel, optional
    inClientID    in  NUMBER,       -- klient, nepouziva se, NULL
    inChannelID   in  NUMBER,       -- kanal, optional
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    ivLangID      in  VARCHAR2,     -- jazyk pro textovy popis operace, optional
    orOperations  out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getLimitsForSignRule( -- vraci obdobove limity spojene s podpisovym pravidlem
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    orLimits      out SYS_REFCURSOR
);
--------------------------------------------------------------------
procedure getAvailableLimits(       -- vraci castky, ktere zbyvaji do prekroceni limitu
    inSignRuleSeq in  Number,
    onDayLimit    out Number,
    onMonthLimit  out Number,
    onYearLimit   out Number
);
--
--------------------------------------------------------------------
procedure AutoGenerateSignRules(           -- automatic generation of signing rules for client - deletes old rules !
    inClientID    in  ClientSignRules.clientid%type
);
 -- in case of error, exception is thrown
--------------------------------------------------------------------
/*
procedure AutoAddSignRulesForUser(           -- automatic generation of signing rules for user - adds new rules for user
    inUserID      in  ClientSignRulesDef.userid%type
);
 -- in case of error, exception is thrown
*/
--------------------------------------------------------------------
procedure GetValidSignRulesForUser (  -- vraci vsechna podpisova pravidla uzivatele pro jejich spravu
    inUserID        in  ClientUsers.UserID%type,    -- Prihlaseny User
    inClientID      in  ClientUsers.ClientID%type,  -- Aktivny klient
    orVSRtab        out sys_refcursor               -- seznam pravidel
);
--------------------------------------------------------------------
procedure GetValidSignRules (  -- vraci vsechna podpisova pravidla pouzitelna pro kombinaci vstupnich parametru
    inChannelID           in  clientsignrulechannel.channelid%TYPE,  -- kanal pozadovany v pravidlu, mandatory
    ivHostUserID          in  Users.HostUserID%type,                 -- Prihlaseny User (PartyID), optional
    ivHostClientID        in  ClientHostAllocation.HostClientID%type,-- klient pozadovany v pravidlu, optional
    inOperID              in  clientsignruleoper.operid%type,        -- pozadovana operace v pravidlu, optional
    ivAccountNumberPrefix in  varchar2,                              -- prefix uctu pozadovaneho v pravidlu, optional
    ivAccountNumber       in  varchar2,                              -- cislo uctu pozadovaneho v pravidlu, optional
    ivBankCode            in  varchar2,                              -- banka uctu pozadovaneho v pravidlu, optional
    inAmount              in  number,                                -- castka operace pro kontrolu limitu v pravidlu, optional
    ivCurrencyId          in  varchar2,                              -- mena castky pro kontrolu limitu v pravidlu, optional
    orVSRtab              out sys_refcursor                          -- seznam pravidel vybranych podle podminek
);
--------------------------------------------------------------------
function getCertSgnStatusForUser( -- vracia 0 alebo 1 podla toho ci uzivatel prilozil k package cert.podpis
    inPackageID   in  NUMBER,
    inUserID      in  NUMBER
) return number;
--------------------------------------------------------------------
--
-- vracia userov pre vsetky prave platne podpisove pravidla, ktore mozu byt pouzite
Procedure GetValidSignRulesUsers (inClientID      in  Clients.ClientID%type,      -- Klient prihlaseneho Usera
                                  inPackageID     in  ClientTransactionPackages.PackageID%type, -- Package, ktere se priradi jedno pravidlo ze seznamu
                                  inCertMethodID  in  pls_integer,
                                  inAddNullPK     in  pls_integer default 1,       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
                                  orUserIDs       out SYS_REFCURSOR,
                                  inCheckLimit    IN  PLS_INTEGER DEFAULT 1        -- 0/1 - nekontrolovat/kontrolovat limit
);
--
-- vraci vsechna prave platna podpisova pravidla, ktera mohou byt pouzita
function ExistValidSignatureRules (inClientID      in  Clients.ClientID%type,      -- Klient prihlaseneho Usera
                                   inPackageID     in  ClientTransactionPackages.PackageID%type, -- Package, ktere se priradi jedno pravidlo ze seznamu
                                   inCertMethodID  in  pls_integer,
                                   inAddNullPK     in  pls_integer default 1,       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
                                   inUserID   in  Users.userid%TYPE default null,
                                   inCheckLimit    IN  PLS_INTEGER DEFAULT 1,        -- 0/1 - nekontrolovat/kontrolovat limit
                                   inChannelID   in  OperationTypeChannels.ChannelID%TYPE DEFAULT NULL
)
return pls_integer;
--
END IIF_CCFG_005;
