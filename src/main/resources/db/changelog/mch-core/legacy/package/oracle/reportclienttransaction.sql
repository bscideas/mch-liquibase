
  CREATE OR REPLACE EDITIONABLE PACKAGE "REPORTCLIENTTRANSACTION"
IS
------------------
-- $release: 6627 $
-- $version: 8.0.35.0 $
------------------
--
-- $Header: /Oracle/UDEBS_RS/UDEBSRS.REPORTCLIENTTRANSACTION.PBK 4     10. 12. 14 13:26 Ilenin $
--
-- MODIFICATION HISTORY
-- Person               Date       Comments
-- -------------------  ---------- ---------------------------------------------------
-- Ilenin               11.07.2014 UDEBS RS
-- Bocak                17.03.2015 GetHistoryReport - input parameter inSeqNum
-- Bocak                24.08.2015 Put - aCalculateAmount
-- Bocak                08.09.2015 Put - aChangePackageStatus
-- Pataky               27.10.2015 GetPackageHistoryReport- overloaded GetHistoryReport procedure for RB transactions
-- Pataky               15.04.2016 Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-------------------------------------------------------------------------------------------------------------------------------
  TYPE TrnCurTyp IS REF CURSOR;
  --
  PROCEDURE Init;
  -- Nulovanie premenych, nastavenie prostredia pre vygenerovanie reportu zmien statusov transakcii a package
  --
  PROCEDURE Report(aOperId NUMBER,aChannelID NUMBER,aPackageID NUMBER,aSeqNum NUMBER,aStatusID NUMBER,
                   aOriginator VARCHAR2,aRelatedData VARCHAR2 := NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0);
  -- Ak je Status pre dany kanal a operaciu reportovatelny (*1) a status este nebol reportovany v aktualne davke
  -- zmeny statuov (medzi volanim Init a Done), tak za vytvori report do tabulky ClientTransactionStatusReports a
  -- ClientTransactionErrors. (v pripade viacerych chyb, je len jeden zaznam o chybe - o prvej z nich)
  -- (*1) O tom ci je status reportovatelny prvotne rozhoduje stlpec ReportToClient z ClientTransactionStatusTypes.
  -- Je to ale len definicia reportovania pre status bez ohladu na kanal a/alebo operaciu. Tato definicia sa da
  -- predefinovat v tabulke StatusReporting a to tak, ze sa do tabulky vlozi zaznam s konkretnou hodnotou OperId a
  -- ChannelId a nastavi sa hodnota ReportToClient na zelanu hodnotu. Stlpec OperId v tabulke StatusReporting
  -- akceptuje NULL hodnotu a ta znamena ze status sa bude reportovat podla ChanelId, StatusId a ReportToClient
  -- pre vsetky mozne operacie.
  -- PR1 : ak sa ma status 90 reportovat iba pr operaciu 20 v kanali 5 a v ostatnych pripadoch nie :
  --       do ClientTransactionStatusTypes sa nastavi ReportToClient = 0 pre StatusId = 90
  --       do StatusReporting sa vlozi zaznam s OperId = 20, ChannelId = 5, StatusId = 90, ReportToClient = 1
  -- PR2 : ak sa NEMA StatusId 190 reportovat iba pre kanal 5 bez ohladu na operaciu, pre ostatne kanaly ano :
  --       do ClientTransactionStatusTypes sa nastavi ReportToClient = 1 pre StatusId = 190
  --       do StatusReporting sa vlozi zaznam s OperId = NULL, ChannelId = 5, StatusId = 190, ReportToClient = 0
  --
  PROCEDURE Put(aOperId NUMBER,aChannelID NUMBER,aPackageID NUMBER,aSeqNum NUMBER,aStatusID NUMBER,
                aOriginator VARCHAR2,aRelatedData VARCHAR2 := NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0,
                aBankRef VARCHAR2 := NULL, aBankEnsurePackageReport VARCHAR2 DEFAULT NULL,
                aCalculateAmount  NUMBER DEFAULT 1,aChangePackageStatus  NUMBER DEFAULT 1,
                inReport      IN  PLS_INTEGER DEFAULT 1);
  -- Zmeni status package, alebo transakcie a zavola report.
  -- Dovoli zmenit status iba vtedy ak nie je momentalny Status konecny a to len na vyssi status. Ak niekto proceduru
  -- zavola tak, ze bude chciet zmenit status na nizsi, alebo bude chciet modifikovat konecny status, volanie sa
  -- ignoruje, status sa nemeni.
  -- Pri jednotransakcnych packages zmeni aj status transakcie a to v pripade ked sa meni status package na final a
  -- tento status je vacsi ako 46 (zmena prisla z bankoveho systemu) - je to len bezpecnostna poistka pre pripad,
  -- ze bankovy system by nedodal report o zmene stavu transakcie.
  -- Pri zmene statusu transakcie na "final" (konecny spravny alebo konecny chybny) modifikuje aj stlpce CTP
  -- CountTransaction_OK a CountTransaction_E a ak je parameter GSP 'BankEnsurePackageReport' nastaveny na 1, tak
  -- pri zmene stavu poslednej transakcie na "final", zmeni aj status package (podla toho kolko je chybnych
  -- a kolko spravnych transakcii)
  --
  PROCEDURE Put(aTransactionID VARCHAR2,aStatusID VARCHAR2,aOriginator VARCHAR2,aRelatedData VARCHAR2 := NULL,
                aErrorID VARCHAR2 := '0',aReportSeq NUMBER := 0, aBankRef VARCHAR2 := NULL);
  -- Pretazenie prvej Put funkcie. TransactionId sa rozlozi do OperId, ChannelId, PackageId, SeqNum
  --
  PROCEDURE Put(aOperId NUMBER,aChannelID NUMBER,aPackageID NUMBER,aSeqNum NUMBER,aStatusID NUMBER,
                aOriginator VARCHAR2,aRelatedData VARCHAR2 := NULL,aErrorID NUMBER :=0,aReport Varchar2,aReportSeq IN OUT NUMBER);
  -- pretizeni prvni funkce za ucelem ziskat jako vystupni parametr pridelene RepoertId
  -- parametr aRepoert ma pouze smysl pro pretizeni, jeho hodnota muze byt jakakoli a nema zadny vyznam
  --
  PROCEDURE PutTrn(aTransactionID VARCHAR2,aStatusID VARCHAR2,aOriginator VARCHAR2,aRelatedData VARCHAR2 := NULL,
                aErrorID NUMBER := 0,aReportSeq IN OUT NUMBER, aBankRef VARCHAR2 := NULL);
  -- pretazenie druhej procedury - ucel = ziskanie ReportSeq (nazov je iny koli kolizii volani)
  --=============================================================================================================
  PROCEDURE PutForce(aOperId NUMBER,aChannelID NUMBER,aPackageId NUMBER,aSeqNum NUMBER,aStatusId NUMBER,
                     aOriginator VARCHAR2,aRelatedData VARCHAR2:=NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0,
                     aBankRef VARCHAR2 := NULL, aBankEnsurePackageReport VARCHAR2 DEFAULT NULL);
  -- Nastavuje packages a transakci v prubeznych i finalnich statusech na jakykoliv novy status.
  -- Procedura je volana napr. z formulare PACKAGEADMIN pro manualni nastaveni statusu.
  -- Package a transakce jiz byly nastavovany volanim standardni Put().
  --=============================================================================================================
  PROCEDURE Done(aReportseq NUMBER := null,aPackageid NUMBER :=null,aLockAttr varchar2:=0);
  -- Cistenie premennych a ak medzi init a done je aspon 1 transakcia, alebo package reportovatelny, zapise sa
  -- udalost pre GMO 3, ktore vygeneruje report.
  -- Pri viacnasobnom volani bez toho aby medzi 1. a 2. volanim bol Init a Put, sa vykona iba raz.
  --
  PROCEDURE GetTransactions (
      pPackageId IN ClientTransactionPackages.PackageId%TYPE,
      pOperId    IN ClientTransactionPackages.OperId%TYPE,
      pTrnCur    OUT TrnCurTyp);
  -- procedura ziska zoznam transakcii daneho package
  --
  PROCEDURE ChangePcgTrnStatus (
      pOperId        IN ClientTransactionPackages.OperId%TYPE,
      pChannelId     IN ClientTransactionPackages.ChannelId%TYPE,
      pPackageId     IN ClientTransactionPackages.PackageId%TYPE,
      pStatusId      IN ClientTransactionPackages.StatusId%TYPE,
      pOriginatorId  IN ClientTransactionStatusReports.Originator%TYPE);
  -- zmena stavu package + zmena stavu vsetkych jeho transakcii
------------------------------------------------------------------------
  PROCEDURE GetValidationReport (
      onReturnCode    OUT pls_integer,
      orErrors        OUT sys_refcursor,
      ocReport        OUT NOCOPY Clob,
      inUserID   IN  Number,
      inChannelID     IN  Number,
      inPackageID     IN  ClientTransactionPackages.PackageId%TYPE
  );
  -- Procedura vrati XML report o validaci a chybach
------------------------------------------------------------------------
END REPORTCLIENTTRANSACTION;
