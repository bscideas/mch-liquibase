
  CREATE OR REPLACE EDITIONABLE PACKAGE "JOURNAL001" IS
  ------------------
  -- $release: 5751 $
  -- $version: 8.0.19.0 $
  ------------------
  -- BSC Praha spol. s r.o.
  -- package umoznujuci manipulaciu s tabulkou Journal
  -- MODIFICATION HISTORY
  -- Person         Date        Comments
  -- ------------   ----------  -------------------------------------------
  -- Ilenin         01.07.2014  UDEBS RS
  -- Bocak          10.02.2015  LogEventPSI
  -- Emmer          07.09.2015  Rewrite Journaling
  -- Pataky         13.10.2015  Details parameter type functions overloaded with CLOB versions, Details in table Journal still remains XMLType - 20009203-1354
  -- Emmer          16.11.2015  Log events only into new structures
  -- Emmer          09.12.2015  Remove obsolete interface
  -- Bocak          04.01.2016  Get back procedures
  -- Pataky         28.04.2017  Modifications to support extension journal tables - 20009203-7142
  -- Emmer          07.09.2017  skipJournal: if EventTypes.loglevel = 0 then nothing is journaled for this event. Event -9 is always journaled.
  -- Emmer          28.02.2018  ERROR_LOG_AUTONOMOUS
  --
  TYPE Rseq IS RECORD(
    seq NUMBER,
    RID ROWID);
  TYPE TRseq IS TABLE OF Rseq INDEX BY PLS_INTEGER;
  ATRseq TRseq;
  Lvl PLS_INTEGER;
  ParEvSeq Journal.Seq%TYPE := NULL;
  --
  -- Check whether EventType exists in table EventTypes
  --   return value - EventID, if not found - then NULL
  FUNCTION CheckEvent(p_Event EventTypes.eventid%TYPE) RETURN NUMBER RESULT_CACHE DETERMINISTIC;
  --
  -- If EventTypes.LogLevel = 0 then journaling is completely skipped
  FUNCTION skipJournal(p_Event EventTypes.eventid%TYPE) RETURN NUMBER RESULT_CACHE DETERMINISTIC;
  --
  -- get xml from clob, if clob is null then function returns null
  FUNCTION get_xml_from_clob(p_clob CLOB) RETURN xmltype;
  --
  -- New overloaded function LogEvent returns new primary journal key (Seq: Journal ID) without commit.
  -- Returned value can be used for USERSESSION.ParentSeq
  FUNCTION LogEventClob(p_Event        IN Journal.eventid%TYPE,
                        p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                        p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                        p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                        p_Details      IN CLOB DEFAULT NULL,
                        p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                        p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION LogEvent(p_Event        IN Journal.eventid%TYPE,
                    p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                    p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                    p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                    p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                    p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                    p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  -- New overloaded procedure LogEvent without commit
  PROCEDURE LogEventClob(p_Event        IN Journal.eventid%TYPE,
                         p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                         p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                         p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                         p_Details      IN CLOB DEFAULT NULL,
                         p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                         p_Duration     IN Journal.Duration%TYPE DEFAULT NULL);
  --
  PROCEDURE LogEvent(p_Event        IN Journal.eventid%TYPE,
                     p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                     p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                     p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                     p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                     p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                     p_Duration     IN Journal.Duration%TYPE DEFAULT NULL);
  --
  -- Obsolete procedure
  PROCEDURE LogEventClob(Event         IN Journal.eventid%TYPE,
                         Fea           IN VARCHAR2,
                         Dt1           IN TIMESTAMP := NULL,
                         Dt2           IN TIMESTAMP := NULL,
                         Ds            IN VARCHAR2 := NULL,
                         aOriginatorID IN Journal.originatorid%TYPE := NULL,
                         aChannelID    IN NUMBER := NULL,
                         LDTYPE        IN VARCHAR2 := NULL,
                         LDID          IN VARCHAR2 := NULL,
                         aClientID     IN Journal.clientid%TYPE := NULL,
                         aExtension    IN CLOB DEFAULT NULL,
                         aUserID       IN Journal.UserID%TYPE DEFAULT NULL,
                         aDetails      IN CLOB DEFAULT NULL,
                         aDuration     IN Journal.Duration%TYPE DEFAULT NULL);
  -- Obsolete procedure
  PROCEDURE LogEvent(Event         IN Journal.eventid%TYPE,
                     Fea           IN VARCHAR2,
                     Dt1           IN TIMESTAMP := NULL,
                     Dt2           IN TIMESTAMP := NULL,
                     Ds            IN VARCHAR2 := NULL,
                     aOriginatorID IN Journal.originatorid%TYPE := NULL,
                     aChannelID    IN NUMBER := NULL,
                     LDTYPE        IN VARCHAR2 := NULL,
                     LDID          IN VARCHAR2 := NULL,
                     aClientID     IN Journal.clientid%TYPE := NULL,
                     aExtension    IN CLOB DEFAULT NULL,
                     aUserID       IN Journal.UserID%TYPE DEFAULT NULL,
                     aDetails      IN Journal.Details%TYPE DEFAULT NULL,
                     aDuration     IN Journal.Duration%TYPE DEFAULT NULL);
  -- New overloaded LogChildEvent without commit
  PROCEDURE LogChildEventClob(p_Event        IN Journal.eventid%TYPE,
                              p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                              p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                              p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                              p_Details      IN CLOB DEFAULT NULL,
                              p_Duration     IN Journal.Duration%TYPE DEFAULT NULL);
  --
  PROCEDURE LogChildEvent(p_Event        IN Journal.eventid%TYPE,
                          p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                          p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                          p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                          p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                          p_Duration     IN Journal.Duration%TYPE DEFAULT NULL);
  --
  -- Obsolete procedure
  PROCEDURE LogChildEvent(Event         IN Journal.eventid%TYPE,
                          Fea           IN VARCHAR2,
                          Dt1           IN TIMESTAMP := NULL,
                          Dt2           IN TIMESTAMP := NULL,
                          Ds            IN VARCHAR2 := NULL,
                          aOriginatorID IN Journal.originatorid%TYPE := NULL,
                          aChannelID    IN NUMBER := NULL,
                          LDTYPE        IN VARCHAR2 := NULL,
                          LDID          IN VARCHAR2 := NULL,
                          aClientID     IN Journal.clientid%TYPE := NULL,
                          aExtension    IN CLOB DEFAULT NULL,
                          aUserID       IN Journal.UserID%TYPE DEFAULT NULL);
  --
  PROCEDURE ToParent;
  --
  PROCEDURE ToChild;
  --
  -- Obsolete procedure
  PROCEDURE PutStatus(St IN NUMBER);
  --
  -- Obsolete function
  FUNCTION GetStatus RETURN NUMBER;
  --
  -- Obsolete procedure
  PROCEDURE PutProgress(St IN NUMBER);
  --
  -- Obsolete function
  FUNCTION GetProgress RETURN NUMBER;
  --
  -- Obsolete procedure
  PROCEDURE PutEndDate(Dt2 IN TIMESTAMP);
  --
  -- Obsolete procedure
  PROCEDURE PutOriginator(Ori IN VARCHAR2);
  --
  -- Obsolete procedure
  PROCEDURE PutChannel(Chann IN VARCHAR2);
  --
  -- Obsolete procedure
  PROCEDURE PutClient(Cli IN VARCHAR2);
  --
  -- Obsolete procedure
  PROCEDURE PutLoginDev(LDTYPE IN VARCHAR2,
                        LDID   IN VARCHAR2);
  --
  -- Obsolete procedure
  PROCEDURE PutDescription(Ds IN VARCHAR2);
  --
  -- Obsolete procedure
  PROCEDURE PutFeature(Fea IN VARCHAR2);
  --
  FUNCTION GetSeq(inAvoidError IN NUMBER DEFAULT NULL) RETURN NUMBER;
  --
  PROCEDURE ToSeq(Seq IN NUMBER);
  --
  PROCEDURE SetParEvSeq(pSeq    IN Journal.Seq%TYPE,
                        pPackId IN ClientTransactionPackages.PackageId%TYPE DEFAULT NULL);
  --
  PROCEDURE ERROR_LOG_AUTONOMOUS(p_OriginatorID VARCHAR2,
                                 p_Details      CLOB,
                                 p_ParEvSeq     NUMBER DEFAULT NULL,
                                 p_EventID      NUMBER DEFAULT -500);
  --
END JOURNAL001;
