
  CREATE OR REPLACE EDITIONABLE PACKAGE "REFTAB_CTET" IS
------------------
-- $release: 078 $
-- $version: 8.0.0.0 $
------------------
-- $Header: /Oracle/UDEBS_EE/UDEBS0105.REFTAB_CTET.PSK 4     22.07.03 21:53 Jaros $
-- ---------   ---------- -------------------------------------------------
-- Ilenin      07.07.2014 UDEBS RS
-- ---------   ---------- -------------------------------------------------
  FUNCTION Existing      (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE) RETURN BOOLEAN;
  --
  FUNCTION TextId        (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          inDefaultVal    IN ClientTransactionErrorTypes.TextId%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION BankErrorCode (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          inDefaultVal    IN ClientTransactionErrorTypes.BankErrorCode%TYPE DEFAULT NULL) RETURN VARCHAR2;
  --
  FUNCTION FatalError    (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          inDefaultVal    IN NUMBER DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION Text          (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          ivLangId        IN SupportedLang.LangId%TYPE) RETURN VARCHAR2;
  --
  FUNCTION ErrorId       (inBankErrorCode IN ClientTransactionErrorTypes.BankErrorCode%TYPE,
                          inDefaultVal    IN ClientTransactionErrorTypes.ErrorId%TYPE DEFAULT NULL) RETURN NUMBER;
END reftab_ctet;
