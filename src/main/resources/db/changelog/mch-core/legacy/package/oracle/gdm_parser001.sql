
  CREATE OR REPLACE EDITIONABLE PACKAGE "GDM_PARSER001" IS
------------------
-- $release: 3122 $
-- $version: 8.0.5.0 $
------------------
--
-- $Header: /Oracle/UDEBS_XPE/UDEBSXPE.GDM_PARSER001.PSK 2     27.01.14 18:24 Pataky $
--
-- MODIFICATION HISTORY
--
-- Person      Date       Comments
-- ----------- ---------- ---------------------------------------------------------
-- Ilenin      07.07.2014 UDEBS RS
-- Pataky      20.07.2015 Optimization of getXPath caching
-- Snederfler  19.08.2015 20009203-1183 - ParseXMLForBranchAndOper - optimization: move select for getting GroupID nearly before using it
--                                        GetDTDAttributes - new function of getting common DTD attributes
-- Emmer       26.02.2016 20009203-3907 - GetDTDAttributes modification
-- Pataky      15.04.2016 Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-- --------------------------------------------------------------------------------
--
PROCEDURE GetDTDAttributes(
    ovDocType   OUT DTDStore.DocType%TYPE,
    ovIIFAttr   OUT DTDStore.IIFAttr%TYPE,
    ovIIFAttr2  OUT DTDStore.IIFAttr2%TYPE,
    ovIIFAttr3  OUT DTDStore.IIFAttr3%TYPE,
    inOperID    IN  DTDStore.operid%TYPE,
    inDocType   IN  DTDStore.DocType%TYPE DEFAULT NULL
);
--
FUNCTION SaveSpaces(ixInputData IN XMLTYPE) RETURN XMLTYPE;
--
FUNCTION ReturnSpaces(ixInputData IN XMLTYPE) RETURN XMLTYPE;
--
FUNCTION getXPath (ivIIFAttrName GDM_Parser.IIFAttrName%TYPE, ivDefault VARCHAR2 DEFAULT NULL) RETURN VARCHAR2 RESULT_CACHE;
--
END GDM_PARSER001;
