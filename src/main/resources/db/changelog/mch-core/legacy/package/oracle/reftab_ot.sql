
  CREATE OR REPLACE EDITIONABLE PACKAGE "REFTAB_OT" AUTHID DEFINER IS
  --
  ------------------
  -- $release: 4619 $
  -- $version: 8.0.5.0 $
  ------------------
  -- $Header: /Oracle/UDEBS_EE/UDEBS0105.REFTAB_OT.PSK 2     1.04.03 15:12 Jaros $
  -- MODIFICATION HISTORY
  -- Person       Date       Comments
  -- ----------   ---------- ------------------------------------------
  -- Bocak        27.02.2017 all functions 
  -- Emmer        17.01.2018 all functions RESULT_CACHE removed, hints stays
  -- ----------   ---------- ------------------------------------------
  --
  FUNCTION Existing(inOperId IN OperationTypes.OperId%TYPE) RETURN BOOLEAN;
  --
  FUNCTION GroupId(inOperId     IN OperationTypes.OperId%TYPE,
                   inDefaultVal IN OperationTypes.GroupId%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION AccountSensitive(inOperId     IN OperationTypes.OperId%TYPE,
                            inDefaultVal IN OperationTypes.AccountSensitive%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION AuthenReq(inOperId     IN OperationTypes.OperId%TYPE,
                     inDefaultVal IN OperationTypes.AuthenReq%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION CertifReq(inOperId     IN OperationTypes.OperId%TYPE,
                     inDefaultVal IN OperationTypes.CertifReq%TYPE DEFAULT NULL,
                     inChannelID  IN OperationTypeChannels.ChannelID%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION TextId(inOperId     IN OperationTypes.OperId%TYPE,
                  inDefaultVal IN OperationTypes.TextId%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION GroupOrder(inOperId     IN OperationTypes.OperId%TYPE,
                      inDefaultVal IN OperationTypes.GroupOrder%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION SignRulesReq(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.SignRulesReq%TYPE DEFAULT NULL,
                        inChannelID  IN OperationTypeChannels.ChannelID%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION SysTabIndicator(inOperId     IN OperationTypes.OperId%TYPE,
                           inDefaultVal IN OperationTypes.SysTabIndicator%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION Priority(inOperId     IN OperationTypes.OperId%TYPE,
                    inDefaultVal IN OperationTypes.Priority%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION OperTypeCode(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.OperTypeCode%TYPE DEFAULT NULL)
    RETURN VARCHAR2;
  --
  FUNCTION BankToClient(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.BankToClient%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION ClientToBank(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.ClientToBank%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION AuthorReq(inOperId     IN OperationTypes.OperId%TYPE,
                     inDefaultVal IN OperationTypes.AuthorReq%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION HistoryKept(inOperId     IN OperationTypes.OperId%TYPE,
                       inDefaultVal IN OperationTypes.HistoryKept%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION Hidden(inOperId     IN OperationTypes.OperId%TYPE,
                  inDefaultVal IN OperationTypes.Hidden%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION CtpRetention(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.CtpRetention%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION RecStatusId(inOperId     IN OperationTypes.OperId%TYPE,
                       inDefaultVal IN OperationTypes.RecStatusId%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION HomeTableName(inOperId     IN OperationTypes.OperId%TYPE,
                         inDefaultVal IN OperationTypes.HomeTableName%TYPE DEFAULT NULL)
    RETURN VARCHAR2;
  --
  FUNCTION PayLimitCum(inOperId     IN OperationTypes.OperId%TYPE,
                       inDefaultVal IN OperationTypes.PayLimitCum%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION PayLimitSingle(inOperId     IN OperationTypes.OperId%TYPE,
                          inDefaultVal IN OperationTypes.PayLimitSingle%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION CopyStatusId(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.CopyStatusId%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION PerformAuthorisation(inOperId     IN OperationTypes.OperId%TYPE,
                                inDefaultVal IN OperationTypes.PerformAuthorisation%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION MakesDebit(inOperId     IN OperationTypes.OperId%TYPE,
                      inDefaultVal IN OperationTypes.MakesDebit%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION NotificationType(inOperId     IN OperationTypes.OperId%TYPE,
                            inDefaultVal IN OperationTypes.NotificationType%TYPE DEFAULT NULL)
    RETURN VARCHAR2;
  --
  FUNCTION Batch(inOperId     IN OperationTypes.OperId%TYPE,
                 inDefaultVal IN OperationTypes.Batch%TYPE DEFAULT NULL) RETURN NUMBER;
  --
  FUNCTION PaymentSystemID(inOperId     IN OperationTypes.OperId%TYPE,
                           inBranchID   IN Branches.BranchID%TYPE,
                           inDefaultVal IN OperPaymentSystems.PaymentSystemID%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION AvailableForClient(inOperId     IN OperationTypes.OperId%TYPE,
                              inDefaultVal IN OperationTypes.AvailableForClient%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
  FUNCTION AvailableForOperator(inOperId     IN OperationTypes.OperId%TYPE,
                                inDefaultVal IN OperationTypes.AvailableForOperator%TYPE DEFAULT NULL)
    RETURN NUMBER;
  --
END REFTAB_OT;
