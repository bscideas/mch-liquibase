
  CREATE OR REPLACE EDITIONABLE PACKAGE "WAC_CATALOGS"
  IS
------------------
-- $release: 1964 $
-- $version: 8.0.0.1 $
------------------
-- $Header: $
------------------
--
--
-- MODIFICATION HISTORY
--
-- Person      Date       Comments
-- ----------- ---------- -----------------------------------------------------
-- Furdik      25.11.2015 Package creation, proc PutText and function ExistsTextInTab (20009213-108)
-- Furdik      02.12.2015 added procs RemText and RemNullTexts (20009213-108)
-- ----------------------------------------------------------------------------
--
  --
  TYPE tRefCursor IS REF CURSOR;
--
-- Procedure to remove all texts gor given TextID  - uses DELETE CASCADE constraint on TEXTS tab
--
PROCEDURE RemText (
      onReturnCode      OUT NUMBER,                 -- 0 - OK , 1 Error
      orErrors          OUT tRefCursor,
      inTextID          IN OUT LanguageTexts.textid%type,
      inRaiseAnyError   IN NUMBER DEFAULT 0);
--
-- Procedure to remove texts in Catalog - TextID set to null, delete from TEXTS tab
PROCEDURE RemNullTexts (
      onReturnCode      OUT NUMBER,                 -- 0 - OK , 1 Error
      orErrors          OUT tRefCursor,
      inTextID          IN OUT LanguageTexts.textid%type,
      ivListName        IN CATALOGATTRIBUTE.CATALOGCODE%type,
      ivColName         IN CATALOGATTRIBUTE.NAME%type,
      inRaiseAnyError   IN NUMBER DEFAULT 0);
--
-- Procedure to insert or update a text in the ListName table for given textid and langid
PROCEDURE PutText (
      onReturnCode      OUT NUMBER,                 -- 0 - OK , 1 Error
      orErrors          OUT tRefCursor,
      ivListName        IN CATALOG.CODE%type,
      inTextID          IN OUT LanguageTexts.textid%type,
      inLangID          IN LanguageTexts.langid%type,
      ivText            IN LanguageTexts.text%type,
      ivPurposeID       IN LanguageTexts.purposeid%type DEFAULT 'DEFAULT',  -- PurposeID  - parameter specifikuje ucel (DEFAULT - povodny text pre prislusne TID, LTD)
      inRaiseAnyError   IN NUMBER DEFAULT 0
);
--
-- test if the TextID column exists in a ListName table
FUNCTION ExistsTextInTab (
    ivListName        IN CATALOG.CODE%type
    ) RETURN NUMBER; -- 0/1
--
END WAC_CATALOGS;
