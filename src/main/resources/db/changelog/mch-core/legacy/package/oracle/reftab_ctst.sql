
  CREATE OR REPLACE EDITIONABLE PACKAGE "REFTAB_CTST"
IS
------------------
-- $release: 1666 $
-- $version: 8.0.2.0 $
------------------
-- $Header: /Oracle/UDEBS_RU/UDEBSRU.REFTAB_CTST.PSK 2     15.12.04 16:55 Matis $
--------------------------------------------------------------------------------
-- Ilenin       07.07.2014  UDEBS RS
-- Ilenin       02.12.2014  Override AllowSign
-- Pataky       02.11.2015  Added Visible function
--------------------------------------------------------------------------------
  FUNCTION Existing       (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE) RETURN BOOLEAN;
  --
  FUNCTION PackageStatus  (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.PackageStatus%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION AllowDel       (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION AllowEdit      (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION AllowCancel    (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION AllowSign      (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.AllowSign%TYPE DEFAULT NULL
                          ) RETURN PLS_INTEGER;
  --
  FUNCTION FinalStatus    (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.FinalStatus%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION ReportToClient (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION ShortText      (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.ShortText%TYPE DEFAULT NULL) RETURN VARCHAR2;
  --
  FUNCTION TextId         (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.TextId%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
  FUNCTION Visible        (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.Visible%TYPE DEFAULT NULL) RETURN PLS_INTEGER;
  --
END reftab_ctst;
