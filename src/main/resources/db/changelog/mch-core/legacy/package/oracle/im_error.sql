
  CREATE OR REPLACE EDITIONABLE PACKAGE "IM_ERROR"
IS
------------------
-- $release: 078 $
-- $version: 8.0.0.0 $
------------------
-- $Header: /Oracle/UDEBS_XPE/UDEBSXPE.IM_ERROR.PSK 4     6. 11. 12 16:55 Ilenin $
------------------
-- Posledni patch z UDEBS: 2854
--
-- MODIFICATION HISTORY
--
-- Person      Date       Comments
-- ----------- ---------- ---------------------------------------------------------
-- Ilenin      21.07.2014 UDEBS RS
-- --------------------------------------------------------------------------------
--
-- PURPOSE :
-- Internal Module
-- registrovanie chyb pocas spracovania v nejakom pl/sql kode, na zaver vyber vsetkych registrovanych chyb
--
-- DOC :
-- Z:\BSC_Praha\Banky\GEMINI\Docs\rules\Exceptions_Classification.doc
-- Z:\BSC_Praha\Banky\GEMINI\Docs\rules\GeminiExceptions.xls
--
-- Aplikacne chyby, ktore vzniknu v UDEBSe sa zatial neprezentuju standardnym sposobom (koli aplikaciam, ktore by ich nevedeli
-- zatial odchytit), ale vyhadzuju sa uzivatelske exceptions, tak ako je to zvykom v doterajsich interfaces.
-- V buducnosti, ked budu vsetky aplikacie citat nazretovy cursor chyb, tak sa budu most aj aplikacne chyby odovzdavat takisto
-- ako businnes.
--
  --
  -- ref cursor
  TYPE tRefCursor IS REF CURSOR;
  --
  gvDbmsOutput NUMBER  := 0;
  gvDelete     BOOLEAN := true;
  --
  -- typ record, koli jednoduchemu fetchovaniu v ostatnych PL/SQL kodoch, kopiruje objekt ERR_OBJ (do objektu sa neda fetchnut), MUSI typovo sediet s resultsetom GetErrors !
  TYPE tRecError IS RECORD (
      ErrorId   ClientTransactionErrorTypes.ExternalErrorId%TYPE,
      Text      ClientTransactionErrorTypes.Description%TYPE,
      Param0    ClientTransactionErrorTypes.Param0%TYPE,
      Param1    ClientTransactionErrorTypes.Param1%TYPE,
      Param2    ClientTransactionErrorTypes.Param2%TYPE,
      Param3    ClientTransactionErrorTypes.Param3%TYPE,
      Param4    ClientTransactionErrorTypes.Param4%TYPE,
      Param5    ClientTransactionErrorTypes.Param5%TYPE,
      Param6    ClientTransactionErrorTypes.Param6%TYPE,
      Param7    ClientTransactionErrorTypes.Param7%TYPE,
      Param8    ClientTransactionErrorTypes.Param8%TYPE,
      Param9    ClientTransactionErrorTypes.Param9%TYPE,
      OccurDate DATE);
  --
  -- typy chyb (podla dokumentu hore), ktore sa mozu generovat z PL/SQL, konsanty - nasobky 2
  ET_USER_EXC        CONSTANT NUMBER := 1;
  ET_BUSINESS_EXC    CONSTANT NUMBER := 2;
  ET_SUCCESSFUL      CONSTANT NUMBER := 4;
  ET_APPLICATION_EXC CONSTANT NUMBER := 8; -- tieto typy su vyhadzovane ako uzivatelske chyby ORA-20000 .. ORA--20999, konstant je definovana iba pre pripad, ze to niekto zle zavola ...
  -- ET_SYSTEM_EXC -- tieto su vyhadzovane ako ORA chyby, vsetky okrem aplikacnych
  -- ET_SYSTEM_ERR -- tieto su vyhadzovane ako ORA chyby, vsetky okrem aplikacnych
  --
  -- exception, ktora je vyhodena ak nastane akakolvek z chyb, okrem ET_SUCCESSFUL
  REG_ANY_ERROR EXCEPTION;
  PRAGMA EXCEPTION_INIT(REG_ANY_ERROR,-20000);
  --
  -- zmaze glob. premennu zoznam chyb.
  PROCEDURE Init;
  --
  -- parametre spoji do jedneho textu
  FUNCTION CombineParams (
      ivParam0  IN ClientTransactionErrorTypes.Param0%TYPE DEFAULT NULL,
      ivParam1  IN ClientTransactionErrorTypes.Param1%TYPE DEFAULT NULL,
      ivParam2  IN ClientTransactionErrorTypes.Param2%TYPE DEFAULT NULL,
      ivParam3  IN ClientTransactionErrorTypes.Param3%TYPE DEFAULT NULL,
      ivParam4  IN ClientTransactionErrorTypes.Param4%TYPE DEFAULT NULL,
      ivParam5  IN ClientTransactionErrorTypes.Param5%TYPE DEFAULT NULL,
      ivParam6  IN ClientTransactionErrorTypes.Param6%TYPE DEFAULT NULL,
      ivParam7  IN ClientTransactionErrorTypes.Param7%TYPE DEFAULT NULL,
      ivParam8  IN ClientTransactionErrorTypes.Param8%TYPE DEFAULT NULL,
      ivParam9  IN ClientTransactionErrorTypes.Param9%TYPE DEFAULT NULL)
  RETURN VARCHAR2;
  --
  -- prida chybu, ErrorId moze byt standardne ako je definovane v XLS (vsetky, aj aplikacne), alebo aj -20999..-20000 co su oraclovske aplikacne chyby
  PROCEDURE AddError (
      inErrorId IN ClientTransactionErrorTypes.ExternalErrorId%TYPE,
      ivText    IN ClientTransactionErrorTypes.Description%TYPE DEFAULT NULL,
      ivParam0  IN ClientTransactionErrorTypes.Param0%TYPE DEFAULT NULL,
      ivParam1  IN ClientTransactionErrorTypes.Param1%TYPE DEFAULT NULL,
      ivParam2  IN ClientTransactionErrorTypes.Param2%TYPE DEFAULT NULL,
      ivParam3  IN ClientTransactionErrorTypes.Param3%TYPE DEFAULT NULL,
      ivParam4  IN ClientTransactionErrorTypes.Param4%TYPE DEFAULT NULL,
      ivParam5  IN ClientTransactionErrorTypes.Param5%TYPE DEFAULT NULL,
      ivParam6  IN ClientTransactionErrorTypes.Param6%TYPE DEFAULT NULL,
      ivParam7  IN ClientTransactionErrorTypes.Param7%TYPE DEFAULT NULL,
      ivParam8  IN ClientTransactionErrorTypes.Param8%TYPE DEFAULT NULL,
      ivParam9  IN ClientTransactionErrorTypes.Param9%TYPE DEFAULT NULL,
      inRaiseAnyError IN NUMBER DEFAULT 0); -- ak je registrovana akakolvek chyba vyhodi sa geRegAnyError
  --
  -- vrati ref cursor chyb
  PROCEDURE GetErrors (
      orErrors  IN OUT tRefCursor);
  --
  -- zistenie ci od volania Init a seriou volani AddError vznikla chyba daneho typu
  FUNCTION ExistErrType (
      inErrType IN NUMBER) -- jedna z konstant (alebo viacero zANDovanych z hlavicky tohoto package)
  RETURN NUMBER; -- 0 chyba daneho typu neexistuje, 1 v serii volani sa vyskytla
  --
END IM_ERROR;
