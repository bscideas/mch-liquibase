
  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "IIF_CCFG_005" IS
------------------
-- $release: 6673 $
-- $version: 8.0.50.3 $
------------------
--
-- Purpose: Podpisova pravidla 2.generace
-- $Header: /Oracle/UDEBS_RS/UDEBSRS.IIF_CCFG_005.PBK 3     2.07.12 17:16 Pataky $
-- MODIFICATION HISTORY
-- Person       Date       Comments
-- ----------   ---------- ------------------------------------------
-- Ilenin       07.07.2014  UDEBS RS
-- Ilenin       23.11.2014  20009183-1042
-- Bocak        26.02.2015  PutSDClientSignRules - chenged erro message for -20001 (20009183-1719)
-- Pataky       04.03.2015  VerifySufficiencySigning - corrected sign.rule evaluation (20009183-1705,20009183-1845)
-- Pataky       04.03.2015  ExistValidSignatureRules - corrected sign.rule selecting (20009183-1705,20009183-1755,20009183-1845)
-- Pataky       05.03.2015  GetValidSignatureRules, SetOrChangeSignatureRule, VerifySignatureRule - exceptions for operId 781 (20009183-1853)
-- Pataky       09.03.2015  PutSDClientSignRules - changed error message Err01
-- Pataky       19.03.2015  ExistValidSignatureRules - corrected sign.rule selecting for ordered signers - 20009183-2047
-- Pataky       20.03.2015  VerifySufficiencySigning - corrected sign.rule evaluation (20009183-2047)
-- Pataky       26.03.2015  VerifySufficiencySigning,GetValidSignatureRules,VerifySignatureRule - unified limit rounding (20009183-2126, 20009183-2136)
-- Pataky       30.04.2015  ExistValidSignatureRules - exception for exchange rates problem added (20009183-2607)
-- Pataky       04.05.2015  ClientSignRules.AmountLimit can be null, evaluations fixed - 20009183-2477
-- Bocak        13.05.2015  VerifySufficiencySigning - set CTP.SigneValDate to SYSDATE when transaction is PARTSIG (20009183-2631)
-- Pataky       22.05.2015  Reviewed using of RESULT CACHE hint in select statements - 20009183-2902
-- Snederfler   25.05.2015  PutSDClientSignRulesDef - optimization
-- Bocak        28.07.2015  ExistValidSignatureRules, GetValidSignatureRules - new input parameter inCheckLimit (20009183-1703)
-- Bocak        26.08.2015  GetValidSignatureRules, SetOrChangeSignatureRule, VerifySignatureRule - exceptions for operId 786 (20009183-4361)
-- Pataky       28.08.2015  GetSumTotal - added exception for RB for own accounts operations, excluded from limit - 20009203-1329
-- Pataky       09.10.2015  VerifySignatureRule, GetValidSignatureRules - added exception for RB for own accounts operations, excluded from limit - 20009203-2317
-- Pataky       09.11.2015  Package amount can be recounted into local currency, based on parameter (because of multicurrency accounts)
-- Pataky       11.12.2015  VerifySufficiencySigning - keep status PARTSIG after multiple rules checking, if signature is attached
-- Emmer        16.12.2015  Removal of links to obsolete journal attributes 20009203-1354
-- Pataky       15.04.2016  Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-- Pataky       13.06.2016  VerifySufficiencySigning - keep status PARTSIG after multiple rules checking fix - 20009203-5429
-- Pataky       21.07.2016  New procedure GetValidSignRules to get sign.rules for external systems - 20009203-5706
-- Bocak        16.08.2016  GetValidSignatureRules - select data from ctp_tmp if package status is 33 - 20009238-282
-- Bocak        24.08.2016  GetValidSignatureRules, SetOrChangeSignatureRule, VerifySignatureRule - exceptions for operId 780 - 20009248-584
-- Pataky       16.09.2016  Removed LastLoginTime from Users table, fixed GetValidSignRules
-- Pataky       13.10.2016  Added OperationTypeChannels support into getOperationsForSignRule, ExistValidSignatureRules, GetValidSignatureRules, VerifySignatureRule procedures - 20009213-2049
-- Furdik       18.11.2016  GetValidSignRulesUsers - returns users for valid signature rules - 20009239-783
-- Pataky       07.12.2016  GetValidSignRules procedure fixed (MCHGIT-1052) - 20009203-6748
-- Pataky       09.12.2016  GetValidSignRules procedure updated - added clients attributes (MCHGIT-1052) - 20009203-6748
-- Snederfler   15.12.2016  GetValidSignRulesForClient and GetValidSignRulesForUser procedures updated - added ValidFrom and ValidUntil attributes - 20009203-6612
-- Furdik       16.12.2016  VerifySufficiencySigning - add TATR option to SLSP - 20009239-2128
-- Pataky       21.12.2016  GetValidSignRules procedure updated - updated search by account (MCHGIT-1135) - 20009203-6748
-- Pataky       06.02.2017  GetValidSignRules procedure updated - added condition on StatusID (MYG-2496) - 20009203-6741
-- Pataky       27.02.2017  GetValidSignRules, GetValidSignatureRules, VerifySignatureRule and VerifySufficiencySigning procedures updated - updated searching rules according to minimal signatures, limits and channels associations
-- Bocak        27.02.2017  GetSumTotal,GetValidSignatureRules,VerifySignatureRule - moved reftab_gsp from select to variables
-- Furdik       14.03.2017  GetSumTotal - add TATR code for tmpDate - 20009239-3117
-- Pataky       14.03.2017  ExistValidSignatureRules procedure updated - signature rule is not available, if user already attached signature - 20009239-3264
-- Pataky       15.03.2017  GetValidSignatureRules procedure fixed - added condition for signature rule channels (MYG-3636) - 20009246-337
-- Pataky       28.03.2017  GetValidSignRules procedure optimized (MYG-3700) - 20009246-337
-- Pataky       31.03.2017  VerifySufficiencySigning procedure updated - added MinimalSignatures check (MYG-3899) - 20009246-337
-- Pataky       10.04.2017  getUsersForSignRule procedure updated - added ClientUsers.StatusID check (MYG-4105) - 20009246-337
-- Pataky       19.04.2017  VerifySufficiencySigning procedure updated - fixed status setting for PARTSIG (not back to FORSIG), removed TATR from conditions - 20009239-4185
-- Furdik       24.04.2017  GetValidSignatureRules - declare lnChangeLimAmount, lnSumTotal as NUMBER - 20009239-4288
-- Pataky       25.04.2017  VerifySufficiencySigning procedure updated - fixed status setting for PARTSIG (not back to FORSIG) even if no signature for checked rule was found - 20009239-4185
-- Furdik       18.05.2017  SetOrChangeSignatureRule add inJournal, VerifySufficiencySigning add inReport - P20009245-4389
-- Emmer        17.07.2017  GetValidSignRules hotfix optimalization (MYG-5227)
-- Emmer        17.07.2017  VerifySignatureRule, GetSumTotal optimalization (MYG-5262)
-- Emmer        20.07.2017  GetValidSignRules optimalization
-- Pataky       08.08.2017  GetValidSignRules further optimalization, removed account number unnecessary conversions
-- Emmer        06.09.2017  transactionstrancache into EXECUTE IMMEDIATE for PRODUCT (MYG-5262)
-- Emmer        27.09.2017  GetSumTotal bugfix (MYG-5964)
-- Emmer        12.12.2017  Add new function ExistValidSignatureRules2 - P20009299-199
-- Emmer        15.01.2018  Add new function ExistValidSignatureRules3 (MYG-7042) - P20009299-265
-- Pataky       05.02.2018  Changes to support backward compatibility after removing SignRuleRoleID from ClientUsers - 20009311-240
-- Pataky       06.04.2018  Fixed mask of start day of week selector in GetSumTotal function to IW
-- Emmer        09.05.2018  Fixed getvalidsignaturerules  with 1000+ of PP (MYG-8203)
-- Emmer        23.05.2018  Fixed getvalidsignaturerules with only one PP (MYG-8414)
-- ----------   ---------- ------------------------------------------
--
UKV         exception;--unique constraint violation
pragma      exception_INIT (UKV,-1);
IKV         exception;--Integrity constraint violation
pragma      exception_INIT (IKV,-2291);
SEC         constant pls_integer := 1/(24*60*60);
--
--=================================================================================================
-- PRO APLIKACNI SERVER (GUS)
--=================================================================================================
-- help functions:
-- 1) vypocet sumy pro kontrolu limitu bezneho obdobi podpisoveho pravidla
function GetSumTotal(
    inSignRuleSeq   in  number,                  -- podpisove pravidlo
    inPeriod        in  pls_integer,                  -- id obdobi podpisoveho pravidla
    ivCurrencyId    IN  VARCHAR2,                     -- id meny, na kterou se vsechny castky prepoctou (mena podpisoveho pravidla)
    inPackageId     in  number default null      -- package, ktera ma byt z vypoctu vyrazena
) return number
is
  ldFrom        date;
  ldTo          date;
  lnSumTotal    ClientTransactionPackages.totalamount%TYPE;
  lDateCheck    varchar2(50):=reftab_gsp('DATEFORINTERVALLIMIT',0,'SIGNEVALDATE');
  tmpDate       date := sysdate;
  --
  lvLocalCurrCTP  VARCHAR2(1):=COALESCE(reftab_gsp('REQUESTS.USE_LOCAL_CURRENCY_IN_CTP',0,'0'),'0');
  lvLocalCurr     VARCHAR2(3):=Reftab_GSP('LOCALCURRENCY',0,'X');
  isRBCZ          NUMBER := CASE WHEN RefTab_GSP('LocalBankCodeAlpha',0,'X') = 'RBCZ' THEN 1 ELSE 0 END;
  v_LocalBankCode VARCHAR2(20) := RefTab_GSP('LocalBankCode',0,'X');
  v_sql           VARCHAR2(32000);
begin
  if lDateCheck = 'VALUEDATE' then
    begin
      select nvl(Valuedate,sysdate) into tmpDate
        from ClientTransactionPackages CTP
       where CTP.PackageID=inPackageId;
    exception when others then null;
    end;
  else
    tmpDate := sysdate;
  end if;
  IF Reftab_GSP('LOCALBANKCODEALPHA') in ('TATR') THEN
    if tmpDate < sysdate then
        tmpDate := sysdate;
    end if;
  END IF;
  if floor(inPeriod/100)=1 then -- na den
    ldFrom:=trunc(tmpDate);
    ldTo  :=trunc(tmpDate+1)-1/24/60/60;
  elsif floor(inPeriod/100)=2 then -- na tyden
    ldFrom:=trunc(tmpDate,'IW');
    ldTo  :=ldFrom+7-1/24/60/60;
  elsif floor(inPeriod/100)=3 then -- na mesic
    ldFrom:=trunc(tmpDate,'mm');
    ldTo  :=trunc(last_day(tmpDate)+1)-1/24/60/60;
  end if;
  if lDateCheck = 'VALUEDATE' then
    IF isRBCZ = 1 THEN
      -- vypocet sumy za podepsane a castecne podepsane packages (prepocet se nedela, byl jiz proveden v ReportClientTransaction.Put)
      v_sql := 'select nvl(sum(GetChangedAmount(CTP.TotalAmount,decode(:lvLocalCurrCTP,''1'',:lvLocalCurr,A.CurrencyID),:ivCurrencyID,A.BranchID)),0)
        from ClientTransactionPackages CTP, Accounts A,
             (select /*+FIRST_ROWS ORDERED*/ distinct CTP1.PackageID
               from ClientTransactionPackages CTP1
       LEFT JOIN Transactions TRN ON CTP1.PackageID = TRN.PackageID
       LEFT JOIN ClientAccounts CA ON CTP1.ClientId = CA.ClientId
       LEFT JOIN Accounts A1 ON CA.AccountID = A1.AccountID
       LEFT JOIN transactionstrancache trnc_acc on trnc_acc.transactionid = trn.transactionid AND trnc_acc.attrid = decode(337,304,12,311,12,5)
       LEFT JOIN transactionstrancache trnc_bc on trnc_bc.transactionid = trn.transactionid AND trnc_bc.attrid = decode(337,304,14,311,14,7)
               where reftab_ctst.FinalStatus(CTP1.StatusID) in (0,1) and CTP1.SignRuleSeq=:inSignRuleSeq
                 and reftab_ot.MakesDebit(CTP1.OperID,0)=1 and CTP1.Packageid<>nvl(:inPackageId,-1)
                 and nvl(CTP1.Valuedate,nvl(CTP1.Signevaldate,CTP1.Borndate)) between TO_DATE('''||TO_CHAR(ldFrom,'DD.MM.YYYY HH24:MI:SS')||''',''DD.MM.YYYY HH24:MI:SS'')
                       and TO_DATE('''||TO_CHAR(ldTo,'DD.MM.YYYY HH24:MI:SS')||''',''DD.MM.YYYY HH24:MI:SS'')
                 and CTP1.StatusID>0
               AND NOT(nvl(trnc_acc.val,''X'') = NVL(A1.accnum,''Y'') AND nvl(trnc_bc.val,''X'') = :v_LocalBankCode)
             and ( CTP1.SigneValDate is null and CTP1.StatusId > 0
                   or (CTP1.SigneValDate between TO_DATE('''||TO_CHAR(ldFrom,'DD.MM.YYYY HH24:MI:SS')||''',''DD.MM.YYYY HH24:MI:SS'')
                       and TO_DATE('''||TO_CHAR(ldTo,'DD.MM.YYYY HH24:MI:SS')||''',''DD.MM.YYYY HH24:MI:SS'') and CTP1.StatusID>=reftab_ot.RecStatusID(CTP1.OperID)))
           ) P
       where CTP.PackageID=P.PackageID and CTP.AccountID=A.AccountID';
       EXECUTE IMMEDIATE v_sql INTO lnSumTotal USING lvLocalCurrCTP, lvLocalCurr,ivCurrencyID,inSignRuleSeq,inPackageId,v_LocalBankCode;
    ELSE
      select nvl(sum(GetChangedAmount(CTP.TotalAmount,decode(lvLocalCurrCTP,'1',lvLocalCurr,A.CurrencyID),ivCurrencyID,A.BranchID)),0) into lnSumTotal
        from ClientTransactionPackages CTP, Accounts A,
             (select PackageID from ClientTransactionPackages CTP1
               where reftab_ctst.FinalStatus(StatusID) in (0,1) and SignRuleSeq=inSignRuleSeq
                 and reftab_ot.MakesDebit(OperID,0)=1 and Packageid<>nvl(inPackageId,-1)
                 and nvl(Valuedate,nvl(Signevaldate,Borndate)) between ldFrom and ldTo
                 and StatusID>0
               AND ( SigneValDate is null and StatusId > 0
               or (SigneValDate between ldFrom and ldTo and StatusID>=reftab_ot.RecStatusID(OperID)))
           ) P
       where CTP.PackageID=P.PackageID and CTP.AccountID=A.AccountID;
    END IF; -- IF isRBCZ = 1
  else
    IF isRBCZ = 1 THEN
      -- vypocet sumy za podepsane a castecne podepsane packages (prepocet se nedela, byl jiz proveden v ReportClientTransaction.Put)
      v_sql := 'select nvl(sum(GetChangedAmount(CTP.TotalAmount,decode(:lvLocalCurrCTP,''1'',:lvLocalCurr,A.CurrencyID),:ivCurrencyID,A.BranchID)),0)
        from ClientTransactionPackages CTP, Accounts A,
             (select /*+FIRST_ROWS ORDERED*/ distinct CTP1.PackageID from ClientTransactionPackages CTP1
       LEFT JOIN Transactions TRN ON CTP1.PackageID = TRN.PackageID
       LEFT JOIN ClientAccounts CA ON CTP1.ClientId = CA.ClientId
       LEFT JOIN Accounts A1 ON CA.AccountID = A1.AccountID
       LEFT JOIN transactionstrancache trnc_acc on trnc_acc.transactionid = trn.transactionid AND trnc_acc.attrid = decode(337,304,12,311,12,5)
       LEFT JOIN transactionstrancache trnc_bc on trnc_bc.transactionid = trn.transactionid AND trnc_bc.attrid = decode(337,304,14,311,14,7)
               where reftab_ctst.FinalStatus(CTP1.StatusID) in (0,1) and CTP1.SignRuleSeq=:inSignRuleSeq
                 and reftab_ot.MakesDebit(CTP1.OperID,0)=1 and CTP1.Packageid<>nvl(:inPackageId,-1)
               AND NOT(nvl(trnc_acc.val,''X'') = NVL(A1.accnum,''Y'') AND nvl(trnc_bc.val,''X'') = :v_LocalBankCode)
                 and ( CTP1.SigneValDate is null and CTP1.StatusId > 0
                       or (CTP1.SigneValDate between TO_DATE('''||TO_CHAR(ldFrom,'DD.MM.YYYY HH24:MI:SS')||''',''DD.MM.YYYY HH24:MI:SS'')
                       and TO_DATE('''||TO_CHAR(ldTo,'DD.MM.YYYY HH24:MI:SS')||''',''DD.MM.YYYY HH24:MI:SS'') and CTP1.StatusID>=reftab_ot.RecStatusID(CTP1.OperID)))
             ) P
       where CTP.PackageID=P.PackageID and CTP.AccountID=A.AccountID';
       EXECUTE IMMEDIATE v_sql INTO lnSumTotal USING lvLocalCurrCTP, lvLocalCurr,ivCurrencyID,inSignRuleSeq,inPackageId,v_LocalBankCode;
    ELSE
      select nvl(sum(GetChangedAmount(CTP.TotalAmount,decode(lvLocalCurrCTP,'1',lvLocalCurr,A.CurrencyID),ivCurrencyID,A.BranchID)),0) into lnSumTotal
        from ClientTransactionPackages CTP, Accounts A,
             (select PackageID from ClientTransactionPackages CTP1
               where reftab_ctst.FinalStatus(StatusID) in (0,1) and SignRuleSeq=inSignRuleSeq
                 and reftab_ot.MakesDebit(OperID,0)=1 and Packageid<>nvl(inPackageId,-1)
               AND ( SigneValDate is null and StatusId > 0
                   or (SigneValDate between ldFrom and ldTo and StatusID>=reftab_ot.RecStatusID(OperID)))
             ) P
       where CTP.PackageID=P.PackageID and CTP.AccountID=A.AccountID;
    END IF; -- IF isRBCZ = 1
  end if; -- if lDateCheck = 'VALUEDATE'
  return lnSumTotal;
End GetSumTotal;
-- end of help functions
-----------------------------------------------------------------------------------------------------
procedure GetValidSignRulesForClient (  -- vraci vsechna podpisova pravidla klienta pro jejich spravu
    inClientID      in  Clients.ClientID%type,       -- Klient prihlaseneho Usera
    orVSRtab        out sys_refcursor                -- seznam pravidel
)
is
begin
  open orVSRtab for
      select
             ClientSignRuleSeq SignRuleID, RuleDescription, AmountLimitCurrID CurrencyID,
             AmountLimit limit_immediate,
             Get_clientsignrulelimit(ClientSignRuleSeq,101) limit_day,
             Get_clientsignrulelimit(ClientSignRuleSeq,201) limit_month,
             Get_clientsignrulelimit(ClientSignRuleSeq,301) limit_year,
             StatusID Status, OrderImportant, ValidFrom, ValidUntil
        from ClientSignRules
       where ClientID=inClientID
         and sysdate between nvl(ValidFrom,trunc(sysdate)) and nvl(ValidUntil,sysdate)
    order by ClientSignRuleSeq;
end GetValidSignRulesForClient;
--------------------------------------------------------------------
procedure GetValidSignRulesForUser (  -- vraci vsechna podpisova pravidla uzivatele pro jejich spravu
    inUserID        in  ClientUsers.UserID%type,    -- Prihlaseny User
    inClientID      in  ClientUsers.ClientID%type,  -- Aktivny klient
    orVSRtab        out sys_refcursor               -- seznam pravidel
)
is
begin
  open orVSRtab for
      select
             CSR.ClientSignRuleSeq SignRuleID,
             CSR.RuleDescription,
             CSR.AmountLimitCurrID CurrencyID,
             CSR.AmountLimit limit_immediate,
             Get_clientsignrulelimit(CSR.ClientSignRuleSeq,101) limit_day,
             Get_clientsignrulelimit(CSR.ClientSignRuleSeq,201) limit_month,
             Get_clientsignrulelimit(CSR.ClientSignRuleSeq,301) limit_year,
             CSR.StatusID Status,
             CSR.OrderImportant,
             CSR.ValidFrom,
             CSR.ValidUntil
        from ClientSignRules CSR,
             clientsignrulesdef CSRD
       where CSR.ClientID=inClientID
         and CSR.ClientSignRuleSeq = CSRD.ClientSignRuleSeq
         and (CSRD.Userid = inUserID or CSRD.signruleroleid = (select cusrr.signruleroleid
                                                                 from clientusers cu, clientusersignrulerole cusrr
                                                                where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = CSR.ClientID
                                                                  and cu.userid = inUserID
                                                              )
             )
         and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
       order by CSR.ClientSignRuleSeq;
end GetValidSignRulesForUser;
--------------------------------------------------------------------
-- vracia userov pre vsetky prave platne podpisove pravidla, ktore mozu byt pouzite
PROCEDURE GetValidSignRulesUsers (inClientID      in  Clients.ClientID%type,      -- Klient prihlaseneho Usera
                                  inPackageID     in  ClientTransactionPackages.PackageID%type, -- Package, ktere se priradi jedno pravidlo ze seznamu
                                  inCertMethodID  in  pls_integer,
                                  inAddNullPK     in  pls_integer default 1,       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
                                  orUserIDs       out SYS_REFCURSOR,
                                  inCheckLimit    IN  PLS_INTEGER DEFAULT 1        -- 0/1 - nekontrolovat/kontrolovat limit
)
IS
lvListSeq      varchar2(32767);
lnUsersCnt     pls_integer;
BEGIN
  lnUsersCnt := 0;
  lvListSeq:='in (';
  for x in (select UserID from ClientUsers where ClientID=inClientID) loop
    if ExistValidSignatureRules(inClientID,inPackageID,inCertMethodID,inAddNullPK,x.UserID,inCheckLimit) = 1 then
      lvListSeq:=lvListSeq||x.UserID||',';
      lnUsersCnt := lnUsersCnt+1;
    end if;
  end loop;
  if lnUsersCnt>0 then
    lvListSeq:=rtrim(lvListSeq,',')||')';
  else
    lvListSeq:=' = -99 and 1=0';  -- nesplnitelna podmienka
  end if;
  open orUserIDs for
    'select UserID from ClientUsers'||
     ' where ClientID='||inClientID||' and UserID '||lvListSeq||
    ' order by 1';
END GetValidSignRulesUsers;
--------------------------------------------------------------------
-- vraci vsechna prave platna podpisova pravidla, ktera mohou byt pouzita
function ExistValidSignatureRules (inClientID      in  Clients.ClientID%type,      -- Klient prihlaseneho Usera
                                   inPackageID     in  ClientTransactionPackages.PackageID%type, -- Package, ktere se priradi jedno pravidlo ze seznamu
                                   inCertMethodID  in  pls_integer,
                                   inAddNullPK     in  pls_integer default 1,       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
                                   inUserID   in  Users.userid%TYPE default null,
                                   inCheckLimit    IN  PLS_INTEGER DEFAULT 1,        -- 0/1 - nekontrolovat/kontrolovat limit
                                   inChannelID     in  OperationTypeChannels.ChannelID%TYPE DEFAULT NULL
)
return pls_integer
is
  lnRetVal      pls_integer := 0;
  orVSRtab      sys_refcursor;
  lnUserCnt     pls_integer;
  lnRoleCnt     pls_integer;
  lnSAUserCnt   pls_integer;
  lnSARoleCnt   pls_integer;
  lnCnt         pls_integer;
  i             pls_integer;
  --
  cursor c is
    select ClientSignRuleSeq SignRuleID, RuleDescription, AmountLimitCurrID CurrencyID,
           AmountLimit limit_immediate,
           Get_clientsignrulelimit(ClientSignRuleSeq,101) limit_day,
           Get_clientsignrulelimit(ClientSignRuleSeq,201) limit_month,
           Get_clientsignrulelimit(ClientSignRuleSeq,301) limit_year,
           StatusID Status,OrderImportant
      from ClientSignRules
     where ClientSignRuleSeq = 0;
  --
  r c%rowtype;
  TYPE rec_SignRulesDef   IS record (UserOrder number, userid number, valid boolean, signruleroleid number);
  TYPE t_SignRulesDef     IS TABLE OF rec_SignRulesDef INDEX BY BINARY_INTEGER;
  ltSignRulesDef          t_SignRulesDef;
  CURSOR cur_SignatureRulesDef(inSignRuleSeq IN NUMBER) IS
    SELECT csr.UserId,csr.UserOrder,csr.signruleroleid,csr.requiredsignaturescount
      FROM ClientSignRulesDef csr
     WHERE csr.ClientSignRuleSeq = inSignRuleSeq
  ORDER BY csr.UserOrder;
begin
  declare
     problem_s_kurzom exception; -- UDEBS::GETCHANGEDAMOUNT Cannot find exchange rate for exchange from currency
     PRAGMA EXCEPTION_INIT (problem_s_kurzom, -20102);
  begin
     GetValidSignatureRules(inClientID,
                            inPackageID,
                            inCertMethodID,
                            orVSRtab,
                            inAddNullPK,
                            inUserID,
                            inCheckLimit,
                            inChannelID
                           );
  exception
     when problem_s_kurzom then
        lnRetVal := 0;
        return lnRetVal;
  end;
  if orVSRtab%isopen then
    loop
      fetch orVSRtab into r;
      exit when orVSRtab%notfound;
      if nvl(r.orderimportant,0) = 1 then -- order is important, get only upcoming signature to check
        -- check if signature is already not attached, and if next one is user or his role
        SELECT count(*)
          into lnCnt
          from SignatureArchive SA,
               (select /*+ RESULT_CACHE */ cu.userid, cusrr.signruleroleid
                                       from clientusers cu, clientusersignrulerole cusrr
                                      where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = inClientID
                                        and cu.userid = inUserID
                                      ) csr
         WHERE PackageId = inPackageID
           AND ContentType = '2'
           AND CertifSig = 1
           AND SA.userid = csr.userid;
        if lnCnt > 0 then
          lnRetVal := 0;
        else
          FOR rec_SignatureRulesDef IN cur_SignatureRulesDef(r.SignRuleId) LOOP
            ltSignRulesDef(cur_SignatureRulesDef%rowcount).UserOrder      := rec_SignatureRulesDef.UserOrder;
            ltSignRulesDef(cur_SignatureRulesDef%rowcount).UserID         := rec_SignatureRulesDef.UserID;
            ltSignRulesDef(cur_SignatureRulesDef%rowcount).SignRuleRoleID := rec_SignatureRulesDef.SignRuleRoleID;
            ltSignRulesDef(cur_SignatureRulesDef%rowcount).Valid := false;
          END LOOP;
          i:=ltSignRulesDef.First;
          for rec in (SELECT SA.UserId, SA.SignRuleRoleID
                        from SignatureArchive SA,
                             (select UserID,
                                     max(SignatureSeq) SignatureSeq
                                FROM SignatureArchive
                               where PackageId = inPackageID
                                 and ContentType='2'
                                 and CertifSig=1
                            group by UserID) B
                       WHERE SA.userid=B.userid
                         AND SA.signatureseq=B.signatureseq
                         AND PackageId = inPackageID
                         AND ContentType='2'
                         AND CertifSig=1
                    ORDER BY SA.signatureseq) loop
            if rec.UserID = nvl(ltSignRulesDef(i).UserId,0) or rec.SignRuleRoleID = nvl(ltSignRulesDef(i).SignRuleRoleID,0) then
              ltSignRulesDef(i).Valid := true; -- next valid signature in correct order
              i:=ltSignRulesDef.Next(i);
              if i is null then
                exit;
              end if;
            else
              -- check if such unused signature is not required in rule later, if so, fail, as it can not be added later - CPA, 14.3.2017 - 20009239-3264
              select count(*) into lnUserCnt from ClientSignRulesDef where ClientSignRuleSeq = r.SignRuleId and UserID is not null and UserID=rec.UserId;
              if lnUserCnt > 0 then
                lnRetVal := 0;
                i:=null;
                exit;
              end if;
            end if;
          end loop;
          if i is null then
            lnRetVal := 0;
          else
            select nvl(sum(case when cu.userid = nvl(ltSignRulesDef(i).UserId,0) then 1 else 0 end),0),
                   nvl(sum(case when cusrr.SignRuleRoleID = nvl(ltSignRulesDef(i).SignRuleRoleID,0) then 1 else 0 end),0)
             into lnUserCnt, lnRoleCnt
            from  clientusers cu, clientusersignrulerole cusrr
            where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = inClientID
              and cu.userid = inUserID
              and (cu.userid = nvl(ltSignRulesDef(i).UserId,0) or cusrr.SignRuleRoleID = nvl(ltSignRulesDef(i).SignRuleRoleID,0));
            if lnUserCnt > 0 then --  user signature is expected in order
              lnRetVal := 1;
            elsif lnRoleCnt > 0  then  --  role signature is expected in order, test if users single signature is not expected later
              select count(*) into lnCnt from ClientSignRulesDef csr
                where csr.ClientSignRuleSeq = r.SignRuleId and csr.UserId in (
                    Select cu.userid from clientusers cu
                    where cu.clientid = inClientID
                      and cu.userid = inUserID)
                   and csr.UserOrder > ltSignRulesDef(i).UserOrder;
              if lnCnt = 0 then -- users single signature is not expected later in rule, so it is expected as rule now
                lnRetVal := 1;
              else              -- users single signature is expected later in rule, so dont allow signature now
                lnRetVal := 0;
              end if;
            else
              lnRetVal := 0;
            end if;
          end if;
        end if;
      else -- order is not important
        -- a) current user is in that signature rule (either with UserID or RoleID)
        select nvl(sum(case when d.userid = csr.userid then 1 else 0 end),0), nvl(sum(case when d.signruleroleid = csr.signruleroleid then 1 else 0 end),0)
          into lnUserCnt, lnRoleCnt
          from ClientSignRulesDef d, (select /*+ RESULT_CACHE */ cu.userid, cusrr.signruleroleid
                                         from clientusers cu, clientusersignrulerole cusrr
                                        where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = inClientID
                                           and cu.userid = inUserID
                                        ) csr
         where d.clientsignruleseq = r.SignRuleId
           and (d.userid = csr.userid or d.signruleroleid = csr.signruleroleid);
        if lnUserCnt > 0 or lnRoleCnt > 0 then
          -- b) current user has not already attached signature
          SELECT nvl(sum(case when SA.userid = csr.userid then 1 else 0 end),0), nvl(sum(case when SA.signruleroleid = csr.signruleroleid then 1 else 0 end),0)
            into lnSAUserCnt, lnSARoleCnt
            from SignatureArchive SA,
                 (select /*+ RESULT_CACHE */ cu.userid, cusrr.signruleroleid
                                         from clientusers cu, clientusersignrulerole cusrr
                                        where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = inClientID
                                           and cu.userid = inUserID
                                        ) csr
           WHERE PackageId = inPackageID
             AND ContentType = '2'
             AND CertifSig = 1
             AND (SA.userid = csr.userid or SA.signruleroleid = csr.signruleroleid);
          -- if user has already used this signature rule, sign rule is not valid
          if lnSAUserCnt > 0 or (lnUserCnt = 0 and lnRoleCnt = 0) then
            lnRetVal := 0;
          elsif (lnUserCnt = 0 and lnRoleCnt > 0 and lnSARoleCnt > 0) then
            -- omit signatures for other users directly (not through role)
            select count(*) into lnCnt from SignatureArchive SA,
                 (select /*+ RESULT_CACHE */ cu.userid, cusrr.signruleroleid
                                         from clientusers cu, clientusersignrulerole cusrr
                                        where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = inClientID
                                          and cu.userid = inUserID
                                        ) csr,
                 (select csrd.UserId from ClientSignRulesDef csrd
                         where csrd.clientsignruleseq = r.SignRuleId) csrdx
            WHERE PackageId = inPackageID
              AND ContentType = '2'
              AND CertifSig = 1
              AND SA.signruleroleid = csr.signruleroleid
              AND SA.UserId = nvl(csrdx.UserId,0);
            if lnRoleCnt <= lnSARoleCnt-lnCnt then
              lnRetVal := 0;
            else
              lnRetVal := 1;
            end if;
          else
            lnRetVal := 1;
          end if;
        else
          lnRetVal := 0;
        end if;
      end if;
      if lnRetVal = 1 then
        exit;
      end if;
    end loop;
    --
    close orVSRtab;
  else
    lnRetVal := 0;
  end if;
  --
  return lnRetVal;
end ExistValidSignatureRules;
--------------------------------------------------------------------
procedure GetValidSignatureRules (  -- vraci vsechna prave platna podpisova pravidla, ktera mohou byt pouzita
    inClientID      in  Clients.ClientID%type,      -- Klient prihlaseneho Usera
    inPackageID     in  ClientTransactionPackages.PackageID%type, -- Package, ktere se priradi jedno pravidlo ze seznamu
    inCertMethodID  in  pls_integer,
    orVSRtab        out sys_refcursor,              -- seznam pravidel
    inAddNullPK     in  pls_integer default 1,       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
    inUserID   in  Users.userid%TYPE default null,
    inCheckLimit    IN  PLS_INTEGER DEFAULT 1,        -- 0/1 - nekontrolovat/kontrolovat limit
    inChannelID     in  OperationTypeChannels.ChannelID%TYPE DEFAULT NULL
)
is
  lnOperID              OperationTypes.OperID%type;  -- operace transakce
  lnAccountID           Accounts.AccountID%type;     -- vlastni ucet
  lnAmount              ClientTransactionPackages.TotalAmount%type;  -- castka package
  lvAmountCurr          Currencies.CurrencyID%type;  -- mena platby
  lnAccountAccessID     ClientAccounts.AccountAccessID%type;
  lnAccSens             Pls_Integer;
  lnMakesDebit          Pls_Integer;
  ltSignRuleSeq         dbms_sql.number_table;
  lvListSeq             CLOB;
  lnChangeLimAmount     NUMBER;
  lnSumTotal            NUMBER;
  ldFrom                date;
  ldTo                  date;
  lnCntErrorLimit       Pls_Integer;
  lnBranchID            Branches.BranchID%type;
  j                     Number;
  lnUserID              Users.userid%TYPE := null;
  lnCancPackageid       Transactions.cancelledpackageid%TYPE:=null;
  lnCntTmp              pls_integer;
  lnCheckLimit          boolean := true;
  lnOwnAccountsTrnCount pls_integer:=0;
  lnCertifReq           pls_integer:=0;
  --
  lvLocalCurrCTP        VARCHAR2(1):=COALESCE(reftab_gsp('REQUESTS.USE_LOCAL_CURRENCY_IN_CTP',0,'0'),'0');
  lvLocalCurr           VARCHAR2(3):=Reftab_GSP('LOCALCURRENCY',0,'X');
  v_sql                 CLOB;
  v_clob_part           CLOB;
  v_c     NUMBER := 0;
begin
  begin
    select OperID,CTP.AccountID,TotalAmount,nvl(CTP.CurrencyID,decode(lvLocalCurrCTP,'1',lvLocalCurr,A.CurrencyID)),nvl(BranchID,0),reftab_ot.AccountSensitive(OperID,0),reftab_ot.MakesDebit(OperID,0),
     reftab_ot.CertifReq(Operid,0,inChannelID)
      into lnOperID,lnAccountID,lnAmount,lvAmountCurr,lnBranchID,lnAccSens,lnMakesDebit, lnCertifReq
      from ClientTransactionPackages CTP,
           Accounts A
     where A.AccountID(+)=CTP.AccountID
      and CTP.StatusId!=33
      and PackageID=inPackageID;
  EXCEPTION WHEN NO_DATA_FOUND THEN
    BEGIN
      select OperID,CTP.AccountID,TotalAmount,nvl(CTP.CurrencyID,decode(lvLocalCurrCTP,'1',lvLocalCurr,A.CurrencyID)),nvl(BranchID,0),reftab_ot.AccountSensitive(OperID,0),reftab_ot.MakesDebit(OperID,0),
       reftab_ot.CertifReq(Operid,0,inChannelID)
        into lnOperID,lnAccountID,lnAmount,lvAmountCurr,lnBranchID,lnAccSens,lnMakesDebit, lnCertifReq
        from CTP_TMP CTP,
             Accounts A
       where A.AccountID(+)=CTP.AccountID
       and PackageID=inPackageID;
    EXCEPTION WHEN NO_DATA_FOUND THEN
      im_error.AddError(2113559,'/unspecified/getvalidsignaturerules/packageid',inPackageID);
    END;
  end;
  begin
   if lnOperID NOT IN (781,786,780) then
    select
           AccountAccessID
      into lnAccountAccessID
      from ClientAccounts
     where ClientID=inClientID
       and AccountID=lnAccountID;
   else -- exception for file transfer operation (781,786,780)
    lnAccountAccessID:= null;
    lvAmountCurr := reftab_gsp('LocalCurrency',0,'EUR');   -- more accounts, so local currency is selected
    select nvl(sum(GetChangedAmount(nvl(amount,0),nvl(currencyId,lvAmountCurr),lvAmountCurr,lnBranchID)),0)
     into lnAmount
     from transactions where packageId = inPackageID;
   end if;
  exception
    when no_data_found then
      im_error.AddError(2113559,'/unspecified/getvalidsignaturerules/clientid',inClientID,lnAccountID);
  end;
  if inUserID is not null then
    BEGIN
      SELECT
             U.userid
        INTO lnUserID
        FROM Users U
       WHERE U.userid=inUserID
         and rownum <= 1;
    EXCEPTION
     when others then
       lnUserID := null;
    end;
  end if;
  for i in (select
                   CSR.ClientSignRuleSeq, CSR.AmountLimit, nvl(CSR.AmountLimitCurrID,lvAmountCurr) AmountLimitCurrID
              from ClientSignRules CSR,
                   ClientSignRuleOper CSRO
             where CSR.ClientSignRuleSeq=CSRO.ClientSignRuleSeq
               and CSR.ClientID=inClientID
               and CSR.StatusID=1
               and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
               and CSRO.OperID=lnOperID
               and lnCertifReq=1 -- only certified operations makes sense
               and (inChannelID is null or exists (select 1 from ClientSignRuleChannel CSRCH where CSR.ClientSignRuleSeq=CSRCH.ClientSignRuleSeq and CSRCH.channelid=inChannelID))
               and (lnUserID is null or exists (select 1
                                                  from ClientSignRulesDef d
                                                 where d.clientsignruleseq = CSR.ClientSignRuleSeq
                                                   and (  d.userid = lnUserID
                                                       or d.signruleroleid = (select cusrr.signruleroleid
                                                                                from clientusers cu, clientusersignrulerole cusrr
                                                                               where cu.clientusersid=cusrr.clientusersid (+) and cu.clientid = CSR.ClientID
                                                                                 and cu.userid = lnUserID
                                                                             )
                                                       )
                                               )
                    )
           ) loop
    if lnAccSens = 1 then -- musi byt svazano pravidlo s uctem
     if lnOperID NOT IN (781,786,780) then
      begin
        select
               ClientSignRuleSeq
          into ltSignRuleSeq(i.ClientSignRuleSeq)
          from ClientSignRuleAccount
         where ClientSignRuleSeq=i.ClientSignRuleSeq
           and AccountAccessID=lnAccountAccessID;
      exception
        when no_data_found then
          goto NEXT_FETCH;
      end;
     else  -- exception for file transfer operation (781,786,780)
      select count(*) into lnCntTmp
          from ClientSignRuleAccount
         where ClientSignRuleSeq=i.ClientSignRuleSeq
           and AccountAccessID in (
               select CA.accountaccessid from ClientAccounts CA, Transactions T
                where CA.ClientID = inClientID
                   and T.packageid = nvl(inPackageID,0)
                   and nvl(T.debitAccountID,0) = CA.AccountID
               );
      if lnCntTmp > 0 then
        ltSignRuleSeq(i.ClientSignRuleSeq):=i.ClientSignRuleSeq;
      else
        goto NEXT_FETCH;
      end if;
     end if;
    else
      ltSignRuleSeq(i.ClientSignRuleSeq):=i.ClientSignRuleSeq;
    end if;
    if lnMakesDebit = 1 AND inCheckLimit=1 then -- kontrola jednorazovych a obdobovych limitu
     IF i.AmountLimit is not null then -- CPA 4.5.2015, AmountLimit can be null - 20009183-2477
      IF lvAmountCurr <> i.AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
        lnChangeLimAmount:=GetChangedAmount(i.AmountLimit,i.AmountLimitCurrID,lvAmountCurr,lnBranchID);
      ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
        lnChangeLimAmount:=i.AmountLimit;
      END IF;
      -- prevody na ucty klienta se vylucuji z kontroly limitu pro RB
      lnCheckLimit := true;
      IF RefTab_GSP('LocalBankCodeAlpha',0,'X') IN ('RBCZ') THEN
       select count(*) into lnOwnAccountsTrnCount
        from Transactions Trn, Accounts A, ClientAccounts CA
        where Trn.Packageid=inPackageID AND A.AccountID = CA.AccountID AND reftab_ctst.FinalStatus(Trn.statusid,0) !=2 and CA.clientid = inClientID
          AND nvl(extractvalue(TRN.trandata,decode(lnOperID,304,'/TranData/DebitAccountNumber/text()',311,'/TranData/DebitAccountNumber/text()','/TranData/CreditAccountNumber/text()')),'X') = A.accnum
          AND nvl(extractvalue(TRN.trandata,decode(lnOperID,304,'/TranData/DebitBankCode/text()',311,'/TranData/DebitBankCode/text()','/TranData/CreditBankCode/text()')),'X') = RefTab_GSP('LocalBankCode',0,'X')
          and rownum<=1;
       IF lnOwnAccountsTrnCount>0 then
         lnCheckLimit := false;
       END IF;
      END IF;
      if lnCheckLimit and lnChangeLimAmount < lnAmount then -- prepoctena castka jednorazoveho limitu je mensi nez castka z package
        ltSignRuleSeq.delete(i.ClientSignRuleSeq);
        goto NEXT_FETCH;
      end if;
     END IF;
     IF lnCheckLimit then
      IF lvAmountCurr <> i.AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
        select count(*)
          into lnCntErrorLimit
          from clientsignrulelimits
         where ClientSignRuleSeq=i.ClientSignRuleSeq
           and GetChangedAmount(AmountLimit,i.AmountLimitCurrID,lvAmountCurr,lnBranchID)<lnAmount;
      ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
        select
               count(*)
          into lnCntErrorLimit
          from clientsignrulelimits
         where ClientSignRuleSeq=i.ClientSignRuleSeq
           and AmountLimit<lnAmount;
      END IF;
      if lnCntErrorLimit > 0 then -- alespon jeden limit pravidla je mensi nez zadana castka
        ltSignRuleSeq.delete(i.ClientSignRuleSeq);
        goto NEXT_FETCH;
      end if;
      for lim in (select AmountLimit,Period
                    from clientsignrulelimits
                   where ClientSignRuleSeq=i.ClientSignRuleSeq
                 ) loop
        lnSumTotal := GetSumTotal(i.ClientSignRuleSeq,lim.Period,lvAmountCurr,inPackageID);
        IF lvAmountCurr <> i.AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
          lnChangelimAmount:=GetChangedAmount(lim.AmountLimit,i.AmountLimitCurrID,lvAmountCurr,lnBranchID);
        ELSE  -- mena uctu a pravidla je stejna, lnAmount je jiz prepoctena castka v mene uctu => zadny prepocet
          lnChangeLimAmount:=lim.AmountLimit;
        END IF;
        if lnSumTotal + lnAmount > lnChangeLimAmount THEN -- suma castek je vetsi nez prepocteny limit
          ltSignRuleSeq.delete(i.ClientSignRuleSeq);
          exit;
        end if;
      end loop;
     end if;
    end if;
    <<NEXT_FETCH>>
    null;
  end loop;
  if ltSignRuleSeq.count>0 then
    lvListSeq:='in (';
    j:=ltSignRuleSeq.first;
    while j is not null loop
      v_c := v_c + 1;
      IF v_c = 900 THEN
        v_c := 0;
        DBMS_LOB.trim(lob_loc => lvListSeq, newlen => DBMS_LOB.getlength(lvListSeq)-1);
        v_clob_part := ') OR ClientSignRuleSeq IN (';
        lvListSeq:= lvListSeq||v_clob_part;
      END IF;
      v_clob_part := ltSignRuleSeq(j)||',';
      lvListSeq:=lvListSeq||v_clob_part;
      j:=ltSignRuleSeq.next(j);
    end loop;
    DBMS_LOB.trim(lob_loc => lvListSeq, newlen => DBMS_LOB.getlength(lvListSeq)-1);
    lvListSeq:=lvListSeq||')';
  else
    lvListSeq:='= -1';  -- nesplnitelna podminka
  end if;
  v_sql := 'select ClientSignRuleSeq SignRuleID, RuleDescription, AmountLimitCurrID CurrencyID,'||
          ' AmountLimit limit_immediate,'||
          ' Get_clientsignrulelimit(ClientSignRuleSeq,101) limit_day,'||
          ' Get_clientsignrulelimit(ClientSignRuleSeq,201) limit_month,'||
          ' Get_clientsignrulelimit(ClientSignRuleSeq,301) limit_year,'||
          ' StatusID Status,OrderImportant'||
     ' from ClientSignRules where ClientSignRuleSeq '||lvListSeq||' UNION '||
    'select 0 SignRuleID, ''<none>'' RuleDescription, reftab_gsp(''LocalCurrency'',0,''EUR'') CurrencyID,'||
          ' 0 limit_immediate, null limit_day, null limit_month, null limit_year,'||
          ' 1 Status, 0 OrderImportant from dual where 1='||to_char(inAddNullPK)||
    ' order by 1';
  open orVSRtab for v_sql;
end GetValidSignatureRules;
--------------------------------------------------------------------
procedure GetValidSignatureRule (                               -- single varianta pro PK
    inSignRuleSeq   in  ClientSignRules.ClientSignRuleSeq%type, -- PK
    orVSRtab        out sys_refcursor,
    inAddNullPK     in  pls_integer default 1       -- 0/1 - pridat/nepridat k zaznamum z tabulky zaznam z dual s PK = 0
)
is
begin
  open orVSRtab for
      select ClientSignRuleSeq SignRuleID, RuleDescription, AmountLimitCurrID CurrencyID,
             AmountLimit limit_immediate,
             Get_clientsignrulelimit(ClientSignRuleSeq,101) limit_day,
             Get_clientsignrulelimit(ClientSignRuleSeq,201) limit_month,
             Get_clientsignrulelimit(ClientSignRuleSeq,301) limit_year,
             StatusID Status,OrderImportant
        from ClientSignRules
       where ClientSignRuleSeq=inSignRuleSeq
       UNION
      select 0 SignRuleID, '<none>' RuleDescription, reftab_gsp('LocalCurrency',0,'EUR') CurrencyID,
             0 limit_immediate, null limit_day, null limit_month, null limit_year,
             1 Status, 0 OrderImportant
        from dual
       where 0=inSignRuleSeq
         and 1=inAddNullPK;
end GetValidSignatureRule;
--------------------------------------------------------------------
procedure GetSignatureRuleForPackage (                          -- vraci pravidlo prirazene package
    onReturnCode    out Number,
    orErrors        out sys_refcursor,
    orSignRule      out sys_refcursor,
    inUserID   in  Users.userid%TYPE,
    inChannelID     in  Channels.ChannelID%type,
    inPackageID     in  ClientTransactionPackages.PackageID%type,
    idCheckoutDate  IN  DATE,
    inOpenError     in  Pls_integer default 1
)
is
begin
  onReturnCode:=0;
  if im_error.ExistErrType(im_error.ET_SUCCESSFUL)=0 then -- neexistuje warning z predchoziho prubehu
    im_error.init;
  end if;
  IF idCheckoutDate IS NOT NULL THEN  -- CTP_TMP
    select decode(Grants(inChannelID, ClientID, inUserID, 1, OperID, AccountID),1,0,1) into onReturnCode
      from CTP_TMP where PackageID=inPackageID;
  ELSE
    select decode(Grants(inChannelID, ClientID, inUserID, 1, OperID, AccountID),1,0,1) into onReturnCode
      from ClientTransactionPackages where PackageID=inPackageID;
  END IF;
  if onReturnCode=0 THEN
    IF idCheckoutDate IS NOT NULL THEN  -- CTP_TMP
      open orSignRule for
          select ClientSignRuleSeq SignRuleID, RuleDescription, AmountLimitCurrID CurrencyID,
                 AmountLimit limit_immediate,
                 Get_clientsignrulelimit(ClientSignRuleSeq,101) limit_day,
                 Get_clientsignrulelimit(ClientSignRuleSeq,201) limit_month,
                 Get_clientsignrulelimit(ClientSignRuleSeq,301) limit_year,
                 CSR.StatusID Status,OrderImportant
            from ClientSignRules CSR,
                 CTP_TMP CTP
           where CSR.ClientSignRuleSeq=CTP.SignRuleSeq
             and CTP.PackageID=inPackageID;
    ELSE
      open orSignRule for
          select ClientSignRuleSeq SignRuleID, RuleDescription, AmountLimitCurrID CurrencyID,
                 AmountLimit limit_immediate,
                 Get_clientsignrulelimit(ClientSignRuleSeq,101) limit_day,
                 Get_clientsignrulelimit(ClientSignRuleSeq,201) limit_month,
                 Get_clientsignrulelimit(ClientSignRuleSeq,301) limit_year,
                 CSR.StatusID Status,OrderImportant
            from ClientSignRules CSR,
                 ClientTransactionPackages CTP
           where CSR.ClientSignRuleSeq=CTP.SignRuleSeq
             and CTP.PackageID=inPackageID;
    END IF;
  else
    onReturnCode:=1;
    im_error.adderror(2113540,'You have no privileges for this operation.');
  end if;
  if inOpenError=1 then im_error.geterrors(orErrors); end if;
exception
  when no_data_found then
    onReturnCode:=1;
    im_error.AddError(2113559,null,'/unspecified/getsignatureruleforpackage/packageid',inPackageID);
    if inOpenError=1 then im_error.geterrors(orErrors); end if;
end GetSignatureRuleForPackage;
--------------------------------------------------------------------
procedure SetOrChangeSignatureRule(
    onReturnCode    out number,
    orErrors        out sys_refcursor,
    inUserID   in  Users.userid%TYPE,
    inChannelID     in  Channels.ChannelID%type,
    inPackageID     in  ClientTransactionPackages.PackageID%type,
    idCheckoutDate  in  date,
    ivSignRuleSeq   in  varchar2,
    inOpenError     in  Pls_integer default 1,
    inJournal       IN  PLS_INTEGER DEFAULT 1
) -- s kontrolou prav
is
  lnAccountID     ClientTransactionPackages.AccountID%type;
  lnOperID        ClientTransactionPackages.OperID%type;
  lnSignRuleSeq   ClientTransactionPackages.SignRuleSeq%type;
  lnOldSignRule   ClientTransactionPackages.SignRuleSeq%type;
  lnClientID      ClientTransactionPackages.ClientID%type;
  lnStatusID      ClientTransactionPackages.StatusID%type;
  lnAccSens       pls_integer;
  lnAccountAccessID number;
  lnCheckoutUserID  pls_integer;
  lnCheckoutChID  pls_integer;
  ldCheckoutDate  date;
  procedure TestSignRule is
  begin
    if lnSignRuleSeq > 0 then   -- 0 = vynulovat pravidlo
      select
             0
        into onReturnCode
        from ClientSignRules CSR,
             ClientSignRuleOper CSRO
       where CSR.ClientSignRuleSeq=CSRO.ClientSignRuleSeq
         and CSR.ClientID=lnClientID
         and CSR.StatusID=1
         and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
         and CSR.ClientSignRuleSeq=lnSignRuleSeq
         and CSRO.OperID=lnOperID;
      if lnAccSens = 1 then -- musi byt svazano pravidlo s uctem
       if nvl(lnOperID,0) NOT IN (781,786,780) then
        select
               0
          into onReturnCode
          from ClientSignRuleAccount
         where ClientSignRuleSeq=lnSignRuleSeq and AccountAccessID=lnAccountAccessID;
       else -- specific functionality for file transfer operation (781,786,780)
        select decode(count(*),0,1,0)
          into onReturnCode
          from ClientSignRuleAccount
         where ClientSignRuleSeq=lnSignRuleSeq and AccountAccessID in (
              select CA.accountaccessid from ClientAccounts CA, Transactions T
                where CA.ClientID = lnClientID
                   and T.packageid = nvl(inPackageID,0)
                   and nvl(T.debitAccountID,0) = CA.AccountID
            );
       end if;
      end if;
    end if;
  exception when no_data_found then
    onReturnCode:=1;
    im_error.AddError(2113581,null,'/unspecified/signruleseq',ivSignRuleSeq);
  end TestSignRule;
  ---------------
  PROCEDURE journaling IS
    lvDesc        VARCHAR2(1024 CHAR);
  BEGIN
    IF inJournal!=1 THEN
      RETURN;
    END IF;
    --
    IF lnSignRuleSeq <> NVL(lnOldSignRule,0) THEN
      IF lnSignRuleSeq = 0 THEN
        lvDesc:='none';
      ELSE
        SELECT RuleDescription INTO lvDesc FROM ClientSignRules
        WHERE CLIENTSIGNRULESEQ=lnSignRuleSeq;
      END IF;
      JOURNAL001.setparevseq(null,inPackageid);
      JOURNAL001.logchildevent(1700008,inUserID,systimestamp,null,lvDesc,'GUSRS',inchannelid,'UserID',inUserID,lnclientid);
    END IF;
  END journaling;
begin
  onReturnCode:=0;
  if im_error.ExistErrType(im_error.ET_SUCCESSFUL)=0 then -- neexistuje warning z predchoziho prubehu
    im_error.init;
  end if;
  begin
    lnSignRuleSeq:=char2num(nvl(ivSignRuleSeq,'0'));
  exception when others then
    onReturnCode:=1;
    im_error.AddError(2113575,null,'/unspecified/signruleseq',ivSignRuleSeq);
  end;
  if onReturnCode = 0 then
    if idCheckoutDate is not null then -- edit
      select CA.AccountID,OperID,UserID,ChannelID,BornDate,NVL(CA.ClientID, CTP_TMP.clientId),reftab_ot.AccountSensitive(OperID,0),AccountAccessID,SignRuleSeq
        into lnAccountID,lnOperID,lnCheckoutUserID,lnCheckoutChID,ldCheckoutDate,lnClientId,lnAccSens,lnAccountAccessID,lnOldSignRule
        from CTP_TMP,
             ClientAccounts CA
       where PackageID=inPackageID
         and CTP_TMP.ClientID=CA.ClientID(+)
         and CTP_TMP.AccountID=CA.AccountID(+);
      TestSignRule;
      if onReturnCode = 0 then
        if Grants(inChannelID,lnClientId, inUserID, 1, lnOperID, lnAccountID) = 1 and
           inUserID=lnCheckoutUserID and inChannelID=lnCheckoutChID and idCheckoutDate=ldCheckoutDate
        then
          update CTP_TMP
             set SignRuleSeq=decode(lnSignRuleSeq,0,null,lnSignRuleSeq)
           where PackageID=inPackageID
             and get_ctst_values('AllowSign',StatusID,0)<>0; -- TEMP,EDIT,PARSED,FORSIG,PARTSIG
          if sql%rowcount=1 then
            onReturnCode:=0;
            journaling;
          else
            onReturnCode:=1;
            im_error.AddError(2113614,null,'/unspecified/setorchangesignaturerule/packageid',inPackageID);
          end if;
        else
          onReturnCode:=1;
          im_error.adderror(2113540,'You have no privileges for this operation.');
        end if;
      end if;
    ELSE  -- no edit
      select CA.AccountID,OperID,NVL(CA.ClientID, CTP.ClientId),reftab_ot.AccountSensitive(OperID,0),AccountAccessID,SignRuleSeq,CTP.statusid
        into lnAccountID,lnOperID,lnClientId,lnAccSens,lnAccountAccessID,lnOldSignRule,lnStatusID
        from ClientTransactionPackages CTP,
             ClientAccounts CA
       where PackageID=inPackageID
         and CTP.ClientID=CA.ClientID(+)
         and CTP.AccountID=CA.AccountID(+);
      TestSignRule;
      if onReturnCode = 0 then
        if Grants(inChannelID, lnClientId, inUserID, 1, lnOperID, lnAccountID) = 1 then
          update ClientTransactionPackages
             set SignRuleSeq=decode(lnSignRuleSeq,0,null,lnSignRuleSeq)
           where PackageID=inPackageID
             and get_ctst_values('AllowSign',StatusID,0)<>0; -- TEMP,EDIT,PARSED,FORSIG,PARTSIG
          if sql%rowcount=1 then
            onReturnCode:=0;
            IF lnStatusID<>-1 THEN  -- neprirazuje se pravidlo tmp package
              journaling;
            END IF;
          else
            onReturnCode:=1;
            im_error.AddError(2113614,null,'/uspecified/setorchangesignaturerule/packageid',inPackageID);
          end if;
        else
          onReturnCode:=1;
          im_error.adderror(2113540,'You have no privileges for this operation.');
        end if;
      end if;
    end if;
  end if; -- onReturnCode = 0
  if inOpenError=1 then im_error.geterrors(orErrors); end if;
exception when no_data_found then
  onReturnCode:=1;
  im_error.AddError(2113559,null,'/uspecified/setorchangesignaturerule/packageid',inPackageID);
  if inOpenError=1 then im_error.geterrors(orErrors); end if;
end SetOrChangeSignatureRule;
--------------------------------------------------------------------
procedure VerifySignatureRule(
    inPackageID   in  ClientTransactionPackages.PackageID%type,
    inSignRuleSeq in  ClientTransactionPackages.SignRuleSeq%type,
    onReturnCode  out number,
    inChannelID   in  OperationTypeChannels.ChannelID%TYPE DEFAULT NULL
)
is
  lnAccountAccessID     ClientAccounts.AccountAccessID%type;
  lnAccSens             Pls_Integer;
  lnMakesDebit          Pls_Integer;
  lnClientID            Pls_Integer;
  lnAmount              ClientTransactionPackages.TotalAmount%type := 0;
  lnTotalAmount         ClientTransactionPackages.TotalAmount%type := 0;
  lnOperID              Number;
  lvAmountCurr          Varchar2(3);
  lnBranchID            Branches.BranchID%type;
  lnAmountLimit         ClientTransactionPackages.TotalAmount%type := 0;
  lvAmountLimitCurrID   Varchar2(3);
  lnChangeLimAmount     ClientTransactionPackages.TotalAmount%type := 0;
  lnSumTotal            ClientTransactionPackages.TotalAmount%type := 0;
  ldFrom                date;
  ldTo                  date;
  lnCntErrorLimit       Pls_Integer;
  lnStatusID            ClientTransactionPackages.StatusID%type;
  lnChannelID           ClientTransactionPackages.ChannelID%type;
  otSubtotals           Subtotal_tab;
  lnCheckLimit          boolean := true;
  lnOwnAccountsTrnCount pls_integer:=0;
  lnCertifReq           pls_integer:=1;
  --
  lvLocalCurrCTP        VARCHAR2(1):=COALESCE(reftab_gsp('REQUESTS.USE_LOCAL_CURRENCY_IN_CTP',0,'0'),'0');
  lvLocalCurr           VARCHAR2(3):=Reftab_GSP('LOCALCURRENCY',0,'X');
  isRBCZ                NUMBER := CASE WHEN RefTab_GSP('LocalBankCodeAlpha',0,'X') = 'RBCZ' THEN 1 ELSE 0 END;
  v_sql                 VARCHAR2(32000);
begin
  onReturnCode:=0;
  -- zistenie vlastnosti AccountSensitive a CertifReq
  select reftab_ot.AccountSensitive(OperID,0), StatusID, ChannelID, OperID, reftab_ot.CertifReq(Operid,0,inChannelID)
    into lnAccSens, lnStatusID, lnChannelID, lnOperID, lnCertifReq
    from ClientTransactionPackages
   where packageId = inPackageID;
  if lnCertifReq=1 then -- makes sense to check only for certified operations
    if lnAccSens = 1 then -- AccountSensitive = 1
     if lnOperID NOT IN (781,786,780) then
      select CTP.ClientID, TotalAmount, decode(lvLocalCurrCTP,'1',lvLocalCurr,A.CurrencyID), A.BranchID, AccountAccessID,
             reftab_ot.AccountSensitive(OperID,0), reftab_ot.MakesDebit(OperID,0)
        into lnClientID, lnTotalAmount, lvAmountCurr, lnBranchID, lnAccountAccessID, lnAccSens, lnMakesDebit
        from ClientAccounts CA,
             ClientTransactionPackages CTP,
             Accounts A
       where CTP.PackageID=inPackageID
         and CTP.SignRuleSeq=inSignRuleSeq
         and CA.ClientID=CTP.ClientID
         and CA.AccountID=CTP.AccountID
         and A.AccountID=CA.AccountID;
     else -- exception for file transfer operation (781,786,780) - not connected to account
      select ClientID, 0, reftab_gsp('LocalCurrency',0,'EUR'), 0, NULL,
             reftab_ot.AccountSensitive(OperID,0), reftab_ot.MakesDebit(OperID,0)
        into lnClientID, lnAmount, lvAmountCurr, lnBranchID, lnAccountAccessID, lnAccSens, lnMakesDebit
        from ClientTransactionPackages
       where PackageID=inPackageID
         and SignRuleSeq=inSignRuleSeq;
      select nvl(sum(GetChangedAmount(nvl(amount,0),nvl(currencyId,lvAmountCurr),lvAmountCurr,lnBranchID)),0)
       into lnAmount
       from transactions where packageId = inPackageID;
     end if;
    else -- AccountSensitive = 0
      select ClientID, TotalAmount, CurrencyID, NULL, NULL,
             reftab_ot.AccountSensitive(OperID,0), reftab_ot.MakesDebit(OperID,0)
        into lnClientID, lnAmount, lvAmountCurr, lnBranchID, lnAccountAccessID, lnAccSens, lnMakesDebit
        from ClientTransactionPackages
       where PackageID=inPackageID
         and SignRuleSeq=inSignRuleSeq;
    end if;
    select 0, CSR.AmountLimit, nvl(CSR.AmountLimitCurrID,lvAmountCurr) AmountLimitCurrID
      into onReturnCode, lnAmountLimit,lvAmountLimitCurrID
      from ClientSignRules CSR,
           ClientSignRuleOper CSRO
     where CSR.ClientSignRuleSeq=CSRO.ClientSignRuleSeq
       and CSR.ClientID=lnClientID
       and CSR.StatusID=1
       and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
       and CSR.ClientSignRuleSeq=inSignRuleSeq
       and CSRO.OperID=lnOperID;
    if lnAccSens = 1 then -- musi byt svazano pravidlo s uctem
     if lnOperID NOT IN (781,786,780) then
      select 0
        into onReturnCode
        from ClientSignRuleAccount
       where ClientSignRuleSeq=inSignRuleSeq
         and AccountAccessID=lnAccountAccessID;
     else -- exception for file transfer operation (781,786,780) - not connected to account
      select decode(count(*),0,1,0)
        into onReturnCode
        from ClientSignRuleAccount
       where ClientSignRuleSeq=inSignRuleSeq
            and AccountAccessID in (
                 select CA.accountaccessid from ClientAccounts CA, Transactions T
                  where CA.ClientID = lnClientID
                     and T.packageid = nvl(inPackageID,0)
                     and nvl(T.debitAccountID,0) = CA.AccountID
                 );
     end if;
    end if;
  else
    onReturnCode := 1;
  end if;
  if onReturnCode = 0 and lnMakesDebit = 1 then -- kontrola jednorazovych a obdobovych limitu
    -- prevody na ucty klienta se vylucuji z kontroly limitu pro RB
    lnCheckLimit := true;
    IF isRBCZ = 1 THEN
     v_sql := 'select count(*)
      from Transactions Trn
       JOIN ClientAccounts CA ON CA.clientid = :lnClientID
       JOIN Accounts A ON A.AccountID = CA.AccountID
       LEFT JOIN transactionstrancache trnc_acc on trnc_acc.transactionid = trn.transactionid AND trnc_acc.attrid = decode(:lnOperID,304,12,311,12,5)
       LEFT JOIN transactionstrancache trnc_bc on trnc_bc.transactionid = trn.transactionid AND trnc_bc.attrid = decode(:lnOperID,304,14,311,14,7)
      where Trn.Packageid=:inPackageID
       AND reftab_ctst.FinalStatus(Trn.statusid,0) !=2
        AND nvl(trnc_acc.val,''X'') = A.accnum
        AND nvl(trnc_bc.val,''X'') = RefTab_GSP(''LocalBankCode'',0,''X'')
        and rownum<=1';
     EXECUTE IMMEDIATE v_sql INTO lnOwnAccountsTrnCount USING lnClientID, lnOperID, lnOperID, inPackageID;
     IF lnOwnAccountsTrnCount>0 then
       lnCheckLimit := false;
     END IF;
    END IF; -- IF isRBCZ = 1
   IF lnCheckLimit and lnAmountLimit is not null then -- CPA 4.5.2015 , limitAmount not necessary - 20009183-2477
    IF lvAmountCurr <> lvAmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
      lnChangeLimAmount:=GetChangedAmount(lnAmountLimit,lvAmountLimitCurrID,lvAmountCurr,lnBranchID);
    ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
      lnChangeLimAmount:=lnAmountLimit;
    END IF;
   end if;
   if lnCheckLimit then
    begin
      -- vypocitame aktualny total amount pre vsetky transakcie package
      lnAmount := GetPackageAmount (otSubtotals,
                                    inPackageId,
                                    lnOperID,
                                    inStatusID => NULL,
                                    inSeqNum => NULL);
      journal001.logevent(1700012,'VerifySignatureRule.CurrentTotalAmount',systimestamp,systimestamp,'<PackageID>'||inPackageId||'</PackageID><TotalAmount>'||to_char(lnTotalAmount)||'</TotalAmount><SignRuleSeq>'||to_char(inSignRuleSeq)||'</SignRuleSeq><TotalAmount>'||to_char(lnAmount)||'</TotalAmount><AmountCurrency>'||lvAmountCurr||'</AmountCurrency><AmountLimit>'||to_char(lnAmountLimit)||'</AmountLimit><AmountLimitCurrID>'||lvAmountLimitCurrID||'</AmountLimitCurrID>',
                         'IIF_CCFG_005',lnChannelID,null,null,lnClientID);
    exception
      when others then
        journal001.logevent(1700012,'VerifySignatureRule.Error',systimestamp,systimestamp,'<PackageID>'||inPackageId||'</PackageID><TotalAmount>'||to_char(lnTotalAmount)||'</TotalAmount><SignRuleSeq>'||to_char(inSignRuleSeq)||'</SignRuleSeq><TotalAmount>'||to_char(lnAmount)||'</TotalAmount><AmountCurrency>'||lvAmountCurr||'</AmountCurrency><AmountLimit>'||to_char(lnAmountLimit)||'</AmountLimit><AmountLimitCurrID>'||lvAmountLimitCurrID||'</AmountLimitCurrID>',
                           'IIF_CCFG_005', lnChannelID,null,null,lnClientID, aExtension => dbms_utility.format_error_stack);
    end;
   IF lnAmountLimit is not null then -- CPA 4.5.2015 , limitAmount not necessary - 20009183-2477
    if lnChangeLimAmount < lnAmount then -- prepoctena castka jednorazoveho limitu je mensi nez castka z package
      onReturnCode:=1;
    end if;
   end if;
    IF lvAmountCurr <> lvAmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
      select count(*)
        into lnCntErrorLimit
        from clientsignrulelimits
       where ClientSignRuleSeq=inSignRuleSeq
         and GetChangedAmount(AmountLimit,lvAmountLimitCurrID,lvAmountCurr,lnBranchID)<lnAmount;
    ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
      select count(*)
        into lnCntErrorLimit
        from clientsignrulelimits
       where ClientSignRuleSeq=inSignRuleSeq
         and AmountLimit<lnAmount;
    END IF;
    if lnCntErrorLimit > 0 then -- alespon jeden limit pravidla je mensi nez zadana castka
      onReturnCode:=1;
    end if;
    if onReturnCode = 0 then -- dosud bez chyb
      for lim in (select AmountLimit,Period
                    from clientsignrulelimits
                   where ClientSignRuleSeq=inSignRuleSeq
                 ) loop
        lnSumTotal := GetSumTotal(inSignRuleSeq,lim.Period,lvAmountCurr,inPackageID);
        IF lvAmountCurr <> lvAmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
          lnChangelimAmount:=GetChangedAmount(lim.AmountLimit,lvAmountLimitCurrID,lvAmountCurr,lnBranchID);
        ELSE  -- mena uctu a pravidla je stejna, lnAmount je jiz prepoctena castka v mene uctu => zadny prepocet
          lnChangeLimAmount:=lim.AmountLimit;
        END IF;
        if lnSumTotal + lnAmount > lnChangeLimAmount THEN -- suma castek je vetsi nez prepocteny limit
          onReturnCode:=1;
          exit;
        end if;
      end loop;
    end if;
   end if;
  end if;
exception when others then
  onReturnCode:=1;
end VerifySignatureRule;
--
procedure VerifySufficiencySigning( -- testuje podpisy podle pravidla
    inPackageID   in  ClientTransactionPackages.PackageID%type,
    inSignRuleSeq in  ClientTransactionPackages.SignRuleSeq%type,
    onReturnCode  out number,
    orErrors      out sys_refcursor,
    inOpenError   in  Pls_integer default 1,
    inReport      IN  PLS_INTEGER DEFAULT 1
)
is
  lnSignRuleSeq           ClientTransactionPackages.SignRuleSeq%type;
  TYPE rec_Signatures     IS record (SignatureOrder number, userid number, used boolean, signruleroleid number);
  TYPE t_Signatures       IS TABLE OF rec_Signatures INDEX BY BINARY_INTEGER;
  TYPE rec_SignRulesDef   IS record (UserOrder number, userid number, valid boolean, signruleroleid number);
  TYPE t_SignRulesDef     IS TABLE OF rec_SignRulesDef INDEX BY BINARY_INTEGER;
  ltSignatures            t_Signatures;
  ltSignRulesDef          t_SignRulesDef;
  lnOrderImportant        Pls_integer;
  lnMinimalSignatures     ClientSignRules.minimalsignatures%TYPE;
  lnCountOfUsedSignatures pls_integer:=0;
  lnSignatureOrder        Pls_integer := 0;
  lnOperID                Pls_integer;
  lnChannelID             Pls_integer;
  lnStatusID              Pls_integer;
  lnOldStatusID           Pls_integer;
  lbPartiallySigned       Boolean;
  lbSufficientlySigned    Boolean := TRUE;
  i                       Pls_integer;
  j                       Pls_integer;
  lnSignatureFound        Pls_integer;
  k                       Pls_integer;
  ldSigneValDate          DATE;
  CURSOR cur_Signatures(inPckID in number) IS
    SELECT SA.UserId, SA.SignRuleRoleID
      from SignatureArchive SA,
           (select UserID,
                   max(SignatureSeq) SignatureSeq
              FROM SignatureArchive
             where PackageId = inPckId
               and ContentType='2'
               and CertifSig=1
          group by UserID) B
     WHERE SA.userid=B.userid
       AND SA.signatureseq=B.signatureseq
       AND PackageId = inPckId
       AND ContentType='2'
       AND CertifSig=1
  ORDER BY SA.signatureseq;
  -- uzivatele, kteri musi podepsat dokument platny podle daneho podpisoveho pravidla, serazeni podle poradi
  CURSOR cur_SignatureRulesDef(inSignRuleSeq IN NUMBER) IS
    SELECT csr.UserId,csr.UserOrder,csr.signruleroleid,csr.requiredsignaturescount
      FROM ClientSignRulesDef csr
     WHERE csr.ClientSignRuleSeq = inSignRuleSeq
  ORDER BY csr.UserOrder;
begin
  onReturnCode:=0;
  if im_error.ExistErrType(im_error.ET_SUCCESSFUL)=0 then -- neexistuje warning z predchoziho prubehu
    im_error.init;
  end if;
  begin
    select SignRuleSeq,OperId,ChannelID,StatusID
      into lnSignRuleSeq,lnOperID,lnChannelID, lnOldStatusID
      from ClientTransactionPackages
     where PackageID=inPackageID
       and nvl(SignRuleSeq,0)=nvl(inSignRuleSeq,nvl(SignRuleSeq,0));
  exception
    when no_data_found then
      onreturnCode:=1;
      im_error.AddError(2113559,null,'/unspecified/verifysufficiencysigning/packageid',inPackageID,inSignRuleSeq);
  end;
  if lnSignRuleSeq is not null then -- jiz prirazene podpisove pravidlo
    VerifySignatureRule(inPackageID, lnSignRuleSeq, onReturnCode);
  elsif reftab_ot.SignRulesReq(lnOperID,0)=1 then -- podpis musi byt podle pravidla, ktere neni dosud nastaveno
    onReturnCode:=1;
  end if;
  if onReturnCode = 1 then
    IF Reftab_GSP('LOCALBANKCODEALPHA') in ('SLSP') THEN
      lnOldStatusID := null;
    END IF;
    lbPartiallySigned := nvl(lbPartiallySigned,FALSE);
    lbSufficientlySigned:=FALSE;
    onReturnCode:=0;
    if lnSignRuleSeq is not null then
      im_error.AddError(2105346,'Signature rule was set to null during verification of sufficienty of signing because rule was recognized as unsufficient or as invalid.',
                                '/unspecified/verifysufficiencysigning/packageid',inPackageID,inSignRuleSeq);-- nastavene pravidlo je jiz neplatne a bylo smazano
    end if;
  else -- platne pravidlo nebo operace nevyzaduje pravidlo, ale jenom podpis
    -- podpisy do pomocne tabulky
    FOR rec_Signatures IN cur_Signatures(inPackageID) LOOP
      ltSignatures(cur_Signatures%rowcount).SignatureOrder := cur_Signatures%rowcount;
      ltSignatures(cur_Signatures%rowcount).UserID         := rec_Signatures.UserID;
      ltSignatures(cur_Signatures%rowcount).SignRuleRoleID := rec_Signatures.SignRuleRoleID;
      ltSignatures(cur_Signatures%rowcount).Used           := false;
    END LOOP;
    if reftab_ot.SignRulesReq(lnOperID,0)=1 then -- musi byt podpis podle pravidla
      -- users do pomocne tabulky
      FOR rec_SignatureRulesDef IN cur_SignatureRulesDef(lnSignRuleSeq) LOOP
        ltSignRulesDef(cur_SignatureRulesDef%rowcount).UserOrder      := rec_SignatureRulesDef.UserOrder;
        ltSignRulesDef(cur_SignatureRulesDef%rowcount).UserID         := rec_SignatureRulesDef.UserID;
        ltSignRulesDef(cur_SignatureRulesDef%rowcount).SignRuleRoleID := rec_SignatureRulesDef.SignRuleRoleID;
        ltSignRulesDef(cur_SignatureRulesDef%rowcount).Valid := false;
      END LOOP;
      select OrderImportant, MinimalSignatures
        into lnOrderImportant, lnMinimalSignatures
        from ClientSignRules
       where ClientSignRuleSeq=lnSignRuleSeq;
      i:=ltSignRulesDef.First;
      while i is not null LOOP
        j:=ltSignatures.First;
        lnSignatureFound := null;
        while j is not null LOOP
          if ltSignatures(j).Used = false then  -- skip used signatures
            if ltSignatures(j).UserID = nvl(ltSignRulesDef(i).UserId,0) then
              -- userid found, use it
              lnSignatureFound := j;
              exit;
            end if;
            if ltSignatures(j).SignRuleRoleID = nvl(ltSignRulesDef(i).SignRuleRoleID,0) then
              -- signruleroleid found, check rest of definitions if direct userid in can be found
              k:=ltSignRulesDef.First;
              while k is not null LOOP
                if ltSignRulesDef(k).Valid = false then  -- skip used signatures
                  if ltSignatures(j).UserID = nvl(ltSignRulesDef(k).UserId,0) then
                    exit;
                  end if;
                end if;
                k:=ltSignRulesDef.Next(k);
              end loop;
              if k is null then -- no direct user definition found, using signature as role
                lnSignatureFound := j;
                exit;
              end if;
            end if;
          end if;
          j:=ltSignatures.Next(j);
        end loop;
        IF lnSignatureFound is not null THEN
          IF lnOrderImportant = 1 THEN
            IF ( ((ltSignatures(lnSignatureFound).UserID = nvl(ltSignRulesDef(i).UserId,0))  AND lnSignatureOrder < ltSignatures(lnSignatureFound).SignatureOrder)
                OR ((ltSignatures(lnSignatureFound).SignRuleRoleID = nvl(ltSignRulesDef(i).SignRuleRoleID,0))  AND lnSignatureOrder < ltSignatures(lnSignatureFound).SignatureOrder)
               )
            THEN -- naposledy overeny podpis package musi predchazet stavajicimu podpisu
              IF i = 1 THEN -- prvni uzivatel podle pravidla
                ltSignRulesDef(i).Valid:=True;
                -- zapamatujeme si poradi overeneho podpisu
                lnSignatureOrder:=ltSignatures(lnSignatureFound).SignatureOrder;
                lbPartiallySigned := TRUE;
                ltSignatures(lnSignatureFound).Used := true;
              ELSIF ltSignRulesDef(i-1).Valid then -- predchozi podpis je platny => stavajici bude taky platny
                ltSignRulesDef(i).Valid:=True;
                -- zapamatujeme si poradi overeneho podpisu
                lnSignatureOrder:=ltSignatures(lnSignatureFound).SignatureOrder;
                lbPartiallySigned := TRUE;
                ltSignatures(lnSignatureFound).Used := true;
              ELSE -- podpis vlozen mimo poradi stanovene pravidlem
                ltSignRulesDef(i).Valid:=False;
                lbPartiallySigned := nvl(lbPartiallySigned,FALSE);
                lbSufficientlySigned:=FALSE;
              END IF;
            ELSE -- posledni overeny podpis nepredchazi stavajicimu
              ltSignRulesDef(i).Valid:=False;
              lbPartiallySigned := nvl(lbPartiallySigned,FALSE);
              lbSufficientlySigned:=FALSE;
            END IF;
          ELSE -- nezalezi na poradi => podpis je platny
            ltSignRulesDef(i).Valid:=True;
            lbPartiallySigned := TRUE;
            ltSignatures(lnSignatureFound).Used := true;
          END IF;
          IF ltSignatures(lnSignatureFound).Used and lnMinimalSignatures is not null then -- check count of used signatures, if it is not sufficient
            lnCountOfUsedSignatures:=lnCountOfUsedSignatures+1;
            if lnCountOfUsedSignatures >= lnMinimalSignatures then
              lbSufficientlySigned:=TRUE;
              i := null;
              EXIT;
            end if;
          END IF;
        ELSE -- dosud neexistuje podpis
          -- lnOldStatusID:=null;  -- commented due to 20009239-4185 by CPA, 25.4.2017, old status always remains, because of cyclic signature rules tests
          ltSignRulesDef(i).Valid:=False;
          lbPartiallySigned := nvl(lbPartiallySigned,FALSE);
          lbSufficientlySigned:=FALSE;
        END IF;
        i:=ltSignRulesDef.Next(i);
      END LOOP;
    elsif reftab_ot.CertifReq(lnOperID,0)=1 then -- musi byt platny podpis
      if ltSignatures.count=0 then
        lbSufficientlySigned:=False;
        lbPartiallySigned:=False;
      end if;
    end if;
  end if;   -- onReturnCode=1
  if lbSufficientlySigned then
    update ClientTransactionPackages
       set SigneValDate=sysdate
     where PackageID=inPackageID;
    lnStatusID:=get_ot_values('RECSTATUSID',lnOperID);
  elsif lbPartiallySigned or nvl(lnOldStatusID,31) = 32 THEN
    IF Reftab_GSP('LOCALBANKCODEALPHA') in ('SLSP') THEN
      ldSigneValDate:=SYSDATE;
    ELSE
      ldSigneValDate:=NULL;
    END IF;
    update ClientTransactionPackages
       set SigneValDate=ldSigneValDate
     where PackageID=inPackageID;
    lnStatusID:=32;
  else
    update ClientTransactionPackages
       set SigneValDate=null
     where PackageID=inPackageID;
    lnStatusID:=31;
  end if;
  ReportClientTransaction.Init;
--  ReportClientTransaction.Put(lnOperID, lnChannelID, inPackageID, 0, lnStatusID, 'VERIFYSUFFSIGN');
  ReportClientTransaction.Put(aOperId     => lnOperID,
                              aChannelID  => lnChannelID,
                              aPackageId  => inPackageID,
                              aSeqNum     => 0,
                              aStatusId   => lnStatusID,
                              aOriginator => 'VERIFYSUFFSIGN',
--                              aRelatedData VARCHAR2:=NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0,aBankRef VARCHAR2 := NULL, aBankEnsurePackageReport VARCHAR2 DEFAULT NULL,
--                              aCalculateAmount  NUMBER DEFAULT 1,aChangePackageStatus  NUMBER DEFAULT 1,
                              inReport    => inReport);
  ReportClientTransaction.Done;
  if inOpenError=1 then im_error.geterrors(orErrors); end if;
end VerifySufficiencySigning;
--
procedure getSgnStatusForRuleInPackage( -- vraci seznam pozadovanych podpisu users podle pravidla a ID ulozenych podpisu
    inUserID in  NUMBER,
    inChannelID   in  NUMBER,
    inPackageID   in  NUMBER,
    orSignStatus  out SYS_REFCURSOR
)
is
  lnAccountID         ClientTransactionPackages.AccountID%type;
  lnOperID            ClientTransactionPackages.OperID%type;
  lnOrderImportant    ClientSignRules.OrderImportant%type;
  lnSignRuleSeq       ClientTransactionPackages.SignRuleSeq%type;
  lnClientId          ClientSignRules.ClientId%type;
begin
  select OrderImportant,AccountID,OperID,SignRuleSeq,CSR.clientid
    into lnOrderImportant,lnAccountID,lnOperID,lnSignRuleSeq,lnClientId
    from ClientSignRules CSR, ClientTransactionPackages CTP
   where CSR.ClientSignRuleSeq=CTP.SignRuleSeq
     and PackageID=inPackageID;
  if Grants(inChannelID, lnClientId, inUserID, 1, lnOperID, lnAccountID) = 1 then
    open orSignStatus for
      select inPackageID PackageID,CSRD.UserID,decode(lnOrderImportant,1,UserOrder,0,0) UserOrder,
             SA.SignatureSeq, SA.MethodID, SA.CertSerialNumber DeviceID, SA.SignRuleRoleID
        from ClientSignRulesDef CSRD,
             SignatureArchive SA,
             (select UserID,max(SignatureSeq) SignatureSeq FROM SignatureArchive
               where PackageId = inPackageId and ContentType='2' and CertifSig=1 group by UserID) MSA
       where CSRD.ClientSignRuleSeq = lnSignRuleSeq
         and CSRD.UserID=MSA.UserID(+)
         and MSA.SignatureSeq=SA.SignatureSeq(+);
  else
    open orSignStatus for
      select 0 PackageID, 0 UserID, 0 UserOrder, 0 SignatureSeq, 0 MethodID, 0 DeviceID, 0 SignRuleRoleID
        from dual
       where 1=2;
  end if;
exception
  when no_data_found then -- neprirazeno pravidlo
    open orSignStatus for
      select 0 PackageID, 0 UserID, 0 UserOrder, 0 SignatureSeq, 0 MethodID, 0 DeviceID, 0 SignRuleRoleID
        from dual
       where 1=2;
end getSgnStatusForRuleInPackage;
--------------------------------------------------------------------
procedure getSgnStatusForUsersInPackage(-- vraci seznam podpisu package a pripadne poradi z definovaneho pravidla pro package
    inUserID in  NUMBER,
    inChannelID   in  NUMBER,
    inPackageID   in  NUMBER,
    orSignStatus  out SYS_REFCURSOR
)
is
  lnAccountID         ClientTransactionPackages.AccountID%type;
  lnOperID            ClientTransactionPackages.OperID%type;
  lnOrderImportant    ClientSignRules.OrderImportant%type;
  lnSignRuleSeq       ClientTransactionPackages.SignRuleSeq%type;
  lnClientId          ClientSignRules.ClientId%type;
  lnCount             number;
begin
  select OrderImportant,AccountID,OperID,SignRuleSeq,CSR.ClientId
    into lnOrderImportant,lnAccountID,lnOperID,lnSignRuleSeq,lnClientId
    from ClientSignRules CSR,
         ClientTransactionPackages CTP
   where CSR.ClientSignRuleSeq(+)=CTP.SignRuleSeq
     and PackageID=inPackageID;
  if Grants(inChannelID, lnClientId, inUserID, 1, lnOperID, lnAccountID) = 1 then
    if lnSignRuleSeq is not null then -- pravidlo prirazeno, ale muze byt prazdne
      select count(*)
        into lnCount
        from ClientSignRulesDef
       where ClientSignRuleSeq=lnSignRuleSeq; -- 0=pravidlo prazdne=>UserOrder=null
      open orSignStatus for
        select inPackageID PackageID,SA.UserID,decode(lnOrderImportant,1,UserOrder,0,decode(lnCount,0,null,0)) UserOrder,
               SA.SignatureSeq, SA.MethodID, SA.CertSerialNumber DeviceID, SA.SignRuleRoleID
          from ClientSignRulesDef CSRD,
               SignatureArchive SA,
               (select UserID,max(SignatureSeq) SignatureSeq FROM SignatureArchive
                 where PackageId = inPackageId and ContentType='2' and CertifSig=1 group by UserID) MSA
         where CSRD.UserID(+)=MSA.UserID
           and CSRD.ClientSignRuleSeq(+)=lnSignRuleSeq
           and MSA.SignatureSeq=SA.SignatureSeq;
    else -- existuji podpisy pro package bez pravidla
      open orSignStatus for
        select SA.PackageID,SA.UserID,null UserOrder,SA.SignatureSeq,SA.MethodID,SA.CertSerialNumber DeviceID, SA.SignRuleRoleID
          from SignatureArchive SA,
               (select max(SignatureSeq) SignatureSeq,PackageID,UserID from SignatureArchive
                 where PackageId = inPackageId and ContentType='2' and CertifSig=1
                 group by PackageID,UserID) MSA
         where SA.SignatureSeq=MSA.SignatureSeq;
    end if;
  else
    open orSignStatus for
      select 0 PackageID, 0 UserID, 0 UserOrder, 0 SignatureSeq, 0 MethodID, 0 DeviceID, 0 SignRuleRoleID
        from dual
       where 1=2;
  end if;
exception
  when no_data_found then
    im_error.AddError(-20102,'Entity does not exist','getSgnStatusForUsersInPackage:PackageID',inPackageID);
end getSgnStatusForUsersInPackage;
--
procedure getSgnStatus( -- vraci podpis podle PK
    inPackageID   in  NUMBER,
    inUserID      in  NUMBER,
    orSignature   out SYS_REFCURSOR
)
is
  lnOrderImportant    ClientSignRules.OrderImportant%type;
  lnSignRuleSeq       ClientTransactionPackages.SignRuleSeq%type;
  lnCount             number;
begin
  select OrderImportant,SignRuleSeq into lnOrderImportant,lnSignRuleSeq
    from ClientSignRules CSR,
         ClientTransactionPackages CTP
   where CSR.ClientSignRuleSeq(+)=CTP.SignRuleSeq
     and PackageID=inPackageID;
  if lnSignRuleSeq is not null then -- pravidlo prirazeno, ale muze byt prazdne
    select count(*)
      into lnCount
      from ClientSignRulesDef
     where ClientSignRuleSeq=lnSignRuleSeq; -- 0=pravidlo prazdne=>UserOrder=null
    open orSignature for
    -- existuje podpis
      select inPackageID PackageID,inUserID UserID,
             decode(lnOrderImportant,1,UserOrder,0,decode(lnCount,0,null,0)) UserOrder,
             SA.SignatureSeq,SA.MethodID,SA.CertSerialNumber DeviceID, SA.SignRuleRoleID
        from ClientSignRulesDef CSRD,
             SignatureArchive SA,
             (select UserID,max(SignatureSeq) SignatureSeq FROM SignatureArchive
               where PackageId = inPackageId and ContentType='2' and CertifSig=1 and UserID=inUserID
               group by UserID) MSA
       where MSA.SignatureSeq=SA.SignatureSeq
         and CSRD.UserID(+)=MSA.UserID
         and CSRD.ClientSignRuleSeq(+) = lnSignRuleSeq
      UNION
    -- existuje pravidlo
      select inPackageID PackageID,inUserID UserID,
             decode(lnOrderImportant,1,UserOrder,0,decode(lnCount,0,null,0)) UserOrder,
             SA.SignatureSeq,SA.MethodID,SA.CertSerialNumber DeviceID, SA.SignRuleRoleID
        from ClientSignRulesDef CSRD,
             SignatureArchive SA,
             (select UserID,max(SignatureSeq) SignatureSeq FROM SignatureArchive
               where PackageId = inPackageId and ContentType='2' and CertifSig=1 and UserID=inUserID
               group by UserID) MSA
       where CSRD.UserID=inUserID
         and CSRD.ClientSignRuleSeq = lnSignRuleSeq
         and CSRD.UserID=MSA.UserID(+)
         and MSA.SignatureSeq=SA.SignatureSeq(+);
  else -- mozna existuji podpisy pro package bez pravidla
    open orSignature for
      select inPackageID PackageID,inUserID UserID,null UserOrder,
             SA.SignatureSeq,SA.MethodID,SA.CertSerialNumber DeviceID, SA.SignRuleRoleID
        from SignatureArchive SA,
             (select max(SignatureSeq) SignatureSeq from SignatureArchive
               where PackageId = inPackageId and ContentType='2' and CertifSig=1 and UserID=inUserID) MSA
       where SA.SignatureSeq=MSA.SignatureSeq;
  end if;
exception
  when no_data_found then
    im_error.AddError(-20102,'Entity does not exist','getSgnStatus:PackageID',inPackageID);
end getSgnStatus;
--------------------------------------------------------------------
procedure getAccountsForSignRule( -- vraci ucty spojene s podpisovym pravidlem
    inUserID in  NUMBER,            -- uzivatel, optional
    inClientID    in  NUMBER,       -- klient, nepouziva se, NULL
    inChannelID   in  NUMBER,       -- kanal, optional
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    orAccounts    out SYS_REFCURSOR
)
is
begin
  open orAccounts for
    select
           distinct
           A.BRANCHID,A.ACCOUNTID,A.ACCTYPEID,A.CURRENCYID,A.NAME,A.ACCNUM,A.BISACCOUNTID,
           A.BISBRANCHID,A.CCACCOUNTID,A.SOURCETYPEID,A.PROCESSINGTYPE,A.TAXID,nvl(A.ADJUSTEDBALANCE,0) AdjustedBalance
      from RightTypes rt,
           ClientRights cr,
           ClientUserRightProfiles curp,
           ClientUsers cu,
           ClientAccounts ca,
           RightAssignClientAccounts raca,
           ClientSignRuleAccount csa,
           ClientSignRules CSR,
           Accounts a
     where ca.clientid = csr.clientid
       and ca.clientid = cu.clientid
       and cu.clientusersid = curp.clientusersid
       and rt.ChannelId = inChannelID
       and rt.AccessTypeId>=1
       and rt.RightId = cr.RightId
       and cu.UserId = nvl(inUserID,cu.UserId)
       and ca.AccountId = a.accountid
       and csa.accountaccessid=ca.accountaccessid
       and raca.ClientRightId = cr.ClientRightId
       and raca.RightProfileId = curp.RightProfileId
       and raca.AccountAccessId = ca.AccountAccessId
       and ca.StatusId=1
       and csa.clientsignruleseq=inSignRuleID and csa.clientsignruleseq = csr.clientsignruleseq;
end getAccountsForSignRule;
--------------------------------------------------------------------
procedure getUsersForSignRule(      -- vraci uzivatele spojene s podpisovym pravidlem
    inUserID in  NUMBER,            -- uzivatel, nepouziva se, NULL
    inChannelID   in  NUMBER,       -- kanal, nepouziva se, NULL
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    orUsers       out SYS_REFCURSOR
)
is
begin
  open orUsers for
    select /*+ INDEX (CSR idx_clientsignrules_statuscli) */ U.userid, U.statusid, U.borndate, U.langid, U.title, U.position,
       U.firstname, U.lastname, U.personalnum, U.identitycardnum,
       U.contactphone, U.contactfax, U.contactgsm, U.contactemail,
       U.address1, U.address2, U.address3, U.address4, U.address5,
       U.address6, U.serviceunblockkey, U.externalcauserid,
       U.ppd_restrict, U.ppd_block, U.ppd_wipe, U.user_options,
       U.middlename, U.xmlext, U.dmtimestamp, U.usertype, U.hostuserid,
       null lastlogintime, U.entitytimestamp, U.birthdate, CSRD.signruleroleid, CSRD.userorder, CSRD.requiredsignaturescount,
       SRR.role, SRR.description, SRR.systemflag
      from ClientUsers CU, Users U, clientusersignrulerole cusrr,
           ClientSignRulesDef CSRD,
           ClientSignRules CSR,
           signruleroles SRR
     where (U.UserID=CSRD.UserID or CSRD.signruleroleid = CUsrr.signruleroleid)
       and CSRD.signruleroleid = SRR.signruleroleid  (+)
       and CU.clientusersid = cusrr.clientusersid (+)
       and CSR.clientsignruleseq=inSignRuleID
       and CSRD.clientsignruleseq=CSR.clientsignruleseq
       and CSR.ClientID = CU.ClientID and U.UserID = CU.UserID and CU.StatusID = 1
  order by CSRD.UserOrder,U.LastName;
end getUsersForSignRule;
--------------------------------------------------------------------
procedure getOperationsForSignRule( -- vraci operace spojene s podpisovym pravidlem
    inUserID in  NUMBER,
    inClientID    in  NUMBER,
    inChannelID   in  NUMBER,
    inSignRuleID  in  NUMBER,
    ivLangID      in  VARCHAR2,
    orOperations  out SYS_REFCURSOR
)
is
begin
  open orOperations for
    select
           distinct
           ot.OperID,gettext(ot.textid,ivLangID) text,ot.groupid,ot.batch,ot.structcode, nvl(otc.signrulesreq,ot.signrulesreq) signrulesreq, nvl(otc.certifreq,ot.certifreq) certifreq
      from RightTypes rt,
           ClientRights cr,
           ClientUserRightProfiles curp,
           ClientUsers cu,
           RightProfileDefinitions rpd,
           ClientSignRuleOper cso,
           ClientSignRules CSR,
           OperationTypes ot,
           OperationTypeChannels otc
     where cr.clientid = csr.ClientID
       and cu.clientusersid = curp.clientusersid
       and cu.clientid = cr.clientid
       and rt.ChannelId = inChannelID
       and rt.AccessTypeId>=1
       and rt.RightId = cr.RightId
       and cu.UserId = nvl(inUserID,cu.UserId)
       and rpd.ClientRightId = cr.ClientRightId
       and rpd.RightProfileId = curp.RightProfileId
       and rt.operid=cso.operid
       and cso.operid=ot.operid
       and ot.operid = otc.operid (+) and nvl(inChannelID,0)=otc.channelid (+)
       and cso.clientsignruleseq=inSignRuleID
       and cso.clientsignruleseq = CSR.clientsignruleseq;
end getOperationsForSignRule;
--------------------------------------------------------------------
procedure getAvailableLimits(
    inSignRuleSeq in  Number,
    onDayLimit    out Number,
    onMonthLimit  out Number,
    onYearLimit   out Number
)
is
  lnSumTotal            ClientTransactionPackages.totalamount%type;
  ldFrom                date;
  ldTo                  date;
  lvAmountLimitCurrID   Varchar2(3);
begin
  select AmountLimitCurrID into lvAmountLimitCurrID from ClientSignRules
   where ClientSignRuleSeq=inSignRuleSeq;
  for lim in (select AmountLimit,Period from clientsignrulelimits
               where ClientSignRuleSeq=inSignRuleSeq
             ) loop
    lnSumTotal := GetSumTotal(inSignRuleSeq,lim.Period,lvAmountLimitCurrID);
    if floor(lim.Period/100)=1 then -- na den
      onDayLimit:= lim.AmountLimit - lnSumTotal;
    elsif floor(lim.Period/100)=2 then -- na tyden
      onMonthLimit:= lim.AmountLimit - lnSumTotal;
    elsif floor(lim.Period/100)=3 then -- na mesic
      onYearLimit:= lim.AmountLimit - lnSumTotal;
    end if;
  end loop;
end getAvailableLimits;
--------------------------------------------------------------------
procedure AutoGenerateSignRules(           -- automatic generation of signing rules for client - deletes old rules !
    inClientID    in  ClientSignRules.clientid%type
) IS
 -- in case of error, exception is thrown
   ldValidUntil date;
BEGIN
  -- invalidate all old signature rules for client
  for rec in (select clientsignruleseq,validuntil
                from clientsignrules
               where clientid = inClientID
                 and statusid = 1
                 and sysdate between validfrom and nvl(validuntil,sysdate+1) ) loop
    if nvl(rec.validuntil,sysdate+1) > sysdate then
      ldValidUntil:=sysdate;
    else
      ldValidUntil:=rec.validuntil;
    end if;
    update clientsignrules
        set validuntil=ldValidUntil,
            statusid=0
      where clientsignruleseq=rec.clientsignruleseq;
  end loop;
  -- loop through client's active users and add rules for them
  for rec in (select userid from clientusers where clientid = inClientID and statusid = 1) loop
    -- AutoAddSignRulesForUser(rec.userid);
    null;
  end loop;
END;
--------------------------------------------------------------------
function getCertSgnStatusForUser( -- vracia 0 alebo 1 podla toho ci uzivatel prilozil k package cert.podpis
    inPackageID   in  NUMBER,
    inUserID      in  NUMBER
) return number is
  lnReturn pls_integer:=0;
BEGIN
  select decode(count(*),0,0,1)
    into lnReturn
    FROM SignatureArchive
   where PackageId = inPackageId
     and ContentType='2'
     and CertifSig=1
     and UserID=inUserID; -- 24.11.2014, MIL: podmienka je, ze sa plni UserID aj v pripade ked UserID vystupuje v podpisovej roli
  return lnReturn;
END getCertSgnStatusForUser;
--------------------------------------------------------------------
--
-- Client-User sign rule roles
--
Procedure PutSDClientSignRuleRole(itClientSignRuleRole IN OUT NOCOPY t_rec_ClientSignRuleRole)
is
  lnSignRuleRoleId  clientusersignrulerole.SignruleRoleId%TYPE;
  lnTextId          SignRuleRoles.textId%TYPE;
  lnClientId        SignRuleRoles.ClientID%TYPE;
begin
  if itClientSignRuleRole.Role is null Or itClientSignRuleRole.Description is null then
    raise_application_error(-20110,'Role And/Or Description must be not empty');
  else
    begin
      select c.ClientId
        into lnClientId
        from clients c
       where c.ClientId = itClientSignRuleRole.ClientId;
    exception
      when no_data_found then
        raise_application_error(-20138,'Client not found [ClientId='||to_char(itClientSignRuleRole.ClientId)||']');
    end;
    -- novy zaznam
    if itClientSignRuleRole.SignRuleRoleId is null then
      begin
        insert into SignRuleRoles (SignRuleRoleId, ClientId, Role, SystemFlag, Description)
                           values (seq_signruleroles.nextval, itClientSignRuleRole.ClientId, upper(itClientSignRuleRole.Role), 0, itClientSignRuleRole.Description)
        returning SignRuleRoleId, Role into itClientSignRuleRole.SignRuleRoleId, itClientSignRuleRole.Role;
      exception
        when dup_val_on_index then
          raise_application_error(-20137,'Sign rule role name should be unique within client [SignRuleRoleId='||upper(itClientSignRuleRole.Role)||']');
      end;
    else
      -- update
      update SignRuleRoles
         set ClientId = NVL(itClientSignRuleRole.ClientId, ClientId),
             Role = NVL(upper(itClientSignRuleRole.Role), Role),
             Description = NVL(itClientSignRuleRole.Description, Description)
       where SignRuleRoleId = itClientSignRuleRole.SignRuleRoleId;
      if sql%rowcount = 0 then
        raise_application_error(-20136,'Sign rule role not found [SignRuleRoleId='||to_char(itClientSignRuleRole.SignRuleRoleId)||']');
      end if;
    end if;
  end if;
end PutSDClientSignRuleRole;
--
--------------------------------------------------------------------
procedure GetValidSignRules (  -- vraci vsechna podpisova pravidla pouzitelna pro kombinaci vstupnich parametru
    inChannelID           in  clientsignrulechannel.channelid%TYPE,  -- kanal pozadovany v pravidlu, mandatory
    ivHostUserID          in  Users.HostUserID%type,                 -- Prihlaseny User (PartyID), optional
    ivHostClientID        in  ClientHostAllocation.HostClientID%type,-- klient pozadovany v pravidlu, optional
    inOperID              in  clientsignruleoper.operid%type,        -- pozadovana operace v pravidlu, optional
    ivAccountNumberPrefix in  varchar2,                              -- prefix uctu pozadovaneho v pravidlu, optional
    ivAccountNumber       in  varchar2,                              -- cislo uctu pozadovaneho v pravidlu, optional
    ivBankCode            in  varchar2,                              -- banka uctu pozadovaneho v pravidlu, optional
    inAmount              in  number,                                -- castka operace pro kontrolu limitu v pravidlu, optional
    ivCurrencyId          in  varchar2,                              -- mena castky pro kontrolu limitu v pravidlu, optional
    orVSRtab              out sys_refcursor                          -- seznam pravidel vybranych podle podminek
) IS
  lnMakesDebit          Pls_Integer := 1;
  lnAccSensitive        Pls_Integer := 1;
  ltSignRuleSeq         dbms_sql.number_table;
  lvListSeq             varchar2(32767);
  lnChangeLimAmount     ClientTransactionPackages.TotalAmount%type;
  lnSumTotal            ClientTransactionPackages.TotalAmount%type;
  lnCntErrorLimit       Pls_Integer;
  j                     Number;
  lvBankCode            varchar2(20):=reftab_gsp('LOCALBANKCODE',0,'5500');
  lvAccountNumber       varchar2(100):=b24_format_accnum(ivAccountNumberPrefix||'-'||ivAccountNumber)||'/'||nvl(ivBankCode,reftab_gsp('LOCALBANKCODE',0,'5500'));
  lvAccountId           ACCOUNTS.ACCOUNTID%TYPE;
  TYPE cur_typ IS REF CURSOR;
  c cur_typ;
  v_query               VARCHAR2(32767);
  i_ClientSignRuleSeq   CLIENTSIGNRULES.CLIENTSIGNRULESEQ%TYPE;
  i_AmountLimitCurrID     CLIENTSIGNRULES.AMOUNTLIMITCURRID%TYPE;
  i_AmountLimit           CLIENTSIGNRULES.AMOUNTLIMIT%TYPE;
  i_minimalsignatures     CLIENTSIGNRULES.MINIMALSIGNATURES%TYPE;
  i_ClientID              CLIENTSIGNRULES.CLIENTID%TYPE;
begin
  if inOperID is not null then
    select nvl(MakesDebit,0),nvl(reftab_ot.AccountSensitive(inOperID,1),1) into lnMakesDebit,lnAccSensitive from operationTypes where operid = inOperID;
  end if;
  if ivAccountNumber is null or lnAccSensitive=0 then
    for i in (select CSR.ClientSignRuleSeq, CSR.AmountLimitCurrID, CSR.AmountLimit, CSR.minimalsignatures, CHA.ClientID
          from ClientSignRules CSR, Clients C, ClientHostAllocation CHA
         where CHA.ClientID = CSR.CLientID and CHA.ClientID = C.ClientID and CHA.hostclientid = nvl(ivHostClientID,CHA.hostclientid)
           and exists (select 1 from ClientSignRulesDef CSRD, Users U, ClientUsers CU, clientusersignrulerole CUSRR
                   where CSRD.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CU.ClientID = CSR.ClientID and CU.Userid = U.UserID and U.HostUserID = nvl(ivHostUserID,U.HostUserID)
                        and CU.clientusersid = CUSRR.clientusersid (+)
                        and (CSRD.UserId = CU.UserId or CSRD.signruleroleid = CUSRR.signruleroleid)
               )
           and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
           and nvl(CSR.statusID,1) = 1
           and exists (select 1 from ClientSignRuleOper CSRO where CSRO.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CSRO.OperID = nvl(inOperID,CSRO.OperID))
           and exists (select 1 from ClientSignRuleChannel CSRC where CSRC.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CSRC.ChannelID = nvl(inChannelID,CSRC.ChannelID))
         order by CSR.ClientSignRuleSeq) loop
      ltSignRuleSeq(i.ClientSignRuleSeq):=i.ClientSignRuleSeq;
      if lnMakesDebit = 1 and inAmount is not null and ivCurrencyId is not null then -- kontrola jednorazovych a obdobovych limitu
       IF i.AmountLimit is not null then -- CPA 4.5.2015, AmountLimit can be null - 20009183-2477
        IF ivCurrencyId <> i.AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
          lnChangeLimAmount:=GetChangedAmount(i.AmountLimit,i.AmountLimitCurrID,ivCurrencyId,0);
        ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
          lnChangeLimAmount:=i.AmountLimit;
        END IF;
        if lnChangeLimAmount < inAmount then -- prepoctena castka jednorazoveho limitu je mensi nez castka z package
          ltSignRuleSeq.delete(i.ClientSignRuleSeq);
          goto NEXT_FETCH;
        end if;
       END IF;
       IF ivCurrencyId <> i.AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
          select count(*) into lnCntErrorLimit from clientsignrulelimits
           where ClientSignRuleSeq=i.ClientSignRuleSeq
             and GetChangedAmount(AmountLimit,i.AmountLimitCurrID,ivCurrencyId,0)<inAmount;
       ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
          select count(*) into lnCntErrorLimit from clientsignrulelimits
           where ClientSignRuleSeq=i.ClientSignRuleSeq
             and AmountLimit<inAmount;
       END IF;
       if lnCntErrorLimit > 0 then -- alespon jeden limit pravidla je mensi nez zadana castka
          ltSignRuleSeq.delete(i.ClientSignRuleSeq);
          goto NEXT_FETCH;
       end if;
       for lim in (select AmountLimit,Period  from clientsignrulelimits where ClientSignRuleSeq=i.ClientSignRuleSeq
                   ) loop
          lnSumTotal := GetSumTotal(i.ClientSignRuleSeq,lim.Period,ivCurrencyId,null);
          IF ivCurrencyId <> i.AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
            lnChangelimAmount:=GetChangedAmount(lim.AmountLimit,i.AmountLimitCurrID,ivCurrencyId,0);
          ELSE  -- mena uctu a pravidla je stejna, lnAmount je jiz prepoctena castka v mene uctu => zadny prepocet
            lnChangeLimAmount:=lim.AmountLimit;
          END IF;
          if lnSumTotal + inAmount > lnChangeLimAmount THEN -- suma castek je vetsi nez prepocteny limit
            ltSignRuleSeq.delete(i.ClientSignRuleSeq);
            exit;
          end if;
       end loop;
      end if;
      <<NEXT_FETCH>>
      null;
    end loop;
  else
    IF nvl(ivBankCode,lvBankCode) = lvBankCode THEN
       SELECT AccountId INTO lvAccountId FROM Accounts A WHERE A.Accnum = lvAccountNumber;
    ELSE
       lvAccountId:=0;
    END IF;
    IF ivAccountNumber IS NOT NULL AND inOperID IS NOT NULL AND ivHostClientID IS NULL AND ivHostUserID IS NULL AND inChannelID IS NOT NULL THEN
     v_query := 'select /*+ORDERED FIRST_ROWS*/ distinct CSR.ClientSignRuleSeq, CSR.AmountLimitCurrID, CSR.AmountLimit, CSR.minimalsignatures, CSR.ClientID
          from ClientAccounts CA /*:ivHostClientID*/
          JOIN ClientSignRuleAccount CSRA ON CSRA.AccountAccessId = CA.AccountAccessID AND NVL(1,:ivHostClientID) = 1 AND NVL(1,:ivHostUserID) = 1
          JOIN ClientSignRules CSR ON CSRA.ClientSignRuleSeq=CSR.ClientSignRuleSeq AND CA.ClientID = CSR.ClientID
          JOIN ClientSignRuleOper CSRO ON CSRO.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CSRO.OperID = :inOperID
          JOIN ClientSignRuleChannel CSRC ON CSRC.ChannelID = :inChannelID AND CSRC.ClientSignRuleSeq=CSR.ClientSignRuleSeq
          JOIN ClientSignRulesDef CSRD ON CSRD.ClientSignRuleSeq=CSR.ClientSignRuleSeq
          JOIN ClientUsers CU ON CU.ClientID = CSR.ClientID and (CSRD.UserId = CU.UserId or CSRD.signruleroleid in (SELECT CUSRR.signruleroleid from ClientUserSignRuleRole CUSRR where CUSRR.clientusersid=CU.clientusersid))
         where CA.AccountID = :lvAccountId
           and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
           and nvl(CSR.statusID,1) = 1
         order by CSR.ClientSignRuleSeq';
    ELSE
     v_query := 'select CSR.ClientSignRuleSeq, CSR.AmountLimitCurrID, CSR.AmountLimit, CSR.minimalsignatures, CHA.ClientID
          from ClientSignRules CSR, Clients C, ClientHostAllocation CHA
         where CHA.ClientID = CSR.CLientID and CHA.ClientID = C.ClientID and CHA.hostclientid = nvl(:ivHostClientID,CHA.hostclientid)
           and exists (select 1 from ClientSignRulesDef CSRD, Users U, ClientUsers CU
                   where CSRD.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CU.ClientID = CSR.ClientID and CU.Userid = U.UserID and U.HostUserID = nvl(:ivHostUserID,U.HostUserID)
                        and (CSRD.UserId = CU.UserId or CSRD.signruleroleid in (SELECT CUSRR.signruleroleid from ClientUserSignRuleRole CUSRR where CUSRR.clientusersid=CU.clientusersid))
               )
           and sysdate between nvl(CSR.ValidFrom,trunc(sysdate)) and nvl(CSR.ValidUntil,sysdate)
           and nvl(CSR.statusID,1) = 1
           and exists (select 1 from ClientSignRuleOper CSRO where CSRO.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CSRO.OperID = nvl(:inOperID,CSRO.OperID))
           and exists (select 1 from ClientSignRuleChannel CSRC where CSRC.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CSRC.ChannelID = nvl(:inChannelID,CSRC.ChannelID))
           and exists (select 1 from ClientSignRuleAccount CSRA, ClientAccounts CA
                where CSRA.ClientSignRuleSeq=CSR.ClientSignRuleSeq and CSRA.AccountAccessId = CA.AccountAccessID and CA.ClientID = CSR.ClientID and CA.AccountID = :lvAccountId)
         order by CSR.ClientSignRuleSeq';
    END IF;
    OPEN c FOR v_query USING ivHostClientID,ivHostUserID,inOperID,inChannelID,lvAccountId;
    LOOP
      FETCH c INTO i_ClientSignRuleSeq, i_AmountLimitCurrID, i_AmountLimit, i_minimalsignatures, i_ClientID;
      EXIT WHEN c%NOTFOUND;
      ltSignRuleSeq(i_ClientSignRuleSeq):=i_ClientSignRuleSeq;
      if lnMakesDebit = 1 and inAmount is not null and ivCurrencyId is not null then -- kontrola jednorazovych a obdobovych limitu
       IF i_AmountLimit is not null then -- CPA 4.5.2015, AmountLimit can be null - 20009183-2477
        IF ivCurrencyId <> i_AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
          lnChangeLimAmount:=GetChangedAmount(i_AmountLimit,i_AmountLimitCurrID,ivCurrencyId,0);
        ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
          lnChangeLimAmount:=i_AmountLimit;
        END IF;
        if lnChangeLimAmount < inAmount then -- prepoctena castka jednorazoveho limitu je mensi nez castka z package
          ltSignRuleSeq.delete(i_ClientSignRuleSeq);
          goto NEXT_FETCH2;
        end if;
       END IF;
       IF ivCurrencyId <> i_AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
          select count(*) into lnCntErrorLimit from clientsignrulelimits
           where ClientSignRuleSeq=i_ClientSignRuleSeq
             and GetChangedAmount(AmountLimit,i_AmountLimitCurrID,ivCurrencyId,0)<inAmount;
       ELSE -- mena uctu a limitu stejna, castka v package je v mene uctu => zadny prepocet
          select count(*) into lnCntErrorLimit from clientsignrulelimits
           where ClientSignRuleSeq=i_ClientSignRuleSeq
             and AmountLimit<inAmount;
       END IF;
       if lnCntErrorLimit > 0 then -- alespon jeden limit pravidla je mensi nez zadana castka
          ltSignRuleSeq.delete(i_ClientSignRuleSeq);
          goto NEXT_FETCH2;
       end if;
       for lim in (select AmountLimit,Period  from clientsignrulelimits where ClientSignRuleSeq=i_ClientSignRuleSeq
                   ) loop
          lnSumTotal := GetSumTotal(i_ClientSignRuleSeq,lim.Period,ivCurrencyId,null);
          IF ivCurrencyId <> i_AmountLimitCurrID THEN   -- mena uctu neni stejna jako mena pravidla => prepocet limitu pravidla
            lnChangelimAmount:=GetChangedAmount(lim.AmountLimit,i_AmountLimitCurrID,ivCurrencyId,0);
          ELSE  -- mena uctu a pravidla je stejna, lnAmount je jiz prepoctena castka v mene uctu => zadny prepocet
            lnChangeLimAmount:=lim.AmountLimit;
          END IF;
          if lnSumTotal + inAmount > lnChangeLimAmount THEN -- suma castek je vetsi nez prepocteny limit
            ltSignRuleSeq.delete(i_ClientSignRuleSeq);
            exit;
          end if;
       end loop;
      end if;
      <<NEXT_FETCH2>>
      null;
    END LOOP;
  end if;
  if ltSignRuleSeq.count>0 then
    lvListSeq:='in (';
    j:=ltSignRuleSeq.first;
    while j is not null loop
      lvListSeq:=lvListSeq||ltSignRuleSeq(j)||',';
      j:=ltSignRuleSeq.next(j);
    end loop;
    lvListSeq:=rtrim(lvListSeq,',')||')';
  else
    lvListSeq:='= -1';  -- nesplnitelna podminka
  end if;
  open orVSRtab for
      'select
             CSR.ClientSignRuleSeq SignRuleID,
             CSR.RuleDescription,
             CSR.AmountLimitCurrID OneTimeLimitCurrencyID,
             CSR.AmountLimit       OneTimeLimitAmount,
             CSR.StatusID Status,
             CSR.OrderImportant,
             CSR.minimalsignatures,
             C.Description ClientDescription,
             CHA.ClientID,
             CHA.HostClientID
        from ClientSignRules CSR, Clients C, ClientHostAllocation CHA
       where CHA.ClientID = CSR.CLientID and CHA.ClientID = C.ClientID and CSR.ClientSignRuleSeq '||lvListSeq;
END GetValidSignRules;
--------------------------------------------------------------------
procedure getLimitsForSignRule( -- vraci obdobove limity spojene s podpisovym pravidlem
    inSignRuleID  in  NUMBER,       -- ID pravidla, mandatory
    orLimits      out SYS_REFCURSOR
) is
BEGIN
  open orLimits for
    select CSRL.period, CSRL.amountlimit, CSR.amountlimitcurrid amountlimitcurrencyid
      from ClientSignRuleLimits CSRL,
           ClientSignRules CSR
     where CSRL.clientsignruleseq=inSignRuleID
       and CSRL.clientsignruleseq=CSR.clientsignruleseq
  order by CSRL.period;
END getLimitsForSignRule;
--------------------------------------------------------------------
END IIF_CCFG_005;
