
  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "GDM_PARSER001" IS
------------------
-- $release: 3122 $
-- $version: 8.0.5.0 $
------------------
--
-- $Header: /Oracle/UDEBS_XPE/UDEBSXPE.GDM_PARSER001.PBK 2     27.01.14 18:24 Pataky $
--
-- MODIFICATION HISTORY
--
-- Person       Date       Comments
-- ------------ ---------- ---------------------------------------------------------
-- Ilenin      07.07.2014 UDEBS RS
-- Pataky      20.07.2015 Optimization of getXPath caching
-- Snederfler  19.08.2015 20009203-1183 - ParseXMLForBranchAndOper - optimization: move select for getting GroupID nearly before using it
--                                        GetDTDAttributes - new function of getting common DTD attributes
-- Emmer       26.02.2016 20009203-3907 - GetDTDAttributes modification
-- Pataky      15.04.2016 Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-- Bocak       18.07.2016 GetDTDAttributes - added filling of IIFAttr3 - 20009245-2041
-- Emmer       17.10.2018 ParseXMLForBranchAndOper skipped for RBCZ
-- --------------------------------------------------------------------------------
  gvBankCode       varchar2(4);
-- -------------------------------------------------------------------------------------------
--
FUNCTION getXPath (ivIIFAttrName GDM_Parser.IIFAttrName%TYPE, ivDefault VARCHAR2 DEFAULT NULL ) RETURN VARCHAR2 RESULT_CACHE IS
  v_XPath GDM_Parser.XPath%TYPE := null;
BEGIN
  Begin
    Select XPath
      into v_XPath
      from GDM_Parser
     where IIFAttr = upper(ivIIFAttrName)
       and statusid=1;
  Exception
    When NO_DATA_FOUND Then
      v_XPath := ivDefault;
  End;
  RETURN v_XPath;
END;
-- ======================================= GetDTDAttributes ==========================================
PROCEDURE GetDTDAttributes(
    ovDocType   OUT DTDStore.DocType%TYPE,
    ovIIFAttr   OUT DTDStore.IIFAttr%TYPE,
    ovIIFAttr2  OUT DTDStore.IIFAttr2%TYPE,
    ovIIFAttr3  OUT DTDStore.IIFAttr3%TYPE,
    inOperID    IN  DTDStore.operid%TYPE,
    inDocType   IN  DTDStore.DocType%TYPE DEFAULT NULL
)
IS
  lbNeXTSearch BOOLEAN := FALSE;
  lnGroupID    DTDStore.groupid%TYPE;
  CURSOR c2 IS SELECT /*+ RESULT_CACHE */ groupid FROM operationgroups START WITH groupid = lnGroupID CONNECT BY groupid = PRIOR parentgroupid;
BEGIN
  -- not allow for branch
  SELECT /*+ RESULT_CACHE */
         DISTINCT doctype, iifattr, iifattr2, iifattr3
    INTO ovDocType, ovIIFAttr, ovIIFAttr2, ovIIFAttr3
    FROM DTDStore
   WHERE ((inDocType IS NULL AND DocType NOT IN ('CTPDATA','EVENTTYPES','ALERTS')) OR (DocType = inDocType))
     AND OperID = inOperID;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    DECLARE
      lvDocType DTDStore.DocType%TYPE;
    BEGIN -- pretizeni pro GroupID
      SELECT /*+ RESULT_CACHE */
             groupid
        INTO lnGroupID
        FROM OperationTypes
       WHERE OperID = inOperID;
      FOR r2 IN c2 LOOP
        BEGIN
          SELECT /*+ RESULT_CACHE */
                 DISTINCT doctype, iifattr, iifattr2, iifattr3
            INTO lvDocType, ovIIFAttr, ovIIFAttr2, ovIIFAttr3
            FROM DTDStore
           WHERE GroupID=r2.GroupID
             AND ((inDocType IS NULL AND DocType NOT IN ('CTPDATA','EVENTTYPES','ALERTS')) OR (DocType = inDocType))
             AND OperId IS NULL;
          EXIT;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN NULL;
        END;
      END LOOP;
      IF lvDocType IS NULL THEN
        RAISE NO_DATA_FOUND;
      ELSE
         ovDocType := lvDocType;
      END IF;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN NULL;
    END;
END GetDTDAttributes;
-- =================================== SaveSPACEs ======================================
FUNCTION SaveSpaces(ixInputData IN XMLTYPE) RETURN XMLTYPE IS
  vData     VARCHAR2(4000 CHAR) := null;
  vMask     VARCHAR2(4000 CHAR) := null;
  vMaxChars NUMBER(5) := 100;
BEGIN
  vData := ixInputData.GetStringVal();
  -- Free Spaces:
  FOR i IN 1..vMaxChars LOOP
    vMask := '>'||LPAD('</',i+2,' ');
    vData := Replace(vData,vMask,' SPACES="'||i||'"></');
  END LOOP;
  -- Free zeros 000000 for ValueDate:
  vMask := '<valuedate>000000</valuedate>';
  vData := Replace(vData,vMask,'<valuedate ZEROS="6"></valuedate>');
  RETURN XMLTYPE(vData);
END SaveSpaces;
-- =================================== ReturnSPACEs ======================================
FUNCTION ReturnSpaces(ixInputData IN XMLTYPE) RETURN XMLTYPE IS
  vData      VARCHAR2(4000 CHAR) := null;
  vMask      VARCHAR2(4000 CHAR) := null;
  vReplace   VARCHAR2(4000 CHAR) := null;
  vTmpString VARCHAR2(4000 CHAR)  := null;
  vSpacePos  NUMBER(5)      := 0;
  vTagBgn    NUMBER(5)      := 0;
  vTagEnd    NUMBER(5)      := 0;
  vEndPos    NUMBER(5)      := 0;
  vSpaces    NUMBER(5)      := 0;
  vTagName   VARCHAR2(500 CHAR)  := null;
BEGIN
  vData := ixInputData.GetStringVal();
  -- Free spaces:
  LOOP
    vSpacePos := NVL(INSTR(vData,' SPACES="',1,1),0);
    EXIT WHEN vSpacePos = 0;
    vTmpString := SUBSTR(vData,1,vSpacePos);
    vTagBgn := NVL(INSTR(vTmpString,'<',-1,1),0);
    vEndPos := NVL(INSTR(vData,' ',vTagBgn,1),0);
    vTagName := SUBSTR(vData,vTagBgn+1,vEndPos-vTagBgn-1);
    vEndPos := NVL(INSTR(vData,'"',vSpacePos,2),0);
    vSpaces := to_number(SUBSTR(vData,vSpacePos+9, vEndPos-(vSpacePos+9)));
    if SUBSTR(vData,vEndPos+1,1) = '/' then
      vTagEnd := NVL(INSTR(vData,'/>',vSpacePos,1)+1,0);
      vMask := SUBSTR(vData,vTagBgn,vTagEnd-vTagBgn+1);
      vReplace := Replace(vMask,' SPACES="'||vSpaces||'"');
      vReplace := Replace(vReplace,'/>','>');
      vReplace := vReplace||LPAD(' ',vSpaces,' ')||'</'||vTagName||'>';
      vData := Replace(vData,vMask,vReplace);
    else
      vTagEnd := NVL(INSTR(vData,'<',vSpacePos,1)+1,0);
      vMask := SUBSTR(vData,vTagBgn,vTagEnd-vTagBgn-1);
      vReplace := Replace(vMask,' SPACES="'||vSpaces||'"');
      vReplace := vReplace||LPAD(' ',vSpaces,' ');
      vData := Replace(vData,vMask,vReplace);
    end if;
  END LOOP;
  -- Free zeros 000000 for ValueDate:
  vMask := '<valuedate ZEROS="6"></valuedate>';
  vData := Replace(vData,vMask,'<valuedate>000000</valuedate>');
  vMask := '<valuedate ZEROS="6"/>';
  vData := Replace(vData,vMask,'<valuedate>000000</valuedate>');
  RETURN XMLTYPE(vData);
END ReturnSpaces;
-- =================================================================================================
BEGIN
    gvBankCode := Reftab_GSP('LOCALBANKCODEALPHA');
END GDM_PARSER001;
