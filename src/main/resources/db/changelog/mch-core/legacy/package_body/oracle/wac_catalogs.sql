
  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "WAC_CATALOGS"
IS
------------------
-- $release: 1964 $
-- $version: 8.0.0.1 $
------------------
-- $Header: $
------------------
--
--
-- MODIFICATION HISTORY
--
-- Person      Date       Comments
-- ----------- ---------- -----------------------------------------------------
-- Furdik      25.11.2015 Package creation, proc PutText and function ExistsTextInTab (20009213-108)
-- Furdik      02.12.2015 added procs RemText and RemNullTexts (20009213-108)
-- ----------------------------------------------------------------------------
--
  --
--
-- Procedure to remove all texts gor given TextID  - uses DELETE CASCADE constraint on TEXTS tab
--
PROCEDURE RemText (
      onReturnCode      OUT NUMBER,                 -- 0 - OK , 1 Error
      orErrors          OUT tRefCursor,
      inTextID          IN OUT LanguageTexts.textid%type,
      inRaiseAnyError   IN NUMBER DEFAULT 0
)
IS
BEGIN
  im_error.Init;
  onReturnCode := 0;
  IF inTextID is not null THEN
     DELETE FROM TEXTS WHERE TextID = inTextID;
     if SQL%rowcount<1 then -- nothing to remove
        onReturnCode := 1;
        IF inRaiseAnyError = 1 THEN
          raise_application_error(-20103,'Text with TextID='||inTextID||' does not exist');
        ELSE
          im_error.AddError(inErrorId => 20103, ivText => 'Text with TextID does not exist',
          ivParam0 => 'TextID = '||to_char(inTextID), inRaiseAnyError => inRaiseAnyError);
          im_error.GetErrors (orErrors);
        END IF;
     end if;
  ELSE -- error - input TextID is null
     onReturnCode := 1;
        IF inRaiseAnyError = 1 THEN
          raise_application_error(-00001,'Error when removing text - input TextID is null');
        ELSE
          im_error.AddError(inErrorId => 00001, ivText => 'Error when removing text - input TextID is null',
          ivParam0 => 'TextID = '||to_char(inTextID), inRaiseAnyError => inRaiseAnyError);
          im_error.GetErrors (orErrors);
        END IF;
  END IF;
EXCEPTION
  WHEN im_error.REG_ANY_ERROR THEN
    onReturnCode := 1;
    IF inRaiseAnyError = 1 THEN RAISE; ELSE im_error.GetErrors (orErrors); END IF;
  WHEN OTHERS THEN
    onReturnCode := 1;
    IF inRaiseAnyError = 1 THEN RAISE; ELSE
       im_error.AddError(inErrorId => 00001, ivText => 'Error when removing text',
          ivParam0 => 'TextID = '||to_char(inTextID), inRaiseAnyError => inRaiseAnyError);
      im_error.GetErrors (orErrors);
    END IF;
END RemText;
--
-- Procedure to remove texts in Catalog - TextID set to null, delete from TEXTS tab
PROCEDURE RemNullTexts (
      onReturnCode      OUT NUMBER,                 -- 0 - OK , 1 Error
      orErrors          OUT tRefCursor,
      inTextID          IN OUT LanguageTexts.textid%type,
      ivListName        IN CATALOGATTRIBUTE.CATALOGCODE%type,
      ivColName         IN CATALOGATTRIBUTE.NAME%type,
      inRaiseAnyError   IN NUMBER DEFAULT 0
)
IS
  doProcessing pls_integer := 1;
  lnRemCheck pls_integer;
BEGIN
  im_error.Init;
  onReturnCode := 0;
  IF inTextID is null OR ivListName is null OR ivColName is null THEN
    doProcessing := 0;
  END IF;
  IF doProcessing = 1 THEN
     EXECUTE IMMEDIATE 'select count(*) from '||ivListName||' where '||ivColName||'='||inTextID INTO lnRemCheck;
     if lnRemCheck > 0 then
       EXECUTE IMMEDIATE 'UPDATE '||ivListName||' SET '||ivColName||'=null WHERE '||ivColName||'='||inTextID;
       DELETE FROM TEXTS WHERE TextID = inTextID;
     else
        onReturnCode := 1;
        IF inRaiseAnyError = 1 THEN
          raise_application_error(-20103,'Text with TextID='||inTextID||' does not exist');
        ELSE
          im_error.AddError(inErrorId => 20103, ivText => 'Text does not exist',
          ivParam0 => 'TextID = '||to_char(inTextID), ivParam1 => 'ListName = '||ivListName,
          ivParam2 => 'ColName = '||ivColName, inRaiseAnyError => inRaiseAnyError);
          im_error.GetErrors (orErrors);
        END IF;
     end if;
  ELSE -- error - TextID or ListName or ColName is null
     onReturnCode := 1;
        IF inRaiseAnyError = 1 THEN
          raise_application_error(-00001,'Error when removing text - some of required inputs is null');
        ELSE
          im_error.AddError(inErrorId => 00001, ivText => 'Error when removing text - some of required inputs is null',
          ivParam0 => 'TextID = '||to_char(inTextID), ivParam1 => 'ListName = '||ivListName,
          ivParam2 => 'ColName = '||ivColName, inRaiseAnyError => inRaiseAnyError);
          im_error.GetErrors (orErrors);
        END IF;
  END IF;
EXCEPTION
  WHEN im_error.REG_ANY_ERROR THEN
    onReturnCode := 1;
    IF inRaiseAnyError = 1 THEN RAISE; ELSE im_error.GetErrors (orErrors); END IF;
  WHEN OTHERS THEN
    onReturnCode := 1;
    IF inRaiseAnyError = 1 THEN RAISE; ELSE
       im_error.AddError(inErrorId => 00001, ivText => 'Error when removing text',
          ivParam0 => 'TextID = '||to_char(inTextID), ivParam1 => 'ListName = '||ivListName,
          ivParam2 => 'ColName = '||ivColName, inRaiseAnyError => inRaiseAnyError);
      im_error.GetErrors (orErrors);
    END IF;
END RemNullTexts;
--
-- Procedure to insert or update a text in the ListName table for given textid and langid
PROCEDURE PutText (
      onReturnCode      OUT NUMBER,                 -- 0 - OK , 1 Error
      orErrors          OUT tRefCursor,
      ivListName        IN CATALOG.CODE%type,
      inTextID          IN OUT LanguageTexts.textid%type,
      inLangID          IN LanguageTexts.langid%type,
      ivText            IN LanguageTexts.text%type,
      ivPurposeID       IN LanguageTexts.purposeid%type DEFAULT 'DEFAULT',  -- PurposeID  - parameter specifikuje ucel (DEFAULT - povodny text pre prislusne TID, LTD)
      inRaiseAnyError   IN NUMBER DEFAULT 0
)
IS
  lnUpdateTypeCheck pls_integer;
BEGIN
  im_error.Init;
  onReturnCode := 0;
  if inTextID is null then -- new record
    IF ivText is not null THEN
      inTextID := getnexttextid(ivListName);
      begin
        INSERT INTO LANGUAGETEXTS (LangID, TextID, Text, PurposeID)
          VALUES (inLangID, inTextID, ivText, ivPurposeID);
      exception when DUP_VAL_ON_INDEX THEN
        UPDATE LANGUAGETEXTS Set Text=ivText
          WHERE LangID=inLangID and TextID=inTextID and PurposeID=ivPurposeID;
      end;
    ELSE
      inTextID := null;
    END IF;
  else -- modify existing
      select count (*) into lnUpdateTypeCheck from LANGUAGETEXTS
      where LangID=inLangID and TextID=inTextID and PurposeID=ivPurposeID;
      if (lnUpdateTypeCheck = 0) then -- insert text for new language
        IF ivText is not null THEN
         INSERT INTO LANGUAGETEXTS (LangID, TextID, Text, PurposeID)
           VALUES (inLangID, inTextID, ivText, ivPurposeID);
        END IF;
      else --if (lnUpdateTypeCheck = 1) then -- update text for existing language
        IF ivText is not null THEN
         UPDATE LANGUAGETEXTS Set Text=ivText
           WHERE LangID=inLangID and TextID=inTextID and PurposeID=ivPurposeID;
        ELSE
         DELETE FROM LANGUAGETEXTS
           WHERE LangID=inLangID and TextID=inTextID and PurposeID=ivPurposeID;
         inTextID := null;
        END IF;
      end if;
  end if;
EXCEPTION
  WHEN DUP_VAL_ON_INDEX THEN
    onReturnCode := 1;
    IF inRaiseAnyError = 1 THEN RAISE;
    ELSE
      im_error.AddError(inErrorId => 00001, ivText => 'Unique constraint error',
        ivParam0 => 'TableName = '||ivListName, ivParam1 => 'LangID = '||inLangID,
        ivParam2 => 'TextID = '||inTextID, ivParam3 => 'PurposeID = '||ivPurposeID,
        inRaiseAnyError => inRaiseAnyError);
      im_error.GetErrors (orErrors);
    END IF;
  WHEN im_error.REG_ANY_ERROR THEN
    onReturnCode := 1;
    IF inRaiseAnyError = 1 THEN RAISE; ELSE im_error.GetErrors (orErrors); END IF;
END PutText;
--
-- test if the TextID column exists in a ListName table
FUNCTION ExistsTextInTab (
    ivListName        IN CATALOG.CODE%type
    )
RETURN NUMBER -- 0/1
IS
  v_column_exists number := 0;
BEGIN
  SELECT count(*) INTO v_column_exists FROM user_tab_cols
  WHERE column_name = 'TEXTID' AND table_name = ivListName;
  RETURN v_column_exists;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 0;
  WHEN OTHERS THEN
    RETURN 0;
END ExistsTextInTab;
--
END WAC_CATALOGS;
