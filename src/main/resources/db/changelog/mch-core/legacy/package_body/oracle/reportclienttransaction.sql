
  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "REPORTCLIENTTRANSACTION"
IS
------------------
-- $release: 6627 $
-- $version: 8.0.35.0 $
------------------
--
-- $Header: /Oracle/UDEBS_RS/UDEBSRS.REPORTCLIENTTRANSACTION.PBK 4     10. 12. 14 13:26 Ilenin $
--
-- MODIFICATION HISTORY
-- Person               Date       Comments
-- -------------------  ---------- ---------------------------------------------------
-- Ilenin               11.07.2014 UDEBS RS
-- Ilenin               18.11.2014 20009183-1042: IIF_EVENT
-- Ilenin               26.01.2015 20009183-1481: Nastavenie statusu package pre status >= 40 podla transakcie
-- Bocak                13.03.2015 GetHistoryReport - changed select (20009183-1909)
-- Bocak                17.03.2015 GetHistoryReport - changed select (20009183-1909), input parameter inSeqNum
-- Bocak                15.04.2015 Put, PutForce - used new column SubTotals_All
-- Bocak                21.04.2015 GetHistoryReport - return RQC only for inSeqNum > 0
-- Pataky               09.06.2015 Put - for RBCZ no alerts are created
-- Pataky               27.07.2015 Put - for RBCZ no events for operid 11 are created
-- Bocak                28.07.2015 Put - prepocitanie sumy SUBTOTALS_ALL vsetkych tranzakcii v packagi (20009183-3839)
-- Bocak                29.07.2015 GetHistoryReport - extended select
-- Bocak                24.08.2015 Put - aCalculateAmount (20009183-4245)
-- Bocak                08.09.2015 Put - aChangePackageStatus
-- Bocak                21.09.2015 changed datatype subtotal_all
-- Bocak                28.09.2015 GetHistoryReport - fixed select
-- Pataky               27.10.2015 GetPackageHistoryReport- overloaded GetHistoryReport procedure for RB transactions - 20009203-2647
-- Bocak                04.11.2015 fix calculating of totalAmount
-- Pataky               25.11.2015 GetPackageHistoryReport- fixed GetHistoryReport procedure for RB transactions, selecting also from A_Journal table - 20009203-2647
-- Emmer                16.12.2015 Removal of links to obsolete journal attributes 20009203-1354
-- Bocak                13.11.2015 GetHistoryReport - fixed getting description from DETAILS column - 20009183-5403
-- Bocak                20.01.2016 GetHistoryReport - 20009183-5434
-- Emmer                01.04.2016 GetPackageHistoryReport - uprava razeni z ASC na DESC (MYG-40) 20009203-3373
-- Pataky               15.04.2016 Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-- Bocak                18.07.2016 ChangePackageStatus - call procedure 'put' with filled aCalculateAmount and aChangePackageStatus -  20009238-249
-- Bocak                25.07.2016 Put - recalculate amounts only for non final statuses - exception for SLSP - 20009238-249
-- Bocak                02.08.2016 Put - ChangePackageAmounts - don't set TotalAmount to 0 for final statuses - SLSP only
-- Bocak                04.10.2016 Put - fix CTP counters - 20009238-320
-- Bocak                17.10.2016 Put - fix CTP counters - 20009245-3179
-- Furdik               13.02.2017 add gStatusReportSeqNum global variable - 20009239-2856
-- Bocak                09.03.2017 Put.ChangePackageStatus - dont change to 89 while status is not over 40
-- Furdik               25.02.2017 Put - add reftab_ot.Batch condition for jednotransakcny package - 20009239-3530
-- Pataky               05.04.2017 Put - add possibility to change final package status for specified banks (RB Jira MYG-4023) - 20009246-337
-- Furdik               18.05.2017 Put, GetHistoryReport - add inReport, StatusReportID - P20009245-4389
-- Furdik               21.06.2017 GetHistoryReport - add RQE type for StatusID=122 - P20009239-3033
-- Bocak                28.08.2017 Put - fix changing to final status for SLSP only
-- Vagner               23.04.2018 GetPackageHistoryReport - copy batches (RBCZ MYG-8021)
-------------------------------------------------------------------------------------------------------------------------------
  gStatusReportId ClientTransactionStatusReports.StatusReportId%TYPE; -- identifikacia reportu
  gPackageId      ClientTransactionPackages.PackageId%TYPE; -- aktialne spracuvany package
  gOriginatorId   UpdateEvents.OriginatorId%TYPE; -- originator (staci ho nastavit v prvom volani)
  gBranchID       Branches.BranchID%TYPE := null; -- identifikace poboeky podle CTP.AccountID
  gStatusReportSeqNum ClientTransactionStatusReports.SeqNum%TYPE; -- seq num
  --
  PROCEDURE Init IS
  BEGIN
    gOriginatorId := NULL;
    gStatusReportId := NULL;
    gPackageId := NULL;
    gBranchID := NULL;
    gStatusReportSeqNum := NULL;
  END Init;
  --
  PROCEDURE Report(aOperId NUMBER,aChannelID NUMBER,aPackageId NUMBER,aSeqNum NUMBER,aStatusId NUMBER,
                   aOriginator VARCHAR2,aRelatedData VARCHAR2:=NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0) IS
    lnErrorSeqNum ClientTransactionErrors.ErrorSeqNum%TYPE := 1;
    vDummy Dual.Dummy%TYPE;
    failCond varchar2(50):='0';
  BEGIN
    IF gBranchID is null THEN
      gBranchID:=Get_BranchID(null,aPackageID);
    END IF;
    IF (reftab_ctst.ReportToClient(aStatusId, aChannelId, aOperId, 0) = 1) THEN -- ak je status reportovatelny
      BEGIN
         failCond:=reftab_gsp('EnableDuplicitReportsOnStatus','0');
         SELECT 'x' INTO vDummy -- kombinacia ctsr.PackageId, ctsr.SeqNum, ctsr.StatusId a cte.ErrorId ma byt UNIQUE (a nemusi to byt v 1 reporte)
          FROM ClientTransactionStatusReports ctsr, ClientTransactionErrors cte
         WHERE ctsr.PackageId = aPackageId
           AND ctsr.SeqNum = aSeqNum
           AND ctsr.StatusId = aStatusId -- koli HB musi esistovat index PackageId, SeqNum, StatusId
           AND failCond='0'
           AND ctsr.StatusReportId = cte.StatusReportId
           AND cte.SeqNum = ctsr.SeqNum
           AND cte.ErrorId = aErrorId
           AND NVL(cte.RelatedData,' ') = NVL(aRelatedData,' '); -- StatusReportId, SeqNum, ErrorSeqNum
      EXCEPTION
        WHEN no_data_found THEN
          IF NVL(aReportSeq,0) > 0 THEN -- ak je report zavolany tak, aby sa priviazal ku konkretnemu StatusReportId (nemaprilis velky vyznam)
            gStatusReportId:=aReportSeq;
          ELSIF gStatusReportId IS NULL THEN -- ak je reportovanie zavolane prvykrat po Init
            gStatusReportId := Seq_ClientTransactionStatusRep.NextVal;
          END IF;
          gStatusReportSeqNum := aSeqNum;
          BEGIN
            INSERT INTO ClientTransactionStatusReports(TransactionID,
                                                       StatusReportID, StatusId, PackageId, SeqNum, OperID, ChannelID, BornDate, Originator)
                                                VALUES(to_char(aOperID)||'.'||to_char(aChannelID)||'.'||to_char(aPackageId)||'.'||to_char(aSeqNum),
                                                       gStatusReportId, aStatusId, aPackageId, aSeqNum, aOperID, aChannelID, systimestamp, aOriginator);
          EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN -- koli vyberu ErrorSeqNum treba zaznam zamknut
              SELECT 'x' INTO vDummy FROM ClientTransactionStatusReports WHERE StatusReportId = gStatusReportId AND SeqNum = aSeqNum FOR UPDATE;
          END;
          BEGIN
            SELECT NVL(MAX(ErrorSeqNum),0)+1 INTO lnErrorSeqNum
              FROM ClientTransactionErrors
             WHERE StatusReportID = gStatusReportId
               AND SeqNum = aSeqNum;
            INSERT INTO ClientTransactionErrors(StatusReportID, SeqNum, ErrorSeqNum, ErrorID, RelatedData)
            VALUES (gStatusReportId, aSeqNum, lnErrorSeqNum, aErrorID, aRelatedData);
          EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
              NULL;
          END;
        WHEN OTHERS THEN NULL; -- pred nasadenim akt. release zalozene data mozu sposobit too_many_rows
      END;
    END IF; -- ak je status reportovatelny
  END Report;
  --========================================================================================================================
  -- procedura je volana napr. z formulare PACKAGEADMIN pro manualni nastaveni statusu, package a transakce jiz
  -- byly nastaveny volani standardni Put(). Akce teto procedury nejsou vazany na FinalStatus.
  --========================================================================================================================
  PROCEDURE PutForce(aOperId NUMBER,aChannelID NUMBER,aPackageId NUMBER,aSeqNum NUMBER,aStatusId NUMBER,
                     aOriginator VARCHAR2,aRelatedData VARCHAR2:=NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0,
                     aBankRef VARCHAR2 := NULL, aBankEnsurePackageReport VARCHAR2 DEFAULT NULL)
  IS
    vTRNStatusID  pls_integer;
    vDummy        varchar2(1 CHAR);
    vClientId     ClientTransactionPackages.ClientId%TYPE;
    vAccountId    ClientTransactionPackages.AccountId%TYPE;
    --
    PROCEDURE ChangePackageStatus(inOrigStatusID in pls_integer) IS -- volana, kdyz se meni status transakce
      vCount            ClientTransactionPackages.CountTransactions%TYPE;
      vCountOK          ClientTransactionPackages.CountTransactions_OK%TYPE;
      vCountErr         ClientTransactionPackages.CountTransactions_E%TYPE;
      vStatusId         ClientTransactionPackages.StatusId%TYPE := NULL;
      lnOrigFinal       pls_integer:=get_ctst_values('FinalStatus',inOrigStatusID);
      lnNewFinal        pls_integer:=get_ctst_values('FinalStatus',aStatusID);
      lnAddOk           NUMBER:=0;
      lnAddE            NUMBER:=0;
    BEGIN
      if lnNewFinal = 0 then -- na prubezny status
        if lnOrigFinal = 0 then
          lnAddE := 0;
          lnAddOK := 0;
        elsif lnOrigFinal = 1 then
          lnAddE := 0;
          lnAddOK := -1;
        elsif lnOrigFinal = 2 then
          lnAddE := -1;
          lnAddOK := 0;
        end if;
      elsif lnNewFinal = 1 then -- na final status OK
        if lnOrigFinal = 0 then
          lnAddE := 0;
          lnAddOK := 1;
        elsif lnOrigFinal = 1 then
          lnAddE := 0;
          lnAddOK := 0;
        elsif lnOrigFinal = 2 then
          lnAddE := -1;
          lnAddOK := 1;
        end if;
      elsif lnNewFinal = 2 then -- na final status Error
        if lnOrigFinal = 0 then
          lnAddE := 1;
          lnAddOK := 0;
        elsif lnOrigFinal = 1 then
          lnAddE := 1;
          lnAddOK := -1;
        elsif lnOrigFinal = 2 then
          lnAddE := 0;
          lnAddOK := 0;
        end if;
      end if;
      UPDATE ClientTransactionPackages
         SET CountTransactions_Ok = nvl(CountTransactions_Ok,0) + lnAddOK,
             CountTransactions_E = nvl(CountTransactions_e,0) + lnAddE
       WHERE PackageId = aPackageId
       RETURNING CountTransactions, CountTransactions_OK, CountTransactions_E INTO vCount, vCountOK, vCountErr;
      IF SQL%ROWCOUNT = 1 AND NVL(aBankEnsurePackageReport, reftab_gsp('BankEnsurePackageReport',gBranchID,'1')) = '0' THEN
        IF vCountOK = vCount THEN -- Status Done
          vStatusId := 90;
        ELSIF vCountErr = vCount THEN -- Status Error
          vStatusId := 51;
        ELSIF vCountOK + vCountErr = vCount THEN -- Status DONE_E
          IF reftab_gsp('FORMALCONTROLSPRINCIP', gBranchID, 'PACKAGE') = 'PACKAGE' THEN
            vStatusId := 89;
          ELSE
            vStatusId := 91;
          END IF;
        END IF;
        IF vStatusId is not NULL THEN
          PutForce(aOperId, aChannelId, aPackageId, 0, vStatusId, aOriginator, aReportSeq => aReportSeq);
        END IF;
      END IF;
    END ChangePackageStatus;
    --
    PROCEDURE ChangePackageAmounts(inOrigStatusID in pls_integer) IS
      vAmount         NUMBER;
      vAmount_All     NUMBER;
      vFinal          ClientTransactionStatusTypes.FinalStatus%TYPE := get_ctst_values('FinalStatus',aStatusId);
      vTotalAmount    ClientTransactionPackages.TotalAmount%TYPE;
      vReqCoverAmount ClientTransactionPackages.ReqCoverAmount%TYPE;
      ltSubtotals     Subtotal_tab:=Subtotal_tab();
      ltSubtotals_All Subtotal_all_tab:=Subtotal_all_tab();
      lnBatch         pls_integer:=get_OT_values('Batch',aOperID);
      lnOrigFinal     pls_integer:=get_ctst_values('FinalStatus',inOrigStatusID);
      lnNewFinal      pls_integer:=get_ctst_values('FinalStatus',aStatusID);
      lnMultipleTotal NUMBER:=0;
      lnMultipleCover NUMBER:=0;
    BEGIN
      IF aSeqNum > 0 THEN
        vAmount := nvl(GetPackageAmount(ltSubtotals, aPackageId, aOperId, NULL, aSeqNum),0);
        vAmount_All := GetPackageAmount_All(ltSubtotals_All, aPackageId, aOperId);
        vAmount:=GREATEST(NVL(vAmount,0),NVL(vAmount_All,0));
        if lnNewFinal = 0 then  -- na prubezny status
          if lnOrigFinal = 0 then
            lnMultipleTotal := 0;
            lnMultipleCover := 0;
          elsif lnOrigFinal = 1 then
            lnMultipleTotal := 0;
            lnMultipleCover := -1;
          elsif lnOrigFinal = 2 then
            lnMultipleTotal := -1;
            lnMultipleCover := -1;
          end if;
        elsif lnNewFinal = 1 then -- na final status OK
          if lnOrigFinal = 0 then
            lnMultipleTotal := 0;
            lnMultipleCover := 1;
          elsif lnOrigFinal = 1 then
            lnMultipleTotal := 0;
            lnMultipleCover := 0;
          elsif lnOrigFinal = 2 then
            lnMultipleTotal := -1;
            lnMultipleCover := 0;
          end if;
        elsif lnNewFinal = 2 then -- na final status Error
          if lnOrigFinal = 0 then
            lnMultipleTotal := 1;
            lnMultipleCover := 1;
          elsif lnOrigFinal = 1 then
            lnMultipleTotal := 1;
            lnMultipleCover := 0;
          elsif lnOrigFinal = 2 then
            lnMultipleTotal := 0;
            lnMultipleCover := 0;
          end if;
        end if;
        UPDATE ClientTransactionPackages
           SET TotalAmount = decode(CountTransactions-CountTransactions_E,0,0,TotalAmount - (lnMultipleTotal * vAmount)),
               ReqCoverAmount = ReqCoverAmount - (lnMultipleCover * vAmount)
         WHERE PackageId = aPackageId;
        if lnBatch=1 then
          begin
            select 'X' into vDummy from ClientTransactionPackages
             where PackageId = aPackageId and Subtotals is not null;  -- pojistka proti ORA-22908
            UPDATE TABLE(Select Subtotals from ClientTransactionPackages where PackageId = aPackageId)
               SET OrigCurrAmount = OrigCurrAmount - (lnMultipleTotal * ltSubtotals(1).OrigCurrAmount),
                   AccCurrAmount = AccCurrAmount - (lnMultipleTotal * ltSubtotals(1).AccCurrAmount)
             WHERE CurrencyID = ltSubtotals(1).CurrencyID;
            UPDATE TABLE(Select Subtotals_All from ClientTransactionPackages where PackageId = aPackageId)
               SET OrigCurrAmount = ltSubtotals_All(1).OrigCurrAmount,
                   AccCurrAmount = ltSubtotals_All(1).AccCurrAmount
             WHERE CurrencyID = ltSubtotals_All(1).CurrencyID;
          exception when no_data_found then
            null;
          end;
        end if;
      END IF;
    EXCEPTION
      WHEN INVALID_NUMBER OR VALUE_ERROR THEN
        -- odchytenie vynimky z volania fcie GetPackageAmounts, ak je problem v konverzii sumy
        NULL; -- nerob nic
    END ChangePackageAmounts;
    --
  BEGIN
    gBranchID:=Get_BranchID(null,aPackageID);
    IF reftab_ctet.FatalError(aErrorId,1) = 1 THEN
      gPackageId := aPackageId;
      gOriginatorId := NVL(aOriginator,gOriginatorId);
      IF aSeqNum = 0 THEN
        UPDATE ClientTransactionPackages
           SET StatusId = aStatusId
         WHERE PackageId = aPackageId
        RETURNING ClientId, AccountId INTO vClientId, vAccountId;
        --
      ELSE -- pro transakci (aSeqNum > 0)
        IF reftab_ot.Existing(aOperId) THEN
          begin
            SELECT T.StatusID INTO vTRNStatusID FROM ClientTransactionPackages CTP, Transactions T
             WHERE CTP.PackageID=T.PackageID AND CTP.PackageId = aPackageId AND SeqNum = aSeqNum
               FOR UPDATE OF CTP.StatusID;
          exception when no_data_found then
            raise_application_error(-20001,'PackageID '||aPackageId||' not found');
          end;
          IF reftab_ot.HomeTableName(aOperId) = 'TRANSACTIONS' THEN
            UPDATE Transactions SET StatusId = aStatusId WHERE PackageId = aPackageId AND SeqNum = aSeqNum;
            IF SQL%ROWCOUNT = 1 THEN
              ChangePackageStatus(vTRNStatusID);
              ChangePackageAmounts(vTRNStatusID);
            END IF;
          END IF;
        END IF;
      END IF; -- IF aSeqNum = 0 THEN
      Report(aOperID, aChannelID, aPackageId, aSeqNum, aStatusId, aOriginator, aRelatedData, aErrorID, aReportSeq);
    END IF; -- FatalError
  END PutForce;
  --
  PROCEDURE Put(aOperId NUMBER,aChannelID NUMBER,aPackageId NUMBER,aSeqNum NUMBER,aStatusId NUMBER,
                aOriginator VARCHAR2,aRelatedData VARCHAR2:=NULL,aErrorID NUMBER := 0,aReportSeq NUMBER := 0,
                aBankRef VARCHAR2 := NULL, aBankEnsurePackageReport VARCHAR2 DEFAULT NULL,
                aCalculateAmount  NUMBER DEFAULT 1,aChangePackageStatus  NUMBER DEFAULT 1,
                inReport      IN  PLS_INTEGER DEFAULT 1)
  IS
    vDummy     Dual.Dummy%TYPE;
    vClientId  ClientTransactionPackages.ClientId%TYPE;
    vAccountId ClientTransactionPackages.AccountId%TYPE;
    --
    PROCEDURE ChangePackageStatus IS
      vCount            ClientTransactionPackages.CountTransactions%TYPE;
      vCountOK          ClientTransactionPackages.CountTransactions_OK%TYPE;
      vCountErr         ClientTransactionPackages.CountTransactions_E%TYPE;
      vStatusId         ClientTransactionPackages.StatusId%TYPE := NULL;
      lnCnt             PLS_INTEGER;
      lnTranStatusID    Transactions.StatusId%TYPE := NULL;
      lnOK              PLS_INTEGER;
      lnERR             PLS_INTEGER;
    BEGIN
      IF reftab_ctst.FinalStatus(aStatusId,0) in (1,2) THEN
        -- fix CTP counters
        SELECT SUM(DECODE(reftab_ctst.FinalStatus(StatusId,0),1,1,0)),
               SUM(DECODE(reftab_ctst.FinalStatus(StatusId,0),2,1,0))
        INTO lnOK,lnERR
        FROM Transactions
        WHERE PackageId=aPackageId;
        UPDATE ClientTransactionPackages
        SET CountTransactions_Ok = lnOK,
            CountTransactions_E = lnERR
        WHERE PackageId = aPackageId
        AND reftab_ctst.FinalStatus(StatusId,0) = 0
        RETURNING CountTransactions, CountTransactions_OK, CountTransactions_E INTO vCount, vCountOK, vCountErr;
        --
/* old version
        UPDATE ClientTransactionPackages
           SET CountTransactions_Ok = NVL(CountTransactions_Ok,0) + DECODE(reftab_ctst.FinalStatus(aStatusId,0),1,1,0),
               CountTransactions_E = NVL(CountTransactions_e,0) + DECODE(reftab_ctst.FinalStatus(aStatusId,0),2,1,0)
         WHERE PackageId = aPackageId
           AND reftab_ctst.FinalStatus(StatusId,0) = 0
         RETURNING CountTransactions, CountTransactions_OK, CountTransactions_E INTO vCount, vCountOK, vCountErr;
*/
        IF SQL%ROWCOUNT = 1 AND NVL(aBankEnsurePackageReport, reftab_gsp('BankEnsurePackageReport',gBranchID,'1')) = '0' THEN
          IF vCountOK = vCount THEN -- Status Done
            IF reftab_ctst.FinalStatus(aStatusId,0)=1 AND reftab_ctst.PackageStatus(aStatusId,0)=1 AND Reftab_GSP('LOCALBANKCODEALPHA')='SLSP' THEN
              vStatusId := aStatusId;
            ELSIF reftab_ctst.FinalStatus(aStatusId,0)=1 AND reftab_ctst.PackageStatus(aStatusId,0)=0 AND Reftab_GSP('LOCALBANKCODEALPHA')='SLSP' THEN
              vStatusId := aStatusId-100;
            ELSE
              vStatusId := 90;
            END IF;
          ELSIF vCountErr = vCount THEN -- Status Failed
            IF ABS(aStatusId-100)>40 THEN
              vStatusId := 89;
            ELSE
              vStatusId:=aStatusId-100;
            END IF;
          ELSIF vCountOK + vCountErr = vCount THEN -- Status DONE_E
            IF reftab_gsp('FORMALCONTROLSPRINCIP', gBranchID, 'PACKAGE') = 'PACKAGE' THEN
              vStatusId := 89;
            ELSE
              vStatusId := 91;
            END IF;
          END IF;
          IF vStatusId is not NULL THEN
            Put(aOperId, aChannelId, aPackageId, 0, vStatusId, aOriginator, aReportSeq => aReportSeq, aCalculateAmount=>aCalculateAmount, aChangePackageStatus=>aChangePackageStatus);
          END IF;
        END IF;
      ELSIF reftab_ctst.FinalStatus(aStatusId,0) in (0) THEN
       -- zmena statusu CTP podla najmensieho statusu TRN
       BEGIN
         SELECT MIN(T.StatusID)
           INTO lnTranStatusID
           FROM Transactions T
          WHERE T.PackageID = aPackageId;
         IF reftab_ctst.Existing(lnTranStatusID-100) THEN
           UPDATE ClientTransactionPackages CTP
              SET CTP.StatusID = lnTranStatusID-100
            WHERE CTP.PackageID = aPackageId
              AND CTP.StatusID < lnTranStatusID-100
              AND CTP.StatusID >= 40;
           IF SQL%ROWCOUNT = 1 THEN
             IF inReport=1 THEN
              Report(aOperId,aChannelID,aPackageId,0,lnTranStatusID-100,aOriginator);
             END IF;
           END IF;
         END IF;
       EXCEPTION
         WHEN no_data_found THEN
           NULL;
       END;
      END IF;
    END ChangePackageStatus;
    --
    PROCEDURE ChangePackageAmounts IS
      vAmount         NUMBER;
      vAmount_All     NUMBER;
      vFinal          ClientTransactionStatusTypes.FinalStatus%TYPE := reftab_ctst.FinalStatus(aStatusId, 0);
      vTotalAmount    ClientTransactionPackages.TotalAmount%TYPE;
      vReqCoverAmount ClientTransactionPackages.ReqCoverAmount%TYPE;
      ltSubtotals     Subtotal_tab:=Subtotal_tab();
      ltSubtotals_All Subtotal_all_tab:=Subtotal_all_tab();
      lnBatch         pls_integer:=get_OT_values('Batch',aOperID);
      lvStruct        OperationTypes.structcode%TYPE := get_OT_values('StructCode',aOperID);
    BEGIN
      IF lvStruct <> 'CANC' THEN  -- cancelrequest ma TotalAmount nastaven pri vytvoreni
        IF aSeqNum > 0 AND vFinal in (1,2) THEN
          vAmount := GetPackageAmount(ltSubtotals, aPackageId, aOperId, NULL, aSeqNum);
          vAmount_All := GetPackageAmount_All(ltSubtotals_All, aPackageId, aOperId); -- prepocitanie sumy vsetkych tranzakcii v packagi
          vAmount:=GREATEST(NVL(vAmount,0),NVL(vAmount_All,0));
          IF Reftab_GSP('LOCALBANKCODEALPHA')='SLSP' THEN
            UPDATE ClientTransactionPackages
               SET TotalAmount = TotalAmount,
                   ReqCoverAmount = ReqCoverAmount - nvl(vAmount,0)
             WHERE PackageId = aPackageId;
          ELSE
            UPDATE ClientTransactionPackages
               SET TotalAmount = decode(CountTransactions-CountTransactions_E,0,0,TotalAmount - DECODE(vFinal, 1, 0, 2, nvl(vAmount,0))),
                   ReqCoverAmount = ReqCoverAmount - nvl(vAmount,0)
             WHERE PackageId = aPackageId;
          END IF;
          if lnBatch=1 then
            begin
              select 'X' into vDummy from ClientTransactionPackages
               where PackageId = aPackageId and Subtotals is not null;  -- pojistka proti ORA-22908
              UPDATE TABLE(Select Subtotals from ClientTransactionPackages where PackageId = aPackageId)
                 SET OrigCurrAmount = OrigCurrAmount - DECODE(vFinal, 1, 0, 2, ltSubtotals(1).OrigCurrAmount),
                     AccCurrAmount = AccCurrAmount - DECODE(vFinal, 1, 0, 2, ltSubtotals(1).AccCurrAmount)
               WHERE CurrencyID = ltSubtotals(1).CurrencyID;
              UPDATE TABLE(Select Subtotals_All from ClientTransactionPackages where PackageId = aPackageId)
                 SET OrigCurrAmount = ltSubtotals_All(1).OrigCurrAmount,
                     AccCurrAmount = ltSubtotals_All(1).AccCurrAmount
               WHERE CurrencyID = ltSubtotals_All(1).CurrencyID;
            exception when no_data_found then
              null;
            end;
          end if;
        ELSIF aSeqNum = 0 THEN
          begin
            SELECT TotalAmount, ReqCoverAmount INTO vTotalAmount, vReqCoverAmount
              FROM ClientTransactionPackages WHERE PackageId = aPackageId;
          exception when no_data_found then
            raise_application_error(-20001,'PackageID '||aPackageId||' not found');
          end;
          --IF vTotalAmount IS NULL OR vReqCoverAmount IS NULL THEN
            vAmount := GetPackageAmount(ltSubtotals, aPackageId, aOperId);
            vAmount_All := GetPackageAmount_All(ltSubtotals_All, aPackageId, aOperId);
            vAmount:=GREATEST(NVL(vAmount,0),NVL(vAmount_All,0));
            if lnBatch = 0 then
              IF Reftab_GSP('LOCALBANKCODEALPHA')='SLSP' THEN
                UPDATE ClientTransactionPackages
                   SET TotalAmount = vAmount,
                       ReqCoverAmount = DECODE(vFinal, 0, vAmount, 0)
                 WHERE PackageId = aPackageId;
              ELSE
                UPDATE ClientTransactionPackages
                   SET TotalAmount = vAmount,
                       ReqCoverAmount = DECODE(vFinal, 0, vAmount, 0)
                 WHERE PackageId = aPackageId;
              END IF;
            ELSE
              IF Reftab_GSP('LOCALBANKCODEALPHA')='SLSP' THEN
                UPDATE ClientTransactionPackages
                   SET TotalAmount = nvl(vAmount,0),
                       ReqCoverAmount = DECODE(vFinal, 0, nvl(vAmount,0), 0),
                       Subtotals = ltSubtotals,
                       Subtotals_All = ltSubtotals_All
                 WHERE PackageId = aPackageId;
              ELSE
                UPDATE ClientTransactionPackages
                   SET TotalAmount = DECODE(vFinal, 2, 0, nvl(vAmount,0)),
                       ReqCoverAmount = DECODE(vFinal, 0, nvl(vAmount,0), 0),
                       Subtotals = ltSubtotals,
                       Subtotals_All = ltSubtotals_All
                 WHERE PackageId = aPackageId;
              END IF;
            end if;
          --END IF;
        END IF;
      END IF;
    EXCEPTION
      WHEN INVALID_NUMBER OR VALUE_ERROR THEN
        -- odchytenie vynimky z volania fcie GetPackageAmounts, ak je problem v konverzii sumy
        NULL; -- nerob nic
    END ChangePackageAmounts;
    --
  BEGIN
    gBranchID:=Get_BranchID(null,aPackageID);
    IF reftab_ctet.FatalError(aErrorId,1) = 1 THEN
      gPackageId := aPackageId;
      gOriginatorId := NVL(aOriginator,gOriginatorId);
      IF aSeqNum = 0 THEN
        UPDATE ClientTransactionPackages
           SET StatusId = aStatusId
         WHERE PackageId = aPackageId
           AND (reftab_ctst.FinalStatus(StatusId,0) = 0 or Reftab_GSP('LOCALBANKCODEALPHA') in ('RBCZ'))
         RETURNING ClientId, AccountId INTO vClientId, vAccountId;
        IF SQL%ROWCOUNT = 1 AND aStatusId >= 46 AND reftab_ctst.FinalStatus(aStatusId,0) != 0 AND reftab_ctst.Existing(aStatusId+100) THEN
          DECLARE -- bezpecnostna poistka pre jednotransakcne packages, keby bankovy system nezmenil status transakcii
            vNumOfTrans NUMBER := 0;
            vTrnCur     TrnCurTyp;
            vSeqNum     Transactions.SeqNum%TYPE;
            vStatusId   Transactions.StatusId%TYPE;
          BEGIN
/*
            GetTransactions (aPackageId, aOperId, vTrnCur);
            LOOP
              FETCH vTrnCur INTO vSeqNum, vStatusId;
              EXIT WHEN vTrnCur%NOTFOUND;
              vNumOfTrans := vNumOfTrans + 1;
            END LOOP;
            CLOSE vTrnCur;
*/
            SELECT COUNT(1) INTO vNumOfTrans FROM Transactions WHERE PackageId=aPackageId;
            IF vNumOfTrans = 1 THEN -- jednotransakcny package
              IF reftab_ot.HomeTableName(aOperId) = 'TRANSACTIONS' AND reftab_ot.Batch(aOperId) = 0 THEN
                UPDATE Transactions
                   SET StatusId = aStatusId + 100
                 WHERE PackageId = aPackageId;
              END IF; -- rozdelenie podla HomeTableName
            END IF; -- jednotransakcny package
          END; -- bezpecnostna poistka pre jednotransakcne packages, keby bankovy system nezmenil status transakcii
        END IF; -- zmena statusu transakcie jednotransakcneho package
        --
        IF aCalculateAmount=1 THEN
          ChangePackageAmounts;
        END IF;
      ELSE
        IF reftab_ot.Existing(aOperId) THEN
          begin
            SELECT 'x'
              INTO vDummy
              FROM ClientTransactionPackages
             WHERE PackageId = aPackageId FOR UPDATE;
          exception
            when no_data_found then
            raise_application_error(-20001,'PackageID '||aPackageId||' not found');
          end;
          IF reftab_ot.HomeTableName(aOperId) = 'TRANSACTIONS' THEN
            UPDATE Transactions
               SET StatusId = aStatusId --, BankRef = NVL(aBankRef, BankRef)
             WHERE PackageId = aPackageId
               AND SeqNum = aSeqNum
               AND ((aStatusId > NVL(StatusId,0)) or (aStatusId between 154 and 159 and NVL(StatusId,0) between 154 and 159));
            IF SQL%ROWCOUNT = 1 THEN
              IF aChangePackageStatus=1 THEN
                ChangePackageStatus;
              END IF;
              IF aCalculateAmount=1 THEN
                ChangePackageAmounts;
              END IF;
            END IF;
          END IF;
        END IF;
      END IF; -- IF aSeqNum = 0 THEN
      IF inReport=1 THEN
        Report(aOperID, aChannelID, aPackageId, aSeqNum, aStatusId, aOriginator, aRelatedData, aErrorID, aReportSeq);
      END IF;
    END IF; -- FatalError
  END Put;
  --
  PROCEDURE Put(aTransactionID VARCHAR2,aStatusId VARCHAR2,aOriginator VARCHAR2,aRelatedData VARCHAR2:=NULL,
                aErrorID VARCHAR2 := '0',aReportSeq NUMBER := 0, aBankRef VARCHAR2 := NULL) IS
    vZac       PLS_INTEGER := 1;
    vKon       PLS_INTEGER;
    vOperID    ClientTransactionStatusReports.OperId%TYPE;
    vChannelID ClientTransactionStatusReports.ChannelId%TYPE;
    vPackageId ClientTransactionStatusReports.PackageId%TYPE;
    vSeqNum    ClientTransactionStatusReports.SeqNum%TYPE;
    --
    PROCEDURE TransIdPiece (onPiece OUT NUMBER) IS
    BEGIN
      vKon:=instr(aTransactionID,'.',vZac);
      IF vKon=0 THEN
        onPiece:=TO_NUMBER(rtrim(substr(aTransactionID,vZac)));
        vZac:=LENGTH(aTransactionID)+1;
      ELSE
        onPiece:=TO_NUMBER(rtrim(substr(aTransactionID,vZac,vKon-vZac)));
        vZac:=vKon+1;
      END IF;
      IF onPiece IS NULL THEN
        RAISE_APPLICATION_ERROR(-20010,'Bad or mising TransactionID');
      END IF;
    EXCEPTION
      WHEN INVALID_NUMBER THEN
        RAISE_APPLICATION_ERROR(-20010,'Bad or mising TransactionID');
    END TransIdPiece;
    --
  BEGIN
     TransIdPiece(vOperID);
     TransIdPiece(vChannelID);
     TransIdPiece(vPackageId);
     TransIdPiece(vSeqNum);
     Put(vOperID,vChannelID,vPackageId,vSeqNum,to_number(aStatusId),aOriginator,aRelatedData,to_number(aErrorID),aReportSeq, aBankRef);
  END Put;
  --
  PROCEDURE Put(aOperId NUMBER,aChannelID NUMBER,aPackageID NUMBER,aSeqNum NUMBER,aStatusID NUMBER,
                aOriginator VARCHAR2,aRelatedData VARCHAR2:=NULL,aErrorID NUMBER := 0,aReport in Varchar2,aReportSeq IN OUT NUMBER) is
  begin
    Put(aOperId,aChannelID,aPackageId,aSeqNum,aStatusId,aOriginator,aRelatedData,aErrorID);
    aReportSeq:=gStatusReportId;
  end Put;
  --
  PROCEDURE PutTrn(aTransactionID VARCHAR2,aStatusID VARCHAR2,aOriginator VARCHAR2,aRelatedData VARCHAR2 := NULL,
                aErrorID NUMBER := 0,aReportSeq IN OUT NUMBER, aBankRef VARCHAR2 := NULL) IS
  BEGIN
    Put(aTransactionID,aStatusID,aOriginator,aRelatedData,to_char(aErrorID),aReportSeq,aBankRef);
    aReportSeq:=gStatusReportId;
  END PutTrn;
  --
  PROCEDURE Done(aReportseq NUMBER := null,aPackageId Number:=null,aLockAttr varchar2:=0) IS
    vClientId UpdateEvents.ClientId%TYPE;
  BEGIN
    IF (gStatusReportId is not null) or (aReportSeq is not null) THEN
      BEGIN
        SELECT ClientId INTO vClientId FROM ClientTransactionPackages WHERE PackageId = nvl(aPackageid,gPackageId);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN vClientId := NULL;
      END;
    END IF;
    if aReportseq is null then
       Init;
    end if;
  END Done;
  --
  PROCEDURE GetTransactions (
      pPackageId IN ClientTransactionPackages.PackageId%TYPE,
      pOperId    IN ClientTransactionPackages.OperId%TYPE,
      pTrnCur    OUT TrnCurTyp)
  IS
  BEGIN
    IF reftab_ot.HomeTableName(pOperId) = 'TRANSACTIONS' THEN
      OPEN pTrnCur FOR SELECT SeqNum, StatusId FROM Transactions WHERE PackageId = pPackageId;
    ELSE
      OPEN pTrnCur FOR SELECT 1 SeqNum, 1 StatusId FROM Dual WHERE 1=2;
    END IF;
  END GetTransactions;
  --
  PROCEDURE ChangePcgTrnStatus (
      pOperId        IN ClientTransactionPackages.OperId%TYPE,
      pChannelId     IN ClientTransactionPackages.ChannelId%TYPE,
      pPackageId     IN ClientTransactionPackages.PackageId%TYPE,
      pStatusId      IN ClientTransactionPackages.StatusId%TYPE,
      pOriginatorId  IN ClientTransactionStatusReports.Originator%TYPE)
  IS
    vTrnCur   TrnCurTyp;
    vSeqNum   Transactions.SeqNum%TYPE;
    vStatusId Transactions.StatusId%TYPE;
  BEGIN
    Init;
    IF reftab_ctst.Existing(pStatusId + 100) THEN
      GetTransactions (
          pPackageId => pPackageId,
          pOperId    => pOperId,
          pTrnCur    => vTrnCur);
      LOOP
        FETCH vTrnCur INTO vSeqNum, vStatusId;
        EXIT WHEN vTrnCur%NOTFOUND;
        Put(
            aOperId     => pOperId,
            aChannelId  => pChannelId,
            aPackageId  => pPackageId,
            aSeqNum     => vSeqNum,
            aStatusId   => pStatusId + 100,
            aOriginator => pOriginatorId);
      END LOOP;
      CLOSE vTrnCur;
    END IF;
    Put(
        aOperId     => pOperId,
        aChannelId  => pChannelId,
        aPackageId  => pPackageId,
        aSeqNum     => 0,
        aStatusId   => pStatusId,
        aOriginator => pOriginatorId);
    Done;
  END ChangePcgTrnStatus;
------------------------------------------------------------------------
 -- Procedura vrati XML report o validaci a chybach
  PROCEDURE GetValidationReport (
      onReturnCode    OUT pls_integer,
      orErrors        OUT sys_refcursor,
      ocReport        OUT NOCOPY Clob,
      inUserID   IN  Number,
      inChannelID     IN  Number,
      inPackageID     IN  ClientTransactionPackages.PackageId%TYPE
  )
  IS
    qryCtx    DBMS_XMLGEN.ctxHandle;
    lrMyCur   sys_refcursor;
    lnRight   pls_integer;
  BEGIN
    im_error.init;
    im_error.adderror(2113540,'You have no privileges for this operation.');
    onReturnCode:=1;
    im_error.geterrors(orErrors);
  END GetValidationReport;
------------------------------------------------------------------------
END REPORTCLIENTTRANSACTION;
