
  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "REFTAB_OT" IS
  ------------------
  -- $release: 4619 $
  -- $version: 8.0.5.0 $
  ------------------
  -- $Header: /Oracle/UDEBS_EE/UDEBS0105.REFTAB_OT.PBK 2     1.04.03 15:12 Jaros $
  -- MODIFICATION HISTORY
  -- Person       Date       Comments
  -- ----------   ---------- ------------------------------------------
  -- Bocak        27.02.2017 all functions RESULT_CACHE
  -- Emmer        17.01.2018 all functions RESULT_CACHE removed, hints stays
  -- ----------   ---------- ------------------------------------------
  TYPE t_OperTypesTable IS TABLE OF OperationTypes%ROWTYPE INDEX BY BINARY_INTEGER;
  TYPE t_OperPayTable IS TABLE OF OperPaymentSystems%ROWTYPE INDEX BY BINARY_INTEGER;
  TYPE t_t_OperPayTable IS TABLE OF t_OperPayTable INDEX BY BINARY_INTEGER;
  gtOperTypes t_OperTypesTable;
  gtOperPayments t_t_OperPayTable;
  --
  FUNCTION Existing(inOperId IN OperationTypes.OperId%TYPE) RETURN BOOLEAN IS
  BEGIN
    IF gtOperTypes.Exists(inOperID) THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  END;
  --
  FUNCTION GroupId(inOperId     IN OperationTypes.OperId%TYPE,
                   inDefaultVal IN OperationTypes.GroupId%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).GroupId;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION AccountSensitive(inOperId     IN OperationTypes.OperId%TYPE,
                            inDefaultVal IN OperationTypes.AccountSensitive%TYPE DEFAULT NULL)
    RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).AccountSensitive;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION AuthenReq(inOperId     IN OperationTypes.OperId%TYPE,
                     inDefaultVal IN OperationTypes.AuthenReq%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).AuthenReq;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION CertifReq(inOperId     IN OperationTypes.OperId%TYPE,
                     inDefaultVal IN OperationTypes.CertifReq%TYPE DEFAULT NULL,
                     inChannelID  IN OperationTypeChannels.ChannelID%TYPE DEFAULT NULL) RETURN NUMBER IS
    lnCertifReq OperationTypeChannels.CertifReq%TYPE;
  BEGIN
    IF inChannelID IS NOT NULL THEN
      BEGIN
        SELECT /*+ RESULT_CACHE */
         certifreq
          INTO lnCertifReq
          FROM OperationTypeChannels
         WHERE operid = inOperId
           AND channelid = inChannelID;
      EXCEPTION
        WHEN no_data_found THEN
          RETURN gtOperTypes(inOperID).CertifReq;
      END;
    ELSE
      RETURN gtOperTypes(inOperID).CertifReq;
    END IF;
    RETURN lnCertifReq;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION TextId(inOperId     IN OperationTypes.OperId%TYPE,
                  inDefaultVal IN OperationTypes.TextId%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).TextId;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION GroupOrder(inOperId     IN OperationTypes.OperId%TYPE,
                      inDefaultVal IN OperationTypes.GroupOrder%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).GroupOrder;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION SignRulesReq(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.SignRulesReq%TYPE DEFAULT NULL,
                        inChannelID  IN OperationTypeChannels.ChannelID%TYPE DEFAULT NULL)
    RETURN NUMBER IS
    lnSignRulesReq OperationTypeChannels.SignRulesReq%TYPE;
  BEGIN
    IF inChannelID IS NOT NULL THEN
      BEGIN
        SELECT /*+ RESULT_CACHE */
         SignRulesReq
          INTO lnSignRulesReq
          FROM OperationTypeChannels
         WHERE operid = inOperId
           AND channelid = inChannelID;
      EXCEPTION
        WHEN no_data_found THEN
          RETURN gtOperTypes(inOperID).SignRulesReq;
      END;
    ELSE
      RETURN gtOperTypes(inOperID).SignRulesReq;
    END IF;
    RETURN lnSignRulesReq;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION SysTabIndicator(inOperId     IN OperationTypes.OperId%TYPE,
                           inDefaultVal IN OperationTypes.SysTabIndicator%TYPE DEFAULT NULL)
    RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).SysTabIndicator;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION Priority(inOperId     IN OperationTypes.OperId%TYPE,
                    inDefaultVal IN OperationTypes.Priority%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).Priority;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION OperTypeCode(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.OperTypeCode%TYPE DEFAULT NULL)
    RETURN VARCHAR2 IS
  BEGIN
    RETURN gtOperTypes(inOperID).OperTypeCode;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION BankToClient(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.BankToClient%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).BankToClient;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION ClientToBank(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.ClientToBank%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).ClientToBank;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION AuthorReq(inOperId     IN OperationTypes.OperId%TYPE,
                     inDefaultVal IN OperationTypes.AuthorReq%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).AuthorReq;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION HistoryKept(inOperId     IN OperationTypes.OperId%TYPE,
                       inDefaultVal IN OperationTypes.HistoryKept%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).HistoryKept;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION Hidden(inOperId     IN OperationTypes.OperId%TYPE,
                  inDefaultVal IN OperationTypes.Hidden%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).Hidden;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION CtpRetention(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.CtpRetention%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).CtpRetention;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION RecStatusId(inOperId     IN OperationTypes.OperId%TYPE,
                       inDefaultVal IN OperationTypes.RecStatusId%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).RecStatusId;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION HomeTableName(inOperId     IN OperationTypes.OperId%TYPE,
                         inDefaultVal IN OperationTypes.HomeTableName%TYPE DEFAULT NULL)
    RETURN VARCHAR2 IS
  BEGIN
    RETURN gtOperTypes(inOperID).HomeTableName;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION PayLimitCum(inOperId     IN OperationTypes.OperId%TYPE,
                       inDefaultVal IN OperationTypes.PayLimitCum%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).PayLimitCum;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION PayLimitSingle(inOperId     IN OperationTypes.OperId%TYPE,
                          inDefaultVal IN OperationTypes.PayLimitSingle%TYPE DEFAULT NULL)
    RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).PayLimitSingle;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION CopyStatusId(inOperId     IN OperationTypes.OperId%TYPE,
                        inDefaultVal IN OperationTypes.CopyStatusId%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).CopyStatusId;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION PerformAuthorisation(inOperId     IN OperationTypes.OperId%TYPE,
                                inDefaultVal IN OperationTypes.PerformAuthorisation%TYPE DEFAULT NULL)
    RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).PerformAuthorisation;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION MakesDebit(inOperId     IN OperationTypes.OperId%TYPE,
                      inDefaultVal IN OperationTypes.MakesDebit%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).MakesDebit;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION NotificationType(inOperId     IN OperationTypes.OperId%TYPE,
                            inDefaultVal IN OperationTypes.NotificationType%TYPE DEFAULT NULL)
    RETURN VARCHAR2 IS
  BEGIN
    RETURN NVL(gtOperTypes(inOperID).NotificationType, inDefaultVal);
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION Batch(inOperId     IN OperationTypes.OperId%TYPE,
                 inDefaultVal IN OperationTypes.Batch%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(InOperId).Batch;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION PaymentSystemID(inOperId     IN OperationTypes.OperId%TYPE,
                           inBranchID   IN Branches.BranchID%TYPE,
                           inDefaultVal IN OperPaymentSystems.PaymentSystemID%TYPE DEFAULT NULL)
    RETURN NUMBER IS
    lnPBID Branches.BranchID%TYPE := inBranchID;
  BEGIN
    RETURN gtOperPayments(lnPBID)(InOperId).PaymentSystemID;
  EXCEPTION
    WHEN OTHERS THEN
      LOOP
        BEGIN
          SELECT /*+ RESULT_CACHE */
           ParentBranchID
            INTO lnPBID
            FROM Branches
           WHERE BranchID = lnPBID;
          RETURN gtOperPayments(lnPBID)(InOperId).PaymentSystemID;
        EXCEPTION
          WHEN OTHERS THEN
            IF lnPBID IS NULL THEN
              RETURN inDefaultVal; -- kdyz se nenajde BranchID v Branches;
            ELSE
              NULL;
            END IF;
        END;
      END LOOP;
  END;
  --
  FUNCTION StructCode(inOperId     IN OperationTypes.OperId%TYPE,
                      inDefaultVal IN OperationTypes.StructCode%TYPE DEFAULT NULL) RETURN VARCHAR2 IS
  BEGIN
    RETURN gtOperTypes(inOperID).StructCode;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION AvailableForClient(inOperId     IN OperationTypes.OperId%TYPE,
                              inDefaultVal IN OperationTypes.AvailableForClient%TYPE DEFAULT NULL)
    RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).AvailableForClient;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
  --
  FUNCTION AvailableForOperator(inOperId     IN OperationTypes.OperId%TYPE,
                                inDefaultVal IN OperationTypes.AvailableForOperator%TYPE DEFAULT NULL)
    RETURN NUMBER IS
  BEGIN
    RETURN gtOperTypes(inOperID).AvailableForOperator;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END;
BEGIN
  DECLARE
    CURSOR c_ot IS
      SELECT /*+ RESULT_CACHE */
       *
        FROM OperationTypes;
    CURSOR c_ops IS
      SELECT /*+ RESULT_CACHE */
       *
        FROM OperPaymentSystems;
  BEGIN
    FOR ot IN c_ot LOOP
      gtOperTypes(ot.OperId) := ot;
    END LOOP;
    FOR ops IN c_ops LOOP
      gtOperPayments(ops.BranchID)(ops.OperID) := ops;
    END LOOP;
  END;
END REFTAB_OT;
