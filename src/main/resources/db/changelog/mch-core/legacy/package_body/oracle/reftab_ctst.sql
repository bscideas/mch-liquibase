
  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "REFTAB_CTST"
IS
------------------
-- $release: 1666 $
-- $version: 8.0.2.0 $
------------------
-- $Header: /Oracle/UDEBS_RU/UDEBSRU.REFTAB_CTST.PBK 2     15.12.04 16:56 Matis $
--------------------------------------------------------------------------------
-- Ilenin       07.07.2014  UDEBS RS
-- Ilenin       02.12.2014  Override AllowSign
-- Pataky       02.11.2015  Added Visible function
--------------------------------------------------------------------------------
  TYPE t_CliTrStatTypesTable   IS TABLE OF ClientTransactionStatusTypes%ROWTYPE INDEX BY BINARY_INTEGER;
  gtCliTrStatTypes             t_CliTrStatTypesTable;
  --
  FUNCTION Existing       (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE) RETURN BOOLEAN IS
  BEGIN
    IF gtCliTrStatTypes.Exists(inStatusId) THEN RETURN TRUE;
    ELSE RETURN FALSE;
    END IF;
  END Existing;
  --------------------------------------------------------------------------------------------------
  FUNCTION PackageStatus  (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.PackageStatus%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
  BEGIN
    RETURN gtCliTrStatTypes(inStatusId).PackageStatus;
  EXCEPTION
    WHEN OTHERS THEN RETURN inDefaultVal;
  END PackageStatus;
  --------------------------------------------------------------------------------------------------
  FUNCTION AllowDel       (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
    vRet StatusReporting.AllowDel%TYPE;
    CURSOR c_sr IS
      SELECT /*+ RESULT_CACHE */
             AllowDel
        FROM StatusReporting
       WHERE StatusId = inStatusId AND
             (NVL(ChannelId,inChannelId) = inChannelId OR (ChannelId IS NULL AND inChannelId IS NULL)) AND
             (NVL(OperId,inOperId) = inOperId OR (OperId IS NULL AND inOperId IS NULL))
       ORDER BY OperId, ChannelId;
  BEGIN
    OPEN c_sr;
    FETCH c_sr INTO vRet;
    IF c_sr%NOTFOUND THEN
      BEGIN
        vRet:=gtCliTrStatTypes(inStatusId).AllowDel;
      EXCEPTION
        WHEN OTHERS THEN
          vRet:=inDefaultVal;
      END;
    END IF;
    CLOSE c_sr;
    RETURN vRet;
  END AllowDel;
  --------------------------------------------------------------------------------------------------
  FUNCTION AllowEdit      (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
    vRet StatusReporting.AllowEdit%TYPE;
    CURSOR c_sr IS
      SELECT /*+ RESULT_CACHE */
             AllowEdit
        FROM StatusReporting
       WHERE StatusId = inStatusId AND
             (NVL(ChannelId,inChannelId) = inChannelId OR (ChannelId IS NULL AND inChannelId IS NULL)) AND
             (NVL(OperId,inOperId) = inOperId OR (OperId IS NULL AND inOperId IS NULL))
       ORDER BY OperId, ChannelId;
  BEGIN
    OPEN c_sr;
    FETCH c_sr INTO vRet;
    IF c_sr%NOTFOUND THEN
      BEGIN
        vRet:=gtCliTrStatTypes(inStatusId).AllowEdit;
      EXCEPTION
        WHEN OTHERS THEN
          vRet:=inDefaultVal;
      END;
    END IF;
    CLOSE c_sr;
    RETURN vRet;
  END AllowEdit;
  --------------------------------------------------------------------------------------------------
  FUNCTION AllowCancel    (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
    vRet StatusReporting.AllowCancel%TYPE;
    CURSOR c_sr IS
      SELECT /*+ RESULT_CACHE */
             AllowCancel
        FROM StatusReporting
       WHERE StatusId = inStatusId AND
             (NVL(ChannelId,inChannelId) = inChannelId OR (ChannelId IS NULL AND inChannelId IS NULL)) AND
             (NVL(OperId,inOperId) = inOperId OR (OperId IS NULL AND inOperId IS NULL))
       ORDER BY OperId, ChannelId;
  BEGIN
    OPEN c_sr;
    FETCH c_sr INTO vRet;
    IF c_sr%NOTFOUND THEN
      BEGIN
        vRet:=gtCliTrStatTypes(inStatusId).AllowCancel;
      EXCEPTION
        WHEN OTHERS THEN
          vRet:=inDefaultVal;
      END;
    END IF;
    CLOSE c_sr;
    RETURN vRet;
  END AllowCancel;
  --------------------------------------------------------------------------------------------------
  FUNCTION AllowSign      (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.AllowSign%TYPE DEFAULT NULL
                          ) RETURN PLS_INTEGER IS
    vRet StatusReporting.AllowSign%TYPE;
    CURSOR c_sr IS
      SELECT /*+ RESULT_CACHE */
             AllowSign
        FROM StatusReporting
       WHERE StatusId = inStatusId AND
             (NVL(ChannelId,inChannelId) = inChannelId OR (ChannelId IS NULL AND inChannelId IS NULL)) AND
             (NVL(OperId,inOperId) = inOperId OR (OperId IS NULL AND inOperId IS NULL))
       ORDER BY OperId, ChannelId;
  BEGIN
    OPEN c_sr;
    FETCH c_sr INTO vRet;
    IF c_sr%NOTFOUND THEN
      BEGIN
        vRet:=gtCliTrStatTypes(inStatusId).AllowSign;
      EXCEPTION
        WHEN OTHERS THEN
          vRet:=inDefaultVal;
      END;
    END IF;
    CLOSE c_sr;
    RETURN vRet;
  END AllowSign;
  --------------------------------------------------------------------------------------------------
  FUNCTION FinalStatus    (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.FinalStatus%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
  BEGIN
    RETURN gtCliTrStatTypes(inStatusId).FinalStatus;
  EXCEPTION
    WHEN OTHERS THEN RETURN inDefaultVal;
  END FinalStatus;
  --------------------------------------------------------------------------------------------------
  FUNCTION ShortText      (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.ShortText%TYPE DEFAULT NULL) RETURN VARCHAR2 IS
  BEGIN
    RETURN gtCliTrStatTypes(inStatusId).ShortText;
  EXCEPTION
    WHEN OTHERS THEN RETURN inDefaultVal;
  END ShortText;
  --------------------------------------------------------------------------------------------------
  FUNCTION TextId         (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inDefaultVal IN ClientTransactionStatusTypes.TextId%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
  BEGIN
    RETURN gtCliTrStatTypes(inStatusId).TextId;
  EXCEPTION
    WHEN OTHERS THEN
      RETURN inDefaultVal;
  END TextId;
  --------------------------------------------------------------------------------------------------
  FUNCTION ReportToClient (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.ReportToClient%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
    vRet StatusReporting.ReportToClient%TYPE;
    CURSOR c_sr IS
      SELECT /*+ RESULT_CACHE */
             ReportToClient
        FROM StatusReporting
       WHERE StatusId = inStatusId AND
             (NVL(ChannelId,inChannelId) = inChannelId OR (ChannelId IS NULL AND inChannelId IS NULL)) AND
             (NVL(OperId,inOperId) = inOperId OR (OperId IS NULL AND inOperId IS NULL))
       ORDER BY OperId, ChannelId;
  BEGIN
    OPEN c_sr;
    FETCH c_sr INTO vRet;
    IF c_sr%NOTFOUND THEN
      BEGIN
        vRet:=gtCliTrStatTypes(inStatusId).ReportToClient;
      EXCEPTION
        WHEN OTHERS THEN
          vRet:=inDefaultVal;
      END;
    END IF;
    CLOSE c_sr;
    RETURN vRet;
  END ReportToClient;
  --
  FUNCTION Visible        (inStatusId   IN ClientTransactionStatusTypes.StatusId%TYPE,
                           inChannelId  IN StatusReporting.ChannelId%TYPE,
                           inOperId     IN StatusReporting.OperId%TYPE DEFAULT NULL,
                           inDefaultVal IN ClientTransactionStatusTypes.Visible%TYPE DEFAULT NULL) RETURN PLS_INTEGER IS
    vRet StatusReporting.Visible%TYPE;
    CURSOR c_sr IS
      SELECT /*+ RESULT_CACHE */
             Visible
        FROM StatusReporting
       WHERE StatusId = inStatusId AND
             (NVL(ChannelId,inChannelId) = inChannelId OR (ChannelId IS NULL AND inChannelId IS NULL)) AND
             (NVL(OperId,inOperId) = inOperId OR (OperId IS NULL AND inOperId IS NULL))
       ORDER BY OperId, ChannelId;
  BEGIN
    OPEN c_sr;
    FETCH c_sr INTO vRet;
    IF c_sr%NOTFOUND THEN
      BEGIN
        vRet:=gtCliTrStatTypes(inStatusId).Visible;
      EXCEPTION
        WHEN OTHERS THEN
          vRet:=inDefaultVal;
      END;
    END IF;
    CLOSE c_sr;
    RETURN vRet;
  END Visible;
  --------------------------------------------------------------------------------------------------
BEGIN
  DECLARE
    CURSOR c_ctst IS
      SELECT /*+ RESULT_CACHE */
             *
        FROM ClientTransactionStatusTypes;
  BEGIN
    FOR ctst IN c_ctst LOOP
      gtCliTrStatTypes(ctst.StatusId) := ctst;
    END LOOP;
  END;
END reftab_ctst;
