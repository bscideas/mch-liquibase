
  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "IM_ERROR"
IS
------------------
-- $release: 078 $
-- $version: 8.0.0.0 $
------------------
-- $Header: /Oracle/UDEBS_XPE/UDEBSXPE.IM_ERROR.PBK 4     6. 11. 12 16:55 Ilenin $
------------------
--
--
-- MODIFICATION HISTORY
--
-- Person      Date       Comments
-- ----------- ---------- ---------------------------------------------------------
-- Ilenin      21.07.2014 UDEBS RS
-- --------------------------------------------------------------------------------
--
  --
  gtErrTab     err_tab; -- zoznam chyb
  gnErrTypes   NUMBER;  -- binarne ANDovane vsetky typy chyb, je to koli tomu, aby sa nemusel prechadzat zoznam na to, aby sa vedelo, ci v zozname je napr BUSSINESS chyba - nemoze sa pokracovat v spracovani dalej ...
  gnActErrType NUMBER;  -- aktualne pridany typ chyby
  gvLangId     SupportedLang.LangId%TYPE := 'en'; -- zatial neesituje procka, ktora by jazyk textu nastavovala, preto bude default 'en'
  --
  -- doplnenie textu (ak je prazdny)
  PROCEDURE GetDefaults (
      inErrorId IN ClientTransactionErrorTypes.ExternalErrorId%TYPE,
      pvText    IN OUT NOCOPY ClientTransactionErrorTypes.Description%TYPE,
      pvParam0  IN OUT NOCOPY ClientTransactionErrorTypes.Param0%TYPE,
      pvParam1  IN OUT NOCOPY ClientTransactionErrorTypes.Param1%TYPE,
      pvParam2  IN OUT NOCOPY ClientTransactionErrorTypes.Param2%TYPE,
      pvParam3  IN OUT NOCOPY ClientTransactionErrorTypes.Param3%TYPE,
      pvParam4  IN OUT NOCOPY ClientTransactionErrorTypes.Param4%TYPE,
      pvParam5  IN OUT NOCOPY ClientTransactionErrorTypes.Param5%TYPE,
      pvParam6  IN OUT NOCOPY ClientTransactionErrorTypes.Param6%TYPE,
      pvParam7  IN OUT NOCOPY ClientTransactionErrorTypes.Param7%TYPE,
      pvParam8  IN OUT NOCOPY ClientTransactionErrorTypes.Param8%TYPE,
      pvParam9  IN OUT NOCOPY ClientTransactionErrorTypes.Param9%TYPE)
      -- tu som umiestnil texty chyb, aby neboli rozhadzane po roznych kodoch, tj. neboli pre tu istu chybu rozne
      -- az sa na to zavedie ciselnik (zatial to je iba v XLS), tak tu tieto texty nebudu ale budu sa v tejto funkcii
      -- vyberat z ciselniku ...
  IS
  BEGIN
    SELECT NVL(pvText,Description), NVL(pvParam0,Param0), NVL(pvParam1,Param1), NVL(pvParam2,Param2), NVL(pvParam3,Param3),
           NVL(pvParam4,Param4), NVL(pvParam5,Param5), NVL(pvParam6,Param6), NVL(pvParam7,Param7), NVL(pvParam8,Param8),
           NVL(pvParam9, NVL(Param9, GetText(TextId,gvLangId)))
      INTO pvText, pvParam0, pvParam1, pvParam2, pvParam3, pvParam4, pvParam5, pvParam6, pvParam7, pvParam8, pvParam9
      FROM ClientTransactionErrorTypes WHERE ExternalErrorId = inErrorId;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      NULL;
  END GetDefaults;
  --
  -- zmaze glob. premennu zoznam chyb.
  PROCEDURE Init
      -- treba zabezpecit aby bola vzdy zavolana minimalne na zaciatku PIF - tj. na zaciatku volania z nejakej aplikacie
      -- v opacnom pripade by sa mohli stat, ze volanie vrati chybu, ktora v nom nikde nenastala
  IS
  BEGIN
    gnErrTypes := 0;
    IF gtErrTab IS NULL THEN
      gtErrTab := err_tab();
    ELSE
      IF gvDelete THEN
        gtErrTab.delete;
      END IF;
    END IF;
  END Init;
  --
  PROCEDURE AddType (
      inErrorId IN NUMBER)
      -- registruje v glob premennej, ci v priebehu volani vznikla spon jedna chyba daneho typu
      -- podla excelu ma na 16^3 pozicii cislo chyby zakodovany jej typ pre :
      -- ET_USER_EXC : 3, ET_BUSINESS_EXC : 4, ET_SUCCESSFUL : 0, ET_APPLICATION_EXC : 5
      -- ET_SYSTEM_EXC : 6, ET_SYSTEM_ERR : 7
  IS
    x1 RAW(10) := utl_raw.cast_to_raw(chr(gnErrTypes));
    x2 RAW(10);
  BEGIN
    IF inErrorId BETWEEN -20999 AND -20000 THEN
      x2 := utl_raw.cast_to_raw(chr(ET_APPLICATION_EXC));
    ELSE
      -- vyberiem len 4. poziciu zprava, vysledok bude 0..16 ~ 0..F
      SELECT DECODE(16 * (trunc(inErrorId/4096)/16 - trunc(trunc(inErrorId/4096)/16)),
                    3, ET_USER_EXC,
                    4, ET_BUSINESS_EXC,
                    0, ET_SUCCESSFUL,
                    5, ET_APPLICATION_EXC,
                       0)
          INTO gnActErrType FROM dual;
       x2 := utl_raw.cast_to_raw(chr(gnActErrType));
    END IF;
  gnErrTypes := ascii(utl_raw.cast_to_varchar2(utl_raw.bit_or(x1,x2)));
  END AddType;
  --
  -- parametre spoji do jedneho textu
  FUNCTION CombineParams (
      ivParam0  IN ClientTransactionErrorTypes.Param0%TYPE DEFAULT NULL,
      ivParam1  IN ClientTransactionErrorTypes.Param1%TYPE DEFAULT NULL,
      ivParam2  IN ClientTransactionErrorTypes.Param2%TYPE DEFAULT NULL,
      ivParam3  IN ClientTransactionErrorTypes.Param3%TYPE DEFAULT NULL,
      ivParam4  IN ClientTransactionErrorTypes.Param4%TYPE DEFAULT NULL,
      ivParam5  IN ClientTransactionErrorTypes.Param5%TYPE DEFAULT NULL,
      ivParam6  IN ClientTransactionErrorTypes.Param6%TYPE DEFAULT NULL,
      ivParam7  IN ClientTransactionErrorTypes.Param7%TYPE DEFAULT NULL,
      ivParam8  IN ClientTransactionErrorTypes.Param8%TYPE DEFAULT NULL,
      ivParam9  IN ClientTransactionErrorTypes.Param9%TYPE DEFAULT NULL)
  RETURN VARCHAR2
  IS
    lvStr VARCHAR2(1847 CHAR); -- raise_application_error dokaze pouzit 2048, text je obmedzeny na 200 znakov + 1 medzera
  BEGIN
    IF ivParam0 IS NOT NULL THEN lvStr := lvStr||'P0="'||substr(ivParam0,1,174)||'" ';  END IF;
    IF ivParam1 IS NOT NULL THEN lvStr := lvStr||'P1="'||substr(ivParam1,1,174)||'" ';  END IF;
    IF ivParam2 IS NOT NULL THEN lvStr := lvStr||'P2="'||substr(ivParam2,1,174)||'" ';  END IF;
    IF ivParam3 IS NOT NULL THEN lvStr := lvStr||'P3="'||substr(ivParam3,1,174)||'" ';  END IF;
    IF ivParam4 IS NOT NULL THEN lvStr := lvStr||'P4="'||substr(ivParam4,1,174)||'" ';  END IF;
    IF ivParam5 IS NOT NULL THEN lvStr := lvStr||'P5="'||substr(ivParam5,1,174)||'" ';  END IF;
    IF ivParam6 IS NOT NULL THEN lvStr := lvStr||'P6="'||substr(ivParam6,1,174)||'" ';  END IF;
    IF ivParam7 IS NOT NULL THEN lvStr := lvStr||'P7="'||substr(ivParam7,1,174)||'" ';  END IF;
    IF ivParam8 IS NOT NULL THEN lvStr := lvStr||'P8="'||substr(ivParam8,1,174)||'" ';  END IF;
    IF ivParam9 IS NOT NULL THEN lvStr := lvStr||'P9="'||substr(ivParam9,1,174)||'" ';  END IF;
    RETURN lvStr;
  END CombineParams;
  --
  -- prida chybu
  PROCEDURE AddError (
      inErrorId IN ClientTransactionErrorTypes.ExternalErrorId%TYPE,
      ivText    IN ClientTransactionErrorTypes.Description%TYPE DEFAULT NULL,
      ivParam0  IN ClientTransactionErrorTypes.Param0%TYPE DEFAULT NULL,
      ivParam1  IN ClientTransactionErrorTypes.Param1%TYPE DEFAULT NULL,
      ivParam2  IN ClientTransactionErrorTypes.Param2%TYPE DEFAULT NULL,
      ivParam3  IN ClientTransactionErrorTypes.Param3%TYPE DEFAULT NULL,
      ivParam4  IN ClientTransactionErrorTypes.Param4%TYPE DEFAULT NULL,
      ivParam5  IN ClientTransactionErrorTypes.Param5%TYPE DEFAULT NULL,
      ivParam6  IN ClientTransactionErrorTypes.Param6%TYPE DEFAULT NULL,
      ivParam7  IN ClientTransactionErrorTypes.Param7%TYPE DEFAULT NULL,
      ivParam8  IN ClientTransactionErrorTypes.Param8%TYPE DEFAULT NULL,
      ivParam9  IN ClientTransactionErrorTypes.Param9%TYPE DEFAULT NULL,
      inRaiseAnyError IN NUMBER DEFAULT 0) -- ak je registrovana akakolvek chyba vyhodi sa geRegAnyError
      -- registruje vyskytnute chyby do globalnej premennej
      -- posledny parameter ma nasledujuce vyuzitie :
      -- ak nastane chyba typu ineho ako ET_SUCCESSFUL, vyhodi sa chyba REG_ANY_ERROR ORA-20000
      -- je to uzitocne najma v pripade, ze v jednom PL/SQL sa vola viacero procedur a kazda z nich moze
      -- skoncit nejakou chybou, bolo by programatorsky neefektivne za kazdym volanim zistovat ci dana chyba
      -- nastala a nasledne povolit/zakazat dalsie volania, takto staci dat do hlavneho bloku jej odchytenie
      -- a zoznam chyb pre toto elementarne volanie bude aj zoznamom chyb pre cely PL/SQL bolk
  IS
    lvText    ClientTransactionErrorTypes.Description%TYPE := ivText;
    lvParam0  ClientTransactionErrorTypes.Param0%TYPE := ivParam0;
    lvParam1  ClientTransactionErrorTypes.Param1%TYPE := ivParam1;
    lvParam2  ClientTransactionErrorTypes.Param2%TYPE := ivParam2;
    lvParam3  ClientTransactionErrorTypes.Param3%TYPE := ivParam3;
    lvParam4  ClientTransactionErrorTypes.Param4%TYPE := ivParam4;
    lvParam5  ClientTransactionErrorTypes.Param5%TYPE := ivParam5;
    lvParam6  ClientTransactionErrorTypes.Param6%TYPE := ivParam6;
    lvParam7  ClientTransactionErrorTypes.Param7%TYPE := ivParam7;
    lvParam8  ClientTransactionErrorTypes.Param8%TYPE := ivParam8;
    lvParam9  ClientTransactionErrorTypes.Param9%TYPE := ivParam9;
    lnRaised NUMBER;
    i NUMBER;
    FUNCTION CombParams RETURN VARCHAR2 IS
    BEGIN
      RETURN CombineParams(lvParam0,lvParam1,lvParam2,lvParam3,lvParam4,lvParam5,lvParam6,lvParam7,lvParam8,lvParam9);
    END;
  BEGIN
    IF gtErrTab IS NULL THEN
      Init;
    END IF;
    GetDefaults(inErrorId, lvText, lvParam0, lvParam1, lvParam2, lvParam3, lvParam4, lvParam5, lvParam6, lvParam7, lvParam8, lvParam9);
    AddType(inErrorId);
    IF gnActErrType = ET_APPLICATION_EXC OR inErrorId BETWEEN -20999 AND -20000 THEN -- aplikacne chyby vyhadzujeme ako exceptions
      IF inErrorId BETWEEN -20999 AND -20000 THEN
        lnRaised := inErrorId;
      ELSE
        SELECT -mod(inErrorId,4096) - 19999 INTO lnRaised FROM dual;
      END IF;
      IF gvDbmsOutput = 1 THEN
        DBMS_OUTPUT.PUT_LINE('ErrorId='||inErrorId||' Text="'||substr(lvText,1,50)||'" '||substr(CombParams,1,170));
      END IF;
      raise_application_error(lnRaised, substr(lvText,1,200)||' '||CombParams);
    ELSE
      i := NVL(gtErrTab.last,0)+1;
      gtErrTab.extend;
      gtErrTab(i) := err_obj.Init(inErrorId, lvText, lvParam0, lvParam1, lvParam2, lvParam3, lvParam4, lvParam5, lvParam6, lvParam7, lvParam8, lvParam9, sysdate);
      IF gvDbmsOutput = 1  THEN
        DBMS_OUTPUT.PUT_LINE('ErrorId='||inErrorId||' Text="'||substr(lvText,1,50)||' '||substr(CombParams,1,170));
      END IF;
    END IF;
    IF inRaiseAnyError = 1 AND gnActErrType IN (ET_USER_EXC, ET_BUSINESS_EXC, ET_APPLICATION_EXC) THEN
      raise_application_error( -20000, inErrorId||' '||substr(lvText,1,293)||' '||CombParams);
    END IF;
  END AddError;
  --
  -- vrati ref cursor chyb
  PROCEDURE GetErrors (
      orErrors  IN OUT tRefCursor)
  IS
  BEGIN
    IF orErrors%ISOPEN THEN
      CLOSE orErrors;
    END IF;
    -- we'll open cursor only if there are errors
    IF gtErrTab.COUNT > 0 THEN
      OPEN orErrors FOR
          SELECT ErrorId, Text, Param0, Param1, Param2, Param3, Param4, Param5, Param6, Param7, Param8, Param9, OccurDate
            FROM the (SELECT CAST(gtErrTab AS err_tab) FROM dual) x;
    END IF;
  END GetErrors;
  --
  -- zisti ci v serii volani existuje dany typ (dane typy) chyb
  FUNCTION ExistErrType (
      inErrType IN NUMBER) -- jedna z konstant (alebo viacero zANDovanych z hlavicky tohoto package)
  RETURN NUMBER -- 0 chyba daneho typu neexistuje, 1 v serii volani sa vyskytla
  IS
    x1 RAW(10) := utl_raw.cast_to_raw(chr(gnErrTypes));
    x2 RAW(10) := utl_raw.cast_to_raw(chr(inErrType));
  BEGIN
    IF ascii(utl_raw.cast_to_varchar2(utl_raw.bit_and(x1,x2))) > 0 THEN
      RETURN 1;
    ELSE
      RETURN 0;
    END IF;
  END ExistErrType;
END IM_ERROR;
