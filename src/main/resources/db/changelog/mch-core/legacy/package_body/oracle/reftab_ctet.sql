
  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "REFTAB_CTET" IS
------------------
-- $release: 078 $
-- $version: 8.0.0.0 $
------------------
-- $Header: /Oracle/UDEBS_EE/UDEBS0105.REFTAB_CTET.PBK 4     22.07.03 21:53 Jaros $
-- ---------   ---------- -------------------------------------------------
-- Ilenin      07.07.2014 UDEBS RS
-- ---------   ---------- -------------------------------------------------
  TYPE t_ErrorTypes IS RECORD (
      TextId         ClientTransactionErrorTypes.TextId%TYPE,
      BankErrorCode  ClientTransactionErrorTypes.BankErrorCode%TYPE,
      FatalError     NUMBER(1)
  );
  TYPE t_ErrorTypesTable IS TABLE OF t_ErrorTypes INDEX BY BINARY_INTEGER;
  gtErrorTypes t_ErrorTypesTable;
  gbInitialized BOOLEAN := FALSE;
  --
  FUNCTION Existing      (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE) RETURN BOOLEAN IS
  BEGIN
    IF gtErrorTypes.Exists(inErrorId) THEN RETURN TRUE;
    ELSE RETURN FALSE;
    END IF;
  END;
  --
  FUNCTION TextId        (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          inDefaultVal    IN ClientTransactionErrorTypes.TextId%TYPE DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtErrorTypes(inErrorId).TextId;
  EXCEPTION
    WHEN OTHERS THEN RETURN inDefaultVal;
  END;
  --
  FUNCTION BankErrorCode (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          inDefaultVal    IN ClientTransactionErrorTypes.BankErrorCode%TYPE DEFAULT NULL) RETURN VARCHAR2 IS
  BEGIN
    RETURN gtErrorTypes(inErrorId).BankErrorCode;
  EXCEPTION
    WHEN OTHERS THEN RETURN inDefaultVal;
  END;
  --
  FUNCTION FatalError    (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          inDefaultVal    IN NUMBER DEFAULT NULL) RETURN NUMBER IS
  BEGIN
    RETURN gtErrorTypes(inErrorId).FatalError;
  EXCEPTION
    WHEN OTHERS THEN RETURN inDefaultVal;
  END;
  --
  FUNCTION Text          (inErrorId       IN ClientTransactionErrorTypes.ErrorId%TYPE,
                          ivLangId        IN SupportedLang.LangId%TYPE) RETURN VARCHAR2 IS
  BEGIN
    RETURN GetText(gtErrorTypes(inErrorId).TextID,ivLangID);
  END;
  --
  FUNCTION ErrorId       (inBankErrorCode IN ClientTransactionErrorTypes.BankErrorCode%TYPE,
                          inDefaultVal    IN ClientTransactionErrorTypes.ErrorId%TYPE DEFAULT NULL) RETURN NUMBER IS
    v_Ret ClientTransactionErrorTypes.ErrorId%TYPE;
  BEGIN
    BEGIN
      SELECT ErrorId INTO v_Ret FROM ClientTransactionErrorTypes
       WHERE BankErrorCode = inBankErrorCode;
    EXCEPTION WHEN OTHERS THEN
       NULL;
    END;
    RETURN NVL(v_Ret,inDefaultVal);
  END;
  --
BEGIN
  DECLARE
    CURSOR c_ctet IS
      SELECT ErrorId, TextId, BankErrorCode, DECODE(CategoryCode,'OKWITHINFOTOLOG', 0, 1) FatalError
      FROM ClientTransactionErrorTypes
      WHERE ErrorId IS NOT NULL;
  BEGIN
    FOR ctet IN c_ctet LOOP
      gtErrorTypes(ctet.ErrorId).TextId        := ctet.TextId;
      gtErrorTypes(ctet.ErrorId).BankErrorCode := ctet.BankErrorCode;
      gtErrorTypes(ctet.ErrorId).FatalError    := ctet.FatalError;
    END LOOP;
    gbInitialized := TRUE;
  END;
END reftab_ctet;
