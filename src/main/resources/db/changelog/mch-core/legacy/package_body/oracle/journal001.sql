
  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "JOURNAL001" IS
  ------------------
  -- $release: 5751 $
  -- $version: 8.0.19.0 $
  ------------------
  -- BSC Praha spol. s r.o.
  -- package umoznujuci manipulaciu s tabulkou Journal
  -- MODIFICATION HISTORY
  -- Person         Date        Comments
  -- ------------   ----------  -------------------------------------------
  -- Ilenin         01.07.2014  UDEBS RS
  -- Bocak          10.02.2015  LogEventPSI
  -- Emmer          07.09.2015  Rewrite Journaling
  -- Bocak          05.10.2015  I2Journal - added exception fk_exception
  -- Bocak          08.10.2015  LogEvent,I2Journal - added aDetails - 20009183-4757
  -- Pataky         13.10.2015  Details parameter type functions overloaded with CLOB versions, Details in table Journal still remains XMLType - 20009203-1354
  -- Bocak          28.10.2015  I2Journal - added another exception fk_exception
  -- Emmer          16.11.2015  Log events only into new structures
  -- Emmer          14.12.2015  Third journaling phase - removal of obsolete functions and procedures + leaving LogEvent/LogChildEvent for backwards compatibility 20009203-1354
  -- Bocak          04.01.2016  Get back procedures
  -- Bocak          13.01.2016  don't use ESCAPE_TEXT_FOR_XML for decription - 20009183-5403
  -- Emmer          15.01.2016  Error journaling while journaling in autonomous transaction
  -- Bocak          22.01.2016  ESCAPE_TEXT_FOR_DES - 20009183-5425
  -- Pataky         15.04.2016  Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
  -- Snederfler     03.05.2016  20009203-4818 - I2Journal - replace_xml_start_tag applied on p_Details when p_Event is NULL
  -- Pataky         28.04.2017  Modifications to support extension journal tables - 20009203-7142
  -- Pataky         18.05.2017  Fixed support of extension journal tables (MYG-4787) - 20009203-7142
  -- Emmer          07.09.2017  skipJournal: if EventTypes.loglevel = 0 then nothing is journaled for this event. Event -9 is always journaled.
  -- Emmer          28.02.2018  ERROR_LOG_AUTONOMOUS
  --
  -----private
  --
  RIDBuf ROWID;
  TYPE tTExtendedJournalingCode IS TABLE OF EventTypes.extendedjournalingcode%TYPE INDEX BY PLS_INTEGER;
  tExtendedJournalingCode tTExtendedJournalingCode;
  --
  -- remove special characters only when string is not in XML format
  FUNCTION ESCAPE_TEXT_FOR_DES(ivText VARCHAR2) RETURN CLOB IS
    lvText VARCHAR2(32000);
    lxTmp XMLTYPE;
    leXMLError EXCEPTION;
    PRAGMA EXCEPTION_INIT(leXMLError, -31011);
  BEGIN
    lvText := REPLACE(ivText, '&', '&amp;');
    lvText := REPLACE(REPLACE(REPLACE(REPLACE(lvText, '<', '&lt;'), '>', '&gt;'), '''', '&apos;'),
                      '"',
                      '&quot;');
    /* optimizations
    BEGIN
      SELECT XMLTYPE(ivText) INTO lxTmp FROM dual;
      lvText:=ivText;
    EXCEPTION
      WHEN leXMLError THEN
          lvText := REPLACE(ivText, '&', '&amp;');
        lvText := REPLACE(REPLACE(REPLACE(REPLACE(lvText, '<', '&lt;'), '>', '&gt;'), '''', '&apos;'),
                          '"',
                          '&quot;');
      WHEN OTHERS THEN
        lvText:=ivText;
    END;
    */
    RETURN lvText;
  END ESCAPE_TEXT_FOR_DES;
  --
  -- used for 1 element extraction from Journal.Details XML
  FUNCTION get_elem_from_xml(p_xml       xmltype,
                             p_elem_name VARCHAR2) RETURN VARCHAR2 IS
    v_return VARCHAR2(4000);
  BEGIN
    SELECT extractvalue(p_xml, '//' || p_elem_name)
      INTO v_return
      FROM dual;
    RETURN v_return;
  END;
  --
  FUNCTION get_xml_from_clob(p_clob CLOB) RETURN xmltype IS
  BEGIN
    IF p_clob IS NULL THEN
      RETURN NULL;
    ELSE
      RETURN xmltype(p_clob);
    END IF;
  END;
  --
  FUNCTION get_clob_from_xml(p_xml xmltype) RETURN CLOB IS
  BEGIN
    IF p_xml IS NULL THEN
      RETURN NULL;
    ELSE
      RETURN p_xml.getClobval();
    END IF;
  END;
  --
  -- replace_xml_start_tag in CLOB, for example <?xml version="1.0" encoding="UTF-8" standalone=''yes''?>
  FUNCTION replace_xml_start_tag(p_CLOB_text CLOB) RETURN CLOB IS
    v_xml_start NUMBER;
    v_xml_end NUMBER;
    v_buffer VARCHAR2(32000);
    v_temp_clob CLOB;
  BEGIN
    v_xml_start := dbms_lob.instr(lob_loc => p_CLOB_text, pattern => '<?xml');
    v_xml_end := dbms_lob.instr(lob_loc => p_CLOB_text, pattern => '?>', offset => v_xml_start);
    v_temp_clob := TO_CLOB(dbms_lob.substr(p_CLOB_text, v_xml_start - 1));
    v_temp_clob := v_temp_clob ||
                   TO_CLOB(dbms_lob.substr(p_CLOB_text,
                                           dbms_lob.getlength(p_CLOB_text) - v_xml_end - 1,
                                           v_xml_end + 2));
    RETURN v_temp_clob;
  END;
  --
  FUNCTION ESCAPE_TEXT_FOR_XML(p_text VARCHAR2) RETURN CLOB IS
    v_text VARCHAR2(32000);
  BEGIN
    v_text := REPLACE(p_text, '&', '&amp;');
    v_text := REPLACE(REPLACE(REPLACE(REPLACE(v_text, '<', '&lt;'), '>', '&gt;'), '''', '&apos;'),
                      '"',
                      '&quot;');
    RETURN v_text;
  END;
  --
  FUNCTION skipJournal(p_Event EventTypes.eventid%TYPE) RETURN NUMBER RESULT_CACHE DETERMINISTIC IS
    v_LogLevel EventTypes.Loglevel%TYPE;
  BEGIN
    SELECT loglevel
      INTO v_LogLevel
      FROM EventTypes
     WHERE EventID = p_Event;
    IF v_LogLevel = 0 THEN
      RETURN 1;
    ELSE
      RETURN 0;
    END IF;
  EXCEPTION
    WHEN no_data_found THEN
      RETURN 0;
  END skipJournal;
  --
  FUNCTION CheckEvent(p_Event EventTypes.eventid%TYPE) RETURN NUMBER RESULT_CACHE DETERMINISTIC IS
    v_EventID EventTypes.eventid%TYPE;
  BEGIN
    SELECT EventID
      INTO v_EventID
      FROM EventTypes
     WHERE EventID = p_Event;
    RETURN v_EventID;
  EXCEPTION
    WHEN no_data_found THEN
      RETURN NULL;
  END CheckEvent;
  --
  PROCEDURE I2Journal_Error(p_Par          IN Journal.parentseq%TYPE,
                            p_Event        IN Journal.eventid%TYPE,
                            p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                            p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                            p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                            p_ErrMsg       IN CLOB DEFAULT NULL) IS
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    IF tExtendedJournalingCode.exists(p_Event)
       AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT1' THEN
      INSERT INTO JOURNAL_EXT1
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         p_ErrMsg,
         systimestamp);
    ELSIF tExtendedJournalingCode.exists(p_Event)
          AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT2' THEN
      INSERT INTO JOURNAL_EXT2
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         p_ErrMsg,
         systimestamp);
    ELSIF tExtendedJournalingCode.exists(p_Event)
          AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT3' THEN
      INSERT INTO JOURNAL_EXT3
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         p_ErrMsg,
         systimestamp);
    ELSE
      INSERT INTO Journal
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         xmltype(p_ErrMsg));
    END IF;
    COMMIT;
  END;
  --
  -- New overloaded I2Journal with direct XMLType input
  FUNCTION I2Journal(p_Par          IN Journal.parentseq%TYPE,
                     p_Event        IN Journal.eventid%TYPE,
                     p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                     p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                     p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                     p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                     p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) RETURN NUMBER IS
    xcERROR EXCEPTION;
    N Journal.seq%TYPE;
    lnEventID EventTypes.EventID%TYPE := NULL;
    lvErrMsg CLOB := NULL;
    v_ChannelID NUMBER;
    v_xmldiff CLOB;
    v_err_repeat BOOLEAN := TRUE;
    fk_exception EXCEPTION;
    tmpClob CLOB := NULL;
    PRAGMA EXCEPTION_INIT(fk_exception, -02291);
    tmp_cnt PLS_INTEGER;
  BEGIN
    -- validacia existencie EventID
    IF p_Event IS NULL THEN
      lvErrMsg := '<Journal><Error>JOURNAL001: Error - mandatory EventID in call is null.</Error>' ||
                  '<OriginatorID>' || ESCAPE_TEXT_FOR_XML(p_OriginatorID) ||
                  '</OriginatorID><Details>' || replace_xml_start_tag(get_clob_from_xml(p_Details)) ||
                  '</Details></Journal>';
      RAISE xcERROR;
    ELSIF p_Event IS NOT NULL THEN
      IF NOT tExtendedJournalingCode.exists(p_Event) THEN
        -- if present in collection, then it exists
        lnEventID := CheckEvent(p_Event);
        IF lnEventID IS NULL THEN
          -- EventID nenalezen - zalogovani chyby (autonomni transakci)
          lvErrMsg := '<Journal><Error>JOURNAL001: Error - mandatory EventID in call not found in EventTypes.</Error>' ||
                      '<EventID>' || p_Event || '</EventID><OriginatorID>' ||
                      ESCAPE_TEXT_FOR_XML(p_OriginatorID) || '</OriginatorID><Details>' ||
                      replace_xml_start_tag(get_clob_from_xml(p_Details)) || '</Details></Journal>';
          RAISE xcERROR;
        END IF;
      ELSE
        lnEventID := p_Event;
      END IF;
    END IF;
    --
    -- insert into Journal (or extension table) and also convert pDetails into XMLType (and thus check its validity)
    --
    IF tExtendedJournalingCode.exists(p_Event)
       AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT1' THEN
      tmpClob := get_clob_from_xml(p_Details);
      INSERT INTO JOURNAL_EXT1
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         tmpClob,
         p_Duration,
         systimestamp)
      RETURNING ROWID, SEQ INTO RidBuf, N;
    ELSIF tExtendedJournalingCode.exists(p_Event)
          AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT2' THEN
      tmpClob := get_clob_from_xml(p_Details);
      INSERT INTO JOURNAL_EXT2
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         tmpClob,
         p_Duration,
         systimestamp)
      RETURNING ROWID, SEQ INTO RidBuf, N;
    ELSIF tExtendedJournalingCode.exists(p_Event)
          AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT3' THEN
      tmpClob := get_clob_from_xml(p_Details);
      INSERT INTO JOURNAL_EXT3
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         tmpClob,
         p_Duration,
         systimestamp)
      RETURNING ROWID, SEQ INTO RidBuf, N;
    ELSE
      BEGIN
        INSERT INTO Journal
          (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration)
        VALUES
          (SEQ_JOURNAL.nextval,
           p_Par,
           p_Event,
           p_OriginatorID,
           p_ClientID,
           p_UserID,
           p_Details,
           p_Duration)
        RETURNING ROWID, SEQ INTO RidBuf, N;
      EXCEPTION
        WHEN fk_exception THEN
          -- check if parentSeq is not present in extension tables
          SELECT COUNT(1)
            INTO tmp_cnt
            FROM JOURNAL_EXT1
           WHERE rownum = 1
             AND seq = p_Par;
          IF tmp_cnt > 0 THEN
            tmpClob := get_clob_from_xml(p_Details);
            INSERT INTO JOURNAL_EXT1
              (SEQ,
               ParentSeq,
               EventID,
               OriginatorID,
               ClientID,
               UserID,
               Details,
               Duration,
               EventTime)
            VALUES
              (SEQ_JOURNAL.nextval,
               p_Par,
               p_Event,
               p_OriginatorID,
               p_ClientID,
               p_UserID,
               tmpClob,
               p_Duration,
               systimestamp)
            RETURNING ROWID, SEQ INTO RidBuf, N;
          ELSE
            SELECT COUNT(1)
              INTO tmp_cnt
              FROM JOURNAL_EXT2
             WHERE rownum = 1
               AND seq = p_Par;
            IF tmp_cnt > 0 THEN
              tmpClob := get_clob_from_xml(p_Details);
              INSERT INTO JOURNAL_EXT2
                (SEQ,
                 ParentSeq,
                 EventID,
                 OriginatorID,
                 ClientID,
                 UserID,
                 Details,
                 Duration,
                 EventTime)
              VALUES
                (SEQ_JOURNAL.nextval,
                 p_Par,
                 p_Event,
                 p_OriginatorID,
                 p_ClientID,
                 p_UserID,
                 tmpClob,
                 p_Duration,
                 systimestamp)
              RETURNING ROWID, SEQ INTO RidBuf, N;
            ELSE
              SELECT COUNT(1)
                INTO tmp_cnt
                FROM JOURNAL_EXT3
               WHERE rownum = 1
                 AND seq = p_Par;
              IF tmp_cnt > 0 THEN
                tmpClob := get_clob_from_xml(p_Details);
                INSERT INTO JOURNAL_EXT3
                  (SEQ,
                   ParentSeq,
                   EventID,
                   OriginatorID,
                   ClientID,
                   UserID,
                   Details,
                   Duration,
                   EventTime)
                VALUES
                  (SEQ_JOURNAL.nextval,
                   p_Par,
                   p_Event,
                   p_OriginatorID,
                   p_ClientID,
                   p_UserID,
                   tmpClob,
                   p_Duration,
                   systimestamp)
                RETURNING ROWID, SEQ INTO RidBuf, N;
              ELSE
                INSERT INTO Journal
                  (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration)
                VALUES
                  (SEQ_JOURNAL.nextval,
                   NULL,
                   p_Event,
                   p_OriginatorID,
                   p_ClientID,
                   p_UserID,
                   p_Details,
                   p_Duration)
                RETURNING ROWID, SEQ INTO RidBuf, N;
              END IF;
            END IF;
          END IF;
      END;
    END IF;
    --
    RETURN N;
  EXCEPTION
    WHEN xcERROR THEN
      ROLLBACK;
      -- kontrola, zda neni chyba jiz zazurnalovana, aby se stejna chyba pro kombinaci EVENT/ORIGINATOR/USER/CLIENT/SESSION/DETAIL neopakovala
      --lvErrMsg := xmltype(lvErrMsg).getclobval();
      FOR d IN (SELECT Details, seq
                  FROM Journal
                 WHERE EventID = -500
                   AND NVL(ParentSeq, -1) = NVL(p_Par, -1)
                   AND NVL(UserID, -1) = NVL(p_UserID, -1)
                   AND NVL(ClientID, -1) = NVL(p_ClientID, -1)
                   AND Details IS NOT NULL
                   AND TRUNC(eventtime) = TRUNC(SYSDATE)) LOOP
        SELECT REPLACE(REPLACE(XMLDIFF(xmltype(lvErrMsg), d.Details),
                               '<xd:xdiff xsi:schemaLocation="http://xmlns.oracle.com/xdb/xdiff.xsd http://xmlns.oracle.com/xdb/xdiff.xsd" xmlns:xd="http://xmlns.oracle.com/xdb/xdiff.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><?oracle-xmldiff operations-in-docorder="true" output-model="snapshot" diff-algorithm="global"?>'),
                       '</xd:xdiff>')
          INTO v_xmldiff
          FROM dual;
        IF v_xmldiff IS NULL THEN
          v_err_repeat := FALSE;
        END IF;
        EXIT WHEN NOT(v_err_repeat);
      END LOOP;
      -- zazurnalovani chyby, pokud se neopakuje
      IF v_err_repeat THEN
        BEGIN
          I2Journal_Error(p_Par          => p_Par,
                          p_Event        => -500,
                          p_OriginatorID => 'I2Journal',
                          p_ClientID     => p_ClientID,
                          p_UserID       => p_UserID,
                          p_ErrMsg       => lvErrMsg);
        EXCEPTION
          WHEN fk_exception THEN
            I2Journal_Error(p_Par          => NULL,
                            p_Event        => -500,
                            p_OriginatorID => 'I2Journal',
                            p_ClientID     => p_ClientID,
                            p_UserID       => p_UserID,
                            p_ErrMsg       => lvErrMsg);
        END;
      END IF;
      --
      im_error.Init;
      im_error.AddError(2117939, -- z clienttransactionerrortypes, kody chyb by mely byt spolecne mezi bankami, parametry nejsou povinne
                        dbms_lob.substr(lvErrMsg, amount => 1000),
                        'EventID',
                        p_Event);
  END I2Journal;
  -- New overloaded I2Journal without commit, CLOB version, target implementation
  FUNCTION I2Journal(p_Par          IN Journal.parentseq%TYPE,
                     p_Event        IN Journal.eventid%TYPE,
                     p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                     p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                     p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                     p_Details      IN CLOB DEFAULT NULL, --Journal.Details%TYPE DEFAULT NULL,
                     p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) RETURN NUMBER IS
    xcERROR EXCEPTION;
    N Journal.seq%TYPE;
    lnEventID EventTypes.EventID%TYPE := NULL;
    lvErrMsg CLOB := NULL;
    v_ChannelID NUMBER;
    v_xmldiff CLOB;
    v_err_repeat BOOLEAN := TRUE;
    fk_exception EXCEPTION;
    PRAGMA EXCEPTION_INIT(fk_exception, -02291);
    tmp_cnt PLS_INTEGER;
  BEGIN
    -- validacia existencie EventID
    IF p_Event IS NULL THEN
      lvErrMsg := '<Journal><Error>JOURNAL001: Error - mandatory EventID in call is null.</Error>' ||
                  '<OriginatorID>' || ESCAPE_TEXT_FOR_XML(p_OriginatorID) ||
                  '</OriginatorID><Details>' || replace_xml_start_tag(p_Details) ||
                  '</Details></Journal>';
      RAISE xcERROR;
    ELSIF p_Event IS NOT NULL THEN
      IF NOT tExtendedJournalingCode.exists(p_Event) THEN
        -- if present in collection, then it exists
        lnEventID := CheckEvent(p_Event);
        IF lnEventID IS NULL THEN
          -- EventID nenalezen - zalogovani chyby (autonomni transakci)
          lvErrMsg := '<Journal><Error>JOURNAL001: Error - mandatory EventID in call not found in EventTypes.</Error>' ||
                      '<EventID>' || p_Event || '</EventID><OriginatorID>' ||
                      ESCAPE_TEXT_FOR_XML(p_OriginatorID) || '</OriginatorID><Details>' ||
                      replace_xml_start_tag(p_Details) || '</Details></Journal>';
          RAISE xcERROR;
        END IF;
      ELSE
        lnEventID := p_Event;
      END IF;
    END IF;
    --
    -- insert into Journal (or extension table) and also convert pDetails into XMLType (and thus check its validity)
    --
    IF tExtendedJournalingCode.exists(p_Event)
       AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT1' THEN
      INSERT INTO JOURNAL_EXT1
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         p_Details,
         p_Duration,
         systimestamp)
      RETURNING ROWID, SEQ INTO RidBuf, N;
    ELSIF tExtendedJournalingCode.exists(p_Event)
          AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT2' THEN
      INSERT INTO JOURNAL_EXT2
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         p_Details,
         p_Duration,
         systimestamp)
      RETURNING ROWID, SEQ INTO RidBuf, N;
    ELSIF tExtendedJournalingCode.exists(p_Event)
          AND tExtendedJournalingCode(p_Event) = 'JOURNAL_EXT3' THEN
      INSERT INTO JOURNAL_EXT3
        (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration, EventTime)
      VALUES
        (SEQ_JOURNAL.nextval,
         p_Par,
         p_Event,
         p_OriginatorID,
         p_ClientID,
         p_UserID,
         p_Details,
         p_Duration,
         systimestamp)
      RETURNING ROWID, SEQ INTO RidBuf, N;
    ELSE
      BEGIN
        INSERT INTO Journal
          (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration)
        VALUES
          (SEQ_JOURNAL.nextval,
           p_Par,
           p_Event,
           p_OriginatorID,
           p_ClientID,
           p_UserID,
           get_xml_from_clob(p_Details),
           p_Duration)
        RETURNING ROWID, SEQ INTO RidBuf, N;
      EXCEPTION
        WHEN fk_exception THEN
          -- check if parentSeq is not present in extension tables
          SELECT COUNT(1)
            INTO tmp_cnt
            FROM JOURNAL_EXT1
           WHERE rownum = 1
             AND seq = p_Par;
          IF tmp_cnt > 0 THEN
            INSERT INTO JOURNAL_EXT1
              (SEQ,
               ParentSeq,
               EventID,
               OriginatorID,
               ClientID,
               UserID,
               Details,
               Duration,
               EventTime)
            VALUES
              (SEQ_JOURNAL.nextval,
               p_Par,
               p_Event,
               p_OriginatorID,
               p_ClientID,
               p_UserID,
               p_Details,
               p_Duration,
               systimestamp)
            RETURNING ROWID, SEQ INTO RidBuf, N;
          ELSE
            SELECT COUNT(1)
              INTO tmp_cnt
              FROM JOURNAL_EXT2
             WHERE rownum = 1
               AND seq = p_Par;
            IF tmp_cnt > 0 THEN
              INSERT INTO JOURNAL_EXT2
                (SEQ,
                 ParentSeq,
                 EventID,
                 OriginatorID,
                 ClientID,
                 UserID,
                 Details,
                 Duration,
                 EventTime)
              VALUES
                (SEQ_JOURNAL.nextval,
                 p_Par,
                 p_Event,
                 p_OriginatorID,
                 p_ClientID,
                 p_UserID,
                 p_Details,
                 p_Duration,
                 systimestamp)
              RETURNING ROWID, SEQ INTO RidBuf, N;
            ELSE
              SELECT COUNT(1)
                INTO tmp_cnt
                FROM JOURNAL_EXT3
               WHERE rownum = 1
                 AND seq = p_Par;
              IF tmp_cnt > 0 THEN
                INSERT INTO JOURNAL_EXT3
                  (SEQ,
                   ParentSeq,
                   EventID,
                   OriginatorID,
                   ClientID,
                   UserID,
                   Details,
                   Duration,
                   EventTime)
                VALUES
                  (SEQ_JOURNAL.nextval,
                   p_Par,
                   p_Event,
                   p_OriginatorID,
                   p_ClientID,
                   p_UserID,
                   p_Details,
                   p_Duration,
                   systimestamp)
                RETURNING ROWID, SEQ INTO RidBuf, N;
              ELSE
                INSERT INTO Journal
                  (SEQ, ParentSeq, EventID, OriginatorID, ClientID, UserID, Details, Duration)
                VALUES
                  (SEQ_JOURNAL.nextval,
                   NULL,
                   p_Event,
                   p_OriginatorID,
                   p_ClientID,
                   p_UserID,
                   get_xml_from_clob(p_Details),
                   p_Duration)
                RETURNING ROWID, SEQ INTO RidBuf, N;
              END IF;
            END IF;
          END IF;
      END;
    END IF;
    --
    RETURN N;
  EXCEPTION
    WHEN xcERROR THEN
      ROLLBACK;
      -- kontrola, zda neni chyba jiz zazurnalovana, aby se stejna chyba pro kombinaci EVENT/ORIGINATOR/USER/CLIENT/SESSION/DETAIL neopakovala
      lvErrMsg := xmltype(lvErrMsg).getclobval();
      FOR d IN (SELECT Details, seq
                  FROM Journal
                 WHERE EventID = -500
                   AND NVL(ParentSeq, -1) = NVL(p_Par, -1)
                   AND NVL(UserID, -1) = NVL(p_UserID, -1)
                   AND NVL(ClientID, -1) = NVL(p_ClientID, -1)
                   AND Details IS NOT NULL
                   AND TRUNC(eventtime) = TRUNC(SYSDATE)) LOOP
        SELECT REPLACE(REPLACE(XMLDIFF(xmltype(lvErrMsg), d.Details),
                               '<xd:xdiff xsi:schemaLocation="http://xmlns.oracle.com/xdb/xdiff.xsd http://xmlns.oracle.com/xdb/xdiff.xsd" xmlns:xd="http://xmlns.oracle.com/xdb/xdiff.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><?oracle-xmldiff operations-in-docorder="true" output-model="snapshot" diff-algorithm="global"?>'),
                       '</xd:xdiff>')
          INTO v_xmldiff
          FROM dual;
        IF v_xmldiff IS NULL THEN
          v_err_repeat := FALSE;
        END IF;
        EXIT WHEN NOT(v_err_repeat);
      END LOOP;
      -- zazurnalovani chyby, pokud se neopakuje
      IF v_err_repeat THEN
        BEGIN
          I2Journal_Error(p_Par          => p_Par,
                          p_Event        => -500,
                          p_OriginatorID => 'I2Journal',
                          p_ClientID     => p_ClientID,
                          p_UserID       => p_UserID,
                          p_ErrMsg       => lvErrMsg);
        EXCEPTION
          WHEN fk_exception THEN
            I2Journal_Error(p_Par          => NULL,
                            p_Event        => -500,
                            p_OriginatorID => 'I2Journal',
                            p_ClientID     => p_ClientID,
                            p_UserID       => p_UserID,
                            p_ErrMsg       => lvErrMsg);
        END;
      END IF;
      --
      im_error.Init;
      im_error.AddError(2117939, -- z clienttransactionerrortypes, kody chyb by mely byt spolecne mezi bankami, parametry nejsou povinne
                        dbms_lob.substr(lvErrMsg, amount => 1000),
                        'EventID',
                        p_Event);
  END I2Journal;
  --
  ----- PUBLIC
  --
  -- New overloaded function LogEvent returns new primary journal key (Seq: Journal ID) without commit. Primary version used for journaling - target implementation.
  -- Returned value can be used for USERSESSION.ParentSeq
  FUNCTION LogEventClob(p_Event        IN Journal.eventid%TYPE,
                        p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                        p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                        p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                        p_Details      IN CLOB DEFAULT NULL,
                        p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                        p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) RETURN NUMBER IS
    N NUMBER := 0;
  BEGIN
    ATRSeq.delete;
    IF skipJournal(p_Event) = 0 THEN
      -- dohledani parent seq z USERSESSION nelze, proto se pouzije bud predana promenna, nebo promenna journal001
      N := I2Journal(p_ParentSeq, --nvl(p_ParentSeq, ParEvSeq),
                     p_Event,
                     p_OriginatorID,
                     p_ClientID,
                     p_UserID,
                     p_Details,
                     p_Duration);
      ATRseq(1).SEQ := N;
      ATRseq(1).RID := RIDBuf;
      Lvl := 1;
    END IF;
    RETURN N;
  END LogEventClob;
  --
  FUNCTION LogEvent(p_Event        IN Journal.eventid%TYPE,
                    p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                    p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                    p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                    p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                    p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                    p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) RETURN NUMBER IS
    N NUMBER := 0;
  BEGIN
    ATRSeq.delete;
    IF skipJournal(p_Event) = 0 THEN
      -- dohledani parent seq z USERSESSION nelze, proto se pouzije bud predana promenna, nebo promenna journal001
      N := I2Journal(p_ParentSeq, --nvl(p_ParentSeq, ParEvSeq),
                     p_Event,
                     p_OriginatorID,
                     p_ClientID,
                     p_UserID,
                     p_Details,
                     p_Duration);
      ATRseq(1).SEQ := N;
      ATRseq(1).RID := RIDBuf;
      Lvl := 1;
    END IF;
    RETURN N;
  END LogEvent;
  --
  -- New overloaded procedure LogEvent without commit
  PROCEDURE LogEventClob(p_Event        IN Journal.eventid%TYPE,
                         p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                         p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                         p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                         p_Details      IN CLOB DEFAULT NULL,
                         p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                         p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) IS
    N NUMBER;
  BEGIN
    ATRSeq.delete;
    IF skipJournal(p_Event) = 0 THEN
      -- dohledani parent seq z USERSESSION nelze, proto se pouzije bud predana promenna, nebo promenna journal001
      N := I2Journal(p_ParentSeq, --nvl(p_ParentSeq, ParEvSeq),
                     p_Event,
                     p_OriginatorID,
                     p_ClientID,
                     p_UserID,
                     p_Details,
                     p_Duration);
      ATRseq(1).SEQ := N;
      ATRseq(1).RID := RIDBuf;
      Lvl := 1;
    END IF;
  END LogEventClob;
  --
  PROCEDURE LogEvent(p_Event        IN Journal.eventid%TYPE,
                     p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                     p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                     p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                     p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                     p_ParentSeq    IN Journal.ParentSeq%TYPE DEFAULT NULL,
                     p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) IS
    N NUMBER;
  BEGIN
    IF skipJournal(p_Event) = 0 THEN
      N := LogEvent(p_Event,
                    p_OriginatorID,
                    p_ClientID,
                    p_UserID,
                    p_Details,
                    p_ParentSeq,
                    p_Duration);
    END IF;
  END LogEvent;
  --
  PROCEDURE LogEventClob(Event         IN Journal.eventid%TYPE,
                         Fea           IN VARCHAR2,
                         Dt1           IN TIMESTAMP := NULL,
                         Dt2           IN TIMESTAMP := NULL,
                         Ds            IN VARCHAR2 := NULL,
                         aOriginatorID IN Journal.originatorid%TYPE := NULL,
                         aChannelID    IN NUMBER := NULL,
                         LDTYPE        IN VARCHAR2 := NULL,
                         LDID          IN VARCHAR2 := NULL,
                         aClientID     IN Journal.clientid%TYPE := NULL,
                         aExtension    IN CLOB DEFAULT NULL,
                         aUserID       IN Journal.UserID%TYPE DEFAULT NULL,
                         aDetails      IN CLOB DEFAULT NULL,
                         aDuration     IN Journal.Duration%TYPE DEFAULT NULL) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    ATRSeq.delete;
    IF skipJournal(Event) = 0 THEN
      v_temp_clob := '<Trandata>' || aDetails;
      v_temp_clob := v_temp_clob || '<Feature>' || ESCAPE_TEXT_FOR_XML(Fea) ||
                     '</Feature><Description>' || ESCAPE_TEXT_FOR_DES(Ds) ||
                     '</Description><StartDate>' || TO_CHAR(Dt1, 'DD.MM.YYYY HH24:MI:SS') ||
                     '</StartDate><EndDate>' || TO_CHAR(Dt2, 'DD.MM.YYYY HH24:MI:SS') ||
                     '</EndDate><ChannelID>' || aChannelID || '</ChannelID><LoginDevID>' ||
                     ESCAPE_TEXT_FOR_XML(LDID) || '</LoginDevID><LoginDevType>' ||
                     ESCAPE_TEXT_FOR_XML(LDTYPE) || '</LoginDevType><Status>0</Status><Extension>';
      v_temp_clob := v_temp_clob || replace_xml_start_tag(aExtension);
      v_temp_clob := v_temp_clob || '</Extension></Trandata>';
      N := I2Journal(ParEvSeq, Event, aOriginatorID, aClientID, aUserID, v_temp_clob, aDuration);
      ATRseq(1).SEQ := N;
      ATRseq(1).RID := RIDBuf;
      Lvl := 1;
    END IF;
  END LogEventClob;
  --
  PROCEDURE LogEvent(Event         IN Journal.eventid%TYPE,
                     Fea           IN VARCHAR2,
                     Dt1           IN TIMESTAMP := NULL,
                     Dt2           IN TIMESTAMP := NULL,
                     Ds            IN VARCHAR2 := NULL,
                     aOriginatorID IN Journal.originatorid%TYPE := NULL,
                     aChannelID    IN NUMBER := NULL,
                     LDTYPE        IN VARCHAR2 := NULL,
                     LDID          IN VARCHAR2 := NULL,
                     aClientID     IN Journal.clientid%TYPE := NULL,
                     aExtension    IN CLOB DEFAULT NULL,
                     aUserID       IN Journal.UserID%TYPE DEFAULT NULL,
                     aDetails      IN Journal.Details%TYPE DEFAULT NULL,
                     aDuration     IN Journal.Duration%TYPE DEFAULT NULL) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    ATRSeq.delete;
    IF skipJournal(Event) = 0 THEN
      v_temp_clob := '<Trandata>' || get_clob_from_xml(aDetails);
      v_temp_clob := v_temp_clob || '<Feature>' || ESCAPE_TEXT_FOR_XML(Fea) ||
                     '</Feature><Description>' || ESCAPE_TEXT_FOR_DES(Ds) ||
                     '</Description><StartDate>' || TO_CHAR(Dt1, 'DD.MM.YYYY HH24:MI:SS') ||
                     '</StartDate><EndDate>' || TO_CHAR(Dt2, 'DD.MM.YYYY HH24:MI:SS') ||
                     '</EndDate><ChannelID>' || aChannelID || '</ChannelID><LoginDevID>' ||
                     ESCAPE_TEXT_FOR_XML(LDID) || '</LoginDevID><LoginDevType>' ||
                     ESCAPE_TEXT_FOR_XML(LDTYPE) || '</LoginDevType><Status>0</Status><Extension>';
      v_temp_clob := v_temp_clob || replace_xml_start_tag(aExtension);
      v_temp_clob := v_temp_clob || '</Extension></Trandata>';
      N := I2Journal(ParEvSeq, Event, aOriginatorID, aClientID, aUserID, v_temp_clob, aDuration);
      ATRseq(1).SEQ := N;
      ATRseq(1).RID := RIDBuf;
      Lvl := 1;
    END IF;
  END LogEvent;
  --
  -- New overloaded LogChildEvent without commit
  PROCEDURE LogChildEventClob(p_Event        IN Journal.eventid%TYPE,
                              p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                              p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                              p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                              p_Details      IN CLOB DEFAULT NULL,
                              p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) IS
    N NUMBER;
    M NUMBER;
  BEGIN
    IF skipJournal(p_Event) = 0 THEN
      M := ATRSeq(Lvl).SEQ;
      N := I2Journal(M, p_Event, p_OriginatorID, p_ClientID, p_UserID, p_Details, p_Duration);
      Lvl := Lvl + 1;
      ATRseq(Lvl).SEQ := N;
      ATRseq(Lvl).RID := RIDBuf;
    END IF;
  END LogChildEventClob;
  --
  PROCEDURE LogChildEvent(p_Event        IN Journal.eventid%TYPE,
                          p_OriginatorID IN Journal.originatorid%TYPE DEFAULT NULL,
                          p_ClientID     IN Journal.clientid%TYPE DEFAULT NULL,
                          p_UserID       IN Journal.UserID%TYPE DEFAULT NULL,
                          p_Details      IN Journal.Details%TYPE DEFAULT NULL,
                          p_Duration     IN Journal.Duration%TYPE DEFAULT NULL) IS
    N NUMBER;
    M NUMBER;
  BEGIN
    IF skipJournal(p_Event) = 0 THEN
      M := ATRSeq(Lvl).SEQ;
      N := I2Journal(M, p_Event, p_OriginatorID, p_ClientID, p_UserID, p_Details, p_Duration);
      Lvl := Lvl + 1;
      ATRseq(Lvl).SEQ := N;
      ATRseq(Lvl).RID := RIDBuf;
    END IF;
  END LogChildEvent;
  --
  PROCEDURE LogChildEvent(Event         IN Journal.eventid%TYPE,
                          Fea           IN VARCHAR2,
                          Dt1           IN TIMESTAMP := NULL,
                          Dt2           IN TIMESTAMP := NULL,
                          Ds            IN VARCHAR2,
                          aOriginatorID IN Journal.originatorid%TYPE := NULL,
                          aChannelID    IN NUMBER := NULL,
                          LDTYPE        IN VARCHAR2 := NULL,
                          LDID          IN VARCHAR2 := NULL,
                          aClientID     IN Journal.clientid%TYPE := NULL,
                          aExtension    IN CLOB DEFAULT NULL,
                          aUserID       IN Journal.UserID%TYPE DEFAULT NULL) IS
    N NUMBER;
    M NUMBER;
    v_temp_clob CLOB;
  BEGIN
    IF skipJournal(Event) = 0 THEN
      M := ATRSeq(Lvl).SEQ;
      v_temp_clob := '<Trandata><Feature>' || ESCAPE_TEXT_FOR_XML(Fea) || '</Feature><Description>' ||
                     ESCAPE_TEXT_FOR_DES(Ds) || '</Description><StartDate>' ||
                     TO_CHAR(Dt1, 'DD.MM.YYYY HH24:MI:SS') || '</StartDate><EndDate>' ||
                     TO_CHAR(Dt2, 'DD.MM.YYYY HH24:MI:SS') || '</EndDate><ChannelID>' || aChannelID ||
                     '</ChannelID><LoginDevID>' || ESCAPE_TEXT_FOR_XML(LDID) ||
                     '</LoginDevID><LoginDevType>' || ESCAPE_TEXT_FOR_XML(LDTYPE) ||
                     '</LoginDevType><Status>0</Status><Extension>';
      v_temp_clob := v_temp_clob || replace_xml_start_tag(aExtension);
      v_temp_clob := v_temp_clob || '</Extension></Trandata>';
      N := I2Journal(M, Event, aOriginatorID, aClientID, aUserID, v_temp_clob);
      Lvl := Lvl + 1;
      ATRseq(Lvl).SEQ := N;
      ATRseq(Lvl).RID := RIDBuf;
    END IF;
  END LogChildEvent;
  --
  PROCEDURE ToParent IS
  BEGIN
    IF LVL > 1 THEN
      Lvl := Lvl - 1;
    END IF;
  END ToParent;
  --
  PROCEDURE ToChild IS
  BEGIN
    IF ATRseq.Exists(Lvl + 1) THEN
      Lvl := Lvl + 1;
    ELSE
      RAISE NO_DATA_FOUND;
    END IF;
  END ToChild;
  --
  PROCEDURE PutStatus(St IN NUMBER) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutStatus</Procedure><Status>' || St ||
                   '</Status></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutStatus;
  --
  FUNCTION GetStatus RETURN NUMBER IS
    st NUMBER;
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>GetStatus</Procedure></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
    RETURN NULL;
  END GetStatus;
  --
  PROCEDURE PutProgress(St IN NUMBER) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutProgress</Procedure><Status>' || St ||
                   '</Status></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutProgress;
  --
  FUNCTION GetProgress RETURN NUMBER IS
    st NUMBER;
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>GetProgress</Procedure></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
    RETURN NULL;
  END GetProgress;
  --
  PROCEDURE PutEndDate(Dt2 IN TIMESTAMP) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutEndDate</Procedure><EndDate>' ||
                   TO_CHAR(Dt2, 'DD.MM.YYYY HH24:MI:SS') || '</EndDate></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutEndDate;
  --
  PROCEDURE PutOriginator(Ori IN VARCHAR2) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutOriginator</Procedure><Originator>' ||
                   ESCAPE_TEXT_FOR_XML(Ori) || '</Originator></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutOriginator;
  --
  PROCEDURE PutChannel(Chann IN VARCHAR2) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutChannel</Procedure><Channel>' || Chann ||
                   '</Channel></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutChannel;
  --
  PROCEDURE PutClient(Cli IN VARCHAR2) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutClient</Procedure><Client>' || Cli ||
                   '</Client></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutClient;
  --
  PROCEDURE PutLoginDev(LDTYPE IN VARCHAR2,
                        LDID   IN VARCHAR2) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutLoginDev</Procedure><LoginDevType>' ||
                   ESCAPE_TEXT_FOR_XML(LDTYPE) || '</LoginDevType><LoginDevID>' ||
                   ESCAPE_TEXT_FOR_XML(LDID) || '</LoginDevID></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutLoginDev;
  --
  PROCEDURE PutDescription(Ds IN VARCHAR2) IS
    N NUMBER;
    v_temp_clob CLOB;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutDescription</Procedure><Description>' ||
                   ESCAPE_TEXT_FOR_DES(Ds) || '</Description></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutDescription;
  --
  PROCEDURE PutFeature(Fea IN VARCHAR2) IS
    v_temp_clob CLOB;
    N NUMBER;
  BEGIN
    -- JIZ SE NEVOLA
    v_temp_clob := '<Trandata><Procedure>PutFeature</Procedure><Feature>' ||
                   ESCAPE_TEXT_FOR_XML(Fea) || '</Feature></Trandata>';
    N := I2Journal(p_Par          => ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => 'JOURNAL001-OBSOLETE',
                   p_Details      => v_temp_clob);
  END PutFeature;
  --
  FUNCTION GetSeq(inAvoidError IN NUMBER DEFAULT NULL) RETURN NUMBER IS
    lnReturn NUMBER := NULL;
  BEGIN
    IF inAvoidError = 1 THEN
      BEGIN
        lnReturn := ATRseq(Lvl).SEQ;
      EXCEPTION
        WHEN OTHERS THEN
          lnReturn := NULL;
      END;
    ELSE
      lnReturn := ATRseq(Lvl).SEQ;
    END IF;
    RETURN lnReturn;
  END GetSeq;
  --
  PROCEDURE ToSeq(Seq IN NUMBER) IS
  BEGIN
    ATRSeq.delete;
    IF Seq IS NOT NULL
       AND Seq <> 0 THEN
      ATRseq(1).SEQ := Seq;
      LVL := 1;
    END IF;
  END ToSeq;
  --
  PROCEDURE Temporary_ERROR_LOG(p_call      VARCHAR2,
                                p_ParEvSeq  NUMBER,
                                p_temp_clob CLOB) IS
    N NUMBER;
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    N := I2Journal(p_Par          => p_ParEvSeq,
                   p_Event        => -500,
                   p_OriginatorID => p_call || ' ERROR',
                   p_Details      => p_temp_clob);
    /*
    INSERT INTO JOURNALEXTENSION
    VALUES
      (N, p_temp_clob);
    */
    COMMIT;
  END;
  --
  PROCEDURE ERROR_LOG_AUTONOMOUS(p_OriginatorID VARCHAR2,
                                 p_Details      CLOB,
                                 p_ParEvSeq     NUMBER DEFAULT NULL,
                                 p_EventID      NUMBER DEFAULT -500) IS
    N NUMBER;
    PRAGMA AUTONOMOUS_TRANSACTION;
  BEGIN
    N := I2Journal(p_Par          => p_ParEvSeq,
                   p_Event        => p_EventID,
                   p_OriginatorID => 'ERROR ' || p_OriginatorID,
                   p_Details      => p_Details);
    COMMIT;
  END;
  --
  PROCEDURE SetParEvSeq(pSeq    IN Journal.Seq%TYPE,
                        pPackId IN ClientTransactionPackages.PackageId%TYPE DEFAULT NULL) IS
  BEGIN
    IF pPackId IS NOT NULL
       AND (pSeq IS NULL OR pSeq = 0) THEN
      SELECT ParentEventSeq
        INTO ParEvSeq
        FROM ClientTransactionPackages
       WHERE PackageId = pPackId;
    ELSE
      ParEvSeq := pSeq;
    END IF;
    IF ParEvSeq IS NOT NULL
       AND ParEvSeq <> 0 THEN
      SELECT MAX(ROWID)
        INTO RIDBuf
        FROM Journal
       WHERE Seq = ParEvSeq; -- max je koli NO_DATA_FOUND
      ATRSeq.delete;
      ATRseq(1).SEQ := ParEvSeq;
      ATRseq(1).RID := RIDBuf;
      Lvl := 1;
    END IF;
  END SetParEvSeq;
  --
BEGIN
  FOR x IN (SELECT /*+ RESULT_CACHE */
             e.EventID, e.ExtendedJournalingCode
              FROM EventTypes e
             WHERE e.ExtendedJournalingCode IS NOT NULL) LOOP
    tExtendedJournalingCode(x.EventID) := x.ExtendedJournalingCode;
  END LOOP;
END JOURNAL001;
