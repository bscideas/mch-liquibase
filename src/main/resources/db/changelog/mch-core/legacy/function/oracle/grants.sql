
  CREATE OR REPLACE EDITIONABLE FUNCTION "GRANTS" (aChannelId       IN Channels.ChannelId%TYPE,                -- ak je kanal NULL, prava sa netestuju (volanie zo spravcovskej aplikacie)
                 aClientId        IN Clients.ClientId%TYPE,                  -- vzdy vyplnene
                 aUserId          IN Users.UserId%TYPE,                      -- ak nie je vyplnene, nejedna sa o prihlasenie cez konkretne zariadenie - testuje sa len podla ClientRights, nie podla profilu (v tom pripade musi byt vyplnene ClientId)
                 aAccessTypeId    IN OperationAccessTypes.AccessTypeId%TYPE, -- typ pristupu - ak existuje vyssie pravo ako view, tj. create, edit, delete, ... nie je nutne aby existoval zaznam s pravom view a test prav uspesne prejde
                 aOperId          IN OperationTypes.OperId%TYPE,             -- prava sa testuju iba ak je operacia AuthorReq
                 aAccountId       IN Accounts.AccountId%TYPE,                -- ak nie je vyplnene testuje sa bez prepojenia profilu na ucet, ak je vyplnene, testuje sa iba ak operacia je AccountSensitive
                 aCheckAnyChannel IN PLS_INTEGER default 0)                  -- ak je poslane 1, tak bez ohledu na aChannelId sa kontroluje existance prava alespon do jednoho (libovolneho) kanalu
RETURN PLS_INTEGER
AUTHID DEFINER
RESULT_CACHE RELIES_ON (RightProfileDefinitions,ClientUsers,ClientUserChannelProfiles,RightAssignClientAccounts)
IS
------------------
-- $release: 6663 $
-- $version: 8.0.14.0 $
------------------
--
-- MODIFICATION HISTORY
--
-- Person       Date       Comments
-- ------------ ---------- ---------------------------------------------------------
-- Ilenin       24.06.2014 UDEBS RS
-- Bocak        23.01.2015 Added check if operation ID exists
-- Pataky       10.03.2015 Added multicurrency support
-- Pataky       27.03.2015 Exception for operid 501 supressed for SLSP
-- Pataky       31.03.2015 Removed exception for operid 501 - 20009183-2059
-- Bocak        22.06.2015 Extend queries by joining on ClientRightProfiles (20009183-3365)
-- Bocak        25.06.2015 Check ClientUsers.StatusId=1 (20009183-3486)
-- Pataky       10.09.2015 Added relies on clause to flush cache to keep rights consistent after DML
-- Pataky       15.04.2016 Changes to support GAAS, removed user logins from UDEBS - 20009203-4607
-- Pataky       07.10.2016 Added AvailableFor columns checking - 20009213-2051
-- Pataky       20.01.2017 Added aCheckAnyChannel parameter - 20009213-1912
-- Pataky       24.01.2017 Changed aCheckAnyChannel to PLS_INTEGER - 20009213-1912
-- Emmer        23.05.2017 Rewrite acc sensitive select + hint FIRST_ROWS (MYG-4841) - P20009246-337
-- Pataky       03.05.2018 Added check of OperationTypes.StatusID column - P20009317-19
-- --------------------------------------------------------------------------------
  vPoc                  PLS_INTEGER := 0; -- standardne nepovolime nic
  vAlwaysSuppAccessVIEW GlobalSystemParameter.Value%TYPE;
  vCheckChannelStatus   GlobalSystemParameter.Value%TYPE := '1';
  lnUserId              Users.UserId%type := null;
BEGIN
  SELECT 1 INTO vPoc FROM OperationTypes WHERE OperId=aOperId and nvl(StatusID,1)=1 and nvl(AvailableForClient,1)=1; -- check inOperID, StatusID and AvailableForClient flag
  vPoc:=0;
  --
  IF reftab_ot.AuthorReq(aOperId,0) = 1 AND (aChannelId IS NOT NULL or nvl(aCheckAnyChannel,0)=1) THEN -- ak sa maju pre operaciu overovat prava, v spravcovskych aplikaciach sa nepozaduje ziadne pravo
    IF aUserId IS NULL THEN -- testuje sa len klient, nejedna sa o prihlasenie cez konkretne zariadenie
      vCheckChannelStatus := reftab_gsp('AlwaysCheckChannelStatus',0,'1');
      SELECT 1
        INTO vPoc
        FROM RightTypes rt,
             ClientRights cr,
             ClientChannelProfiles cchp
       WHERE rt.OperId = aOperId
         AND (rt.ChannelId = aChannelId or nvl(aCheckAnyChannel,0)=1)
         AND ((aAccessTypeId=1 AND rt.AccessTypeId>=1) OR (rt.AccessTypeId = aAccessTypeId))
         AND rt.RightId = cr.RightId
         AND cr.ClientId = aClientId
         AND cr.ClientId = cchp.ClientId
         AND (cchp.StatusId = 1 OR vCheckChannelStatus='0');
    ELSE -- aUserId IS NOT NULL
      IF aAccountId IS NULL OR reftab_ot.AccountSensitive(aOperId)=0 THEN
         vCheckChannelStatus := reftab_gsp('AlwaysCheckChannelStatus',0,'1');
          SELECT /*+FIRST_ROWS*/ 1
            INTO vPoc
            FROM RightTypes rt,
                 RightProfileDefinitions rpd,
                 ClientRights cr,
                 ClientUserRightProfiles curp,
                 ClientChannelProfiles cchp,
                 ClientUserChannelProfiles cuchp,
                 ClientUsers cu,
                 ClientRightProfiles crp
           WHERE -- joins
                 rt.RightId = cr.RightId
             AND rpd.ClientRightId = cr.ClientRightId
             AND rpd.RightProfileId = curp.RightProfileId
             AND curp.clientusersid = cu.clientusersid
             AND cuchp.clientusersid = cu.clientusersid
             AND cu.clientid = cr.ClientId
             AND cchp.ClientId = cr.ClientId
                 -- filters
             AND rt.OperId = aOperId
             AND (rt.ChannelId = aChannelId or nvl(aCheckAnyChannel,0)=1)
             AND ((aAccessTypeId = 1 AND rt.AccessTypeId >= 1) OR (rt.AccessTypeId = aAccessTypeId))
             AND cu.UserId = aUserId
             AND cu.clientid = aClientId
             AND (cchp.StatusId = 1  or vCheckChannelStatus='0')
             AND (cuchp.StatusId = 1  or vCheckChannelStatus='0')
             AND crp.RightProfileId=rpd.RightProfileId
             AND crp.ClientId=cchp.ClientId
             AND crp.StatusId=1
             AND cu.StatusId=1;
       ELSE
        vAlwaysSuppAccessVIEW := reftab_gsp('AlwaysSuppAccessVIEW');
          vCheckChannelStatus := reftab_gsp('AlwaysCheckChannelStatus',0,'1');
          SELECT /*+FIRST_ROWS*/ 1
            INTO vPoc
            FROM ClientUsers cu
            JOIN ClientAccounts ca ON ca.AccountId = aAccountId AND ca.clientid = cu.clientid AND (ca.StatusId = 1 OR (vAlwaysSuppAccessVIEW = '1' AND aAccessTypeId = 1))
            JOIN ClientUserRightProfiles curp ON curp.clientusersid = cu.clientusersid
            JOIN ClientRightProfiles crp ON crp.RightProfileId=curp.RightProfileId AND crp.ClientId=cu.clientid AND crp.StatusId=1
            JOIN ClientUserChannelProfiles cuchp ON cuchp.clientusersid = cu.clientusersid AND (cuchp.StatusId = 1 or vCheckChannelStatus = '0')
            JOIN ClientRights cr ON cuchp.ClientId = cr.ClientId
            JOIN RightTypes rt ON rt.RightId = cr.RightId AND rt.OperId = aOperId
                                  AND ((aAccessTypeId = 1 AND rt.AccessTypeId >= 1) OR (rt.AccessTypeId = aAccessTypeId))
                                  AND (rt.ChannelId = aChannelId or nvl(aCheckAnyChannel,0)=1)
            JOIN RightAssignClientAccounts raca ON raca.ClientRightId = cr.ClientRightId AND raca.RightProfileId = curp.RightProfileId AND raca.AccountAccessId = ca.AccountAccessId
           WHERE cu.clientid = aClientId
             AND cu.UserId = aUserId
             AND cu.StatusId = 1;
      END IF;
    END IF;
  ELSIF reftab_ot.AuthorReq(aOperId,0) = 1 AND reftab_ot.AccountSensitive(aOperId,0) = 0 AND aChannelId IS NULL THEN -- ak sa maju pre operaciu overovat prava, v spravcovskych aplikaciach sa nepozaduje ziadne pravo
    IF aUserId IS NULL THEN -- testuje sa len klient, nejedna sa o prihlasenie cez konkretne zariadenie
      SELECT 1
        INTO vPoc
        FROM RightTypes rt,
             ClientRights cr
       WHERE rt.OperId = aOperId
         AND ((aAccessTypeId=1 AND rt.AccessTypeId>=1) OR (rt.AccessTypeId = aAccessTypeId))
         AND rt.RightId = cr.RightId
         AND cr.ClientId = aClientId;
    END IF;
  ELSIF reftab_ot.AuthorReq(aOperId,0) = 0 THEN
    vPoc := 1;
  END IF;
  RETURN vPoc;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 0;
  WHEN TOO_MANY_ROWS THEN
    RETURN 1;
END grants;
