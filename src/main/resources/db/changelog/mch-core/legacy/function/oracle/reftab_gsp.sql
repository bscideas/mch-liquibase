
  CREATE OR REPLACE EDITIONABLE FUNCTION "REFTAB_GSP" (
    ivParam    IN GlobalSystemParameter.ParamName%TYPE,
    inBranchID IN NUMBER default 0,
    ivDefault  IN GlobalSystemParameter.Value%TYPE DEFAULT NULL)
RETURN VARCHAR2 RESULT_CACHE RELIES_ON (GLOBALSYSTEMPARAMETER) IS
-------------------
-- $release: 5422 $
-- $version: 8.0.3.1 $
-------------------
  lvValue           GlobalSystemParameter.Value%TYPE;
BEGIN
  SELECT Value  INTO lvValue FROM GlobalSystemParameter
   WHERE Upper(ParamName) = Upper(ivParam);
  RETURN nvl(lvValue,ivDefault);
EXCEPTION WHEN NO_DATA_FOUND THEN
  RETURN ivDefault;
END;
