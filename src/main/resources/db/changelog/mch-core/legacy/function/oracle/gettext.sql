
  CREATE OR REPLACE EDITIONABLE FUNCTION "GETTEXT"
  -- Funkcia vrati text zodpovedajuci text z tabulky LanguageTexts_T
  (
  aTextID       NUMBER,                     -- TextID
  aLangID       VARCHAR2,                   -- LangID
  aPurposeID    VARCHAR2 DEFAULT 'DEFAULT'  -- PurposeID  - parameter specifikuje ucel (DFAULT - povodny text pre prislusne TID, LTD);
  ) RETURN VARCHAR2 IS
  ------------------
  -- $release: 009 $
  -- $version: 8.0.0.0 $
  ------------------
  vOutText        LanguageTexts.Text%TYPE;
  vSubstPurposeID TextPurposeTypes.SubstPurposeID%TYPE := null;
  --
BEGIN
  BEGIN
    SELECT Text INTO vOutText
      FROM LanguageTexts
     WHERE PurposeID = aPurposeID
       AND TextID = aTextID
       AND LangID = aLangID;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      BEGIN
        SELECT Text INTO vOutText
          FROM LanguageTexts
         WHERE PurposeID = aPurposeID
           AND TextID = aTextID
           AND LangID = (SELECT SubstLangID FROM SupportedLang WHERE LangID=aLangID);
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          BEGIN
            SELECT SubstPurposeID INTO vSubstPurposeID
              FROM TextPurposeTypes
             WHERE PurposeID != SubstPurposeID
               AND PurposeID = aPurposeID;
            BEGIN
              SELECT Text INTO vOutText
                FROM LanguageTexts
               WHERE PurposeID = vSubstPurposeID
                 AND TextID = aTextID
                 AND LangID = aLangID;
            EXCEPTION
              WHEN NO_DATA_FOUND THEN
                BEGIN
                  SELECT Text INTO vOutText
                    FROM LanguageTexts
                   WHERE PurposeID = vSubstPurposeID
                     AND TextID = aTextID
                     AND LangID = (SELECT SubstLangID FROM SupportedLang WHERE LangID=aLangID);
                EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                    vOutText := TO_CHAR(NULL);
                END;
            END;
          EXCEPTION
            WHEN NO_DATA_FOUND THEN
              vOutText := TO_CHAR(NULL);
          END;
      END;
  END;
  RETURN vOutText;
END;
