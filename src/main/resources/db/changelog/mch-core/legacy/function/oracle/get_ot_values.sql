
  CREATE OR REPLACE EDITIONABLE FUNCTION "GET_OT_VALUES" (
    ivAttrName in Varchar2,
    inOperid   in OperationTypes.OperID%Type,
    inBranchID in OperPaymentSystems.BranchID%Type default 0, -- pro PaymentSystemID
    ivDefVal   in Varchar2 default null,
    inRefTab   in Number default 0                            -- 0 - select z OT, 1 - volani REFTAB_OT.xxx
) RETURN VARCHAR2 IS
------------------
-- $release: 009 $
-- $version: 8.0.0.0 $
------------------
--
-- Purpose: Vraci hodnotu predaneho sloupce z tabulky OPERTIONTYPES
--
-- MODIFICATION HISTORY
-- Person      Date       Comments
-- ----------- ---------- ---------------------
-- Ilenin      07.07.2014 UDEBS RS
--------------------------------------------------------
  lnPBID Branches.BranchID%TYPE:=inBranchID;
  v_Result varchar2(1024 CHAR);
  cmd      varchar2(1024 CHAR);
BEGIN
  if inRefTab = 0 then
    if upper(ivAttrName) = 'PAYMENTSYSTEMID' then
      cmd:='select '||ivAttrName||' from OperPaymentSystems where operid=:opid and branchid=:bid';
      execute immediate cmd into v_Result using inOperID,lnPBID;
    else
      cmd:='select '||ivAttrName||' from OperationTypes where operid=:opid';
      execute immediate cmd into v_Result using inOperID;
    end if;
  elsif upper(ivAttrName) = 'OPERID' then
    cmd:='select reftab_ot.existing(:opid) from dual';
    execute immediate cmd into v_Result using inOperID;
  elsif upper(ivAttrName) = 'PAYMENTSYSTEMID' then
    cmd:='select reftab_ot.'||ivAttrName||'(:opid,:bid,:defval) from dual';
    execute immediate cmd into v_Result using inOperID,lnPBID,ivDefVal;
  else
    cmd:='select reftab_ot.'||ivAttrName||'(:opid,:defval) from dual';
    execute immediate cmd into v_Result using inOperID,ivDefVal;
  end if;
  return v_Result;
EXCEPTION WHEN OTHERS THEN -- pro inRefTab=0 a ivAttrName='PAYMENTSYSTEMID'
  LOOP
    BEGIN
      Select ParentBranchID into lnPBID from Branches
       where BranchID=lnPBID;
      execute immediate cmd into v_Result using inOperID,lnPBID;
      return v_Result;
    EXCEPTION
      WHEN OTHERS THEN
        IF lnPBID IS NULL THEN
          RETURN ivDefVal; -- kdyz se nenajde BranchID v Branches;
        ELSE
          NULL;
        END IF;
    END;
  END LOOP;
END Get_ot_values;
