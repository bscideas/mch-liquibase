
  CREATE OR REPLACE EDITIONABLE FUNCTION "GET_CTST_VALUES" (
    ivAttrName  in Varchar2,
    inStatusID  in ClientTransactionStatusTypes.StatusID%Type,
    inChannelId IN Channels.channelid%TYPE DEFAULT NULL,
    inOperId    IN Operationtypes.operid%TYPE DEFAULT NULL,
    ivDefVal    in Varchar2 default null,
    inRefTab    in Number default 0
) RETURN VARCHAR2 IS
------------------
-- $release: 440 $
-- $version: 8.0.1.0 $
------------------
-- $Header: /Oracle/UDEBS_RS/UDEBSTS.GET_CTST_VALUES.FNC 3     1.04.03 16:24 Ilenin $
-- Purpose:
--
-- MODIFICATION HISTORY
-- Person      Date       Comments
-- ----------- ---------- ---------------------
-- Ilenin      07.07.2014 UDEBS RS
-- Ilenin      02.12.2014 Override AllowSign
--------------------------------------------------------
  v_Result varchar2(1024 CHAR);
  cmd      varchar2(1024 CHAR);
BEGIN
  if inRefTab = 0 THEN
    IF upper(ivAttrName) IN('ALLOWDEL','ALLOWCANCEL','ALLOWEDIT','REPORTTOCLIENT','ALLOWSIGN') THEN
      cmd:='DECLARE'||CHR(10)||
             'CURSOR c_sr IS SELECT '|| ivAttrName || ' FROM StatusReporting WHERE StatusId = :StatusId AND'||CHR(10)||
              '(NVL(ChannelId,:ChannelId) = :ChannelId OR (ChannelId IS NULL AND :ChannelId IS NULL)) AND'||CHR(10)||
              '(NVL(OperId,:OperId) = :OperId OR (OperId IS NULL AND :OperId IS NULL)) ORDER BY OperId, ChannelId;'||CHR(10)||
           'BEGIN'||CHR(10)||
             'OPEN c_sr;'||CHR(10)||
             'FETCH c_sr INTO :Ret;'||CHR(10)||
             'IF c_sr%NOTFOUND THEN'||CHR(10)||
               'BEGIN'||CHR(10)||
                 'Select '|| ivAttrName || ' into :Ret from ClientTransactionStatusTypes where StatusId = :StatusId;'||CHR(10)||
               'EXCEPTION WHEN OTHERS THEN'||CHR(10)||
                 ':Ret := :DefVal;'||CHR(10)||
               'END;'||CHR(10)||
             'END IF;'||CHR(10)||
             'CLOSE c_sr;'||CHR(10)||
           'END;';
      execute immediate cmd using in inStatusID, inChannelId, inOperId, out v_Result, in ivDefval;
    ELSE
      cmd:='begin select '||ivAttrName||' into :ret from ClientTransactionStatusTypes where StatusID=:StatID;'||CHR(10)||
           'exception when no_data_found then :ret := :defval; end;';
      execute immediate cmd USING out v_Result, in inStatusID, ivDefval;
    END IF;
  ELSE
    IF upper(ivAttrName) IN('ALLOWDEL','ALLOWCANCEL','ALLOWEDIT','REPORTTOCLIENT','ALLOWSIGN') THEN
      cmd:='begin :ret:= reftab_ctst.'||ivAttrName||'(:StatID,:Channel,:OperId,:defval); end;';
      execute immediate cmd using out v_result, in inStatusID, inChannelId, inOperId, ivDefval;
    ELSE
      cmd:='begin :ret:= reftab_ctst.'||ivAttrName||'(:StatID,:defval); end;';
      execute immediate cmd using out v_Result, in inStatusID, ivDefval;
    END IF;
  end if;
  return v_Result;
END Get_CTST_values;
