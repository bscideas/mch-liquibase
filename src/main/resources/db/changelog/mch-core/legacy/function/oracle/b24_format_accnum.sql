
  CREATE OR REPLACE EDITIONABLE FUNCTION "B24_FORMAT_ACCNUM" (ivAccNum in Accounts.Accnum%type)
return varchar2
is
------------------
-- $release: 605 $
-- $version: 8.0.0.2 $
------------------
--
-- Purpose: Format account number (ACCOUNTS.ACCNUM = ACC1) for B24
--
-- MODIFICATION HISTORY
-- Person      Date         Comments
-- ---------   -----------  ------------------------------------------
-- Ilenin      12.01.2015   UDEBS RS
--
--------------------------------------------------------------------
  lvAccNum       Accounts.Accnum%type;
  lvAccPrefix    Accounts.Accnum%type;
  lvAcc          Accounts.Accnum%type;
begin
  if instr(ivAccNum, '-') > 0 then
    lvAccPrefix := ltrim(substr(ivAccNum, 1, instr(ivAccNum, '-')-1), '0');
    lvAcc := ltrim(substr(ivAccNum, instr(ivAccNum, '-')+1, length(ivAccNum)),'0');
  else
    lvAccPrefix := null;
    lvAcc := ltrim(ivAccNum,'0');
  end if;
  --
  if lvAccPrefix is not null then
    lvAccNum := substr(lvAccPrefix,1,6)||'-'||substr(lvAcc,1,10);
  else
    lvAccNum := substr(lvAcc,1,10);
  end if;
  --
  return lvAccNum;
end;
