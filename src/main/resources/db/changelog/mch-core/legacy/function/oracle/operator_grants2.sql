
  CREATE OR REPLACE EDITIONABLE FUNCTION "OPERATOR_GRANTS2" (inChannelId    IN Channels.ChannelId%TYPE, -- channel which is used to realize the operation, mandatory (for admin API it is always "system administration" channel)
                 inClientId              IN Clients.ClientId%TYPE, -- the client under which the operation is to be realized, optional (for admin API it is always "bank" service client - if not filled then selected from GSP parameter)
                 inUserId                IN Users.UserId%TYPE, -- client user ID
                 inOperatorId            IN Users.UserId%TYPE,  -- operator user ID, mandatory
                 inAccessTypeId          IN OperationAccessTypes.AccessTypeId%TYPE,  -- operation access type to test for operator, mandatory
                 inOperId                IN OperationTypes.OperId%TYPE,   -- operation ID, mandatory
                 ivOperatorRoles         IN VARCHAR2,    -- comma delimited list of names of operator roles - i.e., right profiles assigned to operator
                 ivMessageCategoryId     IN VARCHAR2 DEFAULT NULL,    -- ID of checked message category - i.e., whether a right for this msg category exists in some of client operator profiles, optional
                 inMessageCategoryCheck  IN NUMBER DEFAULT NULL,      -- flag if message category assignment should be checked, mandatory
                 inTargetClientCategID   IN NUMBER DEFAULT NULL,      -- target client ID - client whose data will be modified or accessed, optional, default is taken from GSP parameter; if used, target client's segment will be checked against role segments
                 inClientCategoryCheck   IN NUMBER DEFAULT NULL       -- flag if target client segment assignment should be checked - optional, default is taken from GSP parameter
                 )
RETURN PLS_INTEGER
AUTHID DEFINER
RESULT_CACHE RELIES_ON (RightProfileDefinitions,RightProfileClientCategories,RightProfileMessageCategories)
IS
------------------
-- $release: 6664 $
-- $version: 8.0.4.0 $
------------------
--
-- MODIFICATION HISTORY
--
-- Person       Date       Comments
-- ------------ ---------- ---------------------------------------------------------
-- Pataky       31.01.2017 Initial version, based on OPERATOR_GRANTS function, only TargetClientID changed to TargetClientCategID (MYG-2294, MYG-2317, MYG-2122, MYG-2280) - 20009203-6996
-- Pataky       12.04.2017 Added check on channel availability for inClientCategoryCheck parameter (MYG-3454) - 20009246-337
-- Pataky       19.07.2017 Updated check on channel availability for inClientCategoryCheck parameter - 20009271-178
-- Pataky       03.05.2018 Added check of OperationTypes.StatusID column - P20009317-19
-- --------------------------------------------------------------------------------
--
  vPoc                  PLS_INTEGER := 0;
  vResult               PLS_INTEGER := 0; -- standardne nepovolime nic
  lnUserId              Users.UserId%type := null;
  lnClientID            Clients.ClientID%type := inClientID;
  lnChannelId           Channels.ChannelId%TYPE;
  lnRightProfileID      ClientRightProfiles.rightprofileid%TYPE;
  lnRightProfileTypeID  ClientRightProfiles.rightprofiletypeid%TYPE;
  row_count             NUMBER := 0;
  lbRightsSet           BOOLEAN := FALSE;
  lbSegmentsSet         BOOLEAN := FALSE;
  lnMessageCategoryCheck pls_integer:=inMessageCategoryCheck;
  lnClientCategoryCheck pls_integer:=inClientCategoryCheck;
BEGIN
  lnChannelId := 11; -- default 11 - System Administration channel
  IF inChannelId is not null THEN
    lnChannelId := inChannelId;
  END IF;
  SELECT 1 INTO vPoc FROM OperationTypes WHERE OperId=inOperId and nvl(StatusID,1)=1 and nvl(AvailableForOperator,1)=1; -- check inOperID, StatusID and AvailableForOperator flag
  IF inClientId is not null then
    SELECT ClientID INTO lnClientID FROM Clients -- check ClientID against GSP TECHNICAL_BANK_CLIENT_ID
      WHERE ClientID=inClientId AND inClientId=(select to_number(value) from GlobalSystemParameter where paramname='TECHNICAL_BANK_CLIENT_ID');
  else
    select to_number(value) INTO lnClientID from GlobalSystemParameter where paramname='TECHNICAL_BANK_CLIENT_ID';
  end if;
  if lnMessageCategoryCheck is null then
    begin
      select nvl(to_number(value),0) INTO lnMessageCategoryCheck from GlobalSystemParameter where paramname='OPERATOR_MESSAGE_CATEGORIES_FILTERING';
    exception when no_data_found then
      lnMessageCategoryCheck:=0;
    end;
  end if;
  begin
    select nvl(to_number(value),lnClientCategoryCheck) INTO lnClientCategoryCheck from GlobalSystemParameter
      where paramname='USE_CLIENT_CATEG_FOR_OPERATOR_AUTH_IN_ONBEHALF' and lnChannelId=41; -- OnBehalf channel exception
  exception when no_data_found then
    null;
  end;
  if lnClientCategoryCheck is null or lnClientCategoryCheck = 1 then
    begin
      select nvl(to_number(value),nvl(lnClientCategoryCheck,0)) INTO lnClientCategoryCheck from GlobalSystemParameter where paramname='OPERATOR_CLIENT_CATEGORIES_FILTERING';
    exception when no_data_found then
      lnClientCategoryCheck:=0;
    end;
  end if;
  vResult := 0;
DECLARE
  CURSOR c1 IS
    select regexp_substr(ivOperatorRoles,'[^,]+', 1, level) rpname from dual
      connect by regexp_substr(ivOperatorRoles, '[^,]+', 1, level) is not null;
    r1 c1%ROWTYPE;
    vRows NUMBER := 0;
  BEGIN
    OPEN c1;
    LOOP
      FETCH c1 INTO r1;
      row_count := row_count + 1;
      EXIT WHEN c1%NOTFOUND;
      BEGIN
       SELECT distinct crp.rightprofileid, nvl(crp.rightprofiletypeid,7) INTO lnRightProfileID, lnRightProfileTypeID
       FROM ClientRightProfiles crp
        WHERE crp.rightprofilename = r1.rpname AND crp.clientid = lnClientId AND crp.statusid = 1;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          CONTINUE;
      END;
      -- if profile type id is 1 or 7, check rights
      if lnRightProfileTypeID in (1,7) then
        BEGIN
         SELECT  count(rt.RightId) INTO vPoc
         FROM RightTypes rt, ClientRights cr, RightProfileDefinitions rpd
          WHERE cr.clientid = lnClientId
           AND rt.OperId = inOperId
           AND rt.ChannelId = lnChannelId
           AND ((inAccessTypeId=1 AND rt.AccessTypeId>=1) OR (rt.AccessTypeId = inAccessTypeId))
           AND rt.RightId = cr.RightId
           AND cr.clientrightid = rpd.clientrightid
           AND rpd.rightprofileid = lnRightProfileID;
         -- for profile type 7, check also segments and message categories
         if vPoc > 0 then
          if lnRightProfileTypeID = 7 then
           IF lnClientCategoryCheck = 1 AND inTargetClientCategID IS NOT NULL THEN
             select count(1) into vPoc
              from rightprofileclientcategories where rightprofileid=lnRightProfileID
                and clientCategID in (select clientcategid from clientcategories
                    connect by clientcategid = prior parentclientcategid
                    start with clientcategid = inTargetClientCategID)
                and rownum <=1;
             IF vPoc > 0 THEN vResult := 1; END IF;
           ELSE
              vResult := 1;
           END IF;
           IF vResult = 1 THEN
             IF lnMessageCategoryCheck = 1 AND ivMessageCategoryId IS NOT NULL THEN
                select count(1) into vPoc
                from RightProfileMessageCategories rpmc
                where rpmc.messagecategoryid = ivMessageCategoryId
                  and rpmc.rightprofileid = lnRightProfileID;
                IF vPoc < 1 THEN vResult := 0; END IF;
             END IF;
           END IF;
           IF vResult = 1 THEN
             lbRightsSet:=true;
             lbSegmentsSet:=true;
           END IF;
          else
           lbRightsSet:=true;
          end if;
         else
          CONTINUE;
         end if;
        EXCEPTION
          WHEN NO_DATA_FOUND THEN
            CONTINUE;
        END;
      end if;
      IF lnClientCategoryCheck = 1 AND inTargetClientCategID IS NOT NULL THEN
        -- if profile type id is 2, check segments only
        if lnRightProfileTypeID in (2) then
           select count(1) into vPoc
            from rightprofileclientcategories where rightprofileid=lnRightProfileID
              and clientCategID in (select clientcategid from clientcategories
                  connect by clientcategid = prior parentclientcategid
                  start with clientcategid = inTargetClientCategID)
              and rownum <=1;
            IF vPoc > 0 THEN lbSegmentsSet:=true; END IF;
        end if;
      ELSE
        lbSegmentsSet:=true;
      END IF;
      --
      EXIT WHEN lbRightsSet and lbSegmentsSet;
    END LOOP;
    CLOSE c1;
  EXCEPTION
    WHEN OTHERS THEN
      --vResult := 0; -- ?? 1 ?
      lbRightsSet:=false;
      lbSegmentsSet:=false;
      IF c1%ISOPEN THEN
        CLOSE c1;
      END IF;
  END;
  --
  IF lbRightsSet and lbSegmentsSet THEN
    RETURN 1;
  ELSE
    RETURN 0;
  END IF;
EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RETURN 0;
  WHEN TOO_MANY_ROWS THEN
    RETURN 0; --?? 1 ?
  WHEN OTHERS THEN
    RETURN 0;
END OPERATOR_GRANTS2;
