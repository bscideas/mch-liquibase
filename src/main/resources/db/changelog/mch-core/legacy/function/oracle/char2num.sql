
  CREATE OR REPLACE EDITIONABLE FUNCTION "CHAR2NUM"
  (A Varchar2,
   inDefaultVal IN NUMBER DEFAULT NULL,
   ivFormat IN VARCHAR2 DEFAULT NULL) RETURN NUMBER DETERMINISTIC
IS
-------------------
-- $release: 009 $
-- $version: 8.0.0.0 $
-------------------
-- MODIFICATION HISTORY
-- Person      Date       Comments
-- ----------- ---------- ---------------------
-- Ilenin      07.07.2014 UDEBS RS
--------------------------------------------------------
   N Number;
BEGIN
  BEGIN
    N:=TO_NUMBER('9.9');
    IF ivFormat IS NULL THEN
      Return TO_NUMBER(REPLACE(A,',','.'));
    ELSE
      Return TO_NUMBER(REPLACE(A,',','.'), ivFormat);
    END IF;
  EXCEPTION
    When OTHERS then
      IF ivFormat IS NULL THEN
        Return TO_NUMBER(REPLACE(A,'.',','));
      ELSE
        Return TO_NUMBER(REPLACE(A,',','.'), ivFormat);
      END IF;
  END;
EXCEPTION
  WHEN others THEN
    IF sqlcode = -6502 AND inDefaultVal IS NOT NULL THEN
      -- v pripade chyby konverzie retazca na cislo, ak je zadana default hodnota, vratim ju
      RETURN inDefaultVal;
    ELSE
      -- inak raisnem chybu
      RAISE;
    END IF;
END;
