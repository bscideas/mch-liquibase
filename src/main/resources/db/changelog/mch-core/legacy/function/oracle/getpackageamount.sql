
  CREATE OR REPLACE EDITIONABLE FUNCTION "GETPACKAGEAMOUNT" (
    otSubtotals OUT Subtotal_tab,
    inPackageId IN  ClientTransactionPackages.PackageId%TYPE,
    inOperId    IN  ClientTransactionPackages.OperId%TYPE DEFAULT NULL,
    inStatusId  IN  ClientTransactionPackages.StatusId%TYPE DEFAULT NULL,
    inSeqNum    IN  Transactions.SeqNum%TYPE DEFAULT NULL)
RETURN NUMBER
IS
------------------
-- $release: 5028 $
-- $version: 8.0.6.0 $
------------------
-- $Header: /Oracle/UDEBS_XPE/UDEBSXPE.GETPACKAGEAMOUNT.FNC 2     22.04.14 16:47 Pataky $
-- posledni patch z UDEBS - 2506
------------------
--
-- Purpose:
-- Funkce pro zadanou package spocita celkovou castku transakci ve statusu p_nStatusId. Pokud neni zadany status,
-- jsou do souctu zahrnuty vsechny transakce.
--
-- Funkce prevadi castky transakci do meny uctu. Mena debetniho uctu je ulozena v prislusne tabulce spolecne s castkou
-- a menou castky. Predpokladem je vyplneni meny debetniho uctu v tabulce.
-- Prevod castky je provaden pomoci funkce GetChangedAmout.
--
-- MODIFICATION HISTORY
-- Person      Date       Comments
-- ----------- ---------- ---------------------
-- Ilenin      07.07.2014 UDEBS RS
-- Pataky      20.07.2015 For RBCZ collection permission LimitAmount changed to MaxAmount - 20009203-825
-- Bocak       12.10.2015 return 0 - SLSP only
-- Pataky      09.11.2015 Package amount can be recounted into local currency, based on parameter (because of multicurrency accounts)
-- Simunek     17.02.2016 SQL optimization (20009183-4886)
-- Pataky      09.03.2017 Added different xpath for currency for cash withdrawals (MYG-3224, MYG-3436) - 20009203-6741
-- Furdik      10.04.2017 return 0 for SLSP, TATR - 20009239-4035
--------------------------------------------------------
  lnPackageSum       NUMBER;
  lnOperId           ClientTransactionPackages.OperId%TYPE;
  lvAccoutCurrencyId Accounts.CurrencyId%TYPE;
  lnBranchID         Branches.BranchID%TYPE;
  lvFromCurr         varchar2(3 CHAR);
  lnAmount           number;
  lnBatch            pls_integer;
  lnAccountId        Accounts.accountid%TYPE;
  lvStructCode       Operationtypes.structcode%TYPE;
  lvXPathAmount      GDM_Parser.xpath%TYPE := GDM_PARSER001.getxpath('Amount');
  lvCollPerXPathAmount GDM_Parser.xpath%TYPE;
  lvXPathCurrId      GDM_Parser.xpath%TYPE := GDM_PARSER001.getxpath('CurrencyId');
  --
  lnPackageId        ClientTransactionPackages.packageId%TYPE := null;
  lvLocalBankCode    varchar2(20);
  lvUseLocalCurrencyInCTP varchar2(10);
  lvLocalCurrency    varchar2(5) := Reftab_GSP('LOCALCURRENCY',0,'X');
BEGIN
  lvLocalBankCode := Reftab_GSP('LOCALBANKCODEALPHA',0,'X');
  lvUseLocalCurrencyInCTP := nvl(reftab_gsp('REQUESTS.USE_LOCAL_CURRENCY_IN_CTP',0,'0'),'0');
  IF lvLocalBankCode IN ('SLSP','TATR') THEN
    otSubtotals:=Subtotal_tab(); otSubTotals.extend;
    otSubtotals(1):=Subtotal_obj('',0,0,0);
    RETURN 0;
  END IF;
  -- kontrola existencie package
  SELECT packageId
    INTO lnPackageId
    FROM ClientTransactionPackages
   WHERE packageId = inPackageId;
  IF lnPackageId IS NULL THEN
    im_error.AddError( 2113559,null,'/unspecified/GetPackageAmount/PackageId',inPackageID);
    RETURN 0;
  END IF;
  --
  SELECT nvl(inOperId,CTP.OperID),
         decode(lvUseLocalCurrencyInCTP,'1',lvLocalCurrency,A.CurrencyID),
         A.BranchID,
         get_OT_values('Batch',nvl(inOperId,CTP.OperID)),
         get_OT_values('StructCode',NVL(inOperId,CTP.operid))
    INTO lnOperId,
         lvAccoutCurrencyId,
         lnBranchID,
         lnBatch,
         lvStructCode
    FROM ClientTransactionPackages CTP,
         Accounts A
   WHERE PackageId = inPackageId
     AND A.AccountID(+)=CTP.AccountID; --outer join kvuli stornu(oper 12), ktere neni accountsensitive a nema vyplneny ucet
  IF lvStructCode = 'FPO' THEN
    lvXPathAmount := GDM_PARSER001.getxpath('PayAmount');
  END IF;
  IF lvStructCode = 'CASHWITHDR' THEN
    lvXPathCurrId := GDM_PARSER001.getxpath('WithdrawalCurrencyId');
  END IF;
  if lvLocalBankCode = 'RBCZ' then
    lvCollPerXPathAmount := GDM_PARSER001.getxpath('MaxAmount');
  else
    lvCollPerXPathAmount := GDM_PARSER001.getxpath('LimitAmount');
  end if;
  IF lvStructCode in ('COLLPER_A','COLLPER_N') THEN
    lvXPathAmount:=lvCollPerXPathAmount;
  END IF;
  IF inSeqNUm is not null THEN
    IF lvStructCode = 'CANC' THEN
      SELECT char2num(extractvalue(T.trandata,decode(get_OT_values('StructCode',CC.operid),
             'FPO',GDM_PARSER001.getxpath('PayAmount'),'COLLPER_N',lvCollPerXPathAmount,'COLLPER_A',lvCollPerXPathAmount,GDM_PARSER001.getxpath('Amount')))),
             nvl(T.CURRENCYID,CTP.CurrencyID),
             CC.accountid
        INTO lnAmount,
             lvFromCurr,
             lnAccountId
        FROM Transactions T,
             Transactions TT,
             ClientTransactionPackages CTP,
             ClientTransactionPackages CC
       WHERE T.PackageId = TT.cancelledpackageid
         AND T.seqnum = DECODE(TT.cancelledseqnum,0,T.SeqNum,TT.cancelledseqnum)
         AND CTP.PackageId = inPackageId
         AND CTP.PackageID=TT.PackageId
         AND NVL(inStatusId, TT.StatusId) = TT.StatusId
         AND TT.SeqNum = inSeqNum
         AND T.packageid = CC.packageid;
    ELSE
      SELECT T.AMOUNT,
             nvl(T.CURRENCYID,CTP.CurrencyID)
        INTO lnAmount,
             lvFromCurr
        FROM Transactions T,
             ClientTransactionPackages CTP
       WHERE CTP.PackageId = inPackageId
         AND CTP.PackageID=T.PackageId
         AND NVL(inStatusId, T.StatusId) =  T.StatusId
         AND T.StatusId <> 139 -- neberu sa do uvahy transakcie, ktore nepresli validaciami
         AND SeqNum = inSeqNum;
    END IF;
    BEGIN
      SELECT /*+ RESULT_CACHE */ CurrencyID
        into lvFromCurr
        FROM Currencies
       WHERE CurrencyID=lvFromCurr
         AND StatusID=1;
      IF lvAccoutCurrencyId IS NULL THEN  -- storno (operid 12)
        SELECT decode(lvUseLocalCurrencyInCTP,'1',lvLocalCurrency,CurrencyID),
               BranchId
          INTO lvAccoutCurrencyId,
               lnBranchID
          FROM Accounts
         WHERE AccountId = lnAccountId;
      END IF;
      lnPackageSum:=nvl(GetChangedAmount( lnAmount, lvFromCurr, lvAccoutCurrencyId, lnBranchID ),0 );
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        lnPackageSum:=null;
    END;
    IF lnBatch = 1 THEN
      otSubtotals:=Subtotal_tab(); otSubTotals.extend;
      otSubtotals(otSubtotals.last):=Subtotal_obj(lvFromCurr,1,lnAmount,lnPackageSum);
    END IF;
  ELSE
    IF lnBatch = 0 THEN
      IF lvStructCode = 'CANC' THEN
        IF lvAccoutCurrencyId IS NOT NULL THEN -- storno (operid 35)
          SELECT /*+ INDEX(Transactions IDX_PAY_PACKAGEID_SEQNUM)*/
                 NVL( SUM( GetChangedAmount( char2num(extractvalue(T.trandata,decode(get_OT_values('StructCode',CC.operid),
                 'FPO',GDM_PARSER001.getxpath('PayAmount'),'COLLPER_N',lvCollPerXPathAmount,'COLLPER_A',lvCollPerXPathAmount,GDM_PARSER001.getxpath('Amount')))),
                 nvl(T.CURRENCYID,CC.CurrencyID), lvAccoutCurrencyId, lnBranchID ) ),0 ) INTO lnPackageSum
            FROM Transactions T,
                 Transactions TT,
                 ClientTransactionPackages CTP,
                 Currencies C,
                 ClientTransactionPackages CC
           WHERE CTP.PackageId = inPackageId
             AND CTP.PackageID=TT.PackageId
             AND NVL(inStatusId, TT.StatusId) =  TT.StatusId
             AND T.PackageId = TT.cancelledpackageid
             AND T.seqnum = DECODE(TT.cancelledseqnum,0,T.SeqNum,TT.cancelledseqnum)
             AND T.packageid = CC.packageid
             AND C.CurrencyID=nvl(T.CURRENCYID,CC.CurrencyID)
             AND C.StatusID=1;
        ELSE  -- storno (operid 12)
          SELECT /*+ INDEX(Transactions IDX_PAY_PACKAGEID_SEQNUM)*/
                 NVL( SUM( GetChangedAmount( char2num(extractvalue(T.trandata,decode(get_OT_values('StructCode',CC.operid),
                 'FPO',GDM_PARSER001.getxpath('PayAmount'),'COLLPER_N',lvCollPerXPathAmount,'COLLPER_A',lvCollPerXPathAmount,GDM_PARSER001.getxpath('Amount')))),
                 nvl(T.CURRENCYID,CC.CurrencyID), decode(lvUseLocalCurrencyInCTP,'1',lvLocalCurrency,A.CurrencyID), A.BranchID ) ),0 ) INTO lnPackageSum
            FROM Transactions T,
                 Transactions TT,
                 ClientTransactionPackages CTP,
                 Currencies C,
                 ClientTransactionPackages CC,
                 Accounts A
           WHERE CTP.PackageId = inPackageId
             AND CTP.PackageID=TT.PackageId
             AND NVL(inStatusId, TT.StatusId) =  TT.StatusId
             AND T.PackageId = TT.cancelledpackageid
             AND T.seqnum = DECODE(TT.cancelledseqnum,0,T.SeqNum,TT.cancelledseqnum)
             AND T.packageid = CC.packageid
             AND CC.accountid = A.accountid
             AND C.CurrencyID=nvl(T.CURRENCYID,CC.CurrencyID)
             AND C.StatusID=1;
        END IF;
      ELSE
        SELECT /*+ INDEX(Transactions IDX_PAY_PACKAGEID_SEQNUM)*/
               NVL( SUM( GetChangedAmount( T.AMOUNT, nvl(T.CURRENCYID,CTP.CurrencyID), lvAccoutCurrencyId, lnBranchID ) ),0 )
          INTO lnPackageSum
          FROM Transactions T,
               ClientTransactionPackages CTP,
               Currencies C
         WHERE CTP.PackageId = inPackageId
           AND CTP.PackageID=T.PackageId
           AND NVL(inStatusId, T.StatusId) =  T.StatusId
           AND T.statusId <> 139 -- neberu sa do uvahy transakcie, ktore nepresli validaciami
           AND C.CurrencyID=nvl(T.CURRENCYID,CTP.CurrencyID)
           AND C.StatusID=1;
      END IF;
    ELSE
      IF lvStructCode = 'CANC' THEN
        IF lvAccoutCurrencyId IS NOT NULL THEN -- storno (operid 35)
          WITH TRAN AS (
            SELECT /*+ INDEX(Transactions IDX_PAY_PACKAGEID_SEQNUM)*/
                   C.Currencyid CurrID,
                   char2num(extractvalue(T.trandata,
                     -- xpath:
                     CASE get_OT_values('StructCode', CC.operid)
                       WHEN 'FPO' THEN GDM_PARSER001.getxpath('PayAmount')
                       WHEN 'COLLPER_N' THEN lvCollPerXPathAmount
                       WHEN 'COLLPER_A' THEN lvCollPerXPathAmount
                       ELSE GDM_PARSER001.getxpath('Amount')
                     END)) Amount
              FROM ClientTransactionPackages CTP
              INNER JOIN Transactions TT
                ON (CTP.PackageID = TT.PackageId
                AND (TT.StatusId = inStatusId OR inStatusId is NULL))
              INNER JOIN Transactions T
                ON (T.PackageId = TT.cancelledpackageid
                AND T.seqnum = CASE TT.cancelledseqnum
                                 WHEN 0 THEN T.SeqNum
                                 ELSE TT.cancelledseqnum
                               END)
              INNER JOIN ClientTransactionPackages CC
                ON (T.packageid = CC.packageid)
              LEFT OUTER JOIN Currencies C
                ON (C.Currencyid = coalesce(T.CURRENCYID, CC.Currencyid)
                AND C.Statusid = 1)
             WHERE CTP.PackageId = inPackageId
          )
          SELECT CAST(MULTISET(
            SELECT
              TRAN.CurrID as CurrID,
              COUNT(*) Cnt,
              SUM(TRAN.Amount) as Amount,
              SUM(CASE TRAN.CurrID
                WHEN NULL THEN NULL
                ELSE coalesce(
                  GetChangedAmount(
                    TRAN.Amount,
                    TRAN.CurrID,
                    lvAccoutCurrencyId,
                    lnBranchID),
                  0)
                END) AccAmount
            FROM TRAN
            GROUP BY TRAN.CurrID
          ) AS subtotal_tab)
          INTO otSubtotals
          FROM dual;
        ELSE  -- storno (operid 12)
          WITH TRAN AS (
            SELECT /*+ INDEX(Transactions IDX_PAY_PACKAGEID_SEQNUM)*/
                   C.Currencyid CurrID,
                   char2num(extractvalue(T.trandata,
                     -- xpath:
                     CASE get_OT_values('StructCode', CC.operid)
                       WHEN 'FPO' THEN GDM_PARSER001.getxpath('PayAmount')
                       WHEN 'COLLPER_N' THEN lvCollPerXPathAmount
                       WHEN 'COLLPER_A' THEN lvCollPerXPathAmount
                       ELSE GDM_PARSER001.getxpath('Amount')
                     END)) Amount,
                   A.CurrencyID AccCurrID,
                   A.BranchID AccBranchID
              FROM ClientTransactionPackages CTP
              INNER JOIN Transactions TT
                ON (CTP.PackageID = TT.PackageId
                AND (TT.StatusId = inStatusId OR inStatusId is NULL))
              INNER JOIN Transactions T
                ON (T.PackageId = TT.cancelledpackageid
                AND T.seqnum = CASE TT.cancelledseqnum
                                 WHEN 0 THEN T.SeqNum
                                 ELSE TT.cancelledseqnum
                               END)
              INNER JOIN ClientTransactionPackages CC
                ON (T.packageid = CC.packageid)
              INNER JOIN Accounts A
                ON (CC.accountid = A.accountid)
              LEFT OUTER JOIN Currencies C
                ON (C.Currencyid = coalesce(T.CURRENCYID, CC.Currencyid)
                AND C.Statusid = 1)
             WHERE CTP.PackageId = inPackageId
          )
          SELECT CAST(MULTISET(
            SELECT
              TRAN.CurrID as CurrID,
              COUNT(*) Cnt,
              SUM(TRAN.Amount) as Amount,
              SUM(CASE TRAN.CurrID
                WHEN NULL THEN NULL -- mena v Currencies neexistuje nebo je neplatna => bez prevodu
                ELSE coalesce(
                  GetChangedAmount(
                    TRAN.Amount,
                    TRAN.CurrID,
                    decode(lvUseLocalCurrencyInCTP, '1', lvLocalCurrency, TRAN.AccCurrID),
                    TRAN.AccBranchID),
                  0)
                END) AccAmount
            FROM TRAN
            GROUP BY TRAN.CurrID
          ) AS subtotal_tab)
          INTO otSubtotals
          FROM dual;
        END IF;
      ELSE
        WITH TRAN AS (
          SELECT /*+ INDEX(Transactions IDX_PAY_PACKAGEID_SEQNUM)*/
                 C.Currencyid CurrID,
                 T.AMOUNT
          FROM ClientTransactionPackages CTP
          INNER JOIN Transactions T
            ON (CTP.PackageID = T.PackageId
            AND (T.StatusId = inStatusId OR inStatusId is NULL)
            -- neberu sa do uvahy transakcie, ktore nepresli validaciami
            AND T.statusId <> 139)
          LEFT OUTER JOIN Currencies C
            ON (C.Currencyid = coalesce(T.CURRENCYID, CTP.Currencyid)
            AND C.Statusid = 1)
          WHERE CTP.PackageId = inPackageId
        )
        SELECT CAST(MULTISET(
          SELECT
            TRAN.CurrID as CurrID,
            COUNT(*) Cnt,
            SUM(TRAN.Amount) as Amount,
            SUM(CASE TRAN.CurrID
              WHEN NULL THEN NULL -- mena v Currencies neexistuje nebo je neplatna => bez prevodu
              ELSE coalesce(
                GetChangedAmount(
                  TRAN.Amount,
                  TRAN.CurrID,
                  lvAccoutCurrencyId,
                  lnBranchID),
                0)
              END) AccAmount
          FROM TRAN
          GROUP BY TRAN.CurrID
        ) AS subtotal_tab)
        INTO otSubtotals
        FROM dual;
      END IF;
      IF otSubtotals.COUNT > 0 THEN -- pri prazdne package (statusid=29) se nenaplni
        FOR i IN otSubtotals.FIRST..otSubtotals.LAST LOOP
          lnPackageSum:=NVL(lnPackageSum,0)+NVL(otSubtotals(i).AccCurrAmount,0);
        END LOOP;
      END IF;
    END IF;
  END IF;
  RETURN lnPackageSum;
EXCEPTION
  WHEN NO_DATA_FOUND THEN -- pri select pro inSeqNum > 0
    IF lnBatch = 1 THEN
      otSubtotals:=Subtotal_tab(); otSubTotals.extend;
    END IF;
    RETURN 0;
END GetPackageAmount;
