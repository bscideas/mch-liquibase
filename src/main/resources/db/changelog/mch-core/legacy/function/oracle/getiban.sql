
  CREATE OR REPLACE EDITIONABLE FUNCTION "GETIBAN" ( vBankID IN VARCHAR2, vAccountNum IN VARCHAR2,
                  vCountry IN VARCHAR2 default 'SK') return varchar2
 as
--------------------
-- $release: 009 $-- $release:
-- $version: 8.0.0.0 $-- $version:
-- $Header:
--------------------
--
-- Purpose: Generate IBAN
--
-- MODIFICATION HISTORY
-- Person      Date    Comments
-- ---------   ------  -------------------------------------------
-- Ilenin      07.07.2014 UDEBS RS
--
--
   ibanacc varchar2(34);
   Aibanacc varchar2(38);
   checksum varchar2(2);
   Aibannum number (38);
   tAccountNum varchar2(34);
 begin
   select lpad(substr(vAccountNum,1,instr(replace(vAccountNum,'-',' '),' ')-1),6,'0') ||
    lpad(substr(vAccountNum,instr(replace(vAccountNum,'-',' '),' ')+1),10,'0') into tAccountNum from dual;
   Aibanacc :=  upper(LPAD(nvl(vBankID,'0'),4,'0') || LPAD(nvl(tAccountNum,'0'),16,'0') || nvl(vCountry,'CZ') ||'00');
   Aibanacc :=  replace (Aibanacc,' ','');
   Aibanacc :=  replace (Aibanacc,',','');
   Aibanacc :=  replace (Aibanacc,'-','');
   Aibanacc :=  replace (Aibanacc,'.','');
   Aibanacc :=  replace (Aibanacc,'/','');
   Aibanacc :=  replace (Aibanacc,'\','');
   Aibanacc :=  replace (Aibanacc,'_','');
   Aibanacc :=  replace (Aibanacc,'(','');
   Aibanacc :=  replace (Aibanacc,')','');
   Aibanacc :=  replace (Aibanacc,'[','');
   Aibanacc :=  replace (Aibanacc,']','');
   Aibanacc :=  replace (Aibanacc,';','');
   Aibanacc :=  replace (Aibanacc,':','');
   Aibanacc :=  replace (Aibanacc,'A','10');
   Aibanacc :=  replace (Aibanacc,'B','11');
   Aibanacc :=  replace (Aibanacc,'C','12');
   Aibanacc :=  replace (Aibanacc,'D','13');
   Aibanacc :=  replace (Aibanacc,'E','14');
   Aibanacc :=  replace (Aibanacc,'F','15');
   Aibanacc :=  replace (Aibanacc,'G','16');
   Aibanacc :=  replace (Aibanacc,'H','17');
   Aibanacc :=  replace (Aibanacc,'I','18');
   Aibanacc :=  replace (Aibanacc,'J','19');
   Aibanacc :=  replace (Aibanacc,'K','20');
   Aibanacc :=  replace (Aibanacc,'L','21');
   Aibanacc :=  replace (Aibanacc,'M','22');
   Aibanacc :=  replace (Aibanacc,'N','23');
   Aibanacc :=  replace (Aibanacc,'O','24');
   Aibanacc :=  replace (Aibanacc,'P','25');
   Aibanacc :=  replace (Aibanacc,'Q','26');
   Aibanacc :=  replace (Aibanacc,'R','27');
   Aibanacc :=  replace (Aibanacc,'S','28');
   Aibanacc :=  replace (Aibanacc,'T','29');
   Aibanacc :=  replace (Aibanacc,'U','30');
   Aibanacc :=  replace (Aibanacc,'V','31');
   Aibanacc :=  replace (Aibanacc,'W','32');
   Aibanacc :=  replace (Aibanacc,'X','33');
   Aibanacc :=  replace (Aibanacc,'Y','34');
   Aibanacc :=  replace (Aibanacc,'Z','35');
   Aibannum := to_number(Aibanacc);
   checksum := LPAD(to_char(98-mod(Aibannum,97)),2,'0');
   ibanacc := upper(nvl(vCountry,'CZ') || checksum || LPAD(nvl(vBankID,'0'),4,'0') || LPAD(nvl(tAccountNum,'0'),16,'0'));
   return ibanacc;
 end;
