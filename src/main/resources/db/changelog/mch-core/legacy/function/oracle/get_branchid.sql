
  CREATE OR REPLACE EDITIONABLE FUNCTION "GET_BRANCHID" (
    pAccountID in Accounts.AccountID%Type,
    pPackageID in ClientTransactionPackages.PackageID%Type default NULL
) RETURN NUMBER IS
------------------
-- $release: 009 $
-- $version: 8.0.0.0 $
------------------
--
-- Purpose: Ziskani BranchID podle uctu nebo packageid
--
-- MODIFICATION HISTORY
-- Person      Date       Comments
-- ----------- ---------- ---------------------
-- Ilenin      07.07.2014 UDEBS RS
--------------------------------------------------------
  lnBID  Branches.BranchID%TYPE;
  lvAcc  Accounts.AccountID%Type;
BEGIN
  if pAccountID is not null then
    select BranchID into lnBID from Accounts where AccountID=pAccountID;
  elsif pPackageID is not null then
    select AccountID into lvAcc from ClientTransactionPackages where PackageID=pPackageID;
    if lvAcc is not null then
      select BranchID into lnBID from Accounts where AccountID=lvAcc;
    end if;
  end if;
  return nvl(lnBID,0);
EXCEPTION WHEN OTHERS THEN
  return 0;
END Get_BranchID;
