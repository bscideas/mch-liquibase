
  CREATE OR REPLACE EDITIONABLE FUNCTION "GET_CLIENTSIGNRULELIMIT" (inSignRuleSeq IN ClientSignRuleLimits.ClientSignRuleSeq%TYPE,
                                                   inPeriod      IN ClientSignRuleLimits.Period%TYPE)
  RETURN NUMBER RESULT_CACHE IS
  ----------------
  -- $release: 2005 $- release ###
  -- $version: 8.0.0.1 $- release ###
  ----------------
  -- Funkce vraci AmountLimit, je pouzita pri otevreni kurzoru z IIF_CCFG005.GetValidSignatureRules
  -- MODIFICATION HISTORY
  -- Person      Date       Comments
  -- ----------- ---------- ---------------------
  -- Ilenin      07.07.2014 UDEBS RS
  -- Emmer       27.01.2016 Add RESULT_CACHE
  --------------------------------------------------------
  lnAmountLimit ClientSignRuleLimits.AmountLimit%TYPE;
BEGIN
  SELECT AmountLimit
    INTO lnAmountLimit
    FROM ClientSignRuleLimits
   WHERE ClientSignRuleSeq = inSignRuleSeq
     AND Period = inPeriod;
  RETURN lnAmountLimit;
EXCEPTION
  WHEN no_data_found THEN
    RETURN NULL;
END;
