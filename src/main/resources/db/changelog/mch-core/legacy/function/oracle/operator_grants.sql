
  CREATE OR REPLACE EDITIONABLE FUNCTION "OPERATOR_GRANTS" (inChannelId    IN Channels.ChannelId%TYPE, -- channel which is used to realize the operation, mandatory (for admin API it is always "system administration" channel)
                 inClientId              IN Clients.ClientId%TYPE, -- the client under which the operation is to be realized, optional (for admin API it is always "bank" service client - if not filled then selected from GSP parameter)
                 inUserId                IN Users.UserId%TYPE, -- client user ID
                 inOperatorId            IN Users.UserId%TYPE,  -- operator user ID, mandatory
                 inAccessTypeId          IN OperationAccessTypes.AccessTypeId%TYPE,  -- operation access type to test for operator, mandatory
                 inOperId                IN OperationTypes.OperId%TYPE,   -- operation ID, mandatory
                 ivOperatorRoles         IN VARCHAR2,    -- comma delimited list of names of operator roles - i.e., right profiles assigned to operator
                 ivMessageCategoryId     IN VARCHAR2 DEFAULT NULL,    -- ID of checked message category - i.e., whether a right for this msg category exists in some of client operator profiles, optional
                 inMessageCategoryCheck  IN NUMBER DEFAULT NULL,      -- flag if message category assignment should be checked, mandatory
                 inTargetClientID        IN NUMBER DEFAULT NULL,      -- target client ID - client whose data will be modified or accessed, optional, default is taken from GSP parameter; if used, target client's segment will be checked against role segments
                 inClientCategoryCheck   IN NUMBER DEFAULT NULL       -- flag if target client segment assignment should be checked - optional, default is taken from GSP parameter
                 )
RETURN PLS_INTEGER
IS
------------------
-- $release: 5257 $
-- $version: 8.0.7.0 $
------------------
--
-- MODIFICATION HISTORY
--
-- Person       Date       Comments
-- ------------ ---------- ---------------------------------------------------------
-- Furdik       25.02.2016 Initial version, based on GRANTS function
-- Furdik       09.03.2016 Added check of Client Categories
-- Pataky       31.08.2016 Changes to support right profile types
-- Pataky       06.09.2016 inClientID, inClientCategoryCheck and inMessageCategoryCheck is optional - taken from GSP parameters
-- Pataky       07.10.2016 Added AvailableFor columns checking - 20009213-2051
-- Bocak        17.01.2017 RESULT_CACHE - 20009245-3714
-- Pataky       31.01.2017 Performance issues fixed regarding client categories (MYG-2294, MYG-2317, MYG-2122, MYG-2280) - 20009203-6996
-- Pataky       15.05.2017 Added check on operator care for client (MYG-4674) - 20009246-337
-- Pataky       22.05.2017 Updated check on operator care for client (MYG-4760) - 20009246-337
-- --------------------------------------------------------------------------------
--
  lnClientCategID       Clients.ClientCategID%TYPE:=null;
  lnOperatorHasCare     pls_integer;
BEGIN
  IF inTargetClientID is not null and nvl(inClientCategoryCheck,1)=1 then
    -- check if operator has given client in care, currently regardles of care type
    SELECT /*+ RESULT_CACHE */ count(*) INTO lnOperatorHasCare FROM ClientOperatorCare COC, Users U
      where COC.ClientId=inTargetClientID and U.HostUserID = COC.HostOperatorID and U.UserID=inOperatorId and rownum<=1;
    IF lnOperatorHasCare=0 THEN
      SELECT ClientCategID INTO lnClientCategID FROM Clients WHERE ClientID=inTargetClientID;
    END IF;
  end if;
  RETURN OPERATOR_GRANTS2 (inChannelId,
                 inClientId,
                 inUserId,
                 inOperatorId,
                 inAccessTypeId,
                 inOperId,
                 ivOperatorRoles,
                 ivMessageCategoryId,
                 inMessageCategoryCheck,
                 lnClientCategID,
                 inClientCategoryCheck
                 );
END OPERATOR_GRANTS;
