
  CREATE OR REPLACE EDITIONABLE FUNCTION "GETPACKAGEAMOUNT_ALL" (
    otSubtotals_All OUT Subtotal_all_tab,
    inPackageId     IN  ClientTransactionPackages.PackageId%TYPE,
    inOperId        IN  ClientTransactionPackages.OperId%TYPE DEFAULT NULL,
    inTMP           IN  NUMBER DEFAULT 1)
RETURN NUMBER
IS
------------------
-- $release: 4330 $
-- $version: 8.0.9.0 $
------------------
-- $Header:  $
-- posledni patch z UDEBS - 2506
------------------
--
-- Purpose:
-- Funkce pro zadanou package spocita celkovou castku transakci ve statusu p_nStatusId. Pokud neni zadany status,
-- jsou do souctu zahrnuty vsechny transakce.
--
-- Funkce prevadi castky transakci do meny uctu. Mena debetniho uctu je ulozena v prislusne tabulce spolecne s castkou
-- a menou castky. Predpokladem je vyplneni meny debetniho uctu v tabulce.
-- Inkasa sa zgrupuju aj podla schemy.
-- Prevod castky je provaden pomoci funkce GetChangedAmout.
--
-- MODIFICATION HISTORY
-- Person      Date       Comments
-- ----------- ---------- ---------------------
-- Bocak       14.04.2015 UDEBS RS
-- Bocak       12.10.2015 tunning main select
-- Pataky      09.11.2015 Package amount can be recounted into local currency, based on parameter (because of multicurrency accounts)
-- Bocak       11.12.2015 recalculate only TMP transactions if exixt
-- Simunek     12.02.2016 SQL optimization (20009183-4886)
-- Bocak       08.03.2016 changed to_number on char2num
-- Bocak       10.06.2016 extended select by joinig on CURRENCY - 20009245-1591
-- Bocak       21.10.2016 added /*+ USE_HASH (C)  */ - 20009245-3261
-- Bocak       31.01.2017 new input parameter inTMP (calling from getpackageamounts) - 20009238-485
--------------------------------------------------------
  lnPackageSum       NUMBER;
  lvAccoutCurrencyId Accounts.CurrencyId%TYPE;
  lnBranchID         Branches.BranchID%TYPE;
  lnBatch            pls_integer;
  lvXPathAmount      GDM_Parser.xpath%TYPE := GDM_PARSER001.getxpath('Amount');
  lvXPathCurrId      GDM_Parser.xpath%TYPE := GDM_PARSER001.getxpath('CurrencyId');
  lvXPathScheme      GDM_Parser.xpath%TYPE := GDM_PARSER001.getxpath('Scheme');
  lvUseLocalCurrencyInCTP varchar2(10);
  --
  lnCount            NUMBER;
  ltSubtotals_All    Subtotal_all_tab:=Subtotal_all_tab();
BEGIN
  lvUseLocalCurrencyInCTP := nvl(reftab_gsp('REQUESTS.USE_LOCAL_CURRENCY_IN_CTP',0,'0'),'0');
  SELECT decode(lvUseLocalCurrencyInCTP,'1',Reftab_GSP('LOCALCURRENCY',0,'X'),A.CurrencyID),
         A.BranchID,
         get_OT_values('Batch',nvl(inOperId,CTP.OperID))
    INTO lvAccoutCurrencyId,
         lnBranchID,
         lnBatch
    FROM ClientTransactionPackages CTP,
         Accounts A
   WHERE PackageId = inPackageId
     AND A.AccountID(+)=CTP.AccountID;
  --
  SELECT COUNT(1) INTO lnCount FROM Transactions_TMP WHERE PackageId = inPackageId;
  IF lnCount>0 AND inTMP=1 THEN
    --
    select subtotals_all into ltSubtotals_All from clienttransactionpackages where packageid=inPackageId;
    --
    --
    IF ltSubtotals_All IS NOT NULL THEN
      for i in ltSubtotals_All.first..ltSubtotals_All.last
      LOOP
        --
        for j in (select tt.seqnum seqnum,CHAR2NUM(extractvalue(tt.trandata,'/TranData/Amount')) Amount,extractvalue(tt.trandata,'/TranData/CurrencyID') CurrencyID,extractvalue(tt.trandata,'/TranData/Scheme') Scheme,
                         NVL(t.seqnum,0) seqnum_old,CHAR2NUM(extractvalue(t.trandata,'/TranData/Amount')) Amount_old,extractvalue(t.trandata,'/TranData/CurrencyID') CurrencyID_old,extractvalue(tt.trandata,'/TranData/Scheme') Scheme_old
                  from transactions_tmp tt,
                       Transactions T
                 WHERE Tt.PackageId=inPackageId
                   AND t.packageid(+)=tt.packageid
                   AND t.transactionid(+)=tt.transactionid
                   AND t.seqnum(+)=tt.seqnum)
        LOOP
          --
          if CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID_old and j.CurrencyID=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')=NVL(j.Scheme,'x') and j.Amount!=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA bola zmenena, MENA nebola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Odrata novu sumu a prepocitanu sumu');
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount-j.Amount;
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID_old and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')=NVL(j.Scheme,'x') and j.Amount!=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA bola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Odrata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount-1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount-j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount-GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID_old and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')=NVL(j.Scheme,'x') and j.Amount=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA nebola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Odrata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount-1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount-j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount-GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')=NVL(j.Scheme,'x') and j.Amount!=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA bola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Prirata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount+1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount+j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount+GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')=NVL(j.Scheme,'x') and j.Amount=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA nebola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Prirata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount+1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount+j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount+GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
    ---------------------------
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID_old and j.CurrencyID=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')!=NVL(j.Scheme,'x') and j.Amount!=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA bola zmenena, MENA nebola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Odrata novu sumu a prepocitanu sumu');
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount-j.Amount;
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID_old and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')!=NVL(j.Scheme,'x') and j.Amount!=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA bola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Odrata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount-1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount-j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount-GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID_old and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')!=NVL(j.Scheme,'x') and j.Amount=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA nebola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Odrata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount-1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount-j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount-GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')!=NVL(j.Scheme,'x') and j.Amount!=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA bola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Prirata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount+1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount+j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount+GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))>0 and ltSubtotals_All(i).CurrencyID=j.CurrencyID and j.CurrencyID!=j.CurrencyID_old and NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme_old,'x') and NVL(j.Scheme_old,'x')!=NVL(j.Scheme,'x') and j.Amount=j.Amount_old then
            DBMS_OUTPUT.PUT_LINE('* SUMA nebola zmenena, MENA bola zmenena !');
            DBMS_OUTPUT.PUT_LINE('* Prirata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount+1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount+j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount+GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          elsif CHAR2NUM(NVL(j.seqnum_old,'0'))=0 AND ltSubtotals_All(i).CurrencyID=j.CurrencyID AND NVL(ltSubtotals_All(i).Scheme,'x')=NVL(j.Scheme,'x') THEN
    ---------------------------
            DBMS_OUTPUT.PUT_LINE('* Nova tranzakcia !');
            DBMS_OUTPUT.PUT_LINE('* Prirata novu sumu, prepocitanu sumu a pocet');
            ltSubtotals_All(i).TranCount:=ltSubtotals_All(i).TranCount+1;
            ltSubtotals_All(i).OrigCurrAmount:=ltSubtotals_All(i).OrigCurrAmount+j.Amount;
            ltSubtotals_All(i).AccCurrAmount:=ltSubtotals_All(i).AccCurrAmount+GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          end if;
        end loop;--j
        --
        if ltSubtotals_All(i).TranCount=0 then
          DBMS_OUTPUT.PUT_LINE('delete');
          ltSubtotals_All.delete(i);
        end if;
      end loop;--i
      --
      -- new transactions
      FOR j IN (select COUNT(*) cnt,
                       SUM(CHAR2NUM(extractvalue(tt.trandata,'/TranData/Amount'))) Amount,
                       extractvalue(tt.trandata,'/TranData/CurrencyID') CurrencyID,
                       extractvalue(tt.trandata,'/TranData/Scheme') Scheme
                  from transactions_tmp tt
                 WHERE Tt.PackageId=inPackageId
                   AND tt.transactionid NOT IN (SELECT t.transactionid FROM Transactions t WHERE t.PackageId=inPackageId)
                   AND (extractvalue(tt.trandata,'/TranData/CurrencyID') NOT IN (SELECT CURRENCYID FROM TABLE(ltSubtotals_All)) OR
                        extractvalue(tt.trandata,'/TranData/Scheme') NOT IN (SELECT SCHEME FROM TABLE(ltSubtotals_All)))
                 GROUP BY extractvalue(tt.trandata,'/TranData/CurrencyID'),extractvalue(tt.trandata,'/TranData/Scheme'))
      LOOP
          ltSubtotals_All.extend;
          ltSubtotals_All(ltSubtotals_All.LAST):=Subtotal_all_obj(NULL,NULL,NULL,NULL,NULL);
          ltSubtotals_All(ltSubtotals_All.LAST).TranCount:=j.cnt;
          ltSubtotals_All(ltSubtotals_All.LAST).OrigCurrAmount:=j.Amount;
          ltSubtotals_All(ltSubtotals_All.LAST).AccCurrAmount:=GetChangedAmount( j.Amount,j.CurrencyID, 'EUR', 0);
          ltSubtotals_All(ltSubtotals_All.LAST).Scheme:=J.Scheme;
          ltSubtotals_All(ltSubtotals_All.LAST).CurrencyID:=j.CurrencyID;
      END LOOP;
      --
      otSubtotals_All:=ltSubtotals_All;
    END IF;
  ELSE
    --
    WITH TRAN AS (
      SELECT /*+ USE_HASH (C)  */ nvl(T.CURRENCYID, CTP.CurrencyID) CurrID,
             CASE WHEN lvXPathScheme IS NULL THEN NULL ELSE extractvalue(trandata,lvXPathScheme) END Scheme,
             T.AMOUNT Amount
      FROM Transactions T,
           ClientTransactionPackages CTP,
           Currencies C
      WHERE CTP.PackageId = inPackageId
        AND CTP.PackageID = T.PackageId
        AND C.CurrencyID=nvl(T.CURRENCYID,CTP.CurrencyID)
        AND C.StatusID=1
        AND T.StatusId!=0
    )
    SELECT CAST(MULTISET(
      SELECT
        TRAN.CurrID,
        TRAN.Scheme,
        COUNT(*) Cnt,
        SUM(TRAN.Amount) Amount,
        NVL( SUM( GetChangedAmount( TRAN.Amount, TRAN.CurrID, lvAccoutCurrencyId, lnBranchID ) ),0 ) AccAmount
      FROM TRAN
      GROUP BY
        TRAN.CurrID,
        TRAN.Scheme
    ) AS SUBTOTAL_ALL_TAB)
    INTO otSubtotals_All
    FROM DUAL;
  END IF;
  --
  IF otSubtotals_All IS NOT NULL THEN
    IF otSubtotals_All.COUNT > 0 THEN     -- pri prazdne package (statusid=29) se nenaplni
      FOR i IN otSubtotals_All.FIRST..otSubtotals_All.LAST LOOP
        lnPackageSum:=NVL(lnPackageSum,0)+NVL(otSubtotals_All(i).AccCurrAmount,0);
      END LOOP;
    END IF;
  END IF;
  --
  RETURN lnPackageSum;
EXCEPTION
  WHEN NO_DATA_FOUND THEN -- pri select pro inSeqNum > 0
dbms_output.put_line('no data found');
    IF lnBatch = 1 THEN
      otSubtotals_All:=Subtotal_all_tab(); otSubTotals_All.extend;
    END IF;
    RETURN 0;
END GetPackageAmount_ALL;
