
  CREATE OR REPLACE EDITIONABLE FUNCTION "GETCHANGEDAMOUNT" (p_nAmount         IN NUMBER,
                                            p_cFromCurrencyID IN VARCHAR2,
                                            p_cToCurrencyID   IN VARCHAR2,
                                            p_nBranchID       IN Branches.BranchID%TYPE,
                                            p_nMiddleRate     IN NUMBER DEFAULT 1) --0 - do not use middle rate, 1 use middle rate - used for amount calculation for eval of sig rules
 RETURN NUMBER IS
  ------------------
  -- $release: 6570 $
  -- $version: 8.0.9.1 $
  ------------------
  --
  -- Purpose: Funkce prepocitava zadanou castku v mene p_cFromCurrencyID a vraci ekvivalent
  -- v mene p_cToCurrencyID. Pro prepocet pouziva tabulku hodnot z kurzovniho listku.
  -- Pritom pro prepocet musi existovat kurzovni listek na menu uctu.
  -- Tento listek musi obsahovat sazbu pro typ sazby 3 = DEVIZA_PRODEJ z meny castky.
  -- MODIFICATION HISTORY
  -- Person      Date       Comments
  -- ----------- ---------- ---------------------
  -- Ilenin      07.07.2014 UDEBS RS
  -- Bocak       19.02.2015 input parameter p_nMiddleRate for deciding whether to use middle rate
  -- Bocak       13.03.2015 added MiddleRate into recursive calling
  -- Pataky      25.03.2015 Compatibility changes to support other banks (with non reversed exchange rate lists)
  -- Pataky      22.05.2015 Performance optimization - 20009183-2902
  -- Bocak       06.06.2015 get newest valied rate 20009183-3118
  -- Simunek     20.10.2015 Performance optimization (20009183-4734)
  -- Bocak       31.05.2016 Truncate only seconds from sysdate - 20009238-191
  -- Furdik      27.10.2016 added options for TATR - 20009239-186
  -- Emmer       09.04.2018 add EXCHANGERATELISTUNITS.UNIT (MYG-7673) - P20009301-68
  -- Pataky      09.04.2015 Generalized usage of units and reversed exchange rate lists (MYG-7673) - P20009301-68
  --------------------------------------------------------
  v_RateValue NUMBER;
  v_RateValue1 NUMBER;
  v_RateValue2 NUMBER;
  v_RateListType NUMBER;
  v_AmountTo NUMBER;
  v_ExchangeListSeq NUMBER;
  --
  v_LocalCurrency Currencies.CURRENCYID%TYPE := reftab_gsp('LOCALCURRENCY', 0, 'EUR');
  v_FromCurrencyID Currencies.CURRENCYID%TYPE := p_cFromCurrencyID;
  v_ToCurrencyID Currencies.CURRENCYID%TYPE := p_cToCurrencyID;
  --
  v_BuyRateType NUMBER; --Non Cash Buy
  v_SellRateType NUMBER; --Non Cash Sell
  --
  v_SysDate DATE := TRUNC(SYSDATE, 'MI'); -- for using SQL RESULT_CACHE
  v_BuyRateTypeMidDefault VARCHAR2(2);
  v_SellRateTypeMidDefault VARCHAR2(2);
  v_RATELISTUNITSETID EXCHANGERATELISTUNITS.RATELISTUNITSETID%TYPE;
  v_RATELISTREVERSED EXCHANGERATELISTS.RATELISTREVERSED%TYPE;
BEGIN
  --
  IF p_cFromCurrencyID IS NULL THEN
    v_FromCurrencyID := v_LocalCurrency;
  END IF;
  IF p_cToCurrencyID IS NULL THEN
    v_ToCurrencyID := v_LocalCurrency;
  END IF;
  --
  IF v_FromCurrencyID = v_ToCurrencyID
     OR v_FromCurrencyID IS NULL
     OR v_ToCurrencyID IS NULL THEN
    RETURN p_nAmount;
  END IF;
  --
  SELECT /*+ RESULT_CACHE */
   RateListTypeID, RateTypeID, RateTypeID
    INTO v_RateListType, v_BuyRateType, v_SellRateType
    FROM Branches
   WHERE RateListTypeID IS NOT NULL
     AND rownum = 1
   START WITH BranchID = p_nBranchID
  CONNECT BY BranchID = PRIOR ParentBranchID;
  --
  v_BuyRateTypeMidDefault := '7';
  v_SellRateTypeMidDefault := '7';
  IF Reftab_GSP('LOCALBANKCODEALPHA') = 'TATR' THEN
    v_BuyRateTypeMidDefault := '5';
    v_SellRateTypeMidDefault := '5';
  END IF;
  --
  IF Reftab_GSP('LOCALBANKCODEALPHA') IN ('SLSP', 'TATR') THEN
    IF p_nMiddleRate = 1 THEN
      v_BuyRateType := TO_NUMBER(reftab_gsp('UDEBS.BUYRATETYPE_MID', 0, v_BuyRateTypeMidDefault));
      v_SellRateType := TO_NUMBER(reftab_gsp('UDEBS.SELLRATETYPE_MID', 0, v_SellRateTypeMidDefault));
    ELSE
      v_BuyRateType := TO_NUMBER(reftab_gsp('UDEBS.BUYRATETYPE', 0, '1'));
      v_SellRateType := TO_NUMBER(reftab_gsp('UDEBS.SELLRATETYPE', 0, '3'));
    END IF;
  END IF;
  --
  IF Reftab_GSP('LOCALBANKCODEALPHA') IN ('SLSP', 'TATR') THEN
    IF v_ToCurrencyID = v_LocalCurrency THEN
      BEGIN
        v_RateValue1 := 1;
        SELECT /*+ RESULT_CACHE */
         decode(nvl(erl.ratelistreversed,0),1, nvl(eru.unit,1) / erv.RATEVALUE, erv.RATEVALUE / nvl(eru.unit,1))
          INTO v_RateValue2
          FROM EXCHANGERATELISTS erl, EXCHANGERATEVALUES erv,
            (select RATELISTUNITSETID, UNIT from EXCHANGERATELISTUNITS
               where TOCURRENCYID = v_LocalCurrency
                 AND FROMCURRENCYID = v_FromCurrencyID
                 AND RATELISTTYPEID = v_RateListType) eru
         WHERE nvl(erl.RATELISTUNITSETID,-1) = eru.RATELISTUNITSETID (+)
           AND erv.EXCHANGERATELISTSEQ = erl.EXCHANGERATELISTSEQ
           AND erl.RATELISTTYPEID = v_RateListType
           AND v_SysDate BETWEEN erl.RATEVALUEDATE AND NVL(erl.RATEENDDATE, v_SysDate)
           AND erl.TOCURRENCYID = v_FromCurrencyID
           AND erv.FROMCURRENCYID = v_LocalCurrency
           AND erv.RATETYPEID = v_SellRateType;
        v_AmountTo := p_nAmount * v_RateValue1 / v_RateValue2;
        RETURN v_AmountTo;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          raise_application_error(-20102,
                                  'UDEBS::GETCHANGEDAMOUNT Cannot find general exchange rate list for branch ' ||
                                  p_nBranchID);
      END;
    ELSIF v_FromCurrencyID = v_LocalCurrency THEN
      BEGIN
        v_RateValue2 := 1;
        SELECT /*+ RESULT_CACHE */
         RATEVALUE
          INTO v_RateValue1
          FROM (SELECT decode(nvl(erl.ratelistreversed,0),1, nvl(eru.unit,1) / erv.RATEVALUE, erv.RATEVALUE / nvl(eru.unit,1)) AS RATEVALUE
                  FROM EXCHANGERATELISTS erl, EXCHANGERATEVALUES erv,
                  (select RATELISTUNITSETID, UNIT from EXCHANGERATELISTUNITS
                    where TOCURRENCYID = v_LocalCurrency
                      AND FROMCURRENCYID = v_ToCurrencyID
                      AND RATELISTTYPEID = v_RateListType) eru
                 WHERE nvl(erl.RATELISTUNITSETID,-1) = eru.RATELISTUNITSETID (+)
                   AND erv.EXCHANGERATELISTSEQ = erl.EXCHANGERATELISTSEQ
                   AND erl.RATELISTTYPEID = v_RateListType
                   AND v_SysDate BETWEEN erl.RATEVALUEDATE AND NVL(erl.RATEENDDATE, v_SysDate)
                   AND erl.TOCURRENCYID = v_ToCurrencyID
                   AND erv.FROMCURRENCYID = v_LocalCurrency
                   AND erv.RATETYPEID = v_BuyRateType
                 ORDER BY erl.RATEVALUEDATE DESC)
         WHERE ROWNUM = 1;
        v_AmountTo := p_nAmount * v_RateValue1 / v_RateValue2;
        RETURN v_AmountTo;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          raise_application_error(-20102,
                                  'UDEBS::GETCHANGEDAMOUNT Cannot find general exchange rate list for branch ' ||
                                  p_nBranchID);
      END;
    ELSE
      v_AmountTo := GETCHANGEDAMOUNT(p_nAmount,
                                     v_FromCurrencyID,
                                     v_LocalCurrency,
                                     p_nBranchID,
                                     p_nMiddleRate);
      v_AmountTo := GETCHANGEDAMOUNT(v_AmountTo,
                                     v_LocalCurrency,
                                     v_ToCurrencyID,
                                     p_nBranchID,
                                     p_nMiddleRate);
      RETURN v_AmountTO;
    END IF;
  ELSE
    BEGIN
      SELECT ExRateListSeq, nvl(RATELISTUNITSETID,-1), nvl(RATELISTREVERSED,0)
        INTO v_ExchangeListSeq, v_RATELISTUNITSETID, v_RATELISTREVERSED
        FROM ExchangeRateLists ERL,
             (SELECT MAX(EXCHANGERATELISTSEQ) ExRateListSeq
                FROM ExchangeRateLists
               WHERE RateListTypeID = v_RateListType
                 AND SYSDATE BETWEEN RateValueDate AND NVL(RATEENDDATE, SYSDATE)) ESEQ
       WHERE ERL.ExchangeRateListSeq = ESEQ.ExRateListSeq;
    EXCEPTION
      WHEN NO_DATA_FOUND THEN
        raise_application_error(-20102,
                                'UDEBS::GETCHANGEDAMOUNT Cannot find general exchange rate list for branch ' ||
                                p_nBranchID);
    END;
    --
    IF v_LocalCurrency = v_FromCurrencyID THEN
      v_RateValue1 := 1;
    ELSE
      BEGIN
        SELECT decode(v_RATELISTREVERSED,1,eru.unit / erv.RateValue, erv.RateValue / eru.unit)
          INTO v_RateValue1
          FROM ExchangeRateValues erv,
                (select RATELISTUNITSETID, UNIT from EXCHANGERATELISTUNITS
                  where TOCURRENCYID = v_LocalCurrency
                    AND FROMCURRENCYID = v_FromCurrencyID
                    AND RATELISTTYPEID = v_RateListType) eru
         WHERE eru.RATELISTUNITSETID (+) = v_RATELISTUNITSETID
           AND erv.FromCurrencyID = v_FromCurrencyID
           AND erv.RateTypeID = v_BuyRateType
           AND erv.ExchangeRateListSeq = v_ExchangeListSeq;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          raise_application_error(-20102,
                                  'UDEBS::GETCHANGEDAMOUNT Cannot find exchange rate for exchange from currency ' ||
                                  nvl(v_FromCurrencyID, 'currency not set'));
      END;
    END IF;
    IF v_LocalCurrency = v_ToCurrencyID THEN
      v_RateValue2 := 1;
    ELSE
      BEGIN
        SELECT decode(v_RATELISTREVERSED,1,eru.unit / erv.RateValue, erv.RateValue / eru.unit)
          INTO v_RateValue2
          FROM ExchangeRateValues erv,
                (select RATELISTUNITSETID, UNIT from EXCHANGERATELISTUNITS
                  where TOCURRENCYID = v_LocalCurrency
                    AND FROMCURRENCYID = v_ToCurrencyID
                    AND RATELISTTYPEID = v_RateListType) eru
         WHERE eru.RATELISTUNITSETID (+) = v_RATELISTUNITSETID
           AND erv.FromCurrencyID = v_ToCurrencyID
           AND erv.RateTypeID = v_SellRateType
           AND erv.ExchangeRateListSeq = v_ExchangeListSeq;
      EXCEPTION
        WHEN NO_DATA_FOUND THEN
          raise_application_error(-20102,
                                  'UDEBS::GETCHANGEDAMOUNT Cannot find exchange rate for exchange from currency ' ||
                                  nvl(v_ToCurrencyID, 'currency not set'));
      END;
    END IF;
    --
    v_AmountTo := p_nAmount * v_RateValue1 / v_RateValue2;
    --
    RETURN v_AmountTO;
  END IF;
END;
