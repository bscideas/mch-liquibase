
  CREATE OR REPLACE EDITIONABLE FUNCTION "GETNEXTTEXTID"
  (tabn varchar2 default null) RETURN  Number  IS
-----------------
-- $release: 7451 $
-- $version: 8.0.2.0 $
-----------------
  tid  Number;
BEGIN
  LOOP
    Select seq_texts.nextval into tid from dual;
    BEGIN
      Insert into texts(textid,texttype) values(tid,null);
      Exit;
    Exception
      When DUP_VAL_ON_INDEX then null;
    END;
  End LOOP;
  Return tid;
END;
