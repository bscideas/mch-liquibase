CREATE OR REPLACE
    PROCEDURE multi_lang_txt(atid number, apid varchar2, alng varchar2, atxt varchar2) IS
    tmp pls_integer;
BEGIN
    SELECT COUNT(*) INTO tmp FROM supportedlang WHERE langid = alng;
    IF tmp > 0 THEN
        INSERT INTO LANGUAGETEXTS(PurposeID, TextID, LangID, Text) VALUES (apid, atid, alng, atxt);
    END IF;
EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN
        UPDATE languagetexts SET text=atxt WHERE textid = atid AND purposeid = apid AND langid = alng;
    WHEN OTHERS THEN
        dbms_output.put_line(substr('purposeid: ' || apid || ', langid: ' || alng || ', text: ' || atxt, 1, 255));
        dbms_output.put_line(substr(sqlerrm, 1, 255));
END;