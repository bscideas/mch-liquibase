CREATE TABLE comp_usr_tab (id NUMBER, name VARCHAR2(30))
/
CREATE SEQUENCE comp_st_seq
/

DECLARE
  nBefore NUMBER;
  nAfter  NUMBER;
  CURSOR cur IS
    SELECT object_type, object_name
      FROM user_objects
     WHERE status<>'VALID'
     ORDER BY OBJECT_NAME;
   PROCEDURE jevtmp_Vykonaj(PR2 IN VARCHAR2) IS
     Acursor INTEGER;
     rows_processed INTEGER;
   BEGIN
     Acursor := dbms_sql.open_cursor;
     dbms_sql.parse(Acursor, PR2, dbms_sql.v7);
      rows_processed := dbms_sql.execute(Acursor);
    dbms_sql.close_cursor(Acursor);
   EXCEPTION
     WHEN OTHERS THEN
       dbms_sql.close_cursor(Acursor);
      raise;
   END;
BEGIN
  LOOP
    SELECT COUNT(*) INTO nBefore FROM user_objects WHERE status <>'VALID';
    INSERT INTO comp_usr_tab(id,name) VALUES (0,'----');
    COMMIT;
    FOR i IN cur LOOP
      INSERT INTO comp_usr_tab(id,name) VALUES (comp_st_seq.nextval,i.object_name);
      COMMIT;
      BEGIN
        IF i.object_type='PACKAGE BODY' THEN
          jevtmp_vykonaj('alter package '||i.object_name||' compile body');
        ELSE
          jevtmp_vykonaj('alter '||i.object_type||' '||i.object_name||' compile');
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
      END;
    END LOOP;
    SELECT COUNT(*) INTO nAfter FROM user_objects WHERE status <>'VALID';
    dbms_output.put_line('Before:'||to_char(nBefore)||' After:'||to_char(nAfter));
    EXIT WHEN nBefore=nAfter;
  END LOOP;
END;
/
SELECT SUBSTR(object_name,1,30) object_name, object_type
  FROM user_objects
 WHERE status<>'VALID' AND object_type not like '%JAVA%'
 ORDER BY 1
/
DROP TABLE comp_usr_tab
/
DROP SEQUENCE comp_st_seq
/
