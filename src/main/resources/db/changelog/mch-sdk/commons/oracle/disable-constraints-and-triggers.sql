begin
    for rec in (select * from user_constraints where constraint_type = 'R' and status = 'ENABLED')
        loop
            begin
                execute immediate 'alter table ' || rec.table_name || ' disable constraint ' || rec.constraint_name;
            exception
                when others then
                    dbms_output.put_line(
                                'ERROR while enabling constraint ' || rec.table_name || '.' || rec.constraint_name || ' ' || SQLCODE || ' -> ' ||
                                SQLERRM);
            end;
        end loop;
    for rec in (select * from user_triggers where status = 'ENABLED')
        loop
            begin
                execute immediate 'alter trigger ' || rec.trigger_name || ' disable';
            exception
                when others then
                    dbms_output.put_line('ERROR while disabling trigger ' || rec.trigger_name || ' ' || SQLCODE || ' -> ' || SQLERRM);
            end;
        end loop;
    commit;
end;