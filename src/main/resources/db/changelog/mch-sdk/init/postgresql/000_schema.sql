
CREATE ROLE ${roleOwner};
CREATE ROLE ${roleGus};
CREATE ROLE ${roleWac};

CREATE USER ${dbUser} WITH ENCRYPTED PASSWORD '${dbUserPassword}';
CREATE USER ${appUserGus} WITH ENCRYPTED PASSWORD '${appUserGusPassword}';
CREATE USER ${appUserWac} WITH ENCRYPTED PASSWORD '${appUserWacPassword}';

GRANT ${roleOwner} TO ${dbUser};

CREATE TABLESPACE ${accountTablespace} OWNER ${roleOwner} LOCATION '${tablespaceDataFolder}/${accountTablespace}';
CREATE TABLESPACE ${archTablespace} OWNER ${roleOwner} LOCATION '${tablespaceDataFolder}/${archTablespace}';
CREATE TABLESPACE ${archIndexTablespace} OWNER ${roleOwner} LOCATION '${tablespaceDataFolder}/${archIndexTablespace}';
CREATE TABLESPACE ${clientTablespace} OWNER ${roleOwner} LOCATION '${tablespaceDataFolder}/${clientTablespace}';
CREATE TABLESPACE ${indexTablespace} OWNER ${roleOwner} LOCATION '${tablespaceDataFolder}/${indexTablespace}';
CREATE TABLESPACE ${journalTablespace} OWNER ${roleOwner} LOCATION '${tablespaceDataFolder}/${journalTablespace}';
CREATE TABLESPACE ${lobTablespace} OWNER ${roleOwner} LOCATION '${tablespaceDataFolder}/${lobTablespace}';
CREATE TABLESPACE ${refTablespace} OWNER ${roleOwner} LOCATION '${tablespaceDataFolder}/${refTablespace}';
CREATE TABLESPACE ${transTablespace} OWNER ${roleOwner} LOCATION '${tablespaceDataFolder}/${transTablespace}';

CREATE DATABASE ${dbName} OWNER ${roleOwner} ENCODING 'UTF-8' TABLESPACE ${clientTablespace} CONNECTION LIMIT -1;

/* This should be executed in ${dbName} */
CREATE SCHEMA ${dbSchema};

ALTER SCHEMA ${dbSchema} OWNER TO ${roleOwner};
ALTER DATABASE ${dbName} SET search_path TO ${dbSchema};

ALTER ROLE ${roleOwner} SET search_path TO ${dbSchema};
ALTER ROLE ${roleOwner} SET default_tablespace = ${clientTablespace};
ALTER ROLE ${roleGus} SET default_tablespace = ${clientTablespace};
ALTER ROLE ${roleWac} SET default_tablespace = ${clientTablespace};

GRANT ALL ON DATABASE ${dbName} TO ${roleOwner};
GRANT ALL ON SCHEMA ${dbSchema} TO ${roleOwner};
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA ${dbSchema} TO ${roleOwner};
GRANT CONNECT ON DATABASE ${dbName} TO ${roleWac}, ${roleGus};
GRANT USAGE ON SCHEMA ${dbSchema} TO ${roleWac}, ${roleGus};
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA ${dbSchema} TO ${roleWac}, ${roleGus};
