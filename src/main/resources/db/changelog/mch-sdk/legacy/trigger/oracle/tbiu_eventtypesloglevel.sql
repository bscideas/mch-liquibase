
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIU_EVENTTYPESLOGLEVEL"
 BEFORE
  INSERT OR UPDATE
 ON EVENTTYPES
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
DECLARE
  v_removal NUMBER := 0;
BEGIN
  IF (NVL(:old.loglevel, 1) <> 0 AND :new.loglevel = 0)
     OR (NVL(:old.loglevel, 1) = 0 AND :new.loglevel <> 0) THEN
    IF (NVL(:old.loglevel, 1) <> 0 AND :new.loglevel = 0) THEN
      v_removal := 1;
    END IF;
    IF :new.eventid = -9 THEN
      :new.loglevel := :old.loglevel;
    ELSE
      DECLARE
        v_OS_USER VARCHAR2(4000);
        v_PROXY_USER VARCHAR2(4000);
        v_SESSION_USER VARCHAR2(4000);
        v_CURRENT_USER VARCHAR2(4000);
        v_SERVICE_NAME VARCHAR2(4000);
        v_SERVER_HOST VARCHAR2(4000);
        v_CLIENT_INFO VARCHAR2(4000);
        v_CLIENT_PROGRAM_NAME VARCHAR2(4000);
      BEGIN
        --zalogovani zmeny
        SELECT SYS_CONTEXT('USERENV', 'OS_USER'),
               SYS_CONTEXT('USERENV', 'PROXY_USER'),
               SYS_CONTEXT('USERENV', 'SESSION_USER'),
               SYS_CONTEXT('USERENV', 'CURRENT_USER'),
               SYS_CONTEXT('USERENV', 'SERVICE_NAME'),
               SYS_CONTEXT('USERENV', 'SERVER_HOST'),
               SYS_CONTEXT('USERENV', 'CLIENT_INFO'),
               SYS_CONTEXT('USERENV', 'CLIENT_PROGRAM_NAME')
          INTO v_OS_USER,
               v_PROXY_USER,
               v_SESSION_USER,
               v_CURRENT_USER,
               v_SERVICE_NAME,
               v_SERVER_HOST,
               v_CLIENT_INFO,
               v_CLIENT_PROGRAM_NAME
          FROM DUAL;
        INSERT INTO Journal
          (SEQ, EventID, OriginatorID, Details)
        VALUES
          (SEQ_JOURNAL.nextval,
           -9,
           'TBIU_EventTypesLogLevel',
           '<Trandata><Description>Journaling ' || CASE WHEN v_removal = 1 THEN 'removal from' ELSE
           'activation for' END || ' particular Event</Description>
  <EventId>' || :new.eventid || '</EventId>
  <OS_USER>' || v_OS_USER || '</OS_USER>
  <PROXY_USER>' || v_PROXY_USER || '</PROXY_USER>
  <SESSION_USER>' || v_SESSION_USER || '</SESSION_USER>
  <CURRENT_USER>' || v_CURRENT_USER || '</CURRENT_USER>
  <SERVICE_NAME>' || v_SERVICE_NAME || '</SERVICE_NAME>
  <SERVER_HOST>' || v_SERVER_HOST || '</SERVER_HOST>
  <CLIENT_INFO>' || v_CLIENT_INFO || '</CLIENT_INFO>
  <CLIENT_PROGRAM_NAME>' || v_CLIENT_PROGRAM_NAME || '</CLIENT_PROGRAM_NAME>
  </Trandata>');
      END;
    END IF;
  END IF;
END TAIU_EventTypesLogLevel;

