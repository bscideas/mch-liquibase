
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBIU_VALIDATORTYPE$EV"
 BEFORE
  INSERT OR UPDATE
 ON VALIDATORTYPE
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
BEGIN
  :new.ENTITYVERSION:=to_char(systimestamp,'YYYYMMDDHH24MISSFF9');
END;

