
  CREATE OR REPLACE EDITIONABLE TRIGGER "TBU_TEXTS"
 BEFORE
   UPDATE OF TEXTID
 ON TEXTS
REFERENCING NEW AS NEW OLD AS OLD
 FOR EACH ROW
 WHEN (NEW.TEXTID<>OLD.TEXTID) Begin
   Update LanguageTexts set TextID=:NEW.TextID where TextID=:OLD.TextID and PurposeID='DEFAULT';
End;

